
/* Drop Tables */

DROP TABLE IF EXISTS administrator;
DROP TABLE IF EXISTS answer_details;
DROP TABLE IF EXISTS answers;
DROP TABLE IF EXISTS form_charge_options;
DROP TABLE IF EXISTS charge_option_plans;
DROP TABLE IF EXISTS form_configs;
DROP TABLE IF EXISTS charge_plans;
DROP TABLE IF EXISTS disp_schedule;
DROP TABLE IF EXISTS form_item_values;
DROP TABLE IF EXISTS form_items;
DROP TABLE IF EXISTS forms_categories;
DROP TABLE IF EXISTS forms;
DROP TABLE IF EXISTS inquiry;
DROP TABLE IF EXISTS contracts;
DROP TABLE IF EXISTS information;
DROP TABLE IF EXISTS m_css;
DROP TABLE IF EXISTS m_form_item_values;
DROP TABLE IF EXISTS m_form_items;




/* Create Tables */

CREATE TABLE administrator
(
	id bigint NOT NULL,
	userid varchar(64) NOT NULL UNIQUE,
	password varchar(64) NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE answers
(
	id varchar(200) NOT NULL,
	form_id varchar(200) NOT NULL,
	start_at timestamp,
	end_at timestamp,
	-- アンケート回答後のID埋め込みに利用する
	foreign_key varchar(100),
	attribute1 varchar(100),
	attribute2 varchar(100),
	attribute3 varchar(100),
	attribute4 varchar(100),
	attribute5 varchar(100),
	is_preview char(1) NOT NULL,
	ip_address varchar(100) NOT NULL,
	web_browser varchar(200) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE answer_details
(
	id bigint NOT NULL,
	answer_id varchar(200) NOT NULL,
	answer_value numeric,
	value_text text,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE charge_option_plans
(
	id bigint NOT NULL,
	charge int NOT NULL,
	tax int NOT NULL,
	description text NOT NULL,
	apply_start_date timestamp NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE charge_plans
(
	id bigint NOT NULL,
	charge int NOT NULL,
	tax int NOT NULL,
	description text NOT NULL,
	note text,
	collect_number int,
	term int,
	output_number int NOT NULL,
	save_term int NOT NULL,
	apply_start_date timestamp NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE contracts
(
	id bigint NOT NULL,
	mail varchar(200) NOT NULL,
	-- 1:仮登録 2:登録 3:停止 9:解約
	status char(1) NOT NULL,
	regist_token varchar(64) NOT NULL,
	token_limit timestamp NOT NULL,
	name varchar(128),
	pass varchar(64),
	cancel_date timestamp,
	created_at timestamp NOT NULL,
	-- 
	-- 
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE disp_schedule
(
	id bigint NOT NULL,
	form_id varchar(200) NOT NULL,
	-- 1:日付 2:時間 3:曜日
	kind char(1) NOT NULL,
	-- カンマ区切りで各要素を格納
	setting_value text NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE forms
(
	id varchar(200) NOT NULL,
	contract_id bigint NOT NULL,
	form_name text NOT NULL,
	form_desc text NOT NULL,
	result_message text NOT NULL,
	total_page_count int NOT NULL,
	css_id bigint,
	org_css text,
	image_url varchar(256),
	-- 1:下書き　2:開始　3:終了　9:中止
	status char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


-- フェーズ1ではこのテーブルは使用しません。
CREATE TABLE forms_categories
(
	id bigint NOT NULL,
	form_id varchar(200) NOT NULL,
	name varchar(200) NOT NULL,
	description text NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE form_charge_options
(
	id bigint NOT NULL,
	form_id varchar(200) NOT NULL,
	charge_option_plan_id bigint NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE form_configs
(
	id bigint NOT NULL,
	form_id varchar(200) NOT NULL,
	collect_number int NOT NULL,
	start_st date NOT NULL,
	end_st date NOT NULL,
	-- 0:掲載中 1:停止中
	stop char(1) NOT NULL,
	output_number int NOT NULL,
	term int NOT NULL,
	output_limit_number int NOT NULL,
	save_term int NOT NULL,
	price int,
	payment_confirm int,
	payment_confirm_date timestamp,
	charge_plan_id bigint,
	-- 0:公開しない 1:公開する
	enc_result_open char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE form_items
(
	id varchar(200) NOT NULL,
	form_id varchar(200) NOT NULL,
	category_id bigint NOT NULL,
	m_form_item_id bigint NOT NULL,
	name varchar(200) NOT NULL,
	page_no int NOT NULL,
	required char(1) NOT NULL,
	select_number int DEFAULT 0,
	-- 選択個数に設定する条件
	condition char(1),
	dispodr int NOT NULL,
	hint varchar(64),
	-- 0:チェックしない 1:チェックする
	duplicate_check char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE form_item_values
(
	id bigint NOT NULL,
	form_item_id varchar(200) NOT NULL,
	form_value varchar(200) NOT NULL,
	ch_length int NOT NULL,
	-- 0:未選択　1:選択
	default_select char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE information
(
	id bigint NOT NULL,
	send_date timestamp NOT NULL,
	subject varchar(128) NOT NULL,
	content text NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE inquiry
(
	id bigint NOT NULL,
	send_date timestamp NOT NULL,
	name varchar(128) NOT NULL,
	subject varchar(128) NOT NULL,
	mail varchar(256) NOT NULL,
	content text NOT NULL,
	-- 0:未対応 1:対応済み
	status char(1) NOT NULL,
	contract_id bigint,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE m_css
(
	id bigint NOT NULL,
	name varchar(64) NOT NULL,
	css_code text NOT NULL,
	disp_image_url varchar(256) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE m_form_items
(
	id bigint NOT NULL,
	name varchar(200) NOT NULL,
	kind char(1) NOT NULL,
	data_type char(1) NOT NULL,
	graph_kind char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE m_form_item_values
(
	id bigint NOT NULL,
	m_form_items_id bigint NOT NULL,
	item_value varchar(200) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	id bigint NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE answer_details
	ADD FOREIGN KEY (answer_id)
	REFERENCES answers (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_charge_options
	ADD FOREIGN KEY (charge_option_plan_id)
	REFERENCES charge_option_plans (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_configs
	ADD FOREIGN KEY (charge_plan_id)
	REFERENCES charge_plans (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE forms
	ADD FOREIGN KEY (contract_id)
	REFERENCES contracts (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE inquiry
	ADD FOREIGN KEY (contract_id)
	REFERENCES contracts (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE answers
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE disp_schedule
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE forms_categories
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_charge_options
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_configs
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_items
	ADD FOREIGN KEY (form_id)
	REFERENCES forms (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_items
	ADD FOREIGN KEY (category_id)
	REFERENCES forms_categories (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_item_values
	ADD FOREIGN KEY (form_item_id)
	REFERENCES form_items (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE forms
	ADD FOREIGN KEY (css_id)
	REFERENCES m_css (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form_items
	ADD FOREIGN KEY (m_form_item_id)
	REFERENCES m_form_items (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE m_form_item_values
	ADD FOREIGN KEY (id)
	REFERENCES m_form_items (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Comments */

COMMENT ON COLUMN answers.foreign_key IS 'アンケート回答後のID埋め込みに利用する';
COMMENT ON COLUMN contracts.status IS '1:仮登録 2:登録 3:停止 9:解約';
COMMENT ON COLUMN contracts.updated_at IS '
';
COMMENT ON COLUMN disp_schedule.kind IS '1:日付 2:時間 3:曜日';
COMMENT ON COLUMN disp_schedule.setting_value IS 'カンマ区切りで各要素を格納';
COMMENT ON COLUMN forms.status IS '1:下書き　2:開始　3:終了　9:中止';
COMMENT ON TABLE forms_categories IS 'フェーズ1ではこのテーブルは使用しません。';
COMMENT ON COLUMN form_configs.stop IS '0:掲載中 1:停止中';
COMMENT ON COLUMN form_configs.enc_result_open IS '0:公開しない 1:公開する';
COMMENT ON COLUMN form_items.condition IS '選択個数に設定する条件';
COMMENT ON COLUMN form_items.duplicate_check IS '0:チェックしない 1:チェックする';
COMMENT ON COLUMN form_item_values.default_select IS '0:未選択　1:選択';
COMMENT ON COLUMN inquiry.status IS '0:未対応 1:対応済み';



