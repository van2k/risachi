#!/bin/bash

isAliveSev=`ps -ef | grep "/van2k/dev_klingon/" | grep -v grep | wc -l`
if [ $isAliveSev = 1 ]; then
  echo "プロセスは生きています"
else
  echo "プロセスが死んます"
  /van2k/dev_klingon/current/Server/klingon/target/universal/stage/bin/klingon -Dconfig.resource=development.conf -Dhttp.port=9000 -Dpidfile.path=/van2k/dev_klingon/shared/pids/RUNNING_PID -mem 512 >> /van2k/dev_klingon/shared/log/play_start.log 2>&1 &
  restart_date=`date "+%Y/%m/%d %H:%M:%S"`
  echo "${restart_date} dev_klingon restart" >> /van2k/sh/process_check/dev_klingon_restart.log
fi

