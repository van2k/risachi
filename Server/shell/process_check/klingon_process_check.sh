#!/bin/bash

isAliveSev=`ps -ef | grep "/van2k/klingon/" | grep -v grep | wc -l`
if [ $isAliveSev = 1 ]; then
  echo "プロセスは生きています"
else
  echo "プロセスが死んます"
  /van2k/klingon/current/Server/klingon/target/universal/stage/bin/klingon -Dconfig.resource=production.conf -Dhttp.port=9001 -Dpidfile.path=/van2k/klingon/shared/pids/RUNNING_PID -mem 512 >> /van2k/klingon/shared/log/play_start.log 2>&1 &
  restart_date=`date "+%Y/%m/%d %H:%M:%S"`
  echo "${restart_date} klingon restart" >> /van2k/sh/process_check/klingon_restart.log
fi

