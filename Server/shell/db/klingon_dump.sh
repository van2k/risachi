#!/bin/bash

current_path=/van2k/sh/db/
pg_dump=/usr/pgsql-9.1/bin/pg_dump
db_name=$1
exec_date=`date "+%Y%m%d"`
dump_file=${db_name}_${exec_date}
scp_host=van2k.cloudapp.net
scp_key=~/.ssh/windows_azure_key
scp_remote_path=/home/van2k/klingon/

# postgresqlのdumpデータ作成
$pg_dump $db_name -U klingon > ${current_path}${dump_file}.sql

# dumpデータの暗号化
/usr/bin/openssl enc -e -aes256 -in ${current_path}${dump_file}.sql -out ${current_path}${dump_file} -kfile ${current_path}klingon_pass.txt
rm ${current_path}${dump_file}.sql

# dumpデータをコピー
/usr/bin/scp -i ${scp_key} ${current_path}${dump_file} van2k@${scp_host}:${scp_remote_path}

rm ${current_path}${dump_file}

