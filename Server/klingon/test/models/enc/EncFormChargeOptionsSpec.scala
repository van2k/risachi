package models.enc

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication

class EncFormChargeOptionsSpec extends Specification {

  "フォームにオプション料金プランの挿入/取得するテスト" should {
    "オプション料金を挿入してそれを取得できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 挿入する
      EncFormChargeOptions.regist(EncFormChargeOptions(1, 10, 3)) must beTrue
      EncFormChargeOptions.regist(EncFormChargeOptions(2, 10, 4)) must beTrue
      EncFormChargeOptions.regist(EncFormChargeOptions(3, 20, 5)) must beTrue
      EncFormChargeOptions.regist(EncFormChargeOptions(4, 20, 6)) must beTrue

      // 全取得
      val all = EncFormChargeOptions.findAll()
      all.length === 4

      // idで取得
      EncFormChargeOptions.findById(1).get.formId === 10
      EncFormChargeOptions.findById(2).get.formId === 10
      EncFormChargeOptions.findById(3).get.formId === 20
      EncFormChargeOptions.findById(4).get.formId === 20

      // formidで取得
      val sel = EncFormChargeOptions.findByFormId(10)
      sel.length === 2
      sel(0).id + sel(1).id === 3
      (sel(0).id == 1 || sel(0).id == 2) must beTrue
      (sel(1).id == 1 || sel(1).id == 2) must beTrue
    }
  }

  "フォームにオプション料金を登録/剥奪するテスト" should {
    "オプションプランを登録/剥奪できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
       // まだ登録されていない状態で登録できる
      EncFormChargeOptions.updateFormChargeOption(1, 1, true) === 1
      EncFormChargeOptions.findByFormId(1).head.chargeOptionPlanId === 1

      // すでに登録されているオプションは２重登録されない
      EncFormChargeOptions.updateFormChargeOption(1, 1, true)
      val result = EncFormChargeOptions.findByFormId(1)
      result.length === 1
      result.head.chargeOptionPlanId === 1

      // すでに登録されているものとは違うオプションを登録できる
      EncFormChargeOptions.updateFormChargeOption(1, 2, true)
      val result2 = EncFormChargeOptions.findByFormId(1)
      result2.length === 2
      (result2(0).chargeOptionPlanId == 1 && result2(1).chargeOptionPlanId == 2 ||
        result2(0).chargeOptionPlanId == 2 && result2(1).chargeOptionPlanId == 1) must beTrue

      // 違うフォームなら同じオプションを登録できる
      EncFormChargeOptions.updateFormChargeOption(2, 1, true) === 1
      val result3 = EncFormChargeOptions.findByFormId(2)
      result3.length === 1
      result3.head.chargeOptionPlanId === 1

      // 剥奪できる
      EncFormChargeOptions.updateFormChargeOption(1, 1, false)
      val result4 = EncFormChargeOptions.findByFormId(1)
      result4.length === 1
      result4.head.chargeOptionPlanId === 2
      val result5 = EncFormChargeOptions.findByFormId(2)
      result5.length === 1
      result5.head.chargeOptionPlanId === 1
    }
  }
}
