package models.enc

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import java.util.Calendar
import java.text.SimpleDateFormat
import collection.mutable

class EncAttendSpec extends Specification {

  val title = "2月の出席予定をご記入ください"
  val question = "2/2"
  val answer_name = "番付太郎"
  val name_title = "お名前"
  val date_title = "日付"
  val answer = ""

  def before:Long = {
    val now = Calendar.getInstance().getTime

    // フォーム挿入
    val form_id = EncForms.regist(EncForms(-1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
      Option(0), Option(""), Option(""), EncForms.STATUS_RUN, "-1", now, now)).getOrElse(-1)

//    println("■form_id--------------------------")
//    println(EncForms.find(form_id).getOrElse("form not found").toString())

    // 質問項目挿入
    val item_id1 = EncFormItems.regist(EncFormItems(-1, form_id.asInstanceOf[Long], 1, 1, "お名前", 1, 150, "0", "1", 0, now, now), now)
    val item_id2 = EncFormItems.regist(EncFormItems(-1, form_id.asInstanceOf[Long], 1, 4, "日付", 1, 2, "0", "1", 1, now, now), now)

//    println("■form_item_id--------------------------")
//    println(EncFormItems.find(item_id1.getOrElse(-1)).getOrElse("item1 not found").toString())
//    println(EncFormItems.find(item_id2.getOrElse(-1)).getOrElse("item2 not found").toString())

    // 質問項目選択肢挿入
    EncFormItemValues.regist(EncFormItemValues(1, item_id1.getOrElse(-1), "dummy", 100, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(2, item_id2.getOrElse(-1), "02/02", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(3, item_id2.getOrElse(-1), "02/03", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(4, item_id2.getOrElse(-1), "02/09", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(5, item_id2.getOrElse(-1), "02/10", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(6, item_id2.getOrElse(-1), "02/11", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(7, item_id2.getOrElse(-1), "02/16", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(8, item_id2.getOrElse(-1), "02/17", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(9, item_id2.getOrElse(-1), "02/23", 0, now, now), now)
    EncFormItemValues.regist(EncFormItemValues(10, item_id2.getOrElse(-1), "02/24", 0, now, now), now)

//    println("■form_item_values--------------------------")
//    println("form_item_values :" + EncFormItemValues.findByFormId(form_id).toString())

    val calTo = Calendar.getInstance()
    calTo.add(Calendar.YEAR, 1)

    // 回答挿入
    val answer1 = EncAnswers.regist(EncAnswers(-1, form_id.asInstanceOf[Long], Calendar.getInstance().getTime, Option(calTo.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "FireFox", now, now))
    Thread.sleep(1000)
    val answer2 = EncAnswers.regist(EncAnswers(-1, form_id.asInstanceOf[Long], Calendar.getInstance().getTime, Option(calTo.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "FireFox", now, now))
    Thread.sleep(1000)
    val answer3 = EncAnswers.regist(EncAnswers(-1, form_id.asInstanceOf[Long], Calendar.getInstance().getTime, Option(calTo.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "FireFox", now, now))

//    println("■answerId--------------------------")
//    println(EncAnswers.find(answer1.getOrElse(-1)).getOrElse("answer1 not found").toString())
//    println(EncAnswers.find(answer2.getOrElse(-1)).getOrElse("answer2 not found").toString())
//    println(EncAnswers.find(answer3.getOrElse(-1)).getOrElse("answer3 not found").toString())

    // 回答詳細挿入
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer1.getOrElse(-1), item_id1.getOrElse(-1), 1, "番付太郎", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer1.getOrElse(-1), item_id2.getOrElse(-1), 2, "2", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer1.getOrElse(-1), item_id2.getOrElse(-1), 3, "3", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer2.getOrElse(-1), item_id1.getOrElse(-1), 1, "番付花子", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer2.getOrElse(-1), item_id2.getOrElse(-1), 4, "4", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer2.getOrElse(-1), item_id2.getOrElse(-1), 5, "5", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer2.getOrElse(-1), item_id2.getOrElse(-1), 6, "6", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer2.getOrElse(-1), item_id2.getOrElse(-1), 8, "8", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer3.getOrElse(-1), item_id1.getOrElse(-1), 1, "番付一郎", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer3.getOrElse(-1), item_id2.getOrElse(-1), 4, "4", Option(now), now, now))
    EncAnswerDetails.regist(EncAnswerDetails(-1, answer3.getOrElse(-1), item_id2.getOrElse(-1), 6, "6", Option(now), now, now))

//    println("■answer_detail_id--------------------------")
//    println("answer1 answer_details :" + EncAnswerDetails.findByAnsweridPageno(answer1.getOrElse(-1), 1).toString())
//    println("answer2 answer_details :" + EncAnswerDetails.findByAnsweridPageno(answer2.getOrElse(-1), 1).toString())
//    println("answer3 answer_details :" + EncAnswerDetails.findByAnsweridPageno(answer3.getOrElse(-1), 1).toString())

    form_id.asInstanceOf[Long]
  }

  "アンケート集計結果が作成できる" should {
    val encAttend = EncAttend(-1, title, Option(name_title), Option(date_title), Option(answer_name), Option(answer))

    "アンケートIDが正しく取得できる" in {
      encAttend.answerId === -1
    }
    "アンケート質問タイトルが正しく取得できる" in {
      encAttend.title === title
    }
    "アンケート名前項目名が正しく取得できる" in {
      encAttend.nameTitle.get === name_title
    }
    "アンケート日付項目名が正しく取得できる" in {
      encAttend.dateTitle.get === date_title
    }
    "アンケート回答者名が正しく取得できる" in {
      encAttend.answerName.get === answer_name
    }
    "アンケート回答が正しく取得できる" in {
      encAttend.answer.get === answer
    }
  }

  "アンケート結果を出欠表形式で取得できる" should {

    "アンケートに対して回答がない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val attend: List[EncAttend] = EncAttend.recordOfAttendance(-1)
        attend.size === 0
      }
    }

    "アンケートに対して回答がある" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val formId = before
//        println("form_id = [" + formId + "]")
        val attends: List[EncAttend] = EncAttend.recordOfAttendance(formId)
//        println("attend = [" + attends + "]")

        var answerId: Long = -1
        var nameTitle: String = ""
        var dateTitle: String = ""
        var answererMap: Map[String, String] = Map.empty[String, String]
        var participations: mutable.MutableList[String] = new mutable.MutableList[String]
        var participationDateMap: Map[String, List[String]] = Map.empty[String, List[String]]
        for (attend: EncAttend <- attends) {
          if (answerId != attend.answerId) {
            if (answerId != -1) {
              participationDateMap = participationDateMap + (answerId.toString -> participations.toList)
              participations = new mutable.MutableList[String]
            }
            answerId = attend.answerId
          }
          attend.nameTitle match {
            case Some(n) => {
              if (nameTitle == "") nameTitle = n
            }
            case _ =>
          }
          attend.dateTitle match {
            case Some(d) => {
              if (dateTitle == "") dateTitle = d
            }
            case _ =>
          }
          attend.answerName match {
            case Some(an) => answererMap = answererMap + (answerId.toString -> an)
            case _ =>
          }
          attend.answer match {
            case Some(a) => participations += a
            case _ =>
          }
        }
        if (!participationDateMap.contains(answerId.toString)) {
          participationDateMap = participationDateMap + (answerId.toString -> participations.toList)
        }
//        println("after datas=======================")
//        println(nameTitle)
//        println(dateTitle)
//        println(answererMap.toString)
//        println(participationDateMap.toString)


        "アンケート回答の名称部分のタイトルが取得できる" in {
          nameTitle === "お名前"
        }
        "アンケート回答の日付部分のタイトルが取得できる" in {
          dateTitle === "日付"
        }
        val checkNames = Array("番付太郎", "番付花子", "番付一郎")
        val checkDates = Array(List("02/02", "02/03"), List("02/09", "02/10", "02/11", "02/17"), List("02/09", "02/11"))
        val nameKeys = answererMap.keysIterator

        for (checkName <- checkNames) {
          if (nameKeys.hasNext) {
            val answerKey:String = nameKeys.next
            "アンケート回答の回答者名が取得できる" in {
              checkName === answererMap.get(answerKey).getOrElse("")
            }
          }
        }

        val dateKeys = answererMap.keysIterator
        for (checkDate <- checkDates) {
          if (dateKeys.hasNext) {
            val answerKey:String = dateKeys.next
            "アンケート回答の参加可能日付が取得できる" in {
              checkDate === participationDateMap.get(answerKey).getOrElse(List)
            }
          }
        }

        "アンケートの回答件数が正しく取得できる" in {
          attends.size === 11
        }

      }
    }

  }

}
