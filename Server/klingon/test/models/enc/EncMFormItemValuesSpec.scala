package models.enc

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import org.joda.time.DateTime
import play.api.test.FakeApplication


class EncMFormItemValuesSpec extends Specification {

  "EncMFormItemValues" should {
    "find by primary keys" in {
      "データが取得できる場合" in {
        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

          // note:データはevolutionの1.sqlで設定済み
          // todo:データとスキームを分離すること

          val testIdFirst = 2
          val testFormItemsIdFirst = 8
          val testItemValueFirst = "青森県"

          val currentTimeStamp = DateTime.now

          val maybeFound = EncMFormItemValues.find(testIdFirst)
          maybeFound.isDefined should beTrue

          maybeFound.get.id === testIdFirst
          maybeFound.get.mFormItemsId === testFormItemsIdFirst
          maybeFound.get.itemValue === testItemValueFirst
//          maybeFound.get.createdAt === currentTimeStamp.toLocalDate.toDate
//          maybeFound.get.updatedAt === currentTimeStamp.toLocalDate.toDate
        }
      }

      "データが取得できない場合" in {
        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

          // note:データはevolutionの26.sqlで設定済み
          val testIdFirst = -1

          val maybeFound = EncMFormItemValues.find(testIdFirst)
          maybeFound.isDefined should beFalse
        }
      }
    }

    "findAll" in {
      "データが取得できる場合" in {
        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

          // note:データはevolutionの26.sqlで設定済み
          // todo:データとスキームを分離すること

          val testIdFirst = 2
          val testFormItemsIdFirst = 8
          val testItemValueFirst = "青森県"

          val currentTimeStamp = DateTime.now

          val maybeFound = EncMFormItemValues.findAll()
          maybeFound.size === 51

          maybeFound(1).id === testIdFirst
          maybeFound(1).mFormItemsId === testFormItemsIdFirst
          maybeFound(1).itemValue === testItemValueFirst
//          maybeFound(1).createdAt === currentTimeStamp.toLocalDate.toDate
//          maybeFound(1).updatedAt === currentTimeStamp.toLocalDate.toDate
        }
      }

      //      "データが取得できない場合" in{
      //        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      //
      //          // note:データはevolutionの26.sqlで設定済み
      //          // todo:データとスキームを分離すること
      //
      //          // todo
      //        }
      //      }


    }

    "findBymformitemid" in {
      "データが取得できる場合" in {
        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

          // note:データはevolutionの26.sqlで設定済み
          // todo:データとスキームを分離すること

          val testFormItemsIdFirst = 10
          val testItemValueFirst = "10代"

          val testFormItemsIdSecond = 10
          val testItemValueSecond = "20代"

          val currentTimeStamp = DateTime.now

          val maybeFound = EncMFormItemValues.findBymformitemid(10)
          maybeFound.size === 2

          maybeFound(0).mFormItemsId === testFormItemsIdFirst
          maybeFound(0).itemValue === testItemValueFirst
//          maybeFound(0).createdAt === currentTimeStamp.toLocalDate.toDate
//          maybeFound(0).updatedAt === currentTimeStamp.toLocalDate.toDate

          maybeFound(1).mFormItemsId === testFormItemsIdSecond
          maybeFound(1).itemValue === testItemValueSecond
//          maybeFound(1).createdAt === currentTimeStamp.toLocalDate.toDate
//          maybeFound(1).updatedAt === currentTimeStamp.toLocalDate.toDate
        }
      }

      "データが取得できない場合" in {
        running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

          val maybeFound = EncMFormItemValues.findBymformitemid(-1)
          maybeFound.size === 0
        }
      }


    }
  }
}

