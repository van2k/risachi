package models.enc

import java.util.{Date, Calendar}
import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import models.DbUpdateResult

class EncAnswerDetailsSpec extends Specification {

	"EncAnswerDetails生成のテスト" should {
		val cal = Calendar.getInstance()

		val now = cal.getTime
		val createAt = now
		val updateAt = now

		val answerDetail = EncAnswerDetails(1, 2, 3, 4, "1", Option(now), createAt, updateAt)

		"インスタンスを生成できる" in {
			answerDetail must not beNull
		}
		"idが正しく取得できる" in {
			answerDetail.id == 1
		}
		"answer_idが正しく取得できる" in {
			answerDetail.answerId == 2
		}
		"form_item_idが正しく取得できる" in {
			answerDetail.formItemId == 3
		}
		"form_item_value_idが正しく取得できる" in {
			answerDetail.formItemValueId == 4
		}
		"answer_valueが正しく取得できる" in {
			answerDetail.answerValue == "1"
		}
		"answer_atが正しく取得できる" in {
			answerDetail.answerAt == Option(now)
		}
		"登録時間を正しく取得できる" in {
			answerDetail.createdAt == createAt
		}
		"更新時間を正しく取得できる" in {
			answerDetail.updatedAt == updateAt
		}
	}

	"登録・検索処理のテスト" should {
		val cal = Calendar.getInstance()

		val answerDetailId = 1
		val answerId = 1
		val formItemId = 1
		val formItemValueId = 3
		val answerValue = "answer"
		val answerAt = cal.getTime
		val createAt = cal.getTime
		val updateAt = cal.getTime
		val pageNo = 1

		val answerDetail = EncAnswerDetails(answerDetailId, answerId, formItemId, formItemValueId, answerValue, Option(answerAt), createAt, updateAt)

		running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
			EncFormItems.regist(EncFormItems(1, 2, 3, 4, "フォームテスト", pageNo, 4, "5", "1", 1, createAt, updateAt), createAt)
			val retValue = EncAnswerDetails.regist(answerDetail)
			"データが正常に登録できる" in {
				retValue == DbUpdateResult.success
			}

			val result = EncAnswerDetails.find(answerDetailId).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"id指定で結果が取得できる" in {
				result.id == answerDetailId
			}

			val result2 = EncAnswerDetails.find(938459).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"存在しないid指定で検索した場合、検索結果が0件となる" in {
				result2.id == -1
			}

			val result3 = EncAnswerDetails.findByFormitemid(formItemId)
			"form_item_id指定で結果が取得できる" in {
				result3.length > 0 && result3.head.formItemId == formItemId
			}

			val result4 = EncAnswerDetails.findByFormitemid(3332)
			"存在しないform_item_id指定で検索した場合、検索結果が0件となる" in {
				result4.length == 0
			}

			val result5 = EncAnswerDetails.findByAnsweridFormitemid(answerId, formItemId)
			"answer_id, form_item_id指定で結果が取得できる" in {
				result5.length > 0 && result5.head.answerId == answerId && result5.head.formItemId == formItemId
			}

			val result6 = EncAnswerDetails.findByAnsweridFormitemid(answerId, 2333)
			"存在しないform_item_id指定で検索した場合、検索結果が0件となる" in {
				result6.length == 0
			}

			val result7 = EncAnswerDetails.findByAnsweridFormitemid(4343, formItemId)
			"存在しないanswer_id指定で検索した場合、検索結果が0件となる" in {
				result7.length == 0
			}

			val result8 = EncAnswerDetails.findByAnsweridFormitemidFormitemvalueid(answerId, formItemId, formItemValueId)
			"answer_id, form_item_id, form_item_value_id指定で結果が取得できる" in {
				result8.length > 0 && result8.head.answerId == answerId && result8.head.formItemId == formItemId && result8.head.formItemValueId == formItemValueId
			}

			val result9 = EncAnswerDetails.findByAnsweridFormitemidFormitemvalueid(answerId, formItemId, 434534)
			"存在しないform_item_value_id指定で検索した場合、検索結果が0件となる" in {
				result9.length == 0
			}

			val result10 = EncAnswerDetails.findByAnsweridFormitemidFormitemvalueid(answerId, 34532, formItemValueId)
			"存在しないform_item_id指定で検索した場合、検索結果が0件となる" in {
				result10.length == 0
			}

			val result11 = EncAnswerDetails.findByAnsweridFormitemidFormitemvalueid(32432, formItemId, formItemValueId)
			"存在しないanswer_id指定で検索した場合、検索結果が0件となる" in {
				result11.length == 0
			}


			val result12 = EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)
			"answer_id, page_no指定で結果が取得できる" in {
				result12.length > 0 && result12.head.answerId == answerId
			}

			val result13 = EncAnswerDetails.findByAnsweridPageno(answerId, 23324)
			"存在しないpage_no指定で検索した場合、検索結果が0件となる" in {
				result13.length == 0
			}

			val result14 = EncAnswerDetails.findByAnsweridPageno(234324, pageNo)
			"存在しないanswer_id指定で検索した場合、検索結果が0件となる" in {
				result14.length == 0
			}

			try {
				EncAnswerDetails.removeByAnsweridPageno(answerId, 3232)
			} catch {
				case e:Exception => {}
			}
			val result15 = EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)

			"存在しないpage_no指定で削除しても削除されない" in {
				result15.length > 0 && result15.head.answerId == answerId
			}

			try {
				EncAnswerDetails.removeByAnsweridPageno(2345234, pageNo)
			} catch {
				case e:Exception => {}
			}
			val result16 = EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)
			"存在しないanswerId指定で削除しても削除されない" in {
				result16.length > 0 && result16.head.answerId == answerId
			}

			EncAnswerDetails.removeByAnsweridPageno(answerId, pageNo)
			val result17 = EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)
			"answer_id, page_no指定で削除できる" in {
				result17.length == 0
			}
		}

		running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
			EncFormItems.regist(EncFormItems(1, 2, 3, 4, "フォームテスト", pageNo, 4, "5", "1", 1, createAt, updateAt), createAt)
			EncAnswerDetails.regist(answerDetail)

			try {
				EncAnswerDetails.removeById(23432)
			} catch {
				case e:Exception => {}
			}
			val result18 = EncAnswerDetails.find(1).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"存在しないid指定で削除しても削除されない" in {
				result18.id == 1
			}
			EncAnswerDetails.removeById(1)
			val result19 = EncAnswerDetails.find(1).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"id指定で削除できる" in {
				result19.id == -1
			}
		}

		running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
			EncFormItems.regist(EncFormItems(1, 2, 3, 4, "フォームテスト", pageNo, 4, "5", "1", 1, createAt, updateAt), createAt)
			EncAnswerDetails.regist(answerDetail)

			EncAnswerDetails.deleteByAnswerId(23423)
			val result20 = EncAnswerDetails.find(1).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"存在しないanswer_id指定で削除しても削除されない" in {
				result20.id == 1
			}
			EncAnswerDetails.deleteByAnswerId(answerId)
			val result21 = EncAnswerDetails.find(1).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"answer_id指定で削除できる" in {
				result21.id == -1
			}
		}

		running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
			val formId = 1
			val cal = Calendar.getInstance()
			val now = cal.getTime

			EncAnswers.regist(EncAnswers(1, formId, now, Option(now), Option(""), Option(""), Option(""), Option(""), Option(""), Option(""), "1", "127.0.0.1", "mozila", now, now))
			EncAnswerDetails.regist(answerDetail)

			val result20 = EncAnswerDetails.deleteByFormid(23423)
			"存在しないform_id指定で削除しても削除されない" in {
				result20 == 0
			}
			val result21 = EncAnswerDetails.deleteByFormid(formId)
			"form_id指定で削除できる" in {
				result21 == 1
			}
		}
	}

	"更新処理のテスト" should {
		val cal = Calendar.getInstance()

		val answerDetailId = 1
		val answerId = 3
		val formItemId = 4
		val formItemValueId = 5
		val answerValue = "update answer"
		val answerAt = cal.getTime
		val createAt = cal.getTime
		val updateAt = cal.getTime
		val now = cal.getTime

		val answerDetail = EncAnswerDetails(1, 2, 3, 4, "1", Option(now), createAt, updateAt)
		val updateAnswerDetail = EncAnswerDetails(answerDetailId, answerId, formItemId, formItemValueId, answerValue, Option(answerAt), createAt, updateAt)

		running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
			EncAnswerDetails.regist(answerDetail)
			val result = EncAnswerDetails.find(answerDetailId).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"データが正常に登録できる" in {
				result.answerId == 2
			}

			try {
				EncAnswerDetails.update(EncAnswerDetails.getBrankAnswerDetail)
			} catch {
				case e:Exception => {
					"存在しないidで更新した場合更新されない" in {
						true
					}
				}
			}

			EncAnswerDetails.update(updateAnswerDetail)
			val result3 = EncAnswerDetails.find(answerDetailId).getOrElse(EncAnswerDetails.getBrankAnswerDetail)
			"データが正常に更新できる" in {
				"answer_idが更新されている" in {
					result3.answerId == answerId
				}
				"form_item_idが更新されている" in {
					result3.formItemId == formItemId
				}
				"form_item_value_idが更新されている" in {
					result3.formItemValueId == formItemValueId
				}
				"answer_valueが更新されている" in {
					result3.answerValue == answerValue
				}
			}
		}
	}
}
