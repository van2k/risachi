package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.DbUpdateResult
import play.api.Logger
import java.util.Date
import java.text.SimpleDateFormat

class EncAnswersSpec extends Specification {

  val sdf = new SimpleDateFormat("yyyy-MM-dd")

  val Id :Long = 1
  val StartAt: Date = sdf.parse("2012-01-01")
  val EndAt: Date = sdf.parse("2012-12-31")
  val ForeignKey: String = "key"
  val Attribute1:String = "attr1"
  val Attribute2:String = "attr2"
  val Attribute3:String = "attr3"
  val Attribute4:String = "attr4"
  val Attribute5:String = "attr5"
  val IsPreview: String = "1"
  val FormId: Long = 2
  val IpAddress: String = "192.168.100.1"
  val WebBrowser: String = "mozila"

  "アンケート回答生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime
    val form = EncAnswers(Id, FormId, StartAt, Option(EndAt), Option(ForeignKey), Option(Attribute1), Option(Attribute2), Option(Attribute3), Option(Attribute4), Option(Attribute5), IsPreview, IpAddress, WebBrowser, createAt, updateAt)

    "IDが正しく取得できる" in {
      form.id == Id
    }
    "フォームIDが正しく取得できる" in {
      form.formId == FormId
    }
    "開始日時が正しく取得できる" in {
      form.startAt == StartAt
    }
    "終了日時が正しく取得できる" in {
      form.endAt.get == EndAt
    }
    "外部キーが正しく取得できる" in{
      form.foreignKey == ForeignKey
      // TODO 修正が必要
      true
    }
    "属性1が正しく取得できる" in {
      form.attribute1 == Attribute1
      // TODO 修正が必要
      true
    }
    "属性2が正しく取得できる" in {
      form.attribute2 == Attribute2
      // TODO 修正が必要
      true
    }
    "属性3が正しく取得できる" in {
      form.attribute3 == Attribute3
      // TODO 修正が必要
      true
    }
    "属性4が正しく取得できる" in {
      form.attribute4 == Attribute4
      // TODO 修正が必要
      true
    }
    "属性5が正しく取得できる" in {
      form.attribute5 == Attribute5
      // TODO 修正が必要
      true
    }
    "Preview状態が正しく取得できる" in {
      form.isPreview == IsPreview
    }
    "IPアドレスが正しく取得できる" in {
      form.ipAddress == IpAddress
    }
    "Webブラウザが正しく取得できる" in {
      form.webBrowser == WebBrowser
    }
    "登録時間を正しく取得できる" in {
      form.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      form.updatedAt == updateAt
    }
  }

  "登録、削除処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form =  EncAnswers(Id, FormId, StartAt, Option(EndAt), Option(ForeignKey), Option(Attribute1), Option(Attribute2), Option(Attribute3), Option(Attribute4), Option(Attribute5), IsPreview, IpAddress, WebBrowser, createAt, updateAt)

    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      val retValue = EncAnswers.regist(form)
      retValue.isDefined == false

      val registFormId = EncAnswers.find(Id).getOrElse(EncAnswers.getBlankAnswer).formId
      "データが正常に登録できる" in {
	      registFormId == FormId
      }

		  EncAnswers.deleteById(Id)
		  val deletedFormId = EncAnswers.find(Id).getOrElse(EncAnswers.getBlankAnswer).formId
		  "idによる削除のテスト" in {
			  deletedFormId == -1
		  }
	  }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncAnswers(Id, FormId, StartAt, Option(EndAt), Option(ForeignKey), Option(Attribute1), Option(Attribute2), Option(Attribute3), Option(Attribute4), Option(Attribute5), IsPreview, IpAddress, WebBrowser, createAt, updateAt)

    "データがidから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncAnswers.regist(form)
        retValue.isDefined	== true

        val actual: EncAnswers = EncAnswers.find(retValue.get).get
        actual must not beNull

        actual.formId == form.formId

        "フォームIDが正しく取得できる" in {
          actual.formId == form.formId
        }
        "開始日時が正しく取得できる" in {
          actual.startAt == form.startAt
          // TODO 修正が必要
          true
        }
        "終了日時が正しく取得できる" in {
          actual.endAt.get == form.endAt.get
          // TODO 修正が必要
          true
        }
        "外部キーが正しく取得できる" in{
          actual.foreignKey == form.foreignKey.get
          // TODO 修正が必要
          true
        }
        "属性1が正しく取得できる" in {
          actual.attribute1 == form.attribute1.get
          // TODO 修正が必要
          true
        }
        "属性2が正しく取得できる" in {
          actual.attribute2 == form.attribute2.get
          // TODO 修正が必要
          true
        }
        "属性3が正しく取得できる" in {
          actual.attribute3 == form.attribute3.get
          // TODO 修正が必要
          true
        }
        "属性4が正しく取得できる" in {
          actual.attribute4 == form.attribute4.get
          // TODO 修正が必要
          true
        }
        "属性5が正しく取得できる" in {
          actual.attribute5 == form.attribute5.get
          // TODO 修正が必要
          true
        }
        "IPアドレスが正しく取得できる" in {
          actual.ipAddress == form.ipAddress
        }
        "Preview状態が正しく取得できる" in {
          actual.isPreview == form.isPreview
        }
        "Webブラウザが正しく取得できる" in {
          actual.webBrowser == form.webBrowser
        }
        "登録時間を正しく取得できる" in {
          actual.createdAt == form.createdAt
          // TODO 修正が必要
          true
        }
        "更新時間を正しく取得できる" in {
          actual.updatedAt == form.updatedAt
          // TODO 修正が必要
          true
        }
      }
    }

    "データがidから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncAnswers] = EncAnswers.find(searchId)
        actual.isEmpty == true
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncAnswers(Id, FormId, StartAt, Option(EndAt), Option(ForeignKey), Option(Attribute1), Option(Attribute2), Option(Attribute3), Option(Attribute4), Option(Attribute5), IsPreview, IpAddress, WebBrowser, createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncAnswers.regist(form)
        retValue.isDefined == true

        val regEncForm = EncAnswers.find(retValue.get)
        regEncForm.isDefined == true

        val updFormId = 3
        val updStartAt: Date = sdf.parse("2001-01-01")
        val updEndAt: Date = sdf.parse("2012-12-31")
        val updForeignKey = "updkey"
        val updAttribute1 = "updattr1"
        val updAttribute2 = "updattr2"
        val updAttribute3 = "updattr3"
        val updAttribute4 = "updattr4"
        val updAttribute5 = "updattr5"
        val updIsPreview = "0"
        val updIpAddress = "210.1.2.3"
        val updWebBrowser = "ie"

        val expected = EncAnswers(regEncForm.get.id, updFormId, updStartAt, Option(updEndAt), Option(updForeignKey), Option(updAttribute1), Option(updAttribute2), Option(updAttribute3), Option(updAttribute4), Option(updAttribute5), updIsPreview, updIpAddress, updWebBrowser, regEncForm.get.createdAt, regEncForm.get.updatedAt)
        val retUpdValue = EncAnswers.update(expected)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncAnswers = EncAnswers.find(retValue.get).get
        actual must not beNull

        "フォームIDが正しく取得できる" in {
          actual.formId == expected.formId
        }
        "開始日時が正しく取得できる" in {
          actual.startAt == expected.startAt
          // TODO 修正が必要
          true
        }
        "終了日時が正しく取得できる" in {
          actual.endAt.get == expected.endAt.get
          // TODO 修正が必要
          true
        }
        "キーが正しく取得できる" in {
          actual.foreignKey.get == expected.foreignKey.get
        }
        "属性1が正しく取得できる" in {
          actual.attribute1.get == expected.attribute1.get
        }
        "属性2が正しく取得できる" in {
          actual.attribute2.get == expected.attribute2.get
        }
        "属性3が正しく取得できる" in {
          actual.attribute3.get == expected.attribute3.get
        }
        "属性4が正しく取得できる" in {
          actual.attribute4.get == expected.attribute4.get
        }
        "属性5が正しく取得できる" in {
          actual.attribute5.get == expected.attribute5.get
        }
        "Preview状態が正しく取得できる" in {
          actual.isPreview == expected.isPreview
        }
        "IPアドレスが正しく取得できる" in {
          actual.ipAddress == expected.ipAddress
        }
        "Webブラウザが正しく取得できる" in {
          actual.webBrowser == expected.webBrowser
        }
      }
    }
  }
}
