package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.DbUpdateResult
import play.api.Logger

class EncMFormItemsSpec extends Specification {

  val ItemId: Long = 10
  val Name = "フォームテスト"
  val Kind = "1"
  val DataType = "2"
  val GraphKind = "3"
  var Regex = Option("")

  "アンケートフォーム生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncMFormItems(ItemId, Name, Kind, DataType, GraphKind, Regex, createAt, updateAt)

    "インスタンスを生成できる" in {
      form must not beNull
    }
    "項目名が正しく取得できる" in {
      form.name == Name
    }
    "区分が正しく取得できる" in {
      form.kind == Kind
    }
    "データ区分が正しく取得できる" in {
      form.dataType == DataType
    }
    "図区分が正しく取得できる" in {
      form.graphKind == GraphKind
    }
    "登録時間を正しく取得できる" in {
      form.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      form.updatedAt == updateAt
    }
  }
/*
  "登録処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime;
    val updateAt = cal.getTime;

    val form = EncMFormItems(ItemId, Name, Kind, DataType, GraphKind, createAt, updateAt)

    "データが正常に登録できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val retValue = EncMFormItems.regist(form)
        retValue == DbUpdateResult.success
      }
    }
  }
*/
 /*
  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime;
    val updateAt = cal.getTime;

    val form = EncMFormItems(12, Name, Kind, DataType, GraphKind, createAt, updateAt)

    "データがformIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncMFormItems.regist(form)
        retValue == DbUpdateResult.success

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 12

        val actual: EncMFormItems = EncMFormItems.find(searchId).get
        actual must not beNull

        actual.itemId == form.itemId

        "項目名が正しく取得できる" in {
          actual.name == form.name
        }
        "区分が正しく取得できる" in {
          actual.kind == form.kind
        }
        "データ区分が正しく取得できる" in {
          actual.dataType == form.dataType
        }
        "図区分が正しく取得できる" in {
          actual.graphKind == form.graphKind
        }
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncMFormItems] = EncMFormItems.find(searchId)
        actual.isEmpty == true
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime;
    val updateAt = cal.getTime;

    val form = EncMFormItems(13, Name, Kind, DataType, GraphKind, createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncMFormItems.regist(form)
        retValue == DbUpdateResult.success

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 13

        val regEncForm: EncMFormItems = EncMFormItems.find(searchId).get
        regEncForm must not beNull

        val updName = "変更した名前"
        val updKind = "9";
        val updDataType = "2";
        val updGraphKind = "3";

        val expected = EncMFormItems(regEncForm.itemId, updName, updKind, updDataType, updGraphKind
          , createAt, regEncForm.updatedAt);
        val retUpdValue = EncMFormItems.update(expected)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncMFormItems = EncMFormItems.find(searchId).get
        actual must not beNull

        "項目名が正しく取得できる" in {
          actual.name == expected.name
        }
        "区分が正しく取得できる" in {
          actual.kind == expected.kind
        }
        "データ区分が正しく取得できる" in {
          actual.dataType == expected.dataType
        }
        "図区分が正しく取得できる" in {
          actual.graphKind == expected.graphKind
        }

      }
    }
  }
  */
}
