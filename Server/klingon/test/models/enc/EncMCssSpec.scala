package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import scalikejdbc.{DB, SQL}


class EncMCssSpec extends Specification {

  val CssId: Long = 1
  val Name = "寒色系の色"
  val CssCode = "body { background-color:PowderBlue; }"
  val DispImageUrl = "/images/140x140.gif"

  "CSS生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val css = EncMCss(CssId, Name, CssCode, DispImageUrl, createAt, updateAt)

    "インスタンスを生成できる" in {
      css must not beNull
    }
    "表示名称が正しく取得できる" in {
      css.name == Name
    }
    "CSSコードが正しく取得できる" in {
      css.cssCode == CssCode
    }
    "表示用画像URLが正しく取得できる" in {
      css.dispImageUrl == DispImageUrl
    }
    "登録時間を正しく取得できる" in {
      css.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      css.updatedAt == updateAt
    }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val css = EncMCss(CssId, Name, CssCode, DispImageUrl, createAt, updateAt)

    "データが全件取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val testDatas :Array[Tuple4[Long, String, String, String]]=Array(
          (1, "青色", "body { background-color:blue }", "/image/blue.gif"),
          (2, "赤色", "body { background-color:red }", "/image/red.gif"),
          (3, "黄色", "body { background-color:yellow }", "/image/yellow.gif"),
          (4, "緑色", "body { background-color:green }", "/image/green.gif"),
          (5, "オレンジ色", "body { background-color:orange }", "/image/orange.gif")
        )

        DB autoCommit { implicit session =>
          // テストデータ投入
          testDatas.foreach {case (i, n, c, d) =>
            SQL("INSERT INTO m_css VALUES(%d, '%s', '%s', '%s', now(), now())".format(i, n, c, d)).update.apply()
          }
        }


        val actual: List[EncMCss] = EncMCss.findAll()
        "件数が正しく取得できる" in {
          actual.length == testDatas.length
        }

        "表示名称、CSSコード、表示用画像URLが正しく取得できる" in {
          for (record <- actual) {
            testDatas.foreach {
              case (i, n, c, d) if i == record.id => {
                "表示名称が正しく取得できる" in {
                  record.name == n
                }
                "CSSコードが正しく取得できる" in {
                  record.cssCode == c
                }
                "表示用画像URLが正しく取得できる" in {
                  record.dispImageUrl == d
                }
              }
              // IDが一致しない場合は評価しない
              case _ =>
            }
          }
          true
        }


      }
    }

    "データがIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        DB autoCommit {
          implicit session =>
          // テストデータ投入
            SQL("INSERT INTO m_css VALUES(1, '%s', '%s', '%s', now(), now())".format(Name, CssCode, DispImageUrl)).update.apply()

        }

        val searchId = 1

        val actual: EncMCss = EncMCss.findById(searchId).get
        actual must not beNull

        "表示名称が正しく取得できる" in {
          actual.name == css.name
        }
        "CSSコードが正しく取得できる" in {
          actual.cssCode == css.cssCode
        }
        "表示用画像URLが正しく取得できる" in {
          actual.dispImageUrl == css.dispImageUrl
        }
      }
    }

    "データがIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        DB autoCommit {
          implicit session =>
          // テストデータ投入
            SQL("INSERT INTO m_css VALUES(1, '%s', '%s', '%s', now(), now())".format(Name, CssCode, DispImageUrl)).update.apply()
        }

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = -1

        val actual: Option[EncMCss] = EncMCss.findById(searchId)
        actual.isEmpty == true
      }
    }
  }

}
