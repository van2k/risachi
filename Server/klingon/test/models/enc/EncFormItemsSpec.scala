package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.{ DbUpdateResult}
import play.api.Logger

class EncFormItemsSpec extends Specification{

  val ItemId: Long = 1
  val FormId: Long = 2
  val CategoryId: Long = 3
  val MItemId: Long = 4
  val Name = "フォームテスト"
  val PageNo: Long = 5
  val Required = "1"
  val SelectNumber: Long = 4
  val Condition = "5"
  val DispOdr = 1

  "アンケートフォーム生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItems(ItemId, FormId, CategoryId, MItemId, Name, PageNo, SelectNumber, Condition, Required, DispOdr
      , createAt, updateAt)

    "インスタンスを生成できる" in {
      form must not beNull
    }
    "項目IDが正しく取得できる" in {
      form.itemId == ItemId
    }
    "フォームIDが正しく取得できる" in {
      form.formId == FormId
    }
    "カテゴリIDが正しく取得できる" in {
      form.categoryId == CategoryId
    }
    "項目名が正しく取得できる" in {
      form.name == Name
    }
    "ページ番号が正しく取得できる" in {
      form.pageNo == PageNo
    }
    "必須を正しく取得できる" in {
      form.required == Required
    }
    "選択数が正しく取得できる" in {
      form.selectNumber == SelectNumber
    }
    "条件を正しく取得できる" in {
      form.condition == Condition
    }
    "表示順を正しく取得できる" in {
	  form.dispodr == DispOdr
    }
    "登録時間を正しく取得できる" in {
      form.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      form.updatedAt == updateAt
    }
  }

  "登録処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItems(ItemId, FormId, CategoryId, MItemId, Name, PageNo, SelectNumber, Condition
      , Required, DispOdr, createAt, updateAt)

    "データが正常に登録できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val retValue = EncFormItems.regist(form,createAt)
        retValue.isDefined == true
      }
    }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItems(ItemId, FormId, CategoryId, MItemId, Name, PageNo, SelectNumber, Condition, Required , DispOdr
      , createAt, updateAt)

    "データがformIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItems.regist(form,createAt)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = retValue.get

        val actual: EncFormItems = EncFormItems.find(searchId).get
        actual must not beNull

        actual.formId == form.formId

        "フォームIDが正しく取得できる" in {
          actual.formId == form.formId
        }
        "カテゴリIDが正しく取得できる" in {
          actual.categoryId == form.categoryId
        }
        "項目マスタID名が正しく取得できる" in {
          actual.mItemId == form.mItemId
        }
        "項目名が正しく取得できる" in {
          actual.name == form.name
        }
        "ページ番号を正しく取得できる" in {
          actual.pageNo == form.pageNo
        }
        "必須を正しく取得できる" in {
          actual.required == form.required
        }
        "選択数が正しく取得できる" in {
          actual.selectNumber == form.selectNumber
        }
        "条件を正しく取得できる" in {
          actual.condition == form.condition
        }
        "表示順を正しく取得できる" in {
	      actual.dispodr == form.dispodr
	    }
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncFormItems] = EncFormItems.find(searchId)
        actual.isEmpty == true
      }
    }

    "データがcontractIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItems.regist(form, createAt)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormId = retValue.get

        val actualList: List[EncFormItems] = EncFormItems.findByFormId(searchFormId)
        actualList must not beNull

        actualList.foreach {
          actual => {
            actual.formId == form.formId

            "フォームIDが正しく取得できる" in {
              actual.formId == form.formId
            }
            "カテゴリIDが正しく取得できる" in {
              actual.categoryId == form.categoryId
            }
            "項目マスタID名が正しく取得できる" in {
              actual.mItemId == form.mItemId
            }
            "項目名が正しく取得できる" in {
              actual.name == form.name
            }
            "ページ番号を正しく取得できる" in {
              actual.pageNo == form.pageNo
            }
            "必須を正しく取得できる" in {
              actual.required == form.required
            }

            "選択数が正しく取得できる" in {
              form.selectNumber == SelectNumber
            }
            "条件を正しく取得できる" in {
              form.condition == Condition
            }

	        "選択数が正しく取得できる" in {
              actual.selectNumber == form.selectNumber
            }
            "条件を正しく取得できる" in {
              actual.condition == form.condition
            }
            "表示順を正しく取得できる" in {
              actual.dispodr == form.dispodr
            }
          }
        }
        true
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormId = FormId

        val actual: List[EncFormItems] = EncFormItems.findByFormId(searchFormId)
        actual.isEmpty == true
      }
    }

    "データがformIdとcreateAtから取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItems.regist(form, createAt)
        retValue.isDefined == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormId = FormId

        val actual: EncFormItems = EncFormItems.findByFormIdCreateAt(searchFormId,createAt)
        actual must not beNull
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItems(ItemId, FormId, CategoryId, MItemId, Name, PageNo, SelectNumber, Condition, Required , DispOdr
      , createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItems.regist(form,createAt)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = retValue.get

        val regEncForm: EncFormItems = EncFormItems.find(searchId).get
        regEncForm must not beNull

        val updFormId = FormId + 1
        val updCategoryId = CategoryId + 1;
        val updMItemId = MItemId + 1;
        val updName = "変更した名前"
        val updPageNo = PageNo + 1;
        val updRequired = "0";
        val updSelectNumber = SelectNumber + 1;
        val updCondition = "2";
        val updDispOdr = 2

        val expected = EncFormItems(regEncForm.itemId, updFormId, updCategoryId, updMItemId, updName, updPageNo
          , updSelectNumber, updCondition, updRequired, updDispOdr, createAt, regEncForm.updatedAt)
        val retUpdValue = EncFormItems.update(expected, updateAt)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncFormItems = EncFormItems.find(searchId).get
        actual must not beNull

        "フォームIDが正しく取得できる" in {
          actual.formId == expected.formId
        }
        "カテゴリIDが正しく取得できる" in {
          actual.categoryId == expected.categoryId
        }
        "項目マスタID名が正しく取得できる" in {
          actual.mItemId == expected.mItemId
        }
        "項目名が正しく取得できる" in {
          actual.name == expected.name
        }
        "ページ番号を正しく取得できる" in {
          actual.pageNo == expected.pageNo
        }
        "必須を正しく取得できる" in {
          actual.required == expected.required
        }

        "選択数を正しく取得できる" in {
          actual.selectNumber == expected.selectNumber
        }
        "条件を正しく取得できる" in {
          actual.condition == expected.condition
        }
	    "選択数を正しく取得できる" in {
	       actual.selectNumber == expected.selectNumber
	    }
	    "条件を正しく取得できる" in {
	       actual.condition == expected.condition
	    }
	    "表示順を正しく取得できる" in {
	       actual.dispodr == expected.dispodr
	    }
      }
    }
  }
}
