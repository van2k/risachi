package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.{ DbUpdateResult}
import play.api.Logger

class EncFormSpec extends Specification{
  val FormId: Long = 1
  val ContractId: Long = 999
  val FormName = "フォームテスト"
  val Description = "フォーム説明"
  val Message = "アンケートの回答ありがとうございました。"
  val PageNumber = 2
  val CssId = Option(Long.MinValue)
  val OrgCss = Option("CSS")
  val ImageUrl = Option("URL")
  val Status = "1"
	val UrlKey = "1"

  "アンケートフォーム生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncForms(FormId, ContractId, FormName, Description, Message, PageNumber, CssId, OrgCss, ImageUrl, Status, UrlKey, createAt, updateAt)

    "インスタンスを生成できる" in {
      form must not beNull
    }
    "フォームIDが正しく取得できる" in {
      form.formId == FormId
    }
    "契約者IDが正しく取得できる" in {
      form.contractId == ContractId
    }
    "フォーム名が正しく取得できる" in {
      form.formName == FormName
    }
    "フォーム説明が正しく取得できる" in {
      form.formDesc == Description
    }
    "アンケート回答時挨拶を正しく取得できる" in {
      form.resultMessage == Message
    }
    "アンケートページ数を正しく取得できる" in {
      form.totalPageCount == PageNumber
    }
    "CSS Idを正しく取得できる" in {
      form.cssId.head == CssId.head
    }
    "オリジナルCSSを正しく取得できる" in {
      form.orgCss.head == OrgCss.head
    }
    "画像URLを正しく取得できる" in {
      form.imageUrl.head == ImageUrl.head
    }
    "ステータスを正しく取得できる" in {
      form.status == Status
    }
    "登録時間を正しく取得できる" in {
      form.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      form.updatedAt == updateAt
    }
  }

  "登録処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncForms(FormId, ContractId, FormName, Description, Message, PageNumber, CssId, OrgCss, ImageUrl, Status, UrlKey, createAt, updateAt)

    "データが正常に登録できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val newid = EncForms.regist(form)
        newid must not beNull

        val actual = EncForms.find(newid.get);
        "契約者IDが正しく取得できる" in {
          actual.get.contractId == form.contractId
        }
        "フォーム名が正しく取得できる" in {
          actual.get.formName == form.formName
        }
        "フォーム説明が正しく取得できる" in {
          actual.get.formDesc == form.formDesc
        }
        "アンケート回答時挨拶を正しく取得できる" in {
          actual.get.resultMessage == form.resultMessage
        }
        "アンケートページ数を正しく取得できる" in {
          actual.get.totalPageCount == form.totalPageCount

        }
      }
    }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncForms(FormId, ContractId, FormName, Description, Message, PageNumber,  CssId, OrgCss, ImageUrl, Status, UrlKey, createAt, updateAt)

    "データがformIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncForms.regist(form)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = retValue.get

        val actual: EncForms = EncForms.find(searchId).get
        actual must not beNull

        actual.formId == form.formId

        "フォームIDが正しく取得できる" in {
          actual.formId == searchId
        }
        "契約者IDが正しく取得できる" in {
          actual.contractId == form.contractId
        }
        "フォーム名が正しく取得できる" in {
          actual.formName == form.formName
        }
        "フォーム説明が正しく取得できる" in {
          actual.formDesc == form.formDesc
        }
        "アンケート回答時挨拶を正しく取得できる" in {
          actual.resultMessage == form.resultMessage
        }
        "アンケートページ数を正しく取得できる" in {
          actual.totalPageCount == form.totalPageCount
        }
        "CSS Idを正しく取得できる" in {
          actual.cssId.head == form.cssId.head
        }
        "オリジナルCSSを正しく取得できる" in {
          actual.orgCss.head == form.orgCss.head
        }
        "画像URLを正しく取得できる" in {
          actual.imageUrl.head == form.imageUrl.head
        }
        "ステータスを正しく取得できる" in {
          actual.status == form.status
        }
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncForms] = EncForms.find(searchId)
        actual.isEmpty == true
      }
    }

    "データがcontractIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncForms.regist(form)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchContractId = ContractId

        val actualList: List[EncForms] = EncForms.findByContractId(searchContractId)
        actualList must not beNull

        actualList.foreach {
          actual => {
            actual.formId == form.formId

            "フォームIDが正しく取得できる" in {
              actual.formId == retValue.get
            }
            "契約者IDが正しく取得できる" in {
              actual.contractId == form.contractId
            }
            "フォーム名が正しく取得できる" in {
              actual.formName == form.formName
            }
            "フォーム説明が正しく取得できる" in {
              actual.formDesc == form.formDesc
            }
            "アンケート回答時挨拶を正しく取得できる" in {
              actual.resultMessage == form.resultMessage
            }
            "アンケートページ数を正しく取得できる" in {
              actual.totalPageCount == form.totalPageCount
            }
            "CSS Idを正しく取得できる" in {
              actual.cssId.head == form.cssId.head
            }
            "オリジナルCSSを正しく取得できる" in {
              actual.orgCss.head == form.orgCss.head
            }
            "画像URLを正しく取得できる" in {
              actual.imageUrl.head == form.imageUrl.head
            }
            "ステータスを正しく取得できる" in {
              actual.status == form.status
            }
          }
        }
        true
      }
    }

    "データがcontractIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchContractId = ContractId

        val actual: List[EncForms] = EncForms.findByContractId(searchContractId)
        actual.isEmpty == true
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncForms(FormId, ContractId, FormName, Description, Message, PageNumber,  CssId, OrgCss, ImageUrl, Status, UrlKey, createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncForms.regist(form)

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = retValue.get

        val regEncForm: EncForms = EncForms.find(searchId).get
        regEncForm must not beNull

        val updContractId = ContractId + 1
        val updFormName = "変更した名前"
        val updFormDesc = "変更した説明"
        val updMessage = "変更したメッセージ"
        val updTotalPageCount = PageNumber + 1

        val expected = EncForms(regEncForm.formId, updContractId, updFormName, updFormDesc, updMessage, updTotalPageCount, CssId, OrgCss, ImageUrl, Status, regEncForm.urlKey, createAt, regEncForm.updatedAt)

        val updatedAt = Calendar.getInstance().getTime
        val retUpdValue = EncForms.update(expected, updatedAt)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncForms = EncForms.find(searchId).get
        actual must not beNull

        "フォームIDが正しく取得できる" in {
          actual.formId == expected.formId
        }
        "契約者IDが正しく取得できる" in {
          actual.contractId == expected.contractId
        }
        "フォーム名が正しく取得できる" in {
          actual.formName == expected.formName
        }
        "フォーム説明が正しく取得できる" in {
          actual.formDesc == expected.formDesc
        }
        "アンケート回答時挨拶を正しく取得できる" in {
          actual.resultMessage == expected.resultMessage
        }
        "アンケートページ数を正しく取得できる" in {
          actual.totalPageCount == expected.totalPageCount
        }
        "CSS Idを正しく取得できる" in {
          actual.cssId.head == expected.cssId.head
        }
        "オリジナルCSSを正しく取得できる" in {
          actual.orgCss.head == expected.orgCss.head
        }
        "画像URLを正しく取得できる" in {
          actual.imageUrl.head == expected.imageUrl.head
        }
        "ステータスを正しく取得できる" in {
          actual.status == expected.status
        }
      }
    }

    "ステータスを正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // いくつかフォームを登録する
        val id1 = EncForms.regist(form)
        val id2 = EncForms.regist(EncForms(2, ContractId, FormName, Description, Message, PageNumber, CssId, OrgCss, ImageUrl, Status, "2", createAt, updateAt))
        val id3 = EncForms.regist(EncForms(3, 100, FormName, Description, Message, PageNumber, CssId, OrgCss, ImageUrl, Status, "3", createAt, updateAt))
        val id4 = EncForms.regist(EncForms(4, ContractId, FormName, Description, Message, PageNumber, CssId, OrgCss, ImageUrl, Status, "4", createAt, updateAt))
        id1 must beSome
        id2 must beSome
        id3 must beSome
        id4 must beSome

        // 特定のフォームのステータスを変更できるか
        val new_updated_at = Calendar.getInstance().getTime()
        val ua = EncForms.find(id3.get).get.updatedAt
        EncForms.updateStatus(id3.get, "2", ua, new_updated_at)
        EncForms.find(id3.get).get.status === "2"

        // 特定の契約者のすべてのフォームのステータスを変更できる
        EncForms.updateStatusByContractId(ContractId, "9")
        EncForms.find(id1.get).get.status === "9"
        EncForms.find(id2.get).get.status === "9"
        EncForms.find(id3.get).get.status === "2"
        EncForms.find(id4.get).get.status === "9"
      }
    }

    "対象データがすでに更新されているため更新できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // todo:実装すること
        true
      }
    }

    "画像アップロードのテスト" in {
      "画像が正しくアップできる" in {
        // TODO 実装すること
        true
      }

      "画像のパスが正しくない" in {
        // TODO 実装すること
        true
      }

      "フォームIDが正しくない" in {
        // TODO 実装すること
        true
      }
    }

    "適用CSS更新のテスト" in {
      "CSSが正しく更新できる" in {
        // TODO 実装すること
        true
      }

      "CSSのIDが未指定" in {
        // TODO 実装すること
        true
      }

      "フォームIDが正しくない" in {
        // TODO 実装すること
        true
      }
    }
  }
}
