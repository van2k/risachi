package models.enc

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import java.util.Calendar
import java.text.SimpleDateFormat

class EncFormConfigsSpec extends Specification {
  val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
  val now = Calendar.getInstance().getTime
  val encFormConfigs = EncFormConfigs(1, 1, 10, now, Option(now), "0", 2, 10, 10,
    Option(4), Option(10000), Option(1000), None, Option(1), "0", now, now)

  "支払い確認のテスト" should {
    "支払い確認時刻を現在時刻を登録できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 挿入
      EncFormConfigs.regist(encFormConfigs)
      // 挿入したデータの支払い日を取得
      EncFormConfigs.findByFormid(1).get.paymentConfirmDate must beNone
      // 支払い時刻登録
      EncFormConfigs.setPaymentConfirmDate(1)
      // 挿入したデータの支払い日を取得
      EncFormConfigs.findByFormid(1).get.paymentConfirmDate must beSome
    }

    "支払い確認額を登録できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 挿入
      EncFormConfigs.regist(encFormConfigs)
      // 支払い金額登録
      EncFormConfigs.setPaymentConfirm(1, 2000)
      // 挿入したデータの支払い日を取得
      EncFormConfigs.findByFormid(1).get.paymentConfirm.get === 2000
    }
  }
}
