package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.{ DbUpdateResult}
import play.api.Logger

class EncFormItemValuesSpec extends Specification{

  val ValueId: Long = 1
  val FormItemId: Long = 2
  val FormValue = "テスト"
  val ChLength: Long = 3

  "アンケートフォーム生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItemValues(ValueId, FormItemId, FormValue, ChLength, createAt, updateAt)

    "インスタンスを生成できる" in {
      form must not beNull
    }
    "項目値IDが正しく取得できる" in {
      form.formItemId == FormItemId
    }
    "フォーム値が正しく取得できる" in {
      form.formValue == FormValue
    }
    "文字数が正しく取得できる" in {
      form.chLength == ChLength
    }
  }

  "登録処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItemValues(ValueId, FormItemId, FormValue, ChLength, createAt, updateAt)

    "データが正常に登録できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == DbUpdateResult.success
      }
    }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form =  EncFormItemValues(ValueId, FormItemId, FormValue, ChLength, createAt, updateAt)

    "データがformIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: EncFormItemValues = EncFormItemValues.find(searchId).get
        actual must not beNull

        actual.itemId == form.itemId

        "フォーム値IDが正しく取得できる" in {
          actual.formItemId == form.formItemId
        }
        "フォーム値が正しく取得できる" in {
          actual.formValue == form.formValue
        }
        "文字数が正しく取得できる" in {
          actual.chLength == form.chLength
        }
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncFormItemValues] = EncFormItemValues.find(searchId)
        actual.isEmpty == true
      }
    }

    "データがcontractIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormItemId = FormItemId

        val actualList: List[EncFormItemValues] = EncFormItemValues.findByFormItemId(searchFormItemId)
        actualList must not beNull

        actualList.foreach {
          actual => {
            actual.itemId == form.itemId

            "フォーム値IDが正しく取得できる" in {
              actual.formItemId == form.formItemId
            }
            "フォーム値が正しく取得できる" in {
              actual.formValue == form.formValue
            }
            "文字数が正しく取得できる" in {
              actual.chLength == form.chLength
            }

          }
        }
        true
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormItemId = FormItemId

        val actual: List[EncFormItemValues] = EncFormItemValues.findByFormItemId(searchFormItemId)
        actual.isEmpty == true
      }
    }

    "データがformIdと作成日時から正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: List[EncFormItemValues] = EncFormItemValues.findByFormItemIdCreateAt(form.formItemId, form.createdAt)
        actual.isEmpty === false
        actual.size === 1
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormItemValues(ValueId, FormItemId, FormValue, ChLength, createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val regEncForm: EncFormItemValues = EncFormItemValues.find(searchId).get
        regEncForm must not beNull

        val updFormItemId = FormItemId + 1
        val updFormValue = "変更した名前"
        val updChLength = ChLength + 1

        val expected = EncFormItemValues(regEncForm.itemId, updFormItemId, updFormValue, updChLength, createAt, regEncForm.updatedAt)
        val retUpdValue = EncFormItemValues.update(expected, updateAt)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncFormItemValues = EncFormItemValues.find(searchId).get
        actual must not beNull


	    "フォーム値IDが正しく取得できる" in {
           actual.formItemId == expected.formItemId
        }
        "フォーム値が正しく取得できる" in {
           actual.formValue == expected.formValue
        }
        "文字数が正しく取得できる" in {
           actual.chLength == expected.chLength
        }

      }
    }
  }

  "削除処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val ItemId: Long = 1
    val FormId: Long = 2
    val CategoryId: Long = 3
    val MItemId: Long = 4
    val Name = "フォームテスト"
    val PageNo: Long = 5
    val Required = "1"
    val SelectNumber: Long = 4
    val Condition = "5"
    val DispOdr = 1

    val formItem = EncFormItems(ItemId, FormId, CategoryId, MItemId, Name, PageNo, SelectNumber, Condition, Required , DispOdr
      , createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val regFormItemId = EncFormItems.regist(formItem, createAt)
        regFormItemId.get !== 0

        val form = EncFormItemValues(ValueId, regFormItemId.get, FormValue, ChLength, createAt, updateAt)

        val retValue = EncFormItemValues.regist(form, createAt)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val regEncForm: EncFormItemValues = EncFormItemValues.find(searchId).get
        regEncForm must not beNull

        // 削除処理を行う
        val retCount = EncFormItemValues.deleteByFormid(FormId)
        retCount === 1

      }
    }
  }
}
