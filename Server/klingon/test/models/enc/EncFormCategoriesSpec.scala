package models.enc

import play.api.test._
import play.api.test.Helpers._
import java.util.Calendar
import org.specs2.mutable.Specification
import models.DbUpdateResult
import play.api.Logger

class EncFormCategoriesSpec extends Specification {

  val CategoryId :Long = 1
  val FormId: Long = 2
  val Name = "フォームテスト"
  val Description = "説明"

  "アンケートフォーム生成のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormCategories(CategoryId, FormId, Name, Description, createAt, updateAt)

    "カテゴリIDが正しく取得できる" in {
      form.categoryId == CategoryId
    }
    "フォームIDが正しく取得できる" in {
      form.formId == FormId
    }
    "項目名が正しく取得できる" in {
      form.name == Name
    }
    "説明が正しく取得できる" in {
      form.description == Description
    }
    "登録時間を正しく取得できる" in {
      form.createdAt == createAt
    }
    "更新時間を正しく取得できる" in {
      form.updatedAt == updateAt
    }
  }

  "登録処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormCategories(CategoryId, FormId, Name, Description, createAt, updateAt)

    "データが正常に登録できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val retValue = EncFormCategories.regist(form)
        retValue == DbUpdateResult.success
      }
    }
  }

  "検索処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormCategories(CategoryId, FormId, Name, Description, createAt, updateAt)

    "データがformIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormCategories.regist(form)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: EncFormCategories = EncFormCategories.find(searchId).get
        actual must not beNull

        actual.formId == form.formId

        "カテゴリIDが正しく取得できる" in {
          actual.categoryId == form.categoryId
        }
        "フォームIDが正しく取得できる" in {
          actual.formId == form.formId
        }
        "項目名が正しく取得できる" in {
          actual.name == form.name
        }
        "説明が正しく取得できる" in {
          actual.description == form.description
        }
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val actual: Option[EncFormCategories] = EncFormCategories.find(searchId)
        actual.isEmpty == true
      }
    }

    "データがcontractIdから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormCategories.regist(form)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormId = FormId

        val actualList: List[EncFormCategories] = EncFormCategories.findByFormId(searchFormId)
        actualList must not beNull

        actualList.foreach {
          actual => {
            actual.formId == form.formId

	        "カテゴリIDが正しく取得できる" in {
              actual.categoryId == form.categoryId
            }
            "フォームIDが正しく取得できる" in {
              actual.formId == form.formId
            }
            "項目名が正しく取得できる" in {
              actual.name == form.name
            }
            "説明が正しく取得できる" in {
              actual.description == form.description
            }
          }
        }
        true
      }
    }

    "データがformIdから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchFormId = FormId

        val actual: List[EncFormCategories] = EncFormCategories.findByFormId(searchFormId)
        actual.isEmpty == true
      }
    }
  }

  "更新処理のテスト" should {
    val cal = Calendar.getInstance()

    val createAt = cal.getTime
    val updateAt = cal.getTime

    val form = EncFormCategories(CategoryId, FormId, Name, Description, createAt, updateAt)

    "データが正常に更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = EncFormCategories.regist(form)
        retValue == true

        // テストごとにimmemoryDBは初期化されるため登録したデータのIDは1となる
        val searchId = 1

        val regEncForm: EncFormCategories = EncFormCategories.find(searchId).get
        regEncForm must not beNull

        val updFormId = 3
        val updName = "変更した名前"
        val updDescription = "変更した説明"

        val expected = EncFormCategories(regEncForm.categoryId, updFormId, updName, updDescription, createAt, regEncForm.updatedAt)
        val retUpdValue = EncFormCategories.update(expected)
        retUpdValue === DbUpdateResult.success
        Logger.debug("retUpdValue:" + retUpdValue)

        val actual: EncFormCategories = EncFormCategories.find(searchId).get
        actual must not beNull

        "フォームIDが正しく取得できる" in {
	        actual.formId == expected.formId
	    }
	    "項目名が正しく取得できる" in {
	      actual.name == expected.name
	    }
	    "説明を正しく取得できる" in {
	       actual.description == expected.description
	    }
      }
    }
  }
}
