package models.enc

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import java.util.Calendar
import models.{contract,charge,enc}
import java.text.SimpleDateFormat

class EncFormsSearchSpec extends Specification {

  "フォーム検索のテスト" should {
    val now = Calendar.getInstance().getTime
    val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
    val date_02_10 = dateFormat.parse("2012/02/10")
    val date_03_15 = dateFormat.parse("2012/03/15")
    val date_04_20 = dateFormat.parse("2012/04/20")
    val date_05_25 = dateFormat.parse("2012/05/25")
    var form_id1 = -1L
    var form_id2 = -1L
    var form_id3 = -1L
    var form_id4 = -1L

    // テストで共通の前段処理
    def testSearch[T](block : => T) = running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 契約者を２件挿入
      contract.Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "石川五右衛門", "abc", now) must beTrue
      contract.Contracts.directInsert("b@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "佐々木小次郎", "def", now) must beTrue
      // フォーム挿入
      form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
        None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
      form_id2 = enc.EncForms.regist(enc.EncForms(2, 1, "フォーム名2", "フォーム2です", "結果メッセージ2", 2,
        None, None, None, enc.EncForms.STATUS_EDIT, "2", now, now)).get
      form_id3 = enc.EncForms.regist(enc.EncForms(3, 2, "フォーム名3", "フォーム3です", "結果メッセージ3", 2,
        None, None, None, enc.EncForms.STATUS_CANCEL, "3", now, now)).get
      form_id4 = enc.EncForms.regist(enc.EncForms(4, 2, "フォーム名4", "フォーム4です", "結果メッセージ4", 2,
        None, None, None, enc.EncForms.STATUS_RUN, "4", now, now)).get
      // フォーム設定挿入
      enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
        Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
      enc.EncFormConfigs.regist(enc.EncFormConfigs(2, form_id2, 10, date_03_15, Option(date_04_20), "0", 2, 10, 20,
        Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue
      enc.EncFormConfigs.regist(enc.EncFormConfigs(3, form_id3, 10, date_02_10, Option(date_04_20), "0", 2, 10, 20,
        Option(5), Option(10000), None, None, Option(3), "0", now, now)) must beTrue
      enc.EncFormConfigs.regist(enc.EncFormConfigs(4, form_id4, 10, date_04_20, Option(date_05_25), "0", 2, 10, 20,
        Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue

      block
    }

    "名前で検索できる" in testSearch {
      val result = EncFormsSearch.search("石川", None, None, None, None, null, false, false, false, false)
      result.length === 2
      (result(0).form_id == form_id1 || result(0).form_id == form_id2) must beTrue
      (result(1).form_id == form_id1 || result(1).form_id == form_id2) must beTrue
    }

    "掲載開始日で検索できる1" in testSearch {
      val result = EncFormsSearch.search(null, Option(date_03_15), None, None, None, null, false, false, false, false)
      result.length === 2
      (result(0).form_id == form_id2 || result(0).form_id == form_id4) must beTrue
      (result(1).form_id == form_id2 || result(1).form_id == form_id4) must beTrue
    }

    "掲載開始日で検索できる2" in testSearch {
      val result = EncFormsSearch.search(null, None, Option(date_03_15), None, None, null, false, false, false, false)
      result.length === 3
      result(0).form_id !== form_id4
      result(1).form_id !== form_id4
      result(2).form_id !== form_id4
    }

    "掲載開始日で検索できる3" in testSearch {
      val result = EncFormsSearch.search(null, Option(date_03_15), Option(date_04_20), None, None, null, false, false, false, false)
      result.length === 2
      (result(0).form_id == form_id2 || result(0).form_id == form_id4) must beTrue
      (result(1).form_id == form_id2 || result(1).form_id == form_id4) must beTrue
    }

    "掲載終了日で検索できる1" in testSearch {
      val result = EncFormsSearch.search(null, None, None, Option(date_04_20), None, null, false, false, false, false)
      result.length === 3
      result(0).form_id !== form_id1
      result(1).form_id !== form_id1
      result(2).form_id !== form_id1
    }

    "掲載終了日で検索できる2" in testSearch {
      val result = EncFormsSearch.search(null, None, None, None, Option(date_04_20), null, false, false, false, false)
      result.length === 3
      result(0).form_id !== form_id4
      result(1).form_id !== form_id4
      result(2).form_id !== form_id4
    }

    "掲載終了日で検索できる3" in testSearch {
      val result = EncFormsSearch.search(null, None, None, Option(date_02_10), Option(date_03_15), null, false, false, false, false)
      result.length === 1
      result(0).form_id === form_id1
    }

    "料金プランで検索できる" in testSearch {
      val plans = List("1", "2")
      val result = EncFormsSearch.search(null, None, None, None, None, plans, false, false, false, false)
      result.length === 3
      result(0).form_id !== form_id3
      result(1).form_id !== form_id3
      result(2).form_id !== form_id3
    }

    "支払い確認済みでないものを検索できる" in testSearch {
      val result = EncFormsSearch.search(null, None, None, None, None, null, true, false, false, false)
      result.length === 3
      result(0).form_id !== form_id1
      result(1).form_id !== form_id1
      result(2).form_id !== form_id1
    }

    "支払い確認済みのものを検索できる" in testSearch {
      val result = EncFormsSearch.search(null, None, None, None, None, null, false, true, false, false)
      result.length === 1
      result(0).form_id === form_id1
    }

    "掲載中のものを検索できる" in testSearch {
      val result = EncFormsSearch.search(null, None, None, None, None, null, false, false, true, false)
      result.length === 2
      (result(0).form_id == form_id1 || result(0).form_id == form_id4) must beTrue
      (result(1).form_id == form_id1 || result(1).form_id == form_id4) must beTrue
    }

    "掲載中でないものを検索できる" in testSearch {
      val result = EncFormsSearch.search(null, None, None, None, None, null, false, false, false, true)
      result.length === 2
      (result(0).form_id == form_id2 || result(0).form_id == form_id3) must beTrue
      (result(1).form_id == form_id2 || result(1).form_id == form_id3) must beTrue
    }
  }
}
