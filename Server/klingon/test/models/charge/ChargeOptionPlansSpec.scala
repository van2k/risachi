package models

import org.specs2.mutable._
import org.joda.time._
import models.charge.ChargeOptionPlans
import play.api.test.FakeApplication
import play.api.test.Helpers._


class ChargeOptionPlansSpec extends Specification {

  "ChargeOptionPlans" should {

    "存在する主キーでデータを取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val maybeFound = ChargeOptionPlans.findById(1L)
        maybeFound.isDefined should beTrue
      }
    }

    "すべてのデータを取得できる" in  {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val allResults = ChargeOptionPlans.findAll()
        allResults.size should be_>(0)
      }
    }

    "データの登録が行える" in  {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val plan = ChargeOptionPlans(id = 999L, charge = 123, tax = 123, description = "テストデータ"
          , applyStartDate = DateTime.now.toDate, createdAt = DateTime.now.toDate, updatedAt = DateTime.now.toDate
          , optionType = "a", optionValue = 123)
        val created = ChargeOptionPlans.regist(plan)
        created === true
      }
    }
  }

}
        