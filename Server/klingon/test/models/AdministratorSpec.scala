package models

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication

class AdministratorSpec extends Specification {
  val default_userid = "administrator"
  val default_password = "van2kvan2k"

  "デフォルト管理者認証のテスト" should {
    "正しいIDをパスワードで認証できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.findByUseridAndPass(default_userid, default_password) must beSome
      }
    }

    "間違ったパスワードでははじかれる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.findByUseridAndPass(default_userid, "abcdefghi") must beNone
      }
    }

    "間違ったIDでははじかれる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.findByUseridAndPass("administrater", default_password) must beNone
      }
    }
  }

  "一般管理者のテスト" should {
    "一般管理者を挿入できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
      }
    }

    "一般管理者を削除できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k2", "12345678") must beTrue
        Administrator.remove(Administrator.findByUseridAndPass("van2k", "abcdefgh").get.id) must beTrue
        Administrator.findByUseridAndPass("van2k", "abcdefgh") must beNone
        Administrator.findByUseridAndPass("van2k2", "12345678") must beSome
      }
    }

    "一般管理者を認証できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.findByUseridAndPass("van2k", "abcdefgh") must beSome
        Administrator.findByUseridAndPass("van2k", "12345678") must beNone
        Administrator.findByUseridAndPass("van2k2", "abcdefgh") must beNone
      }
    }

    "useridとパスワードで抽出できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k2", "12345678") must beTrue
        Administrator.insert("van2k3", "qwertyui") must beTrue

        Administrator.findByUseridAndPass("van2k", "abcdefgh").get.id === 2
        Administrator.findByUseridAndPass("van2k2", "12345678").get.id === 3
        Administrator.findByUseridAndPass("van2k3", "qwertyui").get.id === 4
      }
    }

    "idで抽出できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k2", "12345678") must beTrue
        Administrator.insert("van2k3", "qwertyui") must beTrue

        Administrator.findById(2).get.userId === "van2k"
        Administrator.findById(3).get.userId === "van2k2"
        Administrator.findById(4).get.userId === "van2k3"
      }
    }

    "複数の管理者をリストアップできる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k2", "12345678") must beTrue
        Administrator.insert("van2k3", "qwertyui") must beTrue

        val list = Administrator.getAllList()
        list(0).userId === "administrator"
        list(1).userId === "van2k"
        list(2).userId === "van2k2"
        list(3).userId === "van2k3"
      }
    }

    "同じuseridで複数挿入できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k", "12345678") must beFalse
      }
    }

    "パスワードを変更できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Administrator.insert("van2k", "abcdefgh") must beTrue
        Administrator.insert("van2k2", "12345678") must beTrue
        Administrator.insert("van2k3", "qwertyui") must beTrue
        Administrator.changePass(Administrator.findByUseridAndPass("van2k", "abcdefgh").get.id, "asdfghjk") must beTrue
        Administrator.findByUseridAndPass("van2k", "abcdefgh") must beNone
        Administrator.findByUseridAndPass("van2k", "asdfghjk") must beSome
        Administrator.findByUseridAndPass("van2k2", "12345678") must beSome
        Administrator.findByUseridAndPass("van2k3", "qwertyui") must beSome
      }
    }
  }
}
