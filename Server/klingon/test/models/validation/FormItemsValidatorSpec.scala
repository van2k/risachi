package models.validation

import models.QuestionType
import org.specs2.mutable.Specification
import commons.Constants
import play.api.Logger
import collection.SortedMap

class FormItemsValidatorSpec extends Specification {

  // 回答項目文字数
  val normalAnsInputMaxLength:Int = 10

  // タイトル文字数制限の上限の文字列を生成する
  val normalFormTitle:String = "あ" * commons.Constants.questionTitleLength
  val normalFormValues:SortedMap[String, String] = SortedMap("key1" -> "い" * commons.Constants.questionItemLength
    ,  "key2" -> "う" * commons.Constants.questionItemLength,  "key3" -> "え" * commons.Constants.questionItemLength)
  val isRequired:Boolean = true

  "オブジェクト生成のテスト" should {
    var actual:FormItemValueValidator = FormItemValueValidator.createInstance(QuestionType.TextLine, normalFormTitle
      , normalAnsInputMaxLength, isRequired, normalFormValues)
    "テキスト行のチェックロジックインスタンスを生成できる" in {
      actual must not beNull
    }

    actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, normalFormTitle, normalAnsInputMaxLength
      , isRequired, normalFormValues)

    "テキスト複数行のチェックロジックインスタンスを生成できる" in{
      actual must not beNull
    }

    actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)

    "ラジオボタンのチェックロジックインスタンスを生成できる" in{
      actual must not beNull
    }

    actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 0
      , "", false, isRequired, normalFormValues)
    "チェックボックスのチェックロジックインスタンスを生成できる" in{
      actual must not beNull
    }

    actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength
      , isRequired, normalFormValues)

    "ドロップリストのチェックロジックインスタンスを生成できる" in{
      actual must not beNull
    }
  }

  "入力文字数のチェックテスト" should {

    val invalidMinAnswerMaxLength:Int = -1
    val invalidMaxAnswerMaxLength:Int = 4001

    "テキスト行の入力可能文字数がマイナスのためエラーとなる" in{
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, normalFormTitle, invalidMinAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getAnsInputMaxLength === "入力可能文字数は1以上を入力してください。"
    }

    "テキスト行の入力可能文字数が4001文字以上のためエラーとなる" in{
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, normalFormTitle, invalidMaxAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getAnsInputMaxLength === "入力可能文字数は4000文字以内で入力してください。"
    }

    "テキスト複数行の入力可能文字数がマイナスのためエラーとなる" in{
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, normalFormTitle, invalidMinAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getAnsInputMaxLength === "入力可能文字数は1以上を入力してください。"
    }

    "テキスト複数行の入力可能文字数が4000文字以上のためエラーとなる" in{
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, normalFormTitle, invalidMaxAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getAnsInputMaxLength === "入力可能文字数は4000文字以内で入力してください。"
    }

    "ドロップダウンリストチェックは-1文字以下でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, invalidMinAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === true
      actual.getAnsInputMaxLength === ""
    }

    "ドロップダウンリストチェックは4001文字以上でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, invalidMaxAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === true
      actual.getAnsInputMaxLength === ""
    }

    "ラジオボタンチェックは-1文字以下でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, invalidMinAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === true
      actual.getAnsInputMaxLength === ""
    }

    "ラジオボタンチェックは4001文字以上でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, invalidMaxAnswerMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === true
      actual.getAnsInputMaxLength === ""
    }
/*
    "チェックボックスのチェックは-1文字以下でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, invalidMinAnswerMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "チェックボックスのチェックは4001文字以上でも正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, invalidMaxAnswerMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }
    */
  }
  "テキスト行のチェックテスト" should {
    "チェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, normalFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "質問タイトルの文字列長が長すぎるためエラーとなる" in{
      val errorFormTitle:String = "あ" * (commons.Constants.questionTitleLength + 1)
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "%d文字内で入力してください。".format(commons.Constants.questionTitleLength)

    }

    "質問タイトルの文字列長が0文字のためエラーとなる" in{
      val errorFormTitle:String = ""
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が半角空文字のためエラーとなる" in{
      val errorFormTitle:String = " "
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が全角空文字のためエラーとなる" in{
      val errorFormTitle:String = "　"
      val actual = FormItemValueValidator.createInstance(QuestionType.TextLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }
  }

  "テキスト複数行のチェックテスト" should {
    "チェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, normalFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "質問タイトルの文字列長が長すぎるためエラーとなる" in{
      val errorFormTitle:String = "あ" * (commons.Constants.questionTitleLength + 1)
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "%d文字内で入力してください。".format(commons.Constants.questionTitleLength)

    }

    "質問タイトルの文字列長が0文字のためエラーとなる" in{
      val errorFormTitle:String = ""
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が半角空文字のためエラーとなる" in{
      val errorFormTitle:String = " "
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage=== "質問は必ず入力してください。"

    }

    "質問タイトルの文字列が全角空文字のためエラーとなる" in{
      val errorFormTitle:String = "　"
      val actual = FormItemValueValidator.createInstance(QuestionType.TextMultiLine, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }
  }

  "ドロップダウンリストのチェックテスト" should {

    "チェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "質問タイトルの文字列長がエラーとなる" in{
      val errorFormTitle:String = "あ" * (commons.Constants.questionTitleLength + 1)
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "%d文字内で入力してください。".format(commons.Constants.questionTitleLength)

    }

    "質問タイトルの文字列長が0文字のためエラーとなる" in{
      val errorFormTitle:String = ""
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"

    }

    "質問タイトルの文字列が半角空文字のためエラーとなる" in{
      val errorFormTitle:String = " "
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage=== "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が全角空文字のためエラーとなる" in{
      val errorFormTitle:String = "　"
      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, errorFormTitle, normalAnsInputMaxLength
        , isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage=== "質問は必ず入力してください。"

    }

    "質問項目No.1の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * commons.Constants.questionItemLength, "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength
        , isRequired, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 1
      }

      true
    }

    "質問項目No.2の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * commons.Constants.questionItemLength
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength
        , isRequired, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 2
      }

      true
    }

    "質問項目No.1-3の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * (commons.Constants.questionItemLength + 1))

      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength
        , isRequired, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      true
    }
   /*
    "質問項目No.1-3の文字列長が同一のためエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength)
        , "key2" -> "い" * (commons.Constants.questionItemLength), "key3" -> "い" * (commons.Constants.questionItemLength))

      val actual = FormItemValueValidator.createInstance(QuestionType.DropList, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      val errMessage = "同一の値が設定されています。"
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }
      }

      true
    }
    */
  }

  "ラジオボタンのチェックテスト" should {

    "チェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "その他チェックありでチェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, true, isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "その他チェックなしでチェックは正常に終了する" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, false, isRequired, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
    }

    "質問タイトルの文字列長が長すぎるためエラーとなる" in{
      val errorFormTitle:String = "あ" * (commons.Constants.questionTitleLength + 1)
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, errorFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "%d文字内で入力してください。".format(commons.Constants.questionTitleLength)
    }

    "質問タイトルの文字列長が0文字のためエラーとなる" in{
      val errorFormTitle:String = ""
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, errorFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が半角空文字のためエラーとなる" in{
      val errorFormTitle:String = " "
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, errorFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage=== "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が全角空文字のためエラーとなる" in{
      val errorFormTitle:String = "　"
      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, errorFormTitle, normalAnsInputMaxLength, isRequired, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問項目No.1の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * commons.Constants.questionItemLength, "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, isRequired, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 1
      }

      true
    }

    "質問項目No.2の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * commons.Constants.questionItemLength
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, isRequired, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 2
      }

      true
    }

    "質問項目No.1-3の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * (commons.Constants.questionItemLength + 1))

      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, isRequired, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      true
    }
/*
    "質問項目No.1-3の文字列長が同一のためエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength)
        , "key2" -> "い" * (commons.Constants.questionItemLength), "key3" -> "い" * (commons.Constants.questionItemLength))

      val actual = FormItemValueValidator.createInstance(QuestionType.Radio, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      val errMessage = "同一の値が設定されています。"
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }
      }

      true
    }
    */
  }

  /*
    "チェックボックスのチェックテスト" should {
      "チェックは正常に終了する" in {
        val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
          , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
        Logger.debug("質問タイトル:" + normalFormTitle.length)
        actual.executeCheck() === true
      }
    "質問タイトルの文字列長が長すぎるためエラーとなる" in{
      val errorFormTitle:String = "あ" * (commons.Constants.questionTitleLength + 1)
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, errorFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "%d文字内で入力してください。".format(commons.Constants.questionTitleLength)
    }

    "質問タイトルの文字列長が0文字のためエラーとなる" in{
      val errorFormTitle:String = ""
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, errorFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が半角空文字のためエラーとなる" in{
      val errorFormTitle:String = " "
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, errorFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage=== "質問は必ず入力してください。"
    }

    "質問タイトルの文字列が全角空文字のためエラーとなる" in{
      val errorFormTitle:String = "　"
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, errorFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, normalFormValues)
      actual.executeCheck() === false

      actual.getErrorMessage === "質問は必ず入力してください。"
    }

    "質問項目No.1の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * commons.Constants.questionItemLength, "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 1
      }

      true
    }

    "質問項目No.2の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * commons.Constants.questionItemLength
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * commons.Constants.questionItemLength)

      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false
      val errMessage = "%d文字以内で入力してください。"

      actual.getItemErrorMessage.size === 1
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }

        keyValue._1 === 2
      }

      true
    }

    "質問項目No.1-3の文字列長がエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength + 1)
        , "key2" -> "う" * (commons.Constants.questionItemLength + 1), "key3" -> "え" * (commons.Constants.questionItemLength + 1))

      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      true
    }

    "質問項目No.1-3の文字列長が同一のためエラーとなる" in{
      val errorFormValues: SortedMap[String, String] = SortedMap("key1" -> "い" * (commons.Constants.questionItemLength)
        , "key2" -> "い" * (commons.Constants.questionItemLength), "key3" -> "い" * (commons.Constants.questionItemLength))

      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual, true, errorFormValues)
      actual.executeCheck() === false

      actual.getItemErrorMessage.size === 3
      val errMessage = "同一の値が設定されています。"
      actual.getItemErrorMessage.foreach{ keyValue =>
        Logger.debug("key:" + keyValue._1)

        keyValue._2.foreach{
          message =>
            Logger.debug("message:" + message)
            message === errMessage.format(commons.Constants.questionTitleLength)
        }
      }

      true
    }

    "選択項目数に設定される値が-1個の場合エラーとなる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, -1
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === false
      actual.getSelectItemCountError === "チェック個数は1以上項目数以下の値を設定してください。"
    }

    "選択項目数に設定される値が0個の場合エラーとなる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 0
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === false
      actual.getSelectItemCountError === "チェック個数は1以上項目数以下の値を設定してください。"
    }

    "選択項目数に設定される値が1個の場合正常終了となる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 1
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
      actual.getSelectItemCountError must beEmpty

    }

    "選択項目数に設定される値が2個の場合正常終了となる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 2
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
      actual.getSelectItemCountError must beEmpty
    }

    "選択項目数に設定される値が3個の場合正常終了となる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 3
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === true
      actual.getSelectItemCountError must beEmpty
    }

    "選択項目数に設定される値が0個の場合エラーとなる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 4
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === false
      actual.getSelectItemCountError === "チェック個数は1以上項目数以下の値を設定してください。"

    }

    "選択項目数に設定される値が0個の場合エラーとなる" in {
      val actual = FormItemValueValidator.createInstance(QuestionType.Checkbox, normalFormTitle, normalAnsInputMaxLength, 5
        , commons.Constants.selectNumberConditionEqual,true, normalFormValues)
      Logger.debug("質問タイトル:" + normalFormTitle.length)
      actual.executeCheck() === false
      actual.getSelectItemCountError === "チェック個数は1以上項目数以下の値を設定してください。"
    }
  }
  */
}
