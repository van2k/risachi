package models.contract

import org.specs2.mutable.Specification
import play.api.test._
import play.api.test.Helpers._

import java.util.Calendar


class ContractsSpec extends Specification {

  val ContractId: Option[Long] = None
  val Mail = "test@van2k.com"
  val Name = "番付太郎"
  val Pass = "van2ktaro"
  val Token = "gdaoi7dayda9t67BHK7ablBUhlhna78vR76hlNBo7tRU7y"
  val Token2 = "bdo7hgbkufay7bb788bG8uih"

  "契約者情報生成のテスト" should {
    val contracts = Contracts(ContractId, Mail, Contracts.STATUS_INTERIM, Name)

    "契約者IDが正しく取得できる" in {
      contracts.contractId === ContractId
    }
    "メールアドレスが正しく取得できる" in {
      contracts.mail === Mail
    }
    "statusが正しく取得できる" in {
      contracts.status === Contracts.STATUS_INTERIM
    }
    "名前が正しく取得できる" in {
      contracts.name === Name
    }
  }

  "契約者挿入処理のテスト" should {

    "データが正常に挿入できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Contracts.tempRegist(Mail, Token, Calendar.getInstance().getTime)
        Contracts.findByMail(Mail) must beSome
      }
    }

    "仮登録状態のデータは上書きできる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime)
        Contracts.tempRegist(Mail, Token2, cal.getTime) must beTrue
        Contracts.findInterimContractByToken(Token2) must beSome
      }
    }

    "解約状態のデータは上書きできる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.directInsert(Mail, Contracts.STATUS_WITHDRAWN, Name, Token, cal.getTime)
        Contracts.tempRegist(Mail, Token2, cal.getTime) must beTrue
        Contracts.findInterimContractByToken(Token2) must beSome
      }
    }

    "本登録状態のデータは上書きできない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        Contracts.tempRegist(Mail, Token, Calendar.getInstance().getTime)
        Contracts.regist(Contracts.findByMail(Mail).get.contractId.get, Name) must beTrue
        Contracts.tempRegist(Mail, Token2, Calendar.getInstance().getTime) must beFalse
        Contracts.findInterimContractByToken(Token2) must beNone
      }
    }
  }

  "重複チェックのテスト" should {

    val contracts = Contracts(ContractId, Mail, Contracts.STATUS_INTERIM, Name)

    "データがメールアドレスから正常に取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // テストデータを登録する
        val retValue = Contracts.tempRegist(Mail, Token, Calendar.getInstance().getTime)
        retValue == true

        val actual: Contracts = Contracts.findByMail(Mail).get
        actual must not beNull

        "メールアドレスが正しく取得できる" in {
          actual.mail === contracts.mail
        }
      }
    }

    "データがメールアドレスから取得できない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        val noExistMail = "hoge@hoge.com"

        val actual: Option[Contracts] = Contracts.findByMail(noExistMail)
        actual.isEmpty === true

      }
    }
  }

  "本登録のテスト" should {
    "登録後、そのデータを読める" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        val c = Contracts.findByMailAndPass(Mail, "abcdef").get
        c.name === Name
        c.mail === Mail
        c.status === Contracts.STATUS_REGISTERED
      }
    }
  }

  "パスワード登録・変更のテスト" should {

    "登録したパスワードが一致する" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        Contracts.findByMailAndPass(Mail, "abcdef") must beSome
      }
    }
    "登録したパスワードが一致しない" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        Contracts.findByMailAndPass(Mail, "abcdeg") must beNone
      }
    }
  }

  "契約者取得テスト" should {
    "契約者を取得できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        val c = Contracts.findByMailAndPass(Mail, "abcdef")
        val contractId: Long = c.get.contractId.get

        // 契約者IDで取得できる
        Contracts.findById(contractId) must beSome

        // 存在しない契約者IDで取得できない
        Contracts.findById(contractId + 1) must beNone

         // メールアドレスで取得できる
        Contracts.findByMail(Mail) must beSome

        // 間違ったメールアドレスで取得できない
        Contracts.findByMail("hogehoge@hoge.com") must beNone

        // メールアドレスとパスワードの組み合わせで取得できる
        Contracts.findByMailAndPass(Mail, "abcdef") must beSome

        // 間違ったパスワードで取得できない
        Contracts.findByMailAndPass(Mail, "abcdeg") must beNone
      }
    }
  }

  "契約状態変更テスト" should {
    "契約状態を変更できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        // 契約者情報取得
        val c = Contracts.findByMailAndPass(Mail, "abcdef")
        val contractId: Long = c.get.contractId.get

        // 状態変更
        Contracts.changeStatus(contractId, Contracts.STATUS_STOP)

        // 契約者情報取得
        val c2 = Contracts.findById(contractId)
        c2.get.status === Contracts.STATUS_STOP
      }
    }
  }

  "登録トークン変更/無効化テスト" should {
    "トークンを変更/無効化できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        // 契約者情報取得
        val c = Contracts.findByMailAndPass(Mail, "abcdef")
        val contractId: Long = c.get.contractId.get

        // トークン変更
        Contracts.changeRegistToken(contractId, Token2, cal.getTime) must beTrue

        // 新しいトークンで契約者を取り出せる
        Contracts.findByToken(Token2) must beSome

        // 古いトークンでは取り出せない
        Contracts.findByToken(Token) must beNone

        // 古いトークンで無効化しても影響しない
        Contracts.invalidateRegistToken(Token)
        Contracts.findByToken(Token2) must beSome

        // トークン無効化
        Contracts.invalidateRegistToken(Token2)
        Contracts.findByToken(Token2) must beNone
      }
    }
  }

  "パスワード変更テスト" should {
    "パスワードを変更できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        // 登録状態を作る
        // 1時間後の時刻で登録する
        val cal = Calendar.getInstance()
        cal.add(Calendar.HOUR, 1)

        Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
        val contracts = Contracts.findInterimContractByToken(Token)
        Contracts.setPassByMail(Mail, "abcdef")
        Contracts.regist(contracts.get.contractId.get, Name)

        // 契約者情報取得
        val c = Contracts.findByMailAndPass(Mail, "abcdef")
        val contractId: Long = c.get.contractId.get

        // IDでパスワード変更
        Contracts.setPass(contractId, "123456")
        // 新しいパスワードで取り出せる
        Contracts.findByMailAndPass(Mail, "123456") must beSome
        // 古いパスワードでは取り出せない
        Contracts.findByMailAndPass(Mail, "abcdef") must beNone

        // メールでパスワード変更
        Contracts.setPassByMail(Mail, "qwertyuiop")
        // 新しいパスワードで取り出せる
        Contracts.findByMailAndPass(Mail, "qwertyuiop") must beSome
        // 古いパスワードでは取り出せない
        Contracts.findByMailAndPass(Mail, "123456") must beNone

        // トークンでパスワード変更
        Contracts.setPassByToken(Token, "asdfghjkl")
        // 新しいパスワードで取り出せる
        Contracts.findByMailAndPass(Mail, "asdfghjkl") must beSome
        // 古いパスワードでは取り出せない
        Contracts.findByMailAndPass(Mail, "qwertyuiop") must beNone
      }
    }
  }

  "最終ログイン日付更新のテスト" should {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      // 登録状態を作る
      // 1時間後の時刻で登録する
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, 1)
      val lastLoginAt = Calendar.getInstance().getTime()

      Contracts.tempRegist(Mail, Token, cal.getTime) must beTrue
      val contracts = Contracts.findInterimContractByToken(Token)
      Contracts.regist(contracts.get.contractId.get, Name)
      Contracts.updateLastLoginAt(contracts.get.contractId.get, lastLoginAt)
      val checkContract = Contracts.findById(contracts.get.contractId.get)
      val afterLastLoginAt = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(checkContract.get.lastLoginAt.get).toString

      "指定した日付に最終日付が更新できる" in {
        afterLastLoginAt == new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastLoginAt).toString
      }
    }
  }

}
