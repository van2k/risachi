package models.contract

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import java.util.Calendar

class SearchedContractsSpec extends Specification {
  val Token = "gdaoi7dayda9t67BHK7ablBUhlhna78vR76hlNBo7tRU7y"

  // テスト用に複数の契約者を挿入
  def insertContracts = {
    Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "テスト一郎", "abc", Calendar.getInstance().getTime)
    Contracts.directInsert("test2@van2k.com", Contracts.STATUS_REGISTERED, "テスト二郎", "def", Calendar.getInstance().getTime)
    Contracts.directInsert("test3@van2k.com", Contracts.STATUS_REGISTERED, "テスト三郎", "ghi", Calendar.getInstance().getTime)
    Contracts.directInsert("test4@van2k.com", Contracts.STATUS_REGISTERED, "テスト四朗", "jkl", Calendar.getInstance().getTime)
    Contracts.directInsert("test5@van2k.com", Contracts.STATUS_REGISTERED, "テスト五郎", "mno", Calendar.getInstance().getTime)
  }

  "契約者検索テスト" should {
    "名前で検索できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        insertContracts

        val list = SearchedContracts.search("一郎", "", 0, 0, "name", false)
        list.length === 1
        list.head.mail === "test@van2k.com"
      }
    }

    "メールアドレスで検索できる(完全一致)" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        insertContracts

        val list = SearchedContracts.search("", "test2@van2k.com", 0, 0, "mail", false)
        list.length === 1
        list.head.name === "テスト二郎"
      }
    }

    "メールアドレスで検索できる(部分一致)" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
        insertContracts

        val list = SearchedContracts.search("", "test@van2k.", 0, 0, "mail", false)
        list.length === 1
        list(0).name === "テスト一郎"
      }
    }
  }
}
