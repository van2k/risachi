package controllers

import play.api.test.Helpers._
import play.api.test.{PlaySpecification, FakeApplication, TestServer}
import play.api.libs.ws.WS
import org.specs2.mutable.Specification
import models.contract.Contracts
import models.{enc, contract}
import java.util.Calendar
import java.text.SimpleDateFormat
import play.api.mvc.{Cookies, Session}
import commons.Constants

class LoginSpec extends PlaySpecification {
	import controllers._

  "ログイン画面のテスト" should {
    "ログイン画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/loginidx").get).status must equalTo(OK)
      }
    }

    "ログイン状態でログイン画面にアクセスした場合は、フォーム一覧画面に遷移する" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

        Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
          "石川五右衛門", "abc", Calendar.getInstance().getTime) must beTrue

//        val contractId = Contracts.findByMail("a@van2k.com").get.contractId
        val c = Contracts.findByMail("a@van2k.com")
        val contractId = c.get.contractId
        val id = Contracts.findById(contractId.get).get.contractId.get
        id == 1 must beTrue
        val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.get.toString)))


        val ws = await(WS.url("http://localhost:3333/loginidx")
          .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
          .get)
        ws.status must equalTo(OK)
      }
    }

    "正しいID、パスワードを入力したのでログインできる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        Contracts.directInsert("w243fscd2wd3r2s13@van2k.com", contract.Contracts.STATUS_REGISTERED,
          "テスト太郎", "abc", Calendar.getInstance().getTime) must beTrue
        Contracts.setPassByMail("w243fscd2wd3r2s13@van2k.com", "pass");

        await(WS.url("http://localhost:3333/login")
          .post(Map("userid" -> Seq("w243fscd2wd3r2s13@van2k.com"), "password" -> Seq("pass"))))
            .body must not contain("ID、又は、パスワードが異なります")
      }
    }

    "IDが存在しないのでログインできない" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        Contracts.directInsert("w243fscd2wd3r2s13@van2k.com", contract.Contracts.STATUS_REGISTERED,
          "テスト太郎", "abc", Calendar.getInstance().getTime) must beTrue
        Contracts.setPassByMail("w243fscd2wd3r2s13@van2k.com", "pass");

        await(WS.url("http://localhost:3333/login")
          .post(Map("userid" -> Seq("idnashiyo@van2k.com"), "password" -> Seq("pass"))))
          .body must contain("ID、又は、パスワードが異なります")
      }
    }

    "パスワードが正しくないのでログインできない" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        Contracts.directInsert("w243fscd2wd3r2s13@van2k.com", contract.Contracts.STATUS_REGISTERED,
          "テスト太郎", "abc", Calendar.getInstance().getTime) must beTrue
        Contracts.setPassByMail("w243fscd2wd3r2s13@van2k.com", "pass");

        await(WS.url("http://localhost:3333/login")
          .post(Map("userid" -> Seq("idnashiyo@van2k.com"), "password" -> Seq("hogehoge"))))
          .body must contain("ID、又は、パスワードが異なります")
      }
    }
  }
}
