package controllers

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test._
import java.util.Calendar
import models.enc._
import play.api.libs.ws.WS
import java.text.SimpleDateFormat
import play.api.mvc.{Cookie, Session, Cookies}
import models.enc
import scala.Some
import play.api.mvc.Cookie

class EncSpec extends PlaySpecification {
	import controllers._

	"アンケート回答画面のテスト" should {
		"アンケート回答画面が正常に表示できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				val desc = "改行ありの\nフォーム説明"
				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", desc, "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)

				val body =await(WS.url(testUrl)
					.get).body
				body must contain("<title>フォーム名１</title>")
				body must contain("<h2>フォーム名１</h2>")
				"フォーム説明に改行が含まれている場合は<br />が挿入される" in {
					body must contain("改行ありの<br />フォーム説明<br />")
				}

				val testPreviewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val preViewBody =await(WS.url(testPreviewUrl)
					.get).body

				"プレビューモードでアンケート回答画面が正常に表示できる" in {
					preViewBody must contain("<title>フォーム名１</title>")
					preViewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
				}

				val formNotFoundUrl = String.format("http://localhost:3333/enc/%s", "23gvwfc34gt34gadsfae")

				val notFoundBody =await(WS.url(formNotFoundUrl)
					.get).body

				"指定したアンケートフォームに該当するアンケートが存在せずエラーとなる" in {
					notFoundBody must contain("アンケートがありません")
				}

				val testPreviewNotFoundUrl = String.format("http://localhost:3333/encpreview/%s", "23gvwfc34gt34gadsfae")
				val preViewNotFoundBody =await(WS.url(testPreviewNotFoundUrl)
					.get).body

				"指定したアンケートフォームに該当するアンケートが存在せずエラーとなる（プレビューモード）" in {
					preViewNotFoundBody must contain("アンケートがありません")
				}

				val keyContainUrl1 = String.format("http://localhost:3333/enc/%s/0/key1", formId1.toString)
				val keyContainBody1 =await(WS.url(keyContainUrl1)
					.get).body

				"α版では外部キー(key)の機能は塞いでいる為エラーとなる" in {
					keyContainBody1 must contain("不正なパラメータが指定されました")
				}

				val keyContainUrl2 = String.format("http://localhost:3333/enc/%s/0/key1/att1", formId1.toString)

				val keyContainBody2 =await(WS.url(keyContainUrl2)
					.get).body

				"α版では外部キー(attribute1)の機能は塞いでいる為エラーとなる" in {
					keyContainBody2 must contain("不正なパラメータが指定されました")
				}

				val keyContainUrl3 = String.format("http://localhost:3333/enc/%s/0/key1/att1/att2", formId1.toString)

				val keyContainBody3 =await(WS.url(keyContainUrl3)
					.get).body

				"α版では外部キー(attribute2)の機能は塞いでいる為エラーとなる" in {
					keyContainBody3 must contain("不正なパラメータが指定されました")
				}

				val keyContainUrl4 = String.format("http://localhost:3333/enc/%s/0/key1/att1/att2/att3", formId1.toString)

				val keyContainBody4 =await(WS.url(keyContainUrl4)
					.get).body

				"α版では外部キー(attribute3)の機能は塞いでいる為エラーとなる" in {
					keyContainBody4 must contain("不正なパラメータが指定されました")
				}

				val keyContainUrl5 = String.format("http://localhost:3333/enc/%s/0/key1/att1/att2/att3/att4", formId1.toString)

				val keyContainBody5 =await(WS.url(keyContainUrl5)
					.get).body

				"α版では外部キー(attribute4)の機能は塞いでいる為エラーとなる" in {
					keyContainBody5 must contain("不正なパラメータが指定されました")
				}

				val keyContainUrl6 = String.format("http://localhost:3333/enc/%s/0/key1/att1/att2/att3/att4/att5", formId1.toString)

				val keyContainBody6 =await(WS.url(keyContainUrl6)
					.get).body

				"α版では外部キー(attribute5)の機能は塞いでいる為エラーとなる" in {
					keyContainBody6 must contain("不正なパラメータが指定されました")
				}

				val alreadyAnswerUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)

				val alreadyAnswerBody =await(WS.url(alreadyAnswerUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(new Cookie(name = formId1.toString, value = "1", maxAge = Some(604800), domain = None))))
					.get).body

				"既に回答済みの為回答出来ない" in {
					alreadyAnswerBody must contain("このアンケートには既に回答頂いています")
				}

				val previewAnswerUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)

				val previewAnswerBody =await(WS.url(previewAnswerUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(new Cookie(name = formId1.toString, value = "1", maxAge = Some(604800), domain = None))))
					.get).body

				"プレビュー時は既に回答済みでも回答できる" in {
					previewAnswerBody must contain("<title>フォーム名１</title>")
					previewAnswerBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
				}

			}
		}

		"指定したアンケートフォームに該当するアンケート設定が存在せずエラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				await(WS.url(testUrl)
					.get).body must contain("アンケート設定情報がありません")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードでは指定したアンケートフォームに該当するアンケート設定が存在しなくても表示できる" in {
					previewBody must contain("<title>フォーム名１</title>")
					previewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					previewBody must contain("フォーム１です")
				}
			}
		}

		"指定したアンケートフォームに該当するアンケートのステータスが編集中の為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_EDIT, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				await(WS.url(testUrl)
					.get).body must contain("アンケート受付中の状態ではありません")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードでは指定したアンケートフォームに該当するアンケートのステータスが編集中でも表示できる" in {
					previewBody must contain("<title>フォーム名１</title>")
					previewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					previewBody must contain("フォーム１です")
				}
			}
		}

		"指定したアンケートフォームに該当するアンケートのステータスが終了の為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_END, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				await(WS.url(testUrl)
					.get).body must contain("アンケート受付中の状態ではありません")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードでは指定したアンケートフォームに該当するアンケートのステータスが終了でも表示できる" in {
					previewBody must contain("<title>フォーム名１</title>")
					previewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					previewBody must contain("フォーム１です")
				}
			}
		}

		"指定したアンケートフォームに該当するアンケートのステータスが中止の為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_CANCEL, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				await(WS.url(testUrl)
					.get).body must contain("アンケート受付中の状態ではありません")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"指定したアンケートフォームに該当するアンケートのステータスが中止でも表示できる" in {
					previewBody must contain("<title>フォーム名１</title>")
					previewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					previewBody must contain("フォーム１です")
				}
			}
		}

		"指定したアンケートフォームに該当するアンケートの質問項目が存在しない為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				val body =await(WS.url(testUrl)
					.get).body

				body must contain("アンケートの質問項目が存在しません")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"指定したアンケートフォームに該当するアンケートの質問項目が存在しない為エラーとなる（プレビューモード）" in {
					previewBody must contain("アンケートの質問項目が存在しません")
				}
			}
		}

		"アンケートの受付期間が終了している為回答出来ない" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, -1, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				val body =await(WS.url(testUrl)
					.get).body

				body must contain("アンケートの受付期間が終了しています。")

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードではアンケートの受付期間が終了していても表示できる" in {
					previewBody must contain("<title>フォーム名１</title>")
					previewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					previewBody must contain("フォーム１です")
				}
			}
		}

		"入力必須の回答タイトルが確認できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 1, "1", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)
				val body =await(WS.url(testUrl)
					.get).body

				"入力項目がテキストフィールドの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle1))
				}

				"入力項目がテキストエリアの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle2))
				}

				"入力項目がラジオボタンの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle3))
				}

				"入力項目がラジオボタンの回答項目が確認できる" in {
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, radio1))
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, radio2))
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, radio3))
				}

				"入力項目がチェックボックスの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle4))
				}

				"入力項目がチェックボックスの回答項目が確認できる" in {
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId6, itemId4.getOrElse(-1), check1))
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId7, itemId4.getOrElse(-1), check2))
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId8, itemId4.getOrElse(-1), check3))
				}

				"入力項目がプルダウンリストの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle5))
				}

				"入力項目がプルダウンリストの回答項目が確認できる" in {
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId9, drop1))
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId10, drop2))
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId11, drop3))
				}

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードでも入力必須の回答タイトルが確認できる" in {

					"プレビューモードでも入力項目がテキストフィールドの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle1))
					}

					"プレビューモードでも入力項目がテキストエリアの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle2))
					}

					"プレビューモードでも入力項目がラジオボタンの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle3))
					}

					"プレビューモードでも入力項目がラジオボタンの回答項目が確認できる" in {
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, radio1))
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, radio2))
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, radio3))
					}

					"プレビューモードでも入力項目がチェックボックスの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle4))
					}

					"プレビューモードでも入力項目がチェックボックスの回答項目が確認できる" in {
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId6, itemId4.getOrElse(-1), check1))
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId7, itemId4.getOrElse(-1), check2))
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId8, itemId4.getOrElse(-1), check3))
					}

					"プレビューモードでも入力項目がプルダウンリストの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s*</div>".format(itemTitle5))
					}

					"プレビューモードでも入力項目がプルダウンリストの回答項目が確認できる" in {
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId9, drop1))
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId10, drop2))
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId11, drop3))
					}
				}
			}
		}

		"入力必須でない回答タイトルが確認できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "0", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "0", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "0", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 1, "1", "0", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "0", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)

				val body =await(WS.url(testUrl)
					.get).body

				"入力項目がテキストフィールドの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle1))
				}

				"入力項目がテキストエリアの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle2))
				}

				"入力項目がラジオボタンの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle3))
				}

				"入力項目がラジオボタンの回答項目が確認できる" in {
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, radio1))
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, radio2))
					body must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, radio3))
				}

				"入力項目がチェックボックスの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle4))
				}

				"入力項目がチェックボックスの回答項目が確認できる" in {
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId6, itemId4.getOrElse(-1), check1))
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId7, itemId4.getOrElse(-1), check2))
					body must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId8, itemId4.getOrElse(-1), check3))
				}

				"入力項目がプルダウンリストの回答タイトルが確認できる" in {
					body must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle5))
				}

				"入力項目がプルダウンリストの回答項目が確認できる" in {
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId9, drop1))
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId10, drop2))
					body must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId11, drop3))
				}

				val previewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val previewBody = await(WS.url(previewUrl)
					.get).body

				"プレビューモードでも入力必須でない回答タイトルが確認できる" in {

					"プレビューモードでも入力項目がテキストフィールドの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle1))
					}

					"プレビューモードでも入力項目がテキストエリアの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle2))
					}

					"プレビューモードでも入力項目がラジオボタンの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle3))
					}

					"プレビューモードでも入力項目がラジオボタンの回答項目が確認できる" in {
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, radio1))
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, radio2))
						previewBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, radio3))
					}

					"プレビューモードでも入力項目がチェックボックスの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle4))
					}

					"プレビューモードでも入力項目がチェックボックスの回答項目が確認できる" in {
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId6, itemId4.getOrElse(-1), check1))
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId7, itemId4.getOrElse(-1), check2))
						previewBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId8, itemId4.getOrElse(-1), check3))
					}

					"プレビューモードでも入力項目がプルダウンリストの回答タイトルが確認できる" in {
						previewBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle5))
					}

					"プレビューモードでも入力項目がプルダウンリストの回答項目が確認できる" in {
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId9, drop1))
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId10, drop2))
						previewBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId11, drop3))
					}
				}
			}
		}
	}

	"各質問項目のonchangeイベントによる回答情報登録のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			// フォーム挿入
			val now = Calendar.getInstance().getTime
			val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
				None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
			// フォーム設定挿入
			EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
				Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
			// 質問項目挿入
			val itemTitle1 = "テキストフィールドの質問項目"
			val itemTitle2 = "テキストエリアの質問項目"
			val itemTitle3 = "ラジオボタンの質問項目"
			val itemTitle4 = "チェックボックスの質問項目"
			val itemTitle5 = "ドロップダウンの質問項目"
			val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "0", 0, now, now), now)
			val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "0", 1, now, now), now)
			val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "0", 2, now, now), now)
			val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 1, "1", "0", 3, now, now), now)
			val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "0", 4, now, now), now)
			// 質問項目選択肢挿入
			val formItemValueId1 = 1
			val formItemValueId2 = 2
			val formItemValueId3 = 3
			val formItemValueId4 = 4
			val formItemValueId5 = 5
			val formItemValueId6 = 6
			val formItemValueId7 = 7
			val formItemValueId8 = 8
			val formItemValueId9 = 9
			val formItemValueId10 = 10
			val formItemValueId11 = 11
			val radio1 = "ラジオその１"
			val radio2 = "ラジオその２"
			val radio3 = "ラジオその３"
			val check1 = "チェックその１"
			val check2 = "チェックその２"
			val check3 = "チェックその３"
			val drop1 = "ドロップダウンその１"
			val drop2 = "ドロップダウンその２"
			val drop3 = "ドロップダウンその３"

			EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
			EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)
			val endAt = Calendar.getInstance()
			endAt.add(Calendar.MINUTE, 3)
			// 回答内容登録
			val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

			// onchangeイベント内で実行するURLを実行
			val textUrl = String.format("http://localhost:3333/encai/%s/%s", answerId.getOrElse(-1).toString, itemId1.getOrElse(-1).toString)

			await(WS.url(textUrl).get).status must equalTo (OK)
			// onchangeイベント実行時URLによりDBに登録されていることを確認
			val textData = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId1.getOrElse(-1))

			"入力項目がテキストフィールドの回答情報がonchangeイベントで登録できる" in {
				textData.length must equalTo(1)
				textData.head.answerAt must not beNull
			}

			// onchangeイベント内で実行するURLを実行
			val textAreaUrl = String.format("http://localhost:3333/encai/%s/%s", answerId.getOrElse(-1).toString, itemId2.getOrElse(-1).toString)

			await(WS.url(textAreaUrl).get).status must equalTo (OK)
			// onchangeイベント実行時URLによりDBに登録されていることを確認
			val textAreaData = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId2.getOrElse(-1))

			"入力項目がテキストエリアの回答情報がonchangeイベントで登録できる" in {
				textAreaData.length must equalTo(1)
				textAreaData.head.answerAt must not beNull
			}

			// onchangeイベント内で実行するURLを実行
			val radioUrl = String.format("http://localhost:3333/encai/%s/%s", answerId.getOrElse(-1).toString, itemId3.getOrElse(-1).toString)

			await(WS.url(radioUrl).get).status must equalTo (OK)
			// onchangeイベント実行時URLによりDBに登録されていることを確認
			val radioData = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId3.getOrElse(-1))


			"入力項目がラジオボタンの回答情報がonchangeイベントで登録できる" in {
				radioData.length must equalTo(1)
				radioData.head.answerAt must not beNull
			}

			// onchangeイベント内で実行するURLを実行
			val checkUrl = String.format("http://localhost:3333/encai/%s/%s", answerId.getOrElse(-1).toString, itemId4.getOrElse(-1).toString)

			await(WS.url(checkUrl).get).status must equalTo (OK)
			// onchangeイベント実行時URLによりDBに登録されていることを確認
			val checkData = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId4.getOrElse(-1))

			"入力項目がチェックボックスの回答情報がonchangeイベントで登録できる" in {
				checkData.length must equalTo(1)
				checkData.head.answerAt must not beNull
			}

			// onchangeイベント内で実行するURLを実行
			val dropUrl = String.format("http://localhost:3333/encai/%s/%s", answerId.getOrElse(-1).toString, itemId5.getOrElse(-1).toString)

			await(WS.url(dropUrl).get).status must equalTo (OK)
			// onchangeイベント実行時URLによりDBに登録されていることを確認
			val dropData = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId5.getOrElse(-1))

			"入力項目がプルダウンリストの回答情報がonchangeイベントで登録できる" in {
				dropData.length must equalTo(1)
				dropData.head.answerAt must not beNull
			}
		}
	}

	"回答内容送信時のテスト" should {
		"質問項目が1ページの場合のテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 1, "1", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val normalBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
						"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
						"formId" -> Seq(formId1.toString),
						"pageNo" -> Seq("1"),
						"ispreview" -> Seq("false"),
						"mode" -> Seq("0"),
						"send" -> Seq("送信"),
						"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
						"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
						itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
						itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
						itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"正常に回答内容が送信出来る" in {
					normalBody must contain("<title>フォーム名１</title>")
					normalBody must contain("<h2>フォーム名１</h2>")
					normalBody must contain("フォーム１です")
					normalBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textCheckAnswer = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId1.getOrElse(-1))
				val textAreaCheckAnswer = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId2.getOrElse(-1))
				val radioCheckAnswer = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId3.getOrElse(-1))
				val checkBoxCheckAnswer = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId4.getOrElse(-1))
				val dropCheckAnswer = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId5.getOrElse(-1))
				"入力項目がテキストフィールドの回答情報が登録できる" in {
					textCheckAnswer.length must equalTo(1)
					textCheckAnswer.head.answerValue must equalTo(textAnswer)
				}

				"入力項目がテキストエリアの回答情報が登録できる" in {
					textAreaCheckAnswer.length must equalTo(1)
					textAreaCheckAnswer.head.answerValue must equalTo(textAreaAnswer)
				}

				"入力項目がラジオボタンの回答情報が登録できる" in {
					radioCheckAnswer.length must equalTo(1)
					radioCheckAnswer.head.answerValue must equalTo(formItemValueId3.toString)
				}

				"入力項目がチェックボックスの回答情報が登録できる" in {
					checkBoxCheckAnswer.length must equalTo(1)
					checkBoxCheckAnswer.head.answerValue must equalTo(formItemValueId6.toString)
				}

				"入力項目がプルダウンリストの回答情報が登録できる" in {
					dropCheckAnswer.length must equalTo(1)
					dropCheckAnswer.head.answerValue must equalTo(formItemValueId9.toString)
				}

				val textUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(""),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの質問項目が必須項目なのにブランクなのでエラーとなる" in {
					textUnInputBody must contain("<strong>必須項目になります</strong>")
				}

				val textAreaUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(""),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの質問項目が必須項目なのにブランクなのでエラーとなる" in {
					textAreaUnInputBody must contain("<strong>必須項目になります</strong>")
				}

				val radioUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(""),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"ラジオボタンの質問項目が必須項目なのにブランクなのでエラーとなる" in {
					radioUnInputBody must contain("<strong>必須項目になります</strong>")
				}

				val checkUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(""),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの質問項目が必須項目なのにブランクなのでエラーとなる" in {
					checkUnInputBody must contain("<strong>必須項目になります</strong>")
				}

				val dropUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq("")))).body

				"プルダウンリストの質問項目が必須項目なのにブランクなのでエラーとなる" in {
					dropUnInputBody must contain("<strong>必須項目になります</strong>")
				}

				val textLengthCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq("12345678901234567890"),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの入力が半角、文字数上限ちょうどなので登録出来る" in {
					textLengthCheckBody must contain("<title>フォーム名１</title>")
					textLengthCheckBody must contain("<h2>フォーム名１</h2>")
					textLengthCheckBody must contain("フォーム１です")
					textLengthCheckBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textLengthCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq("１２３４５６７８９０１２３４５６７８９０"),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの入力が全角、文字数上限ちょうどなので登録出来る" in {
					textLengthCheckBody2 must contain("<title>フォーム名１</title>")
					textLengthCheckBody2 must contain("<h2>フォーム名１</h2>")
					textLengthCheckBody2 must contain("フォーム１です")
					textLengthCheckBody2 must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textLengthErrCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq("123456789012345678901"),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの入力が半角、文字数オーバーなのでエラーとなる" in {
					textLengthErrCheckBody must contain("文字数上限オーバーです")
					textLengthErrCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textLengthErrCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq("１２３４５６７８９０１２３４５６７８９０１"),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの入力が全角、文字数オーバーなのでエラーとなる" in {
					textLengthErrCheckBody2 must contain("文字数上限オーバーです")
					textLengthErrCheckBody2 must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textAreaLengthCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1), formItemValueId2) -> Seq("12345678901234567890"),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの入力が半角、文字数上限ちょうどなので登録出来る" in {
					textAreaLengthCheckBody must contain("<title>フォーム名１</title>")
					textAreaLengthCheckBody must contain("<h2>フォーム名１</h2>")
					textAreaLengthCheckBody must contain("フォーム１です")
					textAreaLengthCheckBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textAreaLengthCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1), formItemValueId2) -> Seq("１２３４５６７８９０１２３４５６７８９０"),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの入力が全角、文字数上限ちょうどなので登録出来る" in {
					textAreaLengthCheckBody2 must contain("<title>フォーム名１</title>")
					textAreaLengthCheckBody2 must contain("<h2>フォーム名１</h2>")
					textAreaLengthCheckBody2 must contain("フォーム１です")
					textAreaLengthCheckBody2 must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textAreaLengthErrCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1), formItemValueId2) -> Seq("123456789012345678901"),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの入力が半角、文字数オーバーなのでエラーとなる" in {
					textAreaLengthErrCheckBody must contain("文字数上限オーバーです")
					textAreaLengthErrCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textAreaLengthErrCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1), formItemValueId2) -> Seq("１２３４５６７８９０１２３４５６７８９０１"),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの入力が全角、文字数オーバーなのでエラーとなる" in {
					textAreaLengthErrCheckBody2 must contain("文字数上限オーバーです")
					textAreaLengthErrCheckBody2 must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val radioErrCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq("不正データ"),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"回答内容がラジオボタンの選択肢に含まれていない為エラーとなる" in {
					radioErrCheckBody must contain("不正な値が指定されました")
					radioErrCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkErrCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq("不正データ"),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"回答内容がチェックボックスの選択肢に含まれていない為エラーとなる" in {
					checkErrCheckBody must contain("不正な値が指定されました")
					checkErrCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val dropErrCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq("不正データ")))).body

				"回答内容がプルダウンリストの選択肢に含まれていない為エラーとなる" in {
					dropErrCheckBody must contain("不正な値が指定されました")
					dropErrCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
				}
			}

			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "0", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "0", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "0", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 1, "1", "0", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "0", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val textUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(""),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストフィールドの質問項目が必須項目でないのでブランクでもエラーとならない" in {
					textUnInputBody must contain("<title>フォーム名１</title>")
					textUnInputBody must contain("<h2>フォーム名１</h2>")
					textUnInputBody must contain("フォーム１です")
					textUnInputBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val textAreaUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(""),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"テキストエリアの質問項目が必須項目でないのでブランクでもエラーとならない" in {
					textAreaUnInputBody must contain("<title>フォーム名１</title>")
					textAreaUnInputBody must contain("<h2>フォーム名１</h2>")
					textAreaUnInputBody must contain("フォーム１です")
					textAreaUnInputBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val radioUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"ラジオボタンの質問項目が必須項目でないのでブランクでもエラーとならない" in {
					radioUnInputBody must contain("<title>フォーム名１</title>")
					radioUnInputBody must contain("<h2>フォーム名１</h2>")
					radioUnInputBody must contain("フォーム１です")
					radioUnInputBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの質問項目が必須項目でないのでブランクでもエラーとならない" in {
					checkUnInputBody must contain("<title>フォーム名１</title>")
					checkUnInputBody must contain("<h2>フォーム名１</h2>")
					checkUnInputBody must contain("フォーム１です")
					checkUnInputBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val dropUnInputBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString.toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer.toString),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq("")))).body

				"プルダウンリストの質問項目が必須項目でないのでブランクでもエラーとならない" in {
					dropUnInputBody must contain("<title>フォーム名１</title>")
					dropUnInputBody must contain("<h2>フォーム名１</h2>")
					dropUnInputBody must contain("フォーム１です")
					dropUnInputBody must contain("<h4>結果メッセージ１<br /></h4>")
				}
			}


			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId2 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId3 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId4 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId5 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId6 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId7 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId8 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId9 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
				val answerId10 = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val text1 = "ふなっしー"
				// テキストフィールド質問項目に回答を挿入
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId2.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId3.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId4.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId5.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId6.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId7.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId8.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId9.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId10.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))

				// テキストエリア質問項目に回答を挿入
				val textArea1 =
					"""ふなっしー
					  |きゃはー""".stripMargin

				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId2.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId3.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId4.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId5.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId6.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId7.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId8.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId9.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId10.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))

				// ラジオボタン質問項目に回答を挿入
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, formItemValueId3.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId2.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, formItemValueId4.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, formItemValueId5.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId4.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, formItemValueId3.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId5.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, formItemValueId4.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId6.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, formItemValueId5.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId7.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, formItemValueId3.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId8.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, formItemValueId4.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId9.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, formItemValueId5.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId10.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, formItemValueId3.toString, Option(now), now, now))

				// チェックボックス質問項目に回答を挿入
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId6, formItemValueId6.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId2.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId7, formItemValueId7.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId3.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId8, formItemValueId8.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId4.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId6, formItemValueId6.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId5.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId7, formItemValueId7.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId6.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId8, formItemValueId8.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId7.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId6, formItemValueId6.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId8.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId7, formItemValueId7.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId9.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId8, formItemValueId8.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId10.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId6, formItemValueId6.toString, Option(now), now, now))

				// ドロップダウン質問項目に回答を挿入
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId9, formItemValueId9.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId2.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId10, formItemValueId10.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId3.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId11, formItemValueId11.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId4.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId9, formItemValueId9.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId5.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId10, formItemValueId10.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId6.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId11, formItemValueId11.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId7.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId9, formItemValueId9.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId8.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId10, formItemValueId10.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId9.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId11, formItemValueId11.toString, Option(now), now, now))
				EncAnswerDetails.regist(EncAnswerDetails(-1, answerId10.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId9, formItemValueId9.toString, Option(now), now, now))

				val testUrl = String.format("http://localhost:3333/enc/%s", formId1.toString)

				val body =await(WS.url(testUrl)
					.get).body

				"アンケートの受付上限数に達している為登録できない" in {
					body must contain("アンケートは受付上限に達したため終了しています。")
				}

				val testPreviewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val preViewBody =await(WS.url(testPreviewUrl)
					.get).body

				"プレビューモードでは受付上限数に達していても表示できる" in {
					preViewBody must contain("<title>フォーム名１</title>")
					preViewBody must contain("<h2>フォーム名１<span class=\"label label-info\">プレビューモード</span></h2>")
					preViewBody must contain("フォーム１です")
				}
			}

			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 2, "1", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val checkBoxCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個」指定状態で個数ちょうどなので登録出来る" in {
					checkBoxCheckBody must contain("<title>フォーム名１</title>")
					checkBoxCheckBody must contain("<h2>フォーム名１</h2>")
					checkBoxCheckBody must contain("フォーム１です")
					checkBoxCheckBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxAnswers:List[EncAnswerDetails] = EncAnswerDetails.findByAnsweridFormitemid(answerId.getOrElse(-1), itemId4.getOrElse(-1))
				"チェックボックスの複数選択項目が登録されている" in {
					checkBoxAnswers.length must equalTo(2)
				}

				val checkBoxCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個」指定状態で個数未満なので登録出来ない" in {
					checkBoxCheckBody2 must contain("フォーム１です")
					checkBoxCheckBody2 must contain("<strong>2項目、選択（または、入力）してください</strong>")
					checkBoxCheckBody2 must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxCheckBody3 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString, formItemValueId8.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個」指定状態で個数オーバーなので登録出来ない" in {
					checkBoxCheckBody3 must contain("フォーム１です")
					checkBoxCheckBody3 must contain("<strong>2項目、選択（または、入力）してください</strong>")
					checkBoxCheckBody3 must not contain("<h4>結果メッセージ１<br /></h4>")
				}
			}

			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 2, "2", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val checkBoxCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以上」指定状態で個数ちょうどなので登録出来る" in {
					checkBoxCheckBody must contain("<title>フォーム名１</title>")
					checkBoxCheckBody must contain("<h2>フォーム名１</h2>")
					checkBoxCheckBody must contain("フォーム１です")
					checkBoxCheckBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以上」指定状態で個数未満なので登録出来ない" in {
					checkBoxCheckBody2 must contain("フォーム１です")
					checkBoxCheckBody2 must contain("<strong>2項目以上、選択（または、入力）してください</strong>")
					checkBoxCheckBody2 must not contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxCheckBody3 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString, formItemValueId8.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以上」指定状態で個数オーバーなので登録出来る" in {
					checkBoxCheckBody3 must contain("<title>フォーム名１</title>")
					checkBoxCheckBody3 must contain("<h2>フォーム名１</h2>")
					checkBoxCheckBody3 must contain("フォーム１です")
					checkBoxCheckBody3 must contain("<h4>結果メッセージ１<br /></h4>")
				}

			}

			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 1,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 1, 2, "3", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId1 = 1
				val formItemValueId2 = 2
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 20, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val checkBoxCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以下」指定状態で個数ちょうどなので登録出来る" in {
					checkBoxCheckBody must contain("<title>フォーム名１</title>")
					checkBoxCheckBody must contain("<h2>フォーム名１</h2>")
					checkBoxCheckBody must contain("フォーム１です")
					checkBoxCheckBody must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以下」指定状態で個数未満なので登録出来る" in {
					checkBoxCheckBody2 must contain("<title>フォーム名１</title>")
					checkBoxCheckBody2 must contain("<h2>フォーム名１</h2>")
					checkBoxCheckBody2 must contain("フォーム１です")
					checkBoxCheckBody2 must contain("<h4>結果メッセージ１<br /></h4>")
				}

				val checkBoxCheckBody3 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString, formItemValueId7.toString, formItemValueId8.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"チェックボックスの選択「個以下」指定状態で個数オーバーなので登録出来ない" in {
					checkBoxCheckBody3 must contain("フォーム１です")
					checkBoxCheckBody3 must contain("<strong>2項目以下、選択（または、入力）してください</strong>")
					checkBoxCheckBody3 must not contain("<h4>結果メッセージ１<br /></h4>")
				}
			}
		}

		"質問項目が複数ページの場合のテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = EncForms.regist(EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				EncFormConfigs.regist(EncFormConfigs(1, formId1, 10, now, Option(new SimpleDateFormat("yyyy/MM/dd").parse("2030/03/15")), "0", 2, 10, 500,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				// 質問項目挿入
				val itemTitle1 = "テキストフィールドの質問項目"
				val itemTitle2 = "テキストエリアの質問項目"
				val itemTitle3 = "ラジオボタンの質問項目"
				val itemTitle4 = "チェックボックスの質問項目"
				val itemTitle5 = "ドロップダウンの質問項目"
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, itemTitle1, 1, 150, "0", "0", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, itemTitle2, 1, 150, "0", "0", 1, now, now), now)

				val formItemValueId1 = 1
				val formItemValueId2 = 2

				EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)

				val testPreviewUrl = String.format("http://localhost:3333/encpreview/%s", formId1.toString)
				val preViewBody =await(WS.url(testPreviewUrl)
					.get).body

				"ページ内に質問項目が1件もない状態でプレビューを表示するとエラーとなる" in {
					preViewBody must contain("2ページ目のアンケートの質問項目が存在しません。")
				}

				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, itemTitle3, 2, 150, "0", "0", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, itemTitle4, 2, 1, "1", "0", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, itemTitle5, 2, 150, "0", "0", 4, now, now), now)
				// 質問項目選択肢挿入
				val formItemValueId3 = 3
				val formItemValueId4 = 4
				val formItemValueId5 = 5
				val formItemValueId6 = 6
				val formItemValueId7 = 7
				val formItemValueId8 = 8
				val formItemValueId9 = 9
				val formItemValueId10 = 10
				val formItemValueId11 = 11
				val radio1 = "ラジオその１"
				val radio2 = "ラジオその２"
				val radio3 = "ラジオその３"
				val check1 = "チェックその１"
				val check2 = "チェックその２"
				val check3 = "チェックその３"
				val drop1 = "ドロップダウンその１"
				val drop2 = "ドロップダウンその２"
				val drop3 = "ドロップダウンその３"

				EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

				val endAt = Calendar.getInstance()
				endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
				val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))

				val textAnswer = "ふなっしー"
				val textAreaAnswer =
					"""ふなっしー
					  |梨汁プシャー""".stripMargin

				val pageCheckBody = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("1"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"next" -> Seq("次へ"),
					"%s-%s".format(itemId1.getOrElse(-1).toString, formItemValueId1.toString) -> Seq(textAnswer),
					"%s-%s".format(itemId2.getOrElse(-1).toString, formItemValueId2.toString) -> Seq(textAreaAnswer)))).body

				"複数ページ指定状態で次のページが表示出来る" in {
					pageCheckBody must not contain("<h4>結果メッセージ１<br /></h4>")
					pageCheckBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle3))
					pageCheckBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, radio1))
					pageCheckBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, radio2))
					pageCheckBody must contain("<input type=\"radio\" name=\"%s\" onchange=\"sendAnswer('%s');\" value=\"%s\" >%s".format(itemId3.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, radio3))
					pageCheckBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle4))
					pageCheckBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId6, itemId4.getOrElse(-1), check1))
					pageCheckBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId7, itemId4.getOrElse(-1), check2))
					pageCheckBody must contain("<input type=\"checkbox\" name=\"%s\" value=\"%s\"  onchange=\"sendAnswer('%s');\" >%s".format(itemId4.getOrElse(-1), formItemValueId8, itemId4.getOrElse(-1), check3))
					pageCheckBody must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle5))
					pageCheckBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId9, drop1))
					pageCheckBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId10, drop2))
					pageCheckBody must contain("<option value=\"%s\"  >%s</option>".format(formItemValueId11, drop3))
				}

				val pageCheckBody2 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("2"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"back" -> Seq("前へ"),
					itemId3.getOrElse(-1).toString -> Seq(""),
					itemId4.getOrElse(-1).toString -> Seq(""),
					itemId5.getOrElse(-1).toString -> Seq("")))).body

				"複数ページ指定状態で前のページが表示出来る" in {
					pageCheckBody2 must not contain("<h4>結果メッセージ１<br /></h4>")
					pageCheckBody2 must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle1))
					pageCheckBody2 must contain("<div class=\"enc_question_title\">%s</div>".format(itemTitle2))
					pageCheckBody2 must contain(textAnswer)
					pageCheckBody2 must contain(textAreaAnswer)
				}

				val pageCheckBody3 = await(WS.url("http://localhost:3333/_enc/post")
					.post(Map(
					"answerId" -> Seq(answerId.getOrElse(-1).toString),
					"formId" -> Seq(formId1.toString),
					"pageNo" -> Seq("2"),
					"ispreview" -> Seq("false"),
					"mode" -> Seq("0"),
					"send" -> Seq("送信"),
					itemId3.getOrElse(-1).toString -> Seq(formItemValueId3.toString),
					itemId4.getOrElse(-1).toString -> Seq(formItemValueId6.toString),
					itemId5.getOrElse(-1).toString -> Seq(formItemValueId9.toString)))).body

				"複数ページの最終ページで回答結果が送信出来る" in {
					pageCheckBody3 must contain("<title>フォーム名１</title>")
					pageCheckBody3 must contain("<h2>フォーム名１</h2>")
					pageCheckBody3 must contain("フォーム１です")
					pageCheckBody3 must contain("<h4>結果メッセージ１<br /></h4>")
				}
			}
		}

	}
}
