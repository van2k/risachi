package controllers

import play.api.test._
import play.api.test.Helpers._
import play.api.libs.ws._
import org.specs2.mutable.Specification
import models.contract.Contracts
import java.util.Calendar
import play.api.mvc.{Cookies, Session}
import commons.Constants
import models.enc
import models.enc.{EncAnswers, EncAnswerDetails, EncFormItemValues, EncFormItems}
import java.text.SimpleDateFormat

class EncResultSpec extends PlaySpecification {
	import controllers._

	/**
	 * テスト実施用にログイン認証用の契約者IDを文字列で返す
	 *
	 * @return
	 */
	def getContractIdForAuth: String = {
		Contracts.directInsert("a@van2k.com", Contracts.STATUS_REGISTERED,
		"ふなっしー", "abc", Calendar.getInstance().getTime) must beTrue

		val contractId = Contracts.findByMail("a@van2k.com").get.contractId
		Contracts.findById(contractId.get).get.contractId.get.toString
	}

  "アンケート集計結果表示画面のテスト" should {
    "アンケート集計結果画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

	      val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

	      // フォーム挿入
	      val now = Calendar.getInstance().getTime
	      val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
		      None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

	      val testUrl = String.format("http://localhost:3333/report/1/%s/0", formId1.toString)

	      await(WS.url(testUrl)
		      .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
		      .get).body must contain("<h1>フォーム名１ - アンケート結果</h1>")
      }
    }

	  "指定したアンケートフォームに該当するアンケートが存在せずエラーとなる" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			  val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

			  // フォーム挿入
			  val now = Calendar.getInstance().getTime
			  enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
				  None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

			  val testUrl = String.format("http://localhost:3333/report/1/%s/0", "dummy")

			  await(WS.url(testUrl)
				  .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				  .get).status must equalTo(OK)
		  }
	  }

	  "メニューID1の集計結果画面が表示される" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			  val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

			  // フォーム挿入
			  val now = Calendar.getInstance().getTime
			  val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
				  None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

			  val testUrl = String.format("http://localhost:3333/report/1/%s/0", formId1.toString)

			  val body = await(WS.url(testUrl)
				  .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				  .get).body
			  body must contain("<h1>フォーム名１ - アンケート結果</h1>")
			  "ダミーデータの作成ボタンが表示されない" in {
				  body must not contain("ダミーデータの作成(100件)")
			  }
			  "アンケートの投稿画面へボタンが表示されない" in {
				  body must not contain("アンケート投稿画面へ")
			  }
		  }
	  }

	  "メニューID2が存在せずエラーとなる" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			  val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

			  // フォーム挿入
			  val now = Calendar.getInstance().getTime
			  val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
				  None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

			  val testUrl = String.format("http://localhost:3333/report/2/%s/0", formId1.toString)

			  await(WS.url(testUrl)
				  .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				  .get).status must not equalTo (OK)
		  }
	  }

		"未ログイン状態でアンケート集計結果画面を表示すると、ログイン画面が表示される" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

				val testUrl = String.format("http://localhost:3333/report/1/%s/0", formId1.toString)

				await(WS.url(testUrl).get).body must contain("<h1>アンケートフォームサービス ログイン画面</h1>")
			}
		}

	  "プレビューモードでアンケート集計結果画面を表示する" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			  val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

			  // フォーム挿入
			  val now = Calendar.getInstance().getTime
			  val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
				  None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get

			  val testUrl = String.format("http://localhost:3333/report/1/%s/1", formId1.toString)

			  val body = await(WS.url(testUrl)
				  .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				  .get).body
			  body must contain("<h1>フォーム名１ - アンケート結果 <span class=\"label label-info\">プレビューモード</span></h1>")
			  "ダミーデータの作成ボタンが表示される" in {
				  body must contain("ダミーデータの作成(100件)")
			  }
			  "アンケートの投稿画面へボタンが表示される" in {
				  body must contain("アンケート投稿画面へ")
			  }
		  }
	  }

		"入力項目に応じた集計結果が表示出来る" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			  val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

			  // フォーム挿入
			  val now = Calendar.getInstance().getTime
			  val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
				  None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
			  // フォーム設定挿入
			  enc.EncFormConfigs.regist(enc.EncFormConfigs(1, formId1, 10, new SimpleDateFormat("yyyy/MM/dd").parse("2012/02/10"), Option(new SimpleDateFormat("yyyy/MM/dd").parse("2012/03/15")), "0", 2, 10, 20,
				  Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

			  // 質問項目挿入
			  val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
			  val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
			  val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
			  val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
			  val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
			  // 質問項目選択肢挿入
			  val formItemValueId1 = 1
			  val formItemValueId2 = 2
			  val formItemValueId3 = 3
			  val formItemValueId4 = 4
			  val formItemValueId5 = 5
			  val formItemValueId6 = 6
			  val formItemValueId7 = 7
			  val formItemValueId8 = 8
			  val formItemValueId9 = 9
			  val formItemValueId10 = 10
			  val formItemValueId11 = 11
			  val radio1 = "ラジオその１"
			  val radio2 = "ラジオその２"
			  val radio3 = "ラジオその３"
			  val check1 = "チェックその１"
			  val check2 = "チェックその２"
			  val check3 = "チェックその３"
			  val drop1 = "ドロップダウンその１"
			  val drop2 = "ドロップダウンその２"
			  val drop3 = "ドロップダウンその３"

			  EncFormItemValues.regist(EncFormItemValues(formItemValueId1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId3, itemId3.getOrElse(-1), radio1, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId4, itemId3.getOrElse(-1), radio2, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId5, itemId3.getOrElse(-1), radio3, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId6, itemId4.getOrElse(-1), check1, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId7, itemId4.getOrElse(-1), check2, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId8, itemId4.getOrElse(-1), check3, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId9, itemId5.getOrElse(-1), drop1, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId10, itemId5.getOrElse(-1), drop2, 0, now, now), now)
			  EncFormItemValues.regist(EncFormItemValues(formItemValueId11, itemId5.getOrElse(-1), drop3, 0, now, now), now)

			  val endAt = Calendar.getInstance()
			  endAt.add(Calendar.MINUTE, 3)
				// 回答内容登録
			  val answerId = EncAnswers.regist(EncAnswers(-1, formId1, now, Option(endAt.getTime), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "0", "127.0.0.1", "Some(Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0)", now, now))
			  val text1 = "ふなっしー"
			  val text2 = "チーバくん"
			  val text3 = "カムロちゃん"
			  // テキストフィールド質問項目に回答を挿入
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text1, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text2, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId1.getOrElse(-1), formItemValueId1, text3, Option(now), now, now))

			  // テキストエリア質問項目に回答を挿入
			  val textArea1 =
				  """
				    |ふなっしー
				    |きゃはー
				  """.stripMargin

			  val textArea2 =
				  """
				    |チーバくん
				    |鉄っちゃんの聖地
				  """.stripMargin

			  val textArea3 =
				  """
				    |カムロちゃん
				    |佐倉市の公式ゆるキャラ
				  """.stripMargin

			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea1, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea2, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId2.getOrElse(-1), formItemValueId2, textArea3, Option(now), now, now))

			  // ラジオボタン質問項目に回答を挿入
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId3, formItemValueId3.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId4, formItemValueId4.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId3.getOrElse(-1), formItemValueId5, formItemValueId5.toString, Option(now), now, now))

			  // チェックボックス質問項目に回答を挿入
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId6, formItemValueId6.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId7, formItemValueId7.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId4.getOrElse(-1), formItemValueId8, formItemValueId8.toString, Option(now), now, now))

			  // ドロップダウン質問項目に回答を挿入
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId9, formItemValueId9.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId10, formItemValueId10.toString, Option(now), now, now))
			  EncAnswerDetails.regist(EncAnswerDetails(-1, answerId.getOrElse(-1), itemId5.getOrElse(-1), formItemValueId11, formItemValueId11.toString, Option(now), now, now))


			  val testUrl = String.format("http://localhost:3333/report/1/%s/0", formId1.toString)

			  val body = await(WS.url(testUrl)
				  .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				  .get).body

			  "入力項目がテキストフィールドの集計結果が確認できる" in {
				  body must contain(text1)
				  body must contain(text2)
				  body must contain(text3)
			  }


			  "入力項目がテキストエリアの集計結果が確認できる" in {
				  body must contain(textArea1)
				  body must contain(textArea2)
				  body must contain(textArea3)
			  }

			  "入力項目がラジオボタンの集計結果が確認できる" in {
				  body must contain("data.setCell(0, 0, '%s');".format(radio1))
				  body must contain("data.setCell(1, 0, '%s');".format(radio2))
				  body must contain("data.setCell(2, 0, '%s');".format(radio3))
			  }

			  "入力項目がチェックボックスの集計結果が確認できる" in {
				  body must contain("data.setCell(0, 0, '%s');".format(check1))
				  body must contain("data.setCell(1, 0, '%s');".format(check2))
				  body must contain("data.setCell(2, 0, '%s');".format(check3))
			  }

			  "入力項目がドロップダウンリストの集計結果が確認できる" in {
				  body must contain("data.setCell(0, 0, '%s');".format(drop1))
				  body must contain("data.setCell(1, 0, '%s');".format(drop2))
				  body must contain("data.setCell(2, 0, '%s');".format(drop3))
			  }
		  }
	  }
  }

	"ダミーデータ作成のテスト" should {
		"ダミーデータが作成出来る" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, formId1, 10, new SimpleDateFormat("yyyy/MM/dd").parse("2012/02/10"), Option(new SimpleDateFormat("yyyy/MM/dd").parse("2012/03/15")), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/reportdummy/%s/100", formId1.toString)

				await(WS.url(testUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).status must equalTo (OK)

				val dummyCount1 = EncAnswerDetails.findByFormitemid(itemId1.getOrElse(-1)).length
				val dummyCount2 = EncAnswerDetails.findByFormitemid(itemId2.getOrElse(-1)).length
				val dummyCount3 = EncAnswerDetails.findByFormitemid(itemId3.getOrElse(-1)).length
				val dummyCount4 = EncAnswerDetails.findByFormitemid(itemId4.getOrElse(-1)).length
				val dummyCount5 = EncAnswerDetails.findByFormitemid(itemId5.getOrElse(-1)).length

				"DBにもパラメータで指定した件数分ダミーデータが登録される（テキストフィールド）" in {
					dummyCount1 mustEqual 100
				}

				"DBにもパラメータで指定した件数分ダミーデータが登録される（テキストエリア）" in {
					dummyCount2 mustEqual 100
				}

				"DBにもパラメータで指定した件数分ダミーデータが登録される（ラジオボタン）" in {
					dummyCount3 mustEqual 100
				}

				"DBにもパラメータで指定した件数分ダミーデータが登録される（チェックボックス）" in {
					dummyCount4 mustEqual 100
				}

				"DBにもパラメータで指定した件数分ダミーデータが登録される（プルダウンリスト）" in {
					dummyCount5 mustEqual 100
				}

				val reportUrl = String.format("http://localhost:3333/report/1/%s/0", formId1.toString)

				val body = await(WS.url(reportUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body

				"通常モードの場合ダミーデータは集計結果に含まれない" in {
					body must contain("<span id=\"finished_count\">0</span>件")
				}

				val previewUrl = String.format("http://localhost:3333/report/1/%s/1", formId1.toString)

				val previewBody = await(WS.url(previewUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body

				"プレビューモードの場合ダミーデータは集計結果に含まれる" in {
					previewBody must contain("<span id=\"finished_count\">100</span>件")
				}
			}
		}

		"未ログイン状態の為ダミーデータが作成出来ない" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/reportdummy/%s/100", formId1.toString)

				await(WS.url(testUrl)
					.get).body must contain("<h1>アンケートフォームサービス ログイン画面</h1>")

				val dummyCount1 = EncAnswerDetails.findByFormitemid(itemId1.getOrElse(-1)).length
				val dummyCount2 = EncAnswerDetails.findByFormitemid(itemId2.getOrElse(-1)).length
				val dummyCount3 = EncAnswerDetails.findByFormitemid(itemId3.getOrElse(-1)).length
				val dummyCount4 = EncAnswerDetails.findByFormitemid(itemId4.getOrElse(-1)).length
				val dummyCount5 = EncAnswerDetails.findByFormitemid(itemId5.getOrElse(-1)).length

				"DBにダミーデータが登録されない（テキストフィールド）" in {
					dummyCount1 mustEqual 0
				}

				"DBにダミーデータが登録されない（テキストエリア）" in {
					dummyCount2 mustEqual 0
				}

				"DBにダミーデータが登録されない（ラジオボタン）" in {
					dummyCount3 mustEqual 0
				}

				"DBにダミーデータが登録されない（チェックボックス）" in {
					dummyCount4 mustEqual 0
				}

				"DBにダミーデータが登録されない（プルダウンリスト）" in {
					dummyCount5 mustEqual 0
				}
			}
		}

		"パラメータのフォームIDに該当するフォームが存在せずエラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> getContractIdForAuth)))

				// フォーム挿入
				val now = Calendar.getInstance().getTime
				val formId1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
				// 質問項目挿入
				val itemId1 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 1, "テキストフィールドの質問項目", 1, 150, "0", "1", 0, now, now), now)
				val itemId2 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 2, "テキストエリアの質問項目", 1, 150, "0", "1", 1, now, now), now)
				val itemId3 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 3, "ラジオボタンの質問項目", 1, 150, "0", "1", 2, now, now), now)
				val itemId4 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 4, "チェックボックスの質問項目", 1, 1, "0", "1", 3, now, now), now)
				val itemId5 = EncFormItems.regist(EncFormItems(-1, formId1, 1, 5, "ドロップダウンの質問項目", 1, 150, "0", "1", 4, now, now), now)
				// 質問項目選択肢挿入
				EncFormItemValues.regist(EncFormItemValues(1, itemId1.getOrElse(-1), "dummy", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(2, itemId2.getOrElse(-1), "dummy area", 100, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(3, itemId3.getOrElse(-1), "ラジオその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(4, itemId3.getOrElse(-1), "ラジオその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(5, itemId3.getOrElse(-1), "ラジオその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(6, itemId4.getOrElse(-1), "チェックその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(7, itemId4.getOrElse(-1), "チェックその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(8, itemId4.getOrElse(-1), "チェックその３", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(9, itemId5.getOrElse(-1), "ドロップダウンその１", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(10, itemId5.getOrElse(-1), "ドロップダウンその２", 0, now, now), now)
				EncFormItemValues.regist(EncFormItemValues(11, itemId5.getOrElse(-1), "ドロップダウンその３", 0, now, now), now)

				val testUrl = String.format("http://localhost:3333/reportdummy/%s/100", "hogehogehogehog")

				await(WS.url(testUrl)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).status must not equalTo (OK)

				val dummyCount1 = EncAnswerDetails.findByFormitemid(itemId1.getOrElse(-1)).length
				val dummyCount2 = EncAnswerDetails.findByFormitemid(itemId2.getOrElse(-1)).length
				val dummyCount3 = EncAnswerDetails.findByFormitemid(itemId3.getOrElse(-1)).length
				val dummyCount4 = EncAnswerDetails.findByFormitemid(itemId4.getOrElse(-1)).length
				val dummyCount5 = EncAnswerDetails.findByFormitemid(itemId5.getOrElse(-1)).length

				"DBにダミーデータが登録されない（テキストフィールド）" in {
					dummyCount1 mustEqual 0
				}

				"DBにダミーデータが登録されない（テキストエリア）" in {
					dummyCount2 mustEqual 0
				}

				"DBにダミーデータが登録されない（ラジオボタン）" in {
					dummyCount3 mustEqual 0
				}

				"DBにダミーデータが登録されない（チェックボックス）" in {
					dummyCount4 mustEqual 0
				}

				"DBにダミーデータが登録されない（プルダウンリスト）" in {
					dummyCount5 mustEqual 0
				}
			}
		}
	}
}
