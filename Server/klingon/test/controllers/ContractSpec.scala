package controllers

import org.specs2.mutable.Specification
import org.specs2.specification.Before
import play.api.test.Helpers._
import play.api.test._
import play.api.libs.ws._
import models.contract.Contracts
import java.util.Calendar
import models.contract

class ContractSpec extends PlaySpecification with Before {
	import controllers._

  // 前処理
  def before = {

  }

  "契約者登録画面のテスト" should {
    "契約者仮登録画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/regist").get).status must equalTo(OK)
      }
    }
  }

  "契約者仮登録確認画面のテスト" should {
    "契約者仮登録確認画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("w243fscd2wd3r2s13@van2k.com"), "agree" -> Seq("true")))).status must equalTo(OK)
      }
    }

    "メールアドレスが未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("agree" -> Seq("true")))).body must contain("メールアドレスを入力してください。")
      }
    }

    "利用規約に同意していないのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("w243fscd2wd3r2s13@van2k.com"))))
          .body must contain("利用規約に同意しないと登録出来ません。")
      }
    }

    "メールアドレスに全角文字を入力" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("全角文字")))).body must contain("メールアドレスが正しくありません")
      }
    }

    "メールアドレスに半角数値のみを入力" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("12323212")))).body must contain("メールアドレスが正しくありません")
      }
    }

    "メールアドレスに半角英字のみを入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("van2kvan2k.com")))).body must contain("メールアドレスが正しくありません")
      }
    }

    "メールアドレスとして不正なフォーマットで入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("hogehoge@van2@kvan2k.com")))).body must contain("メールアドレスが正しくありません")
      }
    }

    "利用規約が不正な選択値なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("agree" -> Seq("false")))).body must contain("利用規約に同意しないと登録出来ません。")
      }
    }

    "メールアドレスが既に登録されているのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("hogehoge@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/confirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"), "agree" -> Seq("true"))))
          .body must contain("指定されたメールアドレスは既に使用されています。")
      }
    }
  }

  "契約者仮登録確認画面から戻る画面のテスト" should {
    "契約者仮登録確認画面から戻る画面が表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backRegist?mail=hogehoge%40van2k.com").get)
          .body must contain("利用規約に同意する")
      }
    }

    "メールアドレスがブランクでも契約者仮登録確認画面から戻る画面が表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backRegist?mail=").get)
          .body must contain("利用規約に同意する")
      }
    }
  }

  "契約者仮登録画面登録完了画面のテスト" should {
    "契約者仮登録確認画面で登録する" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        services.Contract.testMode = true
        await(WS.url("http://localhost:3333/contract/interimRegist")
          .post(Map("mail" -> Seq("test@van2k.com"))))
            .body must contain("ご入力頂いたメールアドレスに確認のメールを送信しました。")
      }
    }

    "契約者仮登録確認画面でメールアドレスが重複していた場合はエラー" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        services.Contract.testMode = true
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "token", limit.getTime)
        Contracts.findByMail("test@van2k.com") must beSome
        await(WS.url("http://localhost:3333/contract/interimRegist")
          .post(Map("mail" -> Seq("test@van2k.com"))))
          .body must contain("不明なエラーが発生しました。")
      }
    }
  }

  "契約者登録画面のテスト" should {
    "契約者登録画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)

        await(WS.url("%s%s".format("http://localhost:3333/contract/profileRegist/", token)).get)
          .body must contain("必要事項を入力して下さい。")
      }
    }

    "トークンが無効の為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/profileRegist/cdurd6ux6stxynhlhlds45").get)
          .body must contain("このURLは有効期限が切れています。最初からやり直してください。")

      }
    }

    "トークンの有効期限切れの為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, -1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)

        await(WS.url("%s%s".format("http://localhost:3333/contract/profileRegist/", token)).get)
          .body must contain("このURLは有効期限が切れています。最初からやり直してください。")
      }
    }

    "既に登録済みのトークンの為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileRegist/tokentoken").get)
          .body must contain("このURLは有効期限が切れています。最初からやり直してください。")
      }
    }
  }

  "契約者登録確認画面のテスト" should {
    "契約者登録確認画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
                    "name" -> Seq("番付太郎"),
                    "pass.main" -> Seq(pass),
                    "pass.confirm" -> Seq(pass))))
            .body must contain("入力された内容")
      }
    }

    "メールアドレスが未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq(""),
          "name" -> Seq("番付太郎"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(pass))))
          .body must contain("メールアドレスが正しくありません")
      }
    }

    "名前が未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq(""),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(pass))))
          .body must contain("1 文字以上でご入力ください")
      }
    }

    "名前が全角64文字以上なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(pass))))
          .body must contain("64 文字以下でご入力ください")
      }
    }

    "名前が半角64文字以上なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("12345678901234567890123456789012345678901234567890123456789012345"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(pass))))
          .body must contain("64 文字以下でご入力ください")
      }
    }

    "パスワードが未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("番付太郎"),
          "pass.main" -> Seq(""),
          "pass.confirm" -> Seq(pass))))
          .body must contain("6 文字以上でご入力ください")
      }
    }

    "パスワード(確認)が未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("番付太郎"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(""))))
          .body must contain("パスワードが一致しません。")
      }
    }

    "パスワードとパスワード(確認)が一致しないのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "123456"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("番付太郎"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq("oyoyooyoyo"))))
          .body must contain("パスワードが一致しません。")
      }
    }

    "パスワード(確認)が6文字未満なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val pass:String = "12345"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileConfirm")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"),
          "name" -> Seq("番付太郎"),
          "pass.main" -> Seq(pass),
          "pass.confirm" -> Seq(pass))))
          .body must contain("6 文字以上でご入力ください")
      }
    }

	  "パスワード(確認)が64文字よりも多いのでエラーとなる" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			  // 仮登録データの挿入
			  val pass:String = "12345678901234567890123456789012345678901234567890123456789012345"
			  val limit = Calendar.getInstance()
			  limit.add(Calendar.HOUR, 1)

			  Contracts.tempRegist("hogehoge@van2k.com", "token", limit.getTime)
			  await(WS.url("http://localhost:3333/contract/profileConfirm")
				  .post(Map("mail" -> Seq("hogehoge@van2k.com"),
				  "name" -> Seq("番付太郎"),
				  "pass.main" -> Seq(pass),
				  "pass.confirm" -> Seq(pass))))
				  .body must contain("64 文字以下でご入力ください")
		  }
	  }

  }

  "契約者登録確認から戻る画面のテスト" should {
    "契約者登録確認から戻る画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backProfileRegist?mail=hogehoge%40van2k.com&name=name&token=hogehoge")
          .get)
          .body must contain("必要事項を入力して下さい。")

      }
    }

    "パラメータのメールアドレスが未指定なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backProfileRegist?mail=&name=name&token=hogehoge")
          .get)
          .body must contain("必要事項を入力して下さい。")
      }
    }

    "パラメータの名前が未指定なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backProfileRegist?mail=hogehoge%40van2k.com&name=&token=hogehoge")
          .get)
          .body must contain("必要事項を入力して下さい。")
      }
    }

    "パラメータのトークンが未指定なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/backProfileRegist?mail=hogehoge%40van2k.com&name=name&token=")
          .get)
          .body must contain("必要事項を入力して下さい。")
      }
    }

  }

  "契約登録完了画面のテスト" should {
    // /contract/profileSubmit
    "契約登録完了画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileSubmit")
          .post(Map("name" -> Seq("番付太郎"), "token" -> Seq(token))))
            .body must contain("契約者情報の登録が完了しました。")
      }
    }
    "トークンが未入力なのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileSubmit")
          .post(Map("name" -> Seq("番付太郎"), "token" -> Seq(""))))
            .body must contain("仮登録から時間が経っています。最初からやり直してください。")
      }
    }
    "トークンに一致する契約者情報が存在しないのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileSubmit")
          .post(Map("name" -> Seq("番付太郎"), "token" -> Seq("token"))))
          .body must contain("仮登録から時間が経っています。最初からやり直してください。")
      }
    }
    "トークンの有効期限切れの為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 仮登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, -1)

        Contracts.tempRegist("hogehoge@van2k.com", token, limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileSubmit")
          .post(Map("name" -> Seq("番付太郎"), "token" -> Seq(token))))
          .body must contain("仮登録から時間が経っています。最初からやり直してください。")
      }
    }
    "既に登録済みのトークンなのでエラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        // 登録データの挿入
        val token:String = "hoiefjoweighweorgiwjdfwegf243rfgw"
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", token, limit.getTime)
        await(WS.url("http://localhost:3333/contract/profileSubmit")
          .post(Map("name" -> Seq("番付太郎"), "token" -> Seq("token"))))
          .body must contain("仮登録から時間が経っています。最初からやり直してください。")
      }
    }
  }

  "パスワード再設定受付画面のテスト" should {
    "パスワード再設定受付画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/forgotPass").get).status must equalTo(OK)
      }
    }
  }

  "パスワード再設定受付完了画面のテスト" should {
    "パスワード再設定受付完了画面が正常に表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        Contracts.directInsert("w243fscd2wd3r2s13@van2k.com", contract.Contracts.STATUS_REGISTERED,
          "テスト太郎", "abc", Calendar.getInstance().getTime) must beTrue
        services.Contract.testMode = true

        await(WS.url("http://localhost:3333/contract/forgotPassMailSubmit")
          .post(Map("mail" -> Seq("w243fscd2wd3r2s13@van2k.com"))))
          .body must contain("メールを送信しました。ご確認ください。")
      }
    }

    "メールアドレスが未入力の為パスワード再設定受付画面を表示する" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/forgotPassMailSubmit")
          .post(Map("mail" -> Seq(""))))
          .body must contain("メールアドレスを入力してください。")
      }
    }

    "メールアドレスが未登録の為パスワード再設定受付画面を表示する" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/forgotPassMailSubmit")
          .post(Map("mail" -> Seq("hogehoge@van2k.com"))))
          .body must contain("このメールアドレスは登録されていません。")
      }
    }
  }

  "パスワード再設定画面のテスト" should {
    "パスワード再設定画面が表示できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgotPassNewPass/tokentoken").get)
          .body must contain("新しいパスワードを入力してください。")
      }
    }

    "パスワードトークンが不正の為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        await(WS.url("http://localhost:3333/contract/forgotPassNewPass/tokentoken").get)
          .body must contain("ご指定のURLは無効です。")
      }
    }

    "パスワード再設定有効期限切れの為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, -1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgotPassNewPass/tokentoken").get)
          .body must contain("ご指定のURLは無効です。")
      }
    }

	  "パスワードが既に再設定済みの為エラーとなる" in {
		  running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			  val limit = Calendar.getInstance()
			  limit.add(Calendar.HOUR, 1)

			  // メールアドレスを登録
			  Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
			  // トークンを無効にする
			  Contracts.invalidateRegistToken("tokentoken")
			  await(WS.url("http://localhost:3333/contract/forgotPassNewPass/tokentoken").get)
				  .body must contain("ご指定のURLは無効です。")
		  }
	  }
  }

  "パスワード再設定完了画面のテスト" should {
    "パスワードが再設定できる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq("123456"), "pass.confirm" -> Seq("123456"))))
          .body must contain("パスワードの再設定が完了しました。")
      }
    }

    "パスワード未入力の為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq(""), "pass.confirm" -> Seq("123456"))))
          .body must contain("6 文字以上でご入力ください")
      }
    }

    "パスワード（確認）未入力の為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq("123456"), "pass.confirm" -> Seq(""))))
          .body must contain("パスワードが一致しません。")
      }
    }

    "パスワードとパスワード（確認）が一致しない為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq("123456"), "pass.confirm" -> Seq("234567"))))
          .body must contain("パスワードが一致しません。")
      }
    }

    "パスワードが6文字未満の為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq("12345"), "pass.confirm" -> Seq("12345"))))
          .body must contain("6 文字以上でご入力ください")
      }
    }

    "トークンに該当する登録が有効期限切れの為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, -1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("tokentoken"), "pass.main" -> Seq("123456"), "pass.confirm" -> Seq("123456"))))
          .body must contain("ご指定のURLは無効です。")
      }
    }

    "トークンに該当する登録が無い為エラーとなる" in {
      running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
        val limit = Calendar.getInstance()
        limit.add(Calendar.HOUR, 1)

        // メールアドレスを登録
        Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
        await(WS.url("http://localhost:3333/contract/forgorPassNewPassSubmit")
          .post(Map("token" -> Seq("token"), "pass.main" -> Seq("123456"), "pass.confirm" -> Seq("123456"))))
          .body must contain("ご指定のURLは無効です。")
      }
    }
  }

	"新パスワード表示画面のテスト" should {
		"新パスワード表示画面が表示できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val limit = Calendar.getInstance()
				limit.add(Calendar.HOUR, 1)

				// メールアドレスを登録
				Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
				await(WS.url("http://localhost:3333/contract/newPassword/tokentoken").get)
					.body must contain("あなたの新しいパスワードは以下の通りです。")
			}
		}

		"トークンに該当する登録が無い為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

				await(WS.url("http://localhost:3333/contract/newPassword/tokentoken").get)
					.body must contain("ご指定のURLは無効です。")
			}
		}

		"トークンに該当する登録が有効期限切れの為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val limit = Calendar.getInstance()
				limit.add(Calendar.HOUR, -1)

				// メールアドレスを登録
				Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
				await(WS.url("http://localhost:3333/contract/newPassword/tokentoken").get)
					.body must contain("ご指定のURLは無効です。")
			}
		}

		"新パスワードが既に表示されている為エラーとなる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val limit = Calendar.getInstance()
				limit.add(Calendar.HOUR, 1)

				// メールアドレスを登録
				Contracts.directInsert("test@van2k.com", Contracts.STATUS_REGISTERED, "番付太郎", "tokentoken", limit.getTime)
				// トークンを無効にする
				Contracts.invalidateRegistToken("tokentoken")
				await(WS.url("http://localhost:3333/contract/newPassword/tokentoken").get)
					.body must contain("ご指定のURLは無効です。")
			}
		}
	}
}
