package controllers

import org.specs2.mutable.Specification
import org.specs2.mutable.Before
import play.api.test.Helpers._
import play.api.test._
import play.api.libs.ws.WS
import commons.Constants
import play.api.mvc.Session
import play.api.mvc.Cookies
import models.enc.{EncFormConfigs, EncForms}
import java.util.Calendar
import models._
import java.text.SimpleDateFormat
import models.contract.Contracts

class EncConfSpec extends PlaySpecification with Before {
	import controllers._

	// 前処理
	def before = {
		// println("before start")
	}

	// 1フォームの追加
	def addForm1(): (Long, String, String) = {
		val now = Calendar.getInstance().getTime
		val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
		val date_02_10 = dateFormat.parse("2012/02/10")
		val date_03_15 = dateFormat.parse("2012/03/15")

		// 契約者を挿入
		Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
			"複数アンケート用", "a", now) must beTrue

		val formId = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
			None, None, None, enc.EncForms.STATUS_EDIT, "1", now, now)).get

		// フォーム設定挿入
		enc.EncFormConfigs.regist(enc.EncFormConfigs(1, formId, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
			Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

		val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get

		(formId, contractId.toString, "1")
	}

	// 項目の追加
	def addSingleItem(cookie:play.api.mvc.Cookie, pageCount:Seq[String] = Seq("1"),
	                  maxLength:Seq[String] = Seq("100"), contractId:Seq[String] = Seq("1"), dispOdr:Seq[String] = Seq("0"),
	                  formId:Seq[String] = Seq("1"), formItemId:Seq[String] = Seq("-1"),  selectCount:Seq[String] = Seq("1"),
	                  itemId:Seq[String] = Seq("1"),  pageNum: Seq[String] = Seq("1"), questionItem:Seq[String] = Seq("dummy"),
	                  title: Seq[String] = Seq("title"), questionType: Seq[String] = Seq("1"),  required:Seq[String] = Seq("true"),
	                  selectNumber:Seq[String] = Seq("0"), updateAt:Seq[String] = Seq("")) = {
		// 項目を追加する
		await(WS.url("http://localhost:3333/conf/updateformitems")
			.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(cookie)))
			.post(Map("allPageCount" -> pageCount,
			"ansInputMaxLength" -> maxLength,
			"contractId" -> contractId,
			"dispOdr" -> dispOdr,
			"formId" -> formId,
			"formItemId" -> formItemId,
			"itemSelectCount" -> selectCount,
			"mFormItemId" -> itemId,
			"pageNum" -> pageNum,
			"questionItems[0]" -> questionItem,
			"questionTitle" -> title,
			"questionType" -> questionType,
			"required" -> required,
			"selectNumberCondition" -> selectNumber,
			"updatedAtFormItems" -> updateAt))).status must equalTo(OK)
	}

	"アンケート編集画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			val now = Calendar.getInstance().getTime
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			// アンケートの項目を追加する
			val itemId = enc.EncFormItems.regist(enc.EncFormItems(-1, formId, 0, 1, "title", 1, 150, "0","1", 0, now, now), now)
			// アンケートの値を追加する
			enc.EncFormItemValues.regist(enc.EncFormItemValues(-1, itemId.get, "dummy", 100, now, now), now)

			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			// 項目を追加する
			await(WS.url("http://localhost:3333/conf/updateformitems")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("allPageCount" -> Seq("1"),
				"ansInputMaxLength" -> Seq("101"),
				"contractId" -> Seq(contractId),
				"dispOdr" -> Seq("1"),
				"formId" -> Seq(formId.toString),
				"formItemId" -> Seq(itemId.getOrElse(-1).toString),
				"itemSelectCount" -> Seq("150"),
				"mFormItemId" -> Seq("1"),
				"pageNum" -> Seq("1"),
				"questionItems[0]" -> Seq("dummy2"),
				"questionTitle" -> Seq("title1"),
				"questionType" -> Seq("1"),
				"required" -> Seq("false"),
				"selectNumberCondition" -> Seq("0"),
				"updatedAtFormItems" -> Seq("2013-04-06 19:15:44.995")))).status must equalTo(OK)

			// アンケート編集画面を開く
			val body = await(WS.url("http://localhost:3333/conf/formedit?formId=" + formId)
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.get).body

			"編集画面が正常に表示される" in {
				body must contain ("フォーム名1") and contain("title1")
			}
			val tbody = await(WS.url("http://localhost:3333/conf/formedit?formId=23343")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.get).body

			"存在しないアンケート" in {
				tbody must not contain ("フォーム名1") and not contain("title1")
			}
		}
	}

	"アンケート一覧画面(.list)のテスト" should {
		"編集中で、各ボタンが正常に表示できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"複数アンケート用", "a", now) must beTrue

				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_EDIT, "1", now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// フォームが2件表示されることを確認
				await(WS.url("http://localhost:3333/conf/list")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("フォーム名1") and contain("フォーム１です") and contain("編集") and
					contain("削除") and contain("プレビュー")
			}
		}

		"料金設定(.chargeupdate)のテスト" should {
			"編集画面が正常に表示できる" in {
				running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
					val now = Calendar.getInstance().getTime
					Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,     "複数アンケート用", "a", now) must beTrue

					val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get

					val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

					val tbody = await(WS.url("http://localhost:3333/conf/chargeupdate")
						.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
						.post(Map("formId" -> Seq("0"),
						"planId" -> Seq("4"),
						"changePlan" -> Seq("0"),
            "tutorialType" -> Seq("0")))).body

					tbody must contain("質問の追加") and contain("ページの追加")

					// EncFormConfigsに登録されているか確認する
					val ret = EncFormConfigs.findAll()
					ret.length must equalTo(1)
					ret(0).chargePlanId.get must equalTo(4)
				}
			}

			"更新が正常に完了する" in {
				running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
					val now = Calendar.getInstance().getTime
					val enddt = Calendar.getInstance().getTime
					// アンケートを追加する
					val (formId, contractId, urlKey) = addForm1()
					val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

					val tbody = await(WS.url("http://localhost:3333/conf/chargeupdate")
						.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
						.post(Map("formId" -> Seq(formId.toString),
						"planId" -> Seq("4"),
						"changePlan" -> Seq("1"),
            "tutorialType" -> Seq("0")))).body

					// EncFormConfigsに登録されているか確認する
					val ret = EncFormConfigs.findByFormid(formId)
					ret.get.chargePlanId.get must equalTo(4);
				}
			}

			"アンケートが存在しない" in {
				running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
					val now = Calendar.getInstance().getTime
					val (formId, contractId, urlKey) = addForm1()
					val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

					val tbody = await(WS.url("http://localhost:3333/conf/chargeupdate")
						.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
						.post(Map("formId" -> Seq("999"),
						"planId" -> Seq("4"),
						"changePlan" -> Seq("1"),
            "tutorialType" -> Seq("0")))).body

					// EncFormConfigsが更新されていないことを確認
					val ret = EncFormConfigs.findByFormid(formId)
					ret.get.chargePlanId.get must equalTo(1);
				}
			}

			"料金プランが存在しない" in {
				running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
					val now = Calendar.getInstance().getTime
					val (formId, contractId, urlKey) = addForm1()
					val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

					val tbody = await(WS.url("http://localhost:3333/conf/chargeupdate")
						.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
						.post(Map("formId" -> Seq(formId.toString),
						"planId" -> Seq("999"),
						"changePlan" -> Seq("1"),
            "tutorialType" -> Seq("0")))).body

					// EncFormConfigsに登録されているか確認する
					val ret = EncFormConfigs.findByFormid(formId)
					ret.get.chargePlanId.get must not equalTo(999)
				}
			}
		}

		"アンケート一覧画面が正常に表示できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")
				val date_04_20 = dateFormat.parse("2012/04/20")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"複数アンケート用", "a", now) must beTrue

				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
				val form_id2 = enc.EncForms.regist(enc.EncForms(2, 1, "フォーム名2", "フォーム2です", "結果メッセージ2", 2,
					None, None, None, enc.EncForms.STATUS_EDIT, "2", now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				enc.EncFormConfigs.regist(enc.EncFormConfigs(2, form_id2, 10, date_03_15, Option(date_04_20), "0", 2, 10, 20,
					Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// フォームが2件表示されることを確認
				await(WS.url("http://localhost:3333/conf/list")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("フォーム名1") and contain("フォーム名2")
			}
		}

		"お知らせが正常に表示できる" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"複数アンケート用", "a", now) must beTrue

				models.Information.insert("お知らせ件名", "お知らせ本文")

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// DBに登録されたお知らせの内容が表示されているか確認する
				await(WS.url("http://localhost:3333/conf/list")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("お知らせ件名") and contain("お知らせ本文")
			}
		}

		"フォーム削除関連のテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")
				val date_04_20 = dateFormat.parse("2012/04/20")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"複数アンケート用", "a", now) must beTrue

				val urlKey1 = "1"
				val urlKey2 = "2"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, urlKey1, now, now)).get
				val form_id2 = enc.EncForms.regist(enc.EncForms(2, 1, "フォーム名2", "フォーム2です", "結果メッセージ2", 2,
					None, None, None, enc.EncForms.STATUS_EDIT, urlKey2, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
				enc.EncFormConfigs.regist(enc.EncFormConfigs(2, form_id2, 10, date_03_15, Option(date_04_20), "0", 2, 10, 20,
					Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				val body1 = await(WS.url("http://localhost:3333/conf/deleteconfirm/" + urlKey1)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body
				"削除確認画面が正しく表示できる" in {
					body1 must contain ("アンケートフォーム 削除確認")
				}

				val body2 = await(WS.url("http://localhost:3333/conf/deleteconfirm/" + urlKey1).get).body

				"未ログイン状態の場合削除確認画面表示でエラーとなる" in {
					body2 must contain("ログインしてください")
				}

				val body3 = await(WS.url("http://localhost:3333/conf/deleteconfirm/hogehogehogehoeg")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body
				"フォームIDが不正の場合削除確認画面表示でエラーとなる" in {
					body3 must not contain("アンケートフォーム 削除確認")
				}

				val body4 = await(WS.url("http://localhost:3333/conf/deleteform")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("form_id" -> Seq(form_id1.toString)))).body

				"フォーム削除確認画面でフォームの削除にチェックが入っていない為エラーとなる" in {
					body4 must contain("アンケートフォーム削除のチェックボックスが未チェックです")
				}

				val body5 = await(WS.url("http://localhost:3333/conf/deleteform")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("form_id" -> Seq(form_id1.toString),
					"delete_check" -> Seq("1")))).body
				val isFormExist = EncForms.find(form_id1).isEmpty

				"フォーム削除処理が実行される" in {
					isFormExist must beTrue
				}
			}
		}

		"編集中状態から開始状態へ正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId.toString),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(form_id1.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 開始中が表示される
				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/2")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("開始中") and contain("プレビュー") and contain("集計結果") and not contain ("</i> 削除<a>")
			}
		}

		"編集中状態からアンケート項目が0件の状態で開始状態へ" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "", "", "", 1,
					None, None, None, enc.EncForms.STATUS_EDIT, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// 開始中が表示される
				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/2")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body

				enc.EncForms.find(form_id1).getOrElse(enc.EncForms.getBlankForm()).status must equalTo(enc.EncForms.STATUS_EDIT)
			}
		}

		"開始状態から終了状態へ正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/3")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("終了") and contain("プレビュー") and contain("集計結果") and not contain ("</i> 削除<a>")
			}
		}

		"開始状態から中止状態へ正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_RUN, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/4")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("中止") and contain("プレビュー") and contain("集計結果") and not contain ("</i> 削除<a>")
			}
		}

		"中止状態から開始状態へ正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_CANCEL, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId.toString),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(form_id1.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/2")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("開始") and contain("プレビュー") and contain("集計結果") and not contain ("</i> 削除<a>")
			}
		}

		"中止状態から終了状態へ正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
				val date_02_10 = dateFormat.parse("2012/02/10")
				val date_03_15 = dateFormat.parse("2012/03/15")

				// 契約者を挿入
				Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
					"千葉 太郎", "a", now) must beTrue

				val urlKey1 = "1"
				val form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名1", "フォーム１です", "結果メッセージ１", 2,
					None, None, None, enc.EncForms.STATUS_CANCEL, urlKey1, now, now)).get

				// フォーム設定挿入
				enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
					Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue

				val contractId = Contracts.findByMail("a@van2k.com").get.contractId.get
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.toString)))

				await(WS.url("http://localhost:3333/conf/updatestatus/" + urlKey1 + "/3")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body must contain("終了") and contain("プレビュー") and contain("集計結果") and not contain ("</i> 削除<a>")
			}
		}
	}

	"アンケート編集-タイトル,内容更新(.updatetitle)のテスト" should {
    running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
      // アンケートを追加する
      val (formId, contractId, urlKey) = addForm1()
      val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

      // タイトルを更新する
      await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("title"),
        "dataDiv" -> Seq("0")))).status must equalTo(OK)

      val formName = enc.EncForms.find(formId).get.formName
      "タイトル更新が正常に完了する" in {
        // タイトルが更新されているかチェックする
        formName must contain("title")
      }

      val errBody = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901"),
        "dataDiv" -> Seq("0")))).body

      val formName2 = enc.EncForms.find(formId).get.formName

      "タイトルが半角100文字を超えているのでエラーとなる" in {
        errBody must contain("アンケートタイトルは100文字以内でご入力ください。")
        formName2 must contain("title")
      }

      val errBody2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１"),
        "dataDiv" -> Seq("0")))).body

      val formName3 = enc.EncForms.find(formId).get.formName

      "タイトルが全角100文字を超えているのでエラーとなる" in {
        errBody2 must contain("アンケートタイトルは100文字以内でご入力ください。")
        formName3 must contain("title")
      }

      val body = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"),
        "dataDiv" -> Seq("0")))).body

      val successName = enc.EncForms.find(formId).get.formName

      "タイトルが半角100文字なので更新できる" in {
        body must contain("アンケートタイトルを登録しました。")
        successName must contain("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
      }

      val body2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０"),
        "dataDiv" -> Seq("0")))).body

      val successName2 = enc.EncForms.find(formId).get.formName

      "タイトルが全角100文字なので更新できる" in {
        body2 must contain("アンケートタイトルを登録しました。")
        successName2 must contain("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０")
      }

    }

    running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
      // アンケートを追加する
      val (formId, contractId, urlKey) = addForm1()
      val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

      // 説明を更新する
      await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("desc"),
        "dataDiv" -> Seq("1")))).status must equalTo(OK)

      // 説明が更新されているかチェックする
      val formDesc = enc.EncForms.find(formId).get.formDesc

      "説明更新が正常に完了する" in {
        formDesc must contain("desc")
      }

      // 説明を更新する
      val errBody = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901"),
        "dataDiv" -> Seq("1")))).body

      val formDesc2 = enc.EncForms.find(formId).get.formDesc

      "説明が半角2500文字を超えているのでエラーとなる" in {
        errBody must contain("アンケートの説明は2500文字以内でご入力ください。")
        formDesc2 must contain("desc")
      }

      // 説明を更新する
      val errBody2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１"),
        "dataDiv" -> Seq("1")))).body

      val formDesc3 = enc.EncForms.find(formId).get.formDesc

      "説明が全角2500文字を超えているのでエラーとなる" in {
        errBody2 must contain("アンケートの説明は2500文字以内でご入力ください。")
        formDesc3 must contain("desc")
      }

      val body = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"),
        "dataDiv" -> Seq("1")))).body

      val successDesc = enc.EncForms.find(formId).get.formDesc

      "説明が半角2500文字なので更新できる" in {
        body must contain("アンケート説明を登録しました。")
        successDesc must contain("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
      }

      val body2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０"),
        "dataDiv" -> Seq("1")))).body

      val successDesc2 = enc.EncForms.find(formId).get.formDesc

      "説明が全角2500文字なので更新できる" in {
        body must contain("アンケート説明を登録しました。")
        successDesc2 must contain("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０")
      }

    }

    running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
      // アンケートを追加する
      val (formId, contractId, urlKey) = addForm1()
      val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

      // 回答後メッセージを更新する
      await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("回答後メッセージ"),
        "dataDiv" -> Seq("2")))).status must equalTo(OK)

      // 回答後メッセージが更新されているかチェックする
      val resultMessage = enc.EncForms.find(formId).get.resultMessage

      "回答後メッセージ更新が正常に完了する" in {
        resultMessage must contain("回答後メッセージ")
      }

      // 回答後メッセージを更新する
      val errBody = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901"),
        "dataDiv" -> Seq("2")))).body

      val resultMessage2 = enc.EncForms.find(formId).get.resultMessage

      "回答後メッセージが半角1000文字を超えているのでエラーとなる" in {
        errBody must contain("アンケート回答後のメッセージは1000文字以内でご入力ください。")
        resultMessage2 must contain("回答後メッセージ")
      }

      // 回答後メッセージを更新する
      val errBody2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１"),
        "dataDiv" -> Seq("2")))).body

      val resultMessage3 = enc.EncForms.find(formId).get.resultMessage

      "回答後メッセージが全角1000文字を超えているのでエラーとなる" in {
        errBody2 must contain("アンケート回答後のメッセージは1000文字以内でご入力ください。")
        resultMessage3 must contain("回答後メッセージ")
      }

      // 回答後メッセージを更新する
      val body = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"),
        "dataDiv" -> Seq("2")))).body

      // 回答後メッセージが更新されているかチェックする
      val successResultMessage = enc.EncForms.find(formId).get.resultMessage

      "回答後メッセージが半角1000文字なので更新できる" in {
        body must contain("アンケート回答後メッセージを登録しました。")
        successResultMessage must contain("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
      }

      // 回答後メッセージを更新する
      val body2 = await(WS.url("http://localhost:3333/conf/updatetitle")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(Map("formId" -> Seq(formId.toString),
        "contractId" -> Seq(contractId),
        "value" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０"),
        "dataDiv" -> Seq("2")))).body

      // 回答後メッセージが更新されているかチェックする
      val successResultMessage2 = enc.EncForms.find(formId).get.resultMessage

      "回答後メッセージが全角1000文字なので更新できる" in {
        body2 must contain("アンケート回答後メッセージを登録しました。")
        successResultMessage2 must contain("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０")
      }

    }
	}

	"アンケート編集-項目更新(.updateformitems)のテスト" should {
		"テキスト項目の追加が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)
				item.name must contain("title")
				item.pageNo mustEqual 1
				item.required must contain("1")
				item.dispodr mustEqual 0
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId)
				values.length mustEqual 1
				values(0).chLength mustEqual 100
				values(0).formValue must contain("dummy")
			}
		}

		"テキスト項目-入力値チェックのテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq(""))

				// 項目を追加する
				data("questionTitle") = Seq("")
				val tbody1 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"タイトルが未入力時のエラーチェック" in {
					tbody1 must contain("質問は必ず入力してください")
				}

				data("questionTitle") = Seq("title")
				data("ansInputMaxLength") = Seq("0")
				// 項目を追加する
				val tbody2 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"入力文字列の長さが0の時のエラーチェック" in {
					tbody2 must contain("入力可能文字数は1以上を入力してください。")
				}

				data("ansInputMaxLength") = Seq("4001")
				val tbody3 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"入力文字列の長さが4000越え時のエラーチェック" in {
					tbody3 must contain("入力可能文字数は4000文字以内で入力してください。")
				}
			}
		}

		"テキスト項目の更新が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				// アンケートの項目を追加する
				val itemId = enc.EncFormItems.regist(enc.EncFormItems(-1, formId, 0, 1, "title", 1, 150, "0","1", 0, now, now), now)
				// アンケートの値を追加する
				enc.EncFormItemValues.regist(enc.EncFormItemValues(-1, itemId.get, "dummy", 100, now, now), now)

				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("101"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("1"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq(itemId.getOrElse(-1).toString),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy2"),
					"questionTitle" -> Seq("title2"),
					"questionType" -> Seq("1"),
					"required" -> Seq("false"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("2013-04-06 19:15:44.995")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)
				item.name must contain("title2")
				item.pageNo mustEqual 1
				item.required must contain("0")
				item.dispodr mustEqual 1
				item.mItemId mustEqual 1
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId)
				values.length mustEqual 1
				values(0).chLength mustEqual 101
				values(0).formValue must contain("dummy2")
			}
		}

		"テキストエリアの追加が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("300"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("2"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy2"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("2"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)
				item.name must contain("title")
				item.pageNo mustEqual 1
				item.required must contain("1")
				item.dispodr mustEqual 0
				item.mItemId mustEqual 2
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId)
				values.length mustEqual 1
				values(0).chLength mustEqual 300
				values(0).formValue must contain("dummy2")
			}
		}

		"テキストエリア-入力値チェックのテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("2"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq(""))

				// 項目を追加する
				data("questionTitle") = Seq("")
				val tbody1 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"タイトルが未入力時のエラーチェック" in {
					tbody1 must contain("質問は必ず入力してください")
				}

				data("questionTitle") = Seq("title")
				data("ansInputMaxLength") = Seq("0")
				// 項目を追加する
				val tbody2 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"入力文字列の長さが0の時のエラーチェック" in {
					tbody2 must contain("入力可能文字数は1以上を入力してください。")
				}

				data("ansInputMaxLength") = Seq("4001")
				val tbody3 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"入力文字列の長さが4000越え時のエラーチェック" in {
					tbody3 must contain("入力可能文字数は4000文字以内で入力してください。")
				}
			}
		}

		"ラジオ項目の追加が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("0"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("3"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("1"),
					"questionItems[1]" -> Seq("2"),
					"questionItems[2]" -> Seq("3"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("3"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)
				item.name must contain("title")
				item.pageNo mustEqual 1
				item.required must contain("1")
				item.dispodr mustEqual 0
				item.mItemId mustEqual 3
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId).sortWith((x ,y) => x.formValue.toInt < y.formValue.toInt)
				values.length mustEqual 3
				values(0).formValue must contain("1")
				values(1).formValue must contain("2")
				values(2).formValue must contain("3")
			}
		}

		"ラジオ項目-入力値チェックのテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("0"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("3"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("1"),
					"questionItems[1]" -> Seq("2"),
					"questionItems[2]" -> Seq("3"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("3"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq(""))

				// 項目を追加する
				data("questionTitle") = Seq("")
				var tbody1 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"タイトルが未入力時のエラーチェック" in {
					tbody1 must contain("質問は必ず入力してください")
				}

				// 項目を追加する
				data("questionTitle") = Seq("Title")
				data("questionItems[0]") = Seq("")
				var tbody2 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目1の未入力チェック" in {
					tbody2 must contain("質問の項目は必ず入力してください。")
				}

				// 項目を追加する
				data("Title") = Seq("Title")
				data("questionItems[0]") = Seq("1")
				data("questionItems[1]") = Seq("")
				var tbody3 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目2の未入力チェック" in {
					tbody3 must contain("質問の項目は必ず入力してください。")
				}

				// 項目を追加する
				data("Title") = Seq("Title")
				data("questionItems[0]") = Seq("1")
				data("questionItems[1]") = Seq("2")
				data("questionItems[2]") = Seq("")
				var tbody4 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目3の未入力チェック" in {
					tbody4 must contain("質問の項目は必ず入力してください。")
				}

        data("questionItems[2]") = Seq("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901")
        var tbody5 = await(WS.url("http://localhost:3333/conf/updateformitems")
          .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
          .post(data.toMap)).body

        "質問項目の文字数が200文字を超えているのでエラーとなる" in {
          tbody5 must contain("200文字以内で入力してください。")
        }

        data("questionItems[2]") = Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
        var tbody6 = await(WS.url("http://localhost:3333/conf/updateformitems")
          .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
          .post(data.toMap)).body

        val itemList = enc.EncFormItems.findByFormId(formId)
        // 項目が登録されているかチェックする
        val item = itemList(0)

        "質問項目の文字数が200文字なので登録出来る" in {
          tbody6 must not contain("200文字以内で入力してください。")
          item.name must contain("Title")
          item.pageNo mustEqual 1
          item.required must contain("1")
          item.dispodr mustEqual 0
          item.mItemId mustEqual 3
        }
			}
		}

		"チェックボックス項目の追加が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("0"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("2"),
					"mFormItemId" -> Seq("4"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("1"),
					"questionItems[1]" -> Seq("2"),
					"questionItems[2]" -> Seq("3"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("4"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("3"),
					"updatedAtFormItems" -> Seq(""))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)
				item.name must contain("title")
				item.pageNo mustEqual 1
				item.required must contain("1")
				item.dispodr mustEqual 0
				item.mItemId mustEqual 4
				item.selectNumber mustEqual 2
				item.condition mustEqual "3"
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId).sortWith((x ,y) => x.formValue.toInt < y.formValue.toInt)
				values.length mustEqual 3
				values(0).formValue must contain("1")
				values(1).formValue must contain("2")
				values(2).formValue must contain("3")

				val tbody1 = await(WS.url("http://localhost:3333/conf/formedit?formId=" + formId)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body

				"選択個数条件が正常に画面に表示されている" in {
					tbody1 must contain("value=\"2\" class=\"q_answer_div_char_length_input\">")
					tbody1 must contain("<option value=\"3\" selected>個以下</option>")
				}
			}
		}

		"チェックボックス項目-入力値チェックのテスト" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("0"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("1"),
					"mFormItemId" -> Seq("4"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("1"),
					"questionItems[1]" -> Seq("2"),
					"questionItems[2]" -> Seq("3"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("4"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("1"),
					"updatedAtFormItems" -> Seq(""))

				// 項目を追加する
				data("questionTitle") = Seq("")
				var tbody1 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"タイトルが未入力時のエラーチェック" in {
					tbody1 must contain("質問は必ず入力してください")
				}

				// 項目を追加する
				data("questionTitle") = Seq("Title")
				data("questionItems[0]") = Seq("")
				var tbody2 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目1の未入力チェック" in {
					tbody2 must contain("質問の項目は必ず入力してください。")
				}

				// 項目を追加する
				data("Title") = Seq("Title")
				data("questionItems[0]") = Seq("1")
				data("questionItems[1]") = Seq("")
				var tbody3 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目2の未入力チェック" in {
					tbody3 must contain("質問の項目は必ず入力してください。")
				}

				// 項目を追加する
				data("Title") = Seq("Title")
				data("questionItems[0]") = Seq("1")
				data("questionItems[1]") = Seq("2")
				data("questionItems[2]") = Seq("")
				var tbody4 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"質問項目3の未入力チェック" in {
					tbody4 must contain("質問の項目は必ず入力してください。")
				}

        data("questionItems[2]") = Seq("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901")
        var tbody5 = await(WS.url("http://localhost:3333/conf/updateformitems")
          .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
          .post(data.toMap)).body

        "質問項目の文字数が200文字を超えているのでエラーとなる" in {
          tbody5 must contain("200文字以内で入力してください。")
        }

        data("questionItems[2]") = Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
        var tbody6 = await(WS.url("http://localhost:3333/conf/updateformitems")
          .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
          .post(data.toMap)).body

        val itemList = enc.EncFormItems.findByFormId(formId)
        // 項目が登録されているかチェックする
        val item = itemList(0)

        "質問項目の文字数が200文字なので登録出来る" in {
          tbody6 must not contain("200文字以内で入力してください。")
          item.name must contain("Title")
          item.pageNo mustEqual 1
          item.required must contain("1")
          item.dispodr mustEqual 0
          item.mItemId mustEqual 4
        }

				// 項目を追加する
				data("Title") = Seq("Title")
				data("questionItems[0]") = Seq("1")
				data("questionItems[1]") = Seq("2")
				data("questionItems[2]") = Seq("3")
				data("itemSelectCount") = Seq("0")
				var tbody7 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"チェック個数0チェック" in {
					tbody7 must contain("チェック個数は1以上項目数未満の値を設定してください。")
				}

				data("itemSelectCount") = Seq("3")
				var tbody8 = await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(data.toMap)).body

				"チェック個数項目数未満チェック" in {
					tbody8 must contain("チェック個数は1以上項目数未満の値を設定してください。")
				}
			}
		}

		"プルダウン項目の追加が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("0"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("5"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("1"),
					"questionItems[1]" -> Seq("2"),
					"questionItems[2]" -> Seq("3"),
					"questionTitle" -> Seq("title"),
					"questionType" -> Seq("5"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("1"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val itemList = enc.EncFormItems.findByFormId(formId)
				val item = itemList(0)

				item.name must contain("title")
				item.pageNo mustEqual 1
				item.required must contain("1")
				item.dispodr mustEqual 0
				item.mItemId mustEqual 5
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item.itemId).sortWith((x ,y) => x.formValue.toInt < y.formValue.toInt)
				values.length mustEqual 3
				values(0).formValue must contain("1")
				values(1).formValue must contain("2")
				values(2).formValue must contain("3")
			}
		}
	}

	"プルダウン項目-入力値チェックのテスト" in {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			val data =  scala.collection.mutable.Map("allPageCount" -> Seq("1"),
				"ansInputMaxLength" -> Seq("0"),
				"contractId" -> Seq(contractId),
				"dispOdr" -> Seq("0"),
				"formId" -> Seq(formId.toString),
				"formItemId" -> Seq("-1"),
				"itemSelectCount" -> Seq("150"),
				"mFormItemId" -> Seq("3"),
				"pageNum" -> Seq("1"),
				"questionItems[0]" -> Seq("1"),
				"questionItems[1]" -> Seq("2"),
				"questionItems[2]" -> Seq("3"),
				"questionTitle" -> Seq("title"),
				"questionType" -> Seq("5"),
				"required" -> Seq("true"),
				"selectNumberCondition" -> Seq("0"),
				"updatedAtFormItems" -> Seq(""))

			// 項目を追加する
			data("questionTitle") = Seq("")
			var tbody1 = await(WS.url("http://localhost:3333/conf/updateformitems")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(data.toMap)).body

			"タイトルが未入力時のエラーチェック" in {
				tbody1 must contain("質問は必ず入力してください")
			}

			// 項目を追加する
			data("questionTitle") = Seq("Title")
			data("questionItems[0]") = Seq("")
			var tbody2 = await(WS.url("http://localhost:3333/conf/updateformitems")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(data.toMap)).body

			"質問項目1の未入力チェック" in {
				tbody2 must contain("質問の項目は必ず入力してください。")
			}

			// 項目を追加する
			data("Title") = Seq("Title")
			data("questionItems[0]") = Seq("1")
			data("questionItems[1]") = Seq("")
			var tbody3 = await(WS.url("http://localhost:3333/conf/updateformitems")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(data.toMap)).body

			"質問項目2の未入力チェック" in {
				tbody3 must contain("質問の項目は必ず入力してください。")
			}

			// 項目を追加する
			data("Title") = Seq("Title")
			data("questionItems[0]") = Seq("1")
			data("questionItems[1]") = Seq("2")
			data("questionItems[2]") = Seq("")
			var tbody4 = await(WS.url("http://localhost:3333/conf/updateformitems")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(data.toMap)).body

			"質問項目3の未入力チェック" in {
				tbody4 must contain("質問の項目は必ず入力してください。")
			}

      data("questionItems[2]") = Seq("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901")
      var tbody5 = await(WS.url("http://localhost:3333/conf/updateformitems")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(data.toMap)).body

      "質問項目の文字数が200文字を超えているのでエラーとなる" in {
        tbody5 must contain("200文字以内で入力してください。")
      }

      data("questionItems[2]") = Seq("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890")
      var tbody6 = await(WS.url("http://localhost:3333/conf/updateformitems")
        .withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
        .post(data.toMap)).body

      val itemList = enc.EncFormItems.findByFormId(formId)
      // 項目が登録されているかチェックする
      val item = itemList(0)

      "質問項目の文字数が200文字なので登録出来る" in {
        tbody6 must not contain("200文字以内で入力してください。")
        item.name must contain("Title")
        item.pageNo mustEqual 1
        item.required must contain("1")
        item.dispodr mustEqual 0
        item.mItemId mustEqual 5
      }
		}
	}

	"アンケート編集-項目削除(.deleteformitems)のテスト" should {
		"項目の削除が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				// アンケートの項目を追加する
				val itemId = enc.EncFormItems.regist(enc.EncFormItems(-1, formId, 0, 1, "title", 1, 150, "0","1", 0, now, now), now)
				// アンケートの値を追加する
				enc.EncFormItemValues.regist(enc.EncFormItemValues(-1, itemId.get, "dummy", 100, now, now), now)

				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/deleteformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("1"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq(itemId.getOrElse(-1).toString),
					"updatedAtFormItems" -> Seq("2013-04-06 19:15:44.995")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val item = enc.EncFormItems.findByFormId(formId)
				item.length mustEqual 0
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(itemId.get)
				values.length mustEqual 0
			}
		}
	}

	"アンケート編集-ページ削除(.deletepage)のテスト" should {
		"ページの削除が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val now = Calendar.getInstance().getTime
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				// アンケートの項目を追加する
				val itemId1 = enc.EncFormItems.regist(enc.EncFormItems(-1, formId, 0, 1, "title1", 1, 150, "0","1", 0, now, now), now)
				val itemId2 = enc.EncFormItems.regist(enc.EncFormItems(-1, formId, 0, 1, "title2", 2, 150, "0","1", 0, now, now), now)
				// アンケートの値を追加する
				enc.EncFormItemValues.regist(enc.EncFormItemValues(-1, itemId1.get, "dummy1", 100, now, now), now)
				enc.EncFormItemValues.regist(enc.EncFormItemValues(-1, itemId2.get, "dummy2", 100, now, now), now)

				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/deletepage")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("2"),
					"contractId" -> Seq(contractId),
					"formId" -> Seq(formId.toString),
					"pageNum" -> Seq("2")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val item = enc.EncFormItems.findByFormId(formId)
				item.length mustEqual 1
				item(0).pageNo mustEqual 1
				// 項目の値が登録されているかチェックする
				val values = enc.EncFormItemValues.findByFormItemId(item(0).itemId)
				values.length mustEqual 1
			}
		}
	}

	"プレビュー(.preview)のテスト" should {
		"表示が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// テキスト項目を追加する
				addSingleItem(sessionCookie, formId = Seq(formId.toString), contractId = Seq(contractId), questionType = Seq("1"), required = Seq("true"), title= Seq("Name?"))

				// 開始して状態で、プレビューを表示する
				val tbody = await(WS.url("http://localhost:3333/encpreview/" + urlKey)
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body
				tbody must contain("フォーム名1") and contain("Name?")
			}
		}
		"存在しないアンケート" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> "1")))

				// 開始して状態で、プレビューを表示する
				val tbody = await(WS.url("http://localhost:3333/encpreview/aaaaa")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.get).body
				tbody must contain("アンケートがありません") and contain("エラーが発生しました")
			}
		}
	}

	"表示順の更新(.updatedispodr)のテスト" should {
		"表示順の更新が正常に完了する" in {
			running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
				// アンケートを追加する
				val (formId, contractId, urlKey) = addForm1()
				val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("0"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title1"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updateformitems")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("allPageCount" -> Seq("1"),
					"ansInputMaxLength" -> Seq("100"),
					"contractId" -> Seq(contractId),
					"dispOdr" -> Seq("1"),
					"formId" -> Seq(formId.toString),
					"formItemId" -> Seq("-1"),
					"itemSelectCount" -> Seq("150"),
					"mFormItemId" -> Seq("1"),
					"pageNum" -> Seq("1"),
					"questionItems[0]" -> Seq("dummy"),
					"questionTitle" -> Seq("title2"),
					"questionType" -> Seq("1"),
					"required" -> Seq("true"),
					"selectNumberCondition" -> Seq("0"),
					"updatedAtFormItems" -> Seq("")))).status must equalTo(OK)

				// 項目が登録されているかチェックする
				val items = enc.EncFormItems.findByFormId(formId)
				val dispOdrs = List(1, 0)
				var json:String = "["
				items.length mustEqual 2
				for(item <- items){
					json = json.union("{\"formid\":\"" + item.formId + "\",")
					json = json.union("\"pageno\":\"" + item.pageNo + "\",")
					json = json.union("\"itemid\":\"" + item.itemId + "\",")
					json = json.union("\"dispodr\":\"" + dispOdrs(items.indexOf(item)) + "\",")
					json = json.union("\"status\":\"1\",")
					json = json.union("\"name\":\"" + item.name + "\"}")
					if(items.last != item){
						json = json.union(",")
					}
				}
				json =  json.union("]")

				// 項目を追加する
				await(WS.url("http://localhost:3333/conf/updatedispodr")
					.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
					.post(Map("values" -> Seq(json)))).status must equalTo(OK)

				// 表示順が変更されているか確認する
				val items2 = enc.EncFormItems.findByFormId(formId)
				items2(0).itemId mustEqual items(1).itemId
				items2(0).dispodr mustEqual 0
				items2(1).itemId mustEqual items(0).itemId
				items2(1).dispodr mustEqual 1
			}
		}
	}

	"解約確認（パスワード入力）画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			val body = await(WS.url("http://localhost:3333/conf/cancelConfirm")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.get).body

			"解約確認（パスワード入力）画面が表示出来る" in {
				body must contain("<h3>解約確認</h3>")
				body must contain("<form action=\"/conf/cancel\" method=\"POST\">")
			}
			val body2 = await(WS.url("http://localhost:3333/conf/cancelConfirm")
				.get).body
			"未ログイン状態で画面を表示するとエラーとなる" in {
				body2 must contain("ログインしてください")
			}
		}
	}

	"解約確認（同意チェック）画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val pass = "risachiPass"
			Contracts.setPass(contractId.toLong, pass)
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			val body = await(WS.url("http://localhost:3333/conf/cancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("password" -> Seq(pass)))).body

			"解約確認（パスワード入力）画面が表示出来る" in {
				body must contain("<h3>解約手続き</h3>")
				body must contain("<form action=\"/conf/doCancel\" id=\"cancel_form\" method=\"POST\">")
			}

			val body2 = await(WS.url("http://localhost:3333/conf/cancel")
				.post(Map("password" -> Seq(pass)))).body
			"未ログイン状態で画面を表示するとエラーとなる" in {
				body2 must contain("ログインしてください")
			}

			val body3 = await(WS.url("http://localhost:3333/conf/cancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("password" -> Seq("hogehogehoge")))).body
			"パスワードの入力に誤りがある為エラーとなる" in {
				body3 must contain("パスワードが正しくありません")
			}
		}
	}

	"解約手続き完了画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val pass = "risachiPass"
			Contracts.setPass(contractId.toLong, pass)
			val token = Contracts.md5(Constants.CSRF_SALT + Calendar.getInstance.getTime.toString)
			val successCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId, "token" -> token)))
			val loginOnlyCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))
			val tokenOnlyCookie = Session.encodeAsCookie(Session(Map("token" -> token)))

			val body = await(WS.url("http://localhost:3333/conf/doCancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(tokenOnlyCookie)))
				.post(Map("cancel_check" -> Seq("1"),
				"token" -> Seq(token),
				"is_cancel" -> Seq("0")
			))).body

			"未ログイン状態で画面を表示するとエラーとなる" in {
				body must contain("ログインしてください")
			}

			val body2 = await(WS.url("http://localhost:3333/conf/doCancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(successCookie)))
				.post(Map("token" -> Seq(token),
				"is_cancel" -> Seq("0")
			))).body

			"「解約により情報が削除されることに同意する」が未チェックの為エラーとなる" in {
				body2 must contain("解約するには「解約により情報が削除されることに同意する」のチェックが必要です")
			}

			val body3 = await(WS.url("http://localhost:3333/conf/doCancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(loginOnlyCookie)))
				.post(Map("cancel_check" -> Seq("1"),
				"token" -> Seq(token),
				"is_cancel" -> Seq("0")
			))).body

			"契約確認（同意チェック）画面からの遷移でない場合はエラーとなる（CSRF対策のテスト）" in {
				body3 must contain("パラメータが不正です")
			}

			val body4 = await(WS.url("http://localhost:3333/conf/doCancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(successCookie)))
				.post(Map("cancel_check" -> Seq("1"),
				"token" -> Seq(token),
				"is_cancel" -> Seq("1")
			))).body

			"キャンセルボタン押下の為アンケートフォーム一覧画面が表示される" in {
				body4 must contain("フォーム名1")
			}

			val body5 = await(WS.url("http://localhost:3333/conf/doCancel")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(successCookie)))
				.post(Map("cancel_check" -> Seq("1"),
				"token" -> Seq(token),
				"is_cancel" -> Seq("0")
			))).body

			"解約手続き完了画面が表示出来る" in {
				body5 must contain("解約手続きが完了しました。")
			}
		}
	}

	"パスワード変更画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val pass = "risachiPass"
			Contracts.setPass(contractId.toLong, pass)
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			val testBody = await(WS.url("http://localhost:3333/conf/passwd")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.get).body

			"パスワード変更画面が表示出来る" in {
				testBody must contain("<h3>パスワード変更</h3>")
				testBody must contain("<form action=\"/conf/passwdSubmit\"")
			}

			val testBody2 = await(WS.url("http://localhost:3333/conf/passwd")
				.get).body

			"未ログイン状態でパスワード変更画面を表示するとログインエラーとなる" in {
				testBody2 must contain("ログインしてください")
			}
		}
	}

	"パスワード変更完了画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// アンケートを追加する
			val (formId, contractId, urlKey) = addForm1()
			val pass = "risachiPass"
			val newPass = "risachiNewPass"
			Contracts.setPass(contractId.toLong, pass)
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId)))

			val testBody = await(WS.url("http://localhost:3333/conf/passwdSubmit")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("oldpass" -> Seq("risachiPass2"),
				"pass.main" -> Seq("risachiPass2"),
				"pass.confirm" -> Seq("risachiPass2")))).body

			"現在のパスワードが一致しない為エラーとなる" in {
				testBody must contain("現在のパスワードが一致しません。")
			}

			val testBody2 = await(WS.url("http://localhost:3333/conf/passwdSubmit")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("oldpass" -> Seq(pass),
				"pass.main" -> Seq("risachiPass2"),
				"pass.confirm" -> Seq("wrong pass")))).body

			"「新しいパスワード」と「新しいパスワード（確認）」が一致しない為エラーとなる" in {
				testBody2 must contain("<dd class=\"error\">パスワードが一致しません。</dd>")
			}

			val testBody3 = await(WS.url("http://localhost:3333/conf/passwdSubmit")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("oldpass" -> Seq(pass),
				"pass.main" -> Seq("pass"),
				"pass.confirm" -> Seq("pass")))).body

			"新しいパスワードが6文字未満の為エラーとなる" in {
				testBody3 must contain("<dd class=\"error\">6 文字以上でご入力ください</dd>")
			}

			val testBody4 = await(WS.url("http://localhost:3333/conf/passwdSubmit")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("oldpass" -> Seq(pass),
				"pass.main" -> Seq(newPass),
				"pass.confirm" -> Seq(newPass)))).body

			"パスワード変更完了画面が表示出来る" in {
				testBody4 must contain("パスワードを変更しました。")
			}

			val testBody5 = await(WS.url("http://localhost:3333/login")
				.post(Map("userid" -> Seq("a@van2k.com"), "password" -> Seq(newPass)))).body

			"パスワード変更後、変更後のパスワードでログイン出来る" in {
				testBody5 must not contain("ID、又は、パスワードが異なります")
			}

		}
	}
}
