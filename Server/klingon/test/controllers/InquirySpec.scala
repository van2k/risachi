package controllers

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test._
import java.util.Calendar
import models.contract.Contracts
import models.contract
import play.api.mvc.{Cookies, Session}
import commons.Constants
import play.api.libs.ws.WS
import scala._
import scalikejdbc._
import scala.Some

class InquirySpec extends PlaySpecification {
	import controllers._

	"問い合わせ画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			val now = Calendar.getInstance().getTime

			// 契約者を挿入
			Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
				"テスト太郎", "a", now) must beTrue

			val contractId = Contracts.findByMail("a@van2k.com").get.contractId
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.get.toString)))

			val noLoginBody = await(WS.url("http://localhost:3333/inquiry")
				.get).body

			"未ログイン状態の場合はメールアドレス、名前がブランクで問い合わせ画面が表示される" in {
				noLoginBody must contain("<h1>アンケートフォームサービス お問い合わせ</h1>")
				noLoginBody must contain("<input type=\"text\" id=\"mail\" name=\"mail\" value=\"\"")
				noLoginBody must contain("<input type=\"text\" id=\"name\" name=\"name\" value=\"\"")
			}

			val loginBody = await(WS.url("http://localhost:3333/inquiry")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.get).body

			"ログイン状態の場合はメールアドレス、名前に契約者の情報が入力された状態で問い合わせ画面が表示される" in {
				loginBody must contain("<h1>アンケートフォームサービス お問い合わせ</h1>")
				loginBody must contain("<input type=\"text\" id=\"mail\" name=\"mail\" value=\"a@van2k.com\"")
				loginBody must contain("<input type=\"text\" id=\"name\" name=\"name\" value=\"テスト太郎\"")
			}
		}
	}

	"問い合わせ確認画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			val body = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"問い合わせ確認画面が表示できる" in {
				body must contain("<form action=\"/inquiry/submit\"")
			}

			val errTest1 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"件名がブランクの為エラーとなる" in {
				errTest1 must contain("<span class=\"errors text-error\">この項目は必須です</span>")
			}

			val errTest2 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"メールアドレスがブランクの為エラーとなる" in {
				errTest2 must contain("<span class=\"errors text-error\">この項目は必須です</span>")
			}

			val errTest3 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"名前がブランクの為エラーとなる" in {
				errTest3 must contain("<span class=\"errors text-error\">この項目は必須です</span>")
			}

			val errTest4 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー")))).body

			"問い合わせ内容がブランクの為エラーとなる" in {
				errTest4 must contain("<span class=\"errors text-error\">この項目は必須です</span>")
			}

			val errTest5 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"件名が半角129文字以上なのでエラーとなる" in {
				errTest5 must contain("<span class=\"errors text-error\">128 文字以下でご入力ください</span>")
			}

			val errTest6 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"件名が全角129文字以上なのでエラーとなる" in {
				errTest6 must contain("<span class=\"errors text-error\">128 文字以下でご入力ください</span>")
			}

			val errTest7 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("testvan2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

//			val errTest8 = await(WS.url("http://localhost:3333/inquiry/confirm")
//				.post(Map("subject" -> Seq("お問い合わせ件名"),
//				"mail" -> Seq("test@van2k"),
//				"name" -> Seq("ふなっしー"),
//				"content" -> Seq("船橋の名産は何ですか？")))).body

			"メールアドレスのフォーマットが不正なのでエラーとなる" in {
				errTest7 must contain("<span class=\"errors text-error\">メールアドレスが正しくありません</span>")
//				errTest8 must contain("<span class=\"errors text-error\">メールアドレスが正しくありません</span>")
			}

			val errTest9 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("12345678901234567890123456789012345678901234567890123456789012345"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"名前が半角65文字以上なのでエラーとなる" in {
				errTest9 must contain("<span class=\"errors text-error\">64 文字以下でご入力ください</span>")
			}

			val errTest10 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５"),
				"content" -> Seq("船橋の名産は何ですか？")))).body

			"名前が全角65文字以上なのでエラーとなる" in {
				errTest10 must contain("<span class=\"errors text-error\">64 文字以下でご入力ください</span>")
			}

			val errTest11 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901")))).body


			"問い合わせ内容が半角2001文字以上なのでエラーとなる" in {
				errTest11 must contain("<span class=\"errors text-error\">2,000 文字以下でご入力ください</span>")
			}

			val errTest12 = await(WS.url("http://localhost:3333/inquiry/confirm")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１")))).body

			"問い合わせ内容が全角2001文字以上なのでエラーとなる" in {
				errTest12 must contain("<span class=\"errors text-error\">2,000 文字以下でご入力ください</span>")
			}
		}
	}

	"問い合わせ完了画面のテスト" should {
		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {

			val testBody = await(WS.url("http://localhost:3333/inquiry/submit")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？"),
				"back" -> Seq("戻る")))).body

			"問い合わせ確認画面で「戻る」が押下されたので、問い合わせ画面が表示される" in {
				testBody must contain("<form action=\"/inquiry/confirm\"")
			}

			val testBody2 = await(WS.url("http://localhost:3333/inquiry/submit")
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？"),
				"send" -> Seq("送信")))).body

			"問い合わせ確認画面で「送信」が押下されたので、問い合わせ完了画面が表示される" in {
				testBody2 must contain("問い合わせを受け付けました。")
			}

			val searchContext = controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, false)
			val inquirys = models.Inquiry.search(searchContext)

			"未ログイン状態で問い合わせ完了時に問い合わせ内容がDBに保存されている" in {
				inquirys.isEmpty must beFalse
				inquirys.head.mail must beEqualTo("test@van2k.com")
				inquirys.head.contractId must beNone
			}
		}

		running(TestServer(3333, FakeApplication(additionalConfiguration = inMemoryDatabase()))) {
			// 契約者を挿入
			Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
				"テスト太郎", "a", Calendar.getInstance().getTime) must beTrue

			val contractId = Contracts.findByMail("a@van2k.com").get.contractId
			val sessionCookie = Session.encodeAsCookie(Session(Map(Constants.SESSION_CONTRACT_ID -> contractId.get.toString)))

			await(WS.url("http://localhost:3333/inquiry/submit")
				.withHeaders(play.api.http.HeaderNames.COOKIE -> Cookies.encode(Seq(sessionCookie)))
				.post(Map("subject" -> Seq("お問い合わせ件名"),
				"mail" -> Seq("test@van2k.com"),
				"name" -> Seq("ふなっしー"),
				"content" -> Seq("船橋の名産は何ですか？"),
				"send" -> Seq("送信")))).body


			val searchContext = controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, false)
			val inquirys = models.Inquiry.search(searchContext)

			"ログイン状態で問い合わせ完了時に問い合わせ内容がDBに保存されている" in {
				inquirys.isEmpty must beFalse
				inquirys.head.mail must beEqualTo("test@van2k.com")
				inquirys.head.contractId must beEqualTo(Some(1))
			}
		}
	}
}
