package services

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.Calendar
import java.text.SimpleDateFormat

class InquirySpec extends Specification {

  // テストの並行実行を無効にする
  args(sequential=true)

  "sendInquiry" should {
    "問い合わせを追加できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // 問い合わせデータを追加する。
        services.Contract.sendInquiry("件名１", "sato@gmail.com", "佐藤 一郎", "本文１", models.Inquiry.InquiryStatus.UNSUPPORTED, 1, true) must beTrue
        services.Contract.sendInquiry("件名２", "yamaguchi@gmail.com", "山口 一郎", "本文２", models.Inquiry.InquiryStatus.UNSUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名３", "fukai@gmail.com", "深井 一郎", "本文３", models.Inquiry.InquiryStatus.SUPPORTED, 2, true) must beTrue
        services.Contract.sendInquiry("件名４", "kushino@gmail.com", "櫛野 一郎", "本文４", models.Inquiry.InquiryStatus.SUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名５", "yazawa@van2k.com", "谷澤 一郎", "本文５", models.Inquiry.InquiryStatus.SUPPORTED, 3, true) must beTrue

        // 問い合わせデータの件数をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤 一郎", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "山口 一郎", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井 一郎", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "櫛野 一郎", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "谷澤 一郎", "", false, false, false, false)) must have size 1
      }
    }
  }

  "searchInquiry" should {
    "問い合わせを検索できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // 問い合わせデータを追加する。
        val d1 = Calendar.getInstance().getTime()
        services.Contract.sendInquiry("件名１", "sato@gmail.com", "佐藤 一郎", "本文１", models.Inquiry.InquiryStatus.UNSUPPORTED, 1, true) must beTrue
        services.Contract.sendInquiry("件名２", "yamaguchi@gmail.com", "山口 一郎", "本文２", models.Inquiry.InquiryStatus.UNSUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名３", "fukai@gmail.com", "深井 一郎", "本文３", models.Inquiry.InquiryStatus.SUPPORTED, 2, true) must beTrue
        services.Contract.sendInquiry("件名４", "kushino@gmail.com", "櫛野 一郎", "本文４", models.Inquiry.InquiryStatus.SUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名５", "yazawa@van2k.com", "谷澤 一郎", "本文５", models.Inquiry.InquiryStatus.SUPPORTED, 3, true) must beTrue
        val d2 = Calendar.getInstance().getTime()

        // 問い合わせ日付での検索をチェックする。
        val formatter = new SimpleDateFormat("yyyy/MM/dd")
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext(formatter.format(d1), formatter.format(d2), "", "", false, false, false, false)) must have size 5

        // テスト用に問い合わせ日時を変更する。
        var inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        models.Inquiry.changeSendDate(inquiryList.head.id, "2013/01/28")
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "山口 一郎", "", false, false, false, false))
        inquiryList must have size 1
        models.Inquiry.changeSendDate(inquiryList.head.id, "2013/02/03 09:36:21")
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井 一郎", "", false, false, false, false))
        inquiryList must have size 1
        models.Inquiry.changeSendDate(inquiryList.head.id, "2013/02/03 21:03:52")
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "櫛野 一郎", "", false, false, false, false))
        inquiryList must have size 1
        models.Inquiry.changeSendDate(inquiryList.head.id, "2013/02/05 12:28:23")
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "谷澤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        models.Inquiry.changeSendDate(inquiryList.head.id, "2013/02/06 23:59:59")

        // 問い合わせ日付での検索をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "2013/02/06", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/03", "2013/02/03", "", "", false, false, false, false)) must have size 2
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "2013/01/28", "", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/01", "2013/01/28", "", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/06", "", "", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/06", "2013/12/31", "", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/03", "", "", "", false, false, false, false)) must have size 4
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "2013/02/05", "", "", false, false, false, false)) must have size 4
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/03", "2013/02/05", "", "", false, false, false, false)) must have size 3
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/01", "", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "2013/12/31", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/01", "2013/12/31", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "2013/01/27", "", "", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/01", "2013/01/27", "", "", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/07", "", "", "", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/02/07", "2013/12/31", "", "", false, false, false, false)) must have size 0

        // 名前での検索（部分一致）をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "藤 一郎", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "藤", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "谷澤 ", "", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐山", "", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "一人", "", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "男", "", false, false, false, false)) must have size 0

        // メールアドレスでの検索（部分一致）をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "yamaguchi@gmail.com", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "yamaguchi", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "gmail", false, false, false, false)) must have size 4
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "com", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "abe", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "iij4u", false, false, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "jp", false, false, false, false)) must have size 0

        // 会員であるか否かでの検索をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", true, false, false, false)) must have size 3
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, true, false, false)) must have size 2
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", true, true, false, false)) must have size 5

        // 対応状況での検索をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, false)) must have size 5
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, true, false)) must have size 2
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, false, true)) must have size 3
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "", "", false, false, true, true)) must have size 5

        // 複合条件での検索をチェックする。
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "佐藤", "gmail", false, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "佐藤", "gmail", true, false, false, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "佐藤", "gmail", false, true, false, false)) must have size 0
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "佐藤", "gmail", true, false, true, false)) must have size 1
        services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("2013/01/28", "2013/02/06", "佐藤", "gmail", false, true, false, true)) must have size 0
      }
    }
  }

  "changeInquiry" should {
    "問い合わせの対応状況を更新できる" in {
      running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

        // 問い合わせデータを追加する。
        services.Contract.sendInquiry("件名１", "sato@gmail.com", "佐藤 一郎", "本文１", models.Inquiry.InquiryStatus.UNSUPPORTED, 1, true) must beTrue
        services.Contract.sendInquiry("件名２", "yamaguchi@gmail.com", "山口 一郎", "本文２", models.Inquiry.InquiryStatus.UNSUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名３", "fukai@gmail.com", "深井 一郎", "本文３", models.Inquiry.InquiryStatus.SUPPORTED, 2, true) must beTrue
        services.Contract.sendInquiry("件名４", "kushino@gmail.com", "櫛野 一郎", "本文４", models.Inquiry.InquiryStatus.SUPPORTED, 0, false) must beTrue
        services.Contract.sendInquiry("件名５", "yazawa@van2k.com", "谷澤 一郎", "本文５", models.Inquiry.InquiryStatus.SUPPORTED, 3, true) must beTrue

        // 佐藤勇人さんの対応状況を未対応から対応済みに変更する。
        var inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        services.Admin.changeInquiry(inquiryList.head.id, models.Inquiry.InquiryStatus.SUPPORTED)

        // 佐藤勇人さんの対応状況が対応済みになっているか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 山口智さんの対応状況は未対応のままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "山口 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.UNSUPPORTED

        // 深井正樹さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 櫛野亮さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "櫛野 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 谷澤達也さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "谷澤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 深井正樹さんの対応状況を対応済みから未対応に変更する。
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井 一郎", "", false, false, false, false))
        inquiryList must have size 1
        services.Admin.changeInquiry(inquiryList.head.id, models.Inquiry.InquiryStatus.UNSUPPORTED)

        // 深井正樹さんの対応状況が未対応になっているか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "深井 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.UNSUPPORTED

        // 佐藤勇人さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "佐藤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 山口智さんの対応状況は未対応のままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "山口 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.UNSUPPORTED

        // 櫛野亮さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "櫛野 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED

        // 谷澤達也さんの対応状況は対応済みのままか
        inquiryList = services.Admin.searchInquiry(controllers.Admin.SearchInquiryContext("", "", "谷澤 一郎", "", false, false, false, false))
        inquiryList must have size 1
        inquiryList.head.status === models.Inquiry.InquiryStatus.SUPPORTED
      }
    }
  }
}
