package services

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import models.contract.Contracts
import java.util.Calendar
import models.enc.EncForms
import scalikejdbc.DB

class ContractSpec extends Specification {
  "createRegistToken" should {
    "64文字の文字列を返す" in {
      Contract.createRegistToken().length === 64
    }
  }

  "Regist" should {
    "契約者を登録できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 仮登録状態を作る
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, 1)
      val token_limit = cal.getTime
      Contracts.tempRegist("aaa@aaa.aa", "abcdefg", token_limit) must beTrue

      // 登録
      Contract.Regist("abcdefg", "番付太郎") must beNone

      // 登録状態になっているか？
      Contracts.findByMail("aaa@aaa.aa").get.status === Contracts.STATUS_REGISTERED
    }

    "無効なトークンでは登録できない" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 仮登録状態を作る
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, -1)
      val token_limit = cal.getTime
      Contracts.tempRegist("aaa@aaa.aa", "abcdefg", token_limit) must beTrue

      // 登録
      Contract.Regist("abcdefg", "番付太郎") must beSome

      // 登録状態になっているか？
      Contracts.findByMail("aaa@aaa.aa").get.status === Contracts.STATUS_INTERIM
    }
  }

  "changePassword(token: String)" should {
    "パスワードを変更できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 登録状態の契約者作成
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, 1)
      val token_limit = cal.getTime
      Contracts.directInsert("aaa@aaa.aa", Contracts.STATUS_REGISTERED, "番付太郎", "abcdefg", token_limit)

      // パスワード変更
      val pass = Contract.changePassword("abcdefg")
      pass must not be null
      // 新しいパスワードで契約者を取れるか
      Contracts.findByMailAndPass("aaa@aaa.aa", pass) must beSome
    }

    "無効なトークンでは変更できない" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 登録状態の契約者作成
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, -1)
      val token_limit = cal.getTime
      Contracts.directInsert("aaa@aaa.aa", Contracts.STATUS_REGISTERED, "番付太郎", "abcdefg", token_limit)

      // パスワード変更
      val pass = Contract.changePassword("abcdefg")
      pass must beNull
    }
  }

  "withdrawContract" should {
    "退会すると退会状態になり、その契約者に紐づくフォームがすべて削除状態になる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 登録状態の契約者作成
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, -1)
      val token_limit = cal.getTime
      Contracts.directInsert("aaa@aaa.aa", Contracts.STATUS_REGISTERED, "番付太郎", "abcdefg", token_limit)

      // 契約者ID取得
      val contractId = Contracts.findByMail("aaa@aaa.aa").get.contractId.get

      // フォームを挿入
      val now = Calendar.getInstance().getTime
      val formid1 = EncForms.regist(EncForms(1, contractId, "formName", "formDesc", "resultMessage", 1, None, None, None,
        EncForms.STATUS_RUN, "1", now, now)).get
      val formid2 = EncForms.regist(EncForms(2, contractId, "formName", "formDesc", "resultMessage", 1, None, None, None,
        EncForms.STATUS_RUN, "2", now, now)).get
      // 他人のフォームを挿入
      val formid3 = EncForms.regist(EncForms(3, contractId + 1, "formName", "formDesc", "resultMessage", 1, None, None, None,
        EncForms.STATUS_RUN, "3", now, now)).get

      // 退会する
      Contract.withdrawContract(contractId)

      // 退会状態になっているか
      Contracts.findById(contractId).get.status === Contracts.STATUS_WITHDRAWN

      // フォームが削除状態になっているか
      EncForms.find(formid1).get.status === EncForms.STATUS_DELETE
      EncForms.find(formid2).get.status === EncForms.STATUS_DELETE
      // 他人のフォームには影響しない
      EncForms.find(formid3).get.status === EncForms.STATUS_RUN
    }
  }

  "forgotPassChangePass" should {
    "パスワードを変更できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 登録状態の契約者作成
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, 1)
      val token_limit = cal.getTime
      Contracts.directInsert("aaa@aaa.aa", Contracts.STATUS_REGISTERED, "番付太郎", "abcdefg", token_limit)

      // パスワード変更
      Contract.forgotPassChangePass("abcdefg", "newpasswd") must beTrue

      // 新しいパスワードで検索できる
      Contracts.findByMailAndPass("aaa@aaa.aa", "newpasswd") must beSome
    }

    "無効なトークンでは変更できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 登録状態の契約者作成
      val cal = Calendar.getInstance()
      cal.add(Calendar.HOUR, -1)
      val token_limit = cal.getTime
      Contracts.directInsert("aaa@aaa.aa", Contracts.STATUS_REGISTERED, "番付太郎", "abcdefg", token_limit)

      // パスワード変更
      Contract.forgotPassChangePass("abcdefg", "newpasswd") must beFalse

      // 新しいパスワードで検索できる
      Contracts.findByMailAndPass("aaa@aaa.aa", "newpasswd") must beNone

      // パスワード変更
      Contract.forgotPassChangePass("abcdefgh", "newpasswd") must beFalse

      // 新しいパスワードで検索できる
      Contracts.findByMailAndPass("aaa@aaa.aa", "newpasswd") must beNone
    }
  }
}
