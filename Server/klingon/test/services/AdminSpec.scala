package services

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication
import models.{Administrator, enc, contract, charge}
import java.util.Calendar
import java.text.SimpleDateFormat
import models.contract.Contracts

class AdminSpec extends Specification {
  val now = Calendar.getInstance().getTime
  val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
  val date_02_10 = dateFormat.parse("2012/02/10")
  val date_03_15 = dateFormat.parse("2012/03/15")
  val date_04_20 = dateFormat.parse("2012/04/20")
  val date_05_25 = dateFormat.parse("2012/05/25")
  var form_id1 = -1L
  var form_id2 = -1L
  var form_id3 = -1L
  var form_id4 = -1L

  // フォームに関係するテストをするときの共通処理
  def testForms[T](block : => T) = running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
    // 契約者を２件挿入
    Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
      "石川五右衛門", "abc", now) must beTrue
    Contracts.directInsert("b@van2k.com", contract.Contracts.STATUS_REGISTERED,
      "佐々木小次郎", "def", now) must beTrue
    // フォーム挿入
    form_id1 = enc.EncForms.regist(enc.EncForms(1, 1, "フォーム名１", "フォーム１です", "結果メッセージ１", 2,
      None, None, None, enc.EncForms.STATUS_RUN, "1", now, now)).get
    form_id2 = enc.EncForms.regist(enc.EncForms(2, 1, "フォーム名2", "フォーム2です", "結果メッセージ2", 2,
      None, None, None, enc.EncForms.STATUS_EDIT, "2", now, now)).get
    form_id3 = enc.EncForms.regist(enc.EncForms(3, 2, "フォーム名3", "フォーム3です", "結果メッセージ3", 2,
      None, None, None, enc.EncForms.STATUS_CANCEL, "3", now, now)).get
    form_id4 = enc.EncForms.regist(enc.EncForms(4, 2, "フォーム名4", "フォーム4です", "結果メッセージ4", 2,
      None, None, None, enc.EncForms.STATUS_RUN, "4", now, now)).get
    // フォーム設定挿入
    enc.EncFormConfigs.regist(enc.EncFormConfigs(1, form_id1, 10, date_02_10, Option(date_03_15), "0", 2, 10, 20,
      Option(5), Option(10000), Option(1000), Option(now), Option(1), "0", now, now)) must beTrue
    enc.EncFormConfigs.regist(enc.EncFormConfigs(2, form_id2, 10, date_03_15, Option(date_04_20), "0", 2, 10, 20,
      Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue
    enc.EncFormConfigs.regist(enc.EncFormConfigs(3, form_id3, 10, date_02_10, Option(date_04_20), "0", 2, 10, 20,
      Option(5), Option(10000), None, None, Option(3), "0", now, now)) must beTrue
    enc.EncFormConfigs.regist(enc.EncFormConfigs(4, form_id4, 10, date_04_20, Option(date_05_25), "0", 2, 10, 20,
      Option(5), Option(10000), None, None, Option(2), "0", now, now)) must beTrue

    block
  }

  "login" should {
    "管理者でログインできる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      Admin.login("administrator", "van2kvan2k") must beSome
    }

    "idが違うとログインできない" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      Admin.login("administratorr", "van2kvan2k") must beNone
    }

    "パスワードが違うとログインできない" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      Admin.login("administrator", "van1kvan1k") must beNone
    }
  }

  "searchEnc" should {
    "フォーム検索結果をSearchEncRowのリストとして返す" in testForms {
      // 検索
      val result = Admin.searchEnc(Admin.SearchEncContext("石川", None, None, None, None,
        false, false, false, false), List.empty[String])
      result.length === 2
      (result(0).form_id == form_id1 && result(1).form_id == form_id2 ||
        result(0).form_id == form_id2 && result(1).form_id == form_id1) must beTrue
      result(0).contract_name === "石川五右衛門"
      result(1).contract_name === "石川五右衛門"
    }
  }

  "changeEncStatus" should {
    "フォームの状態を変更できる" in testForms {
      // 状態変更
      Admin.changeEncStatus(form_id1, enc.EncForms.STATUS_CANCEL) must beTrue

      // 状態変更されているか
      enc.EncForms.find(form_id1).get.status === enc.EncForms.STATUS_CANCEL
      // 他のフォームに影響がないか
      enc.EncForms.find(form_id2).get.status === enc.EncForms.STATUS_EDIT
      enc.EncForms.find(form_id3).get.status === enc.EncForms.STATUS_CANCEL
      enc.EncForms.find(form_id4).get.status === enc.EncForms.STATUS_RUN
    }

    "フォームIDが間違っていると変更できない" in testForms {
      // 状態変更
      Admin.changeEncStatus(-1, enc.EncForms.STATUS_CANCEL) must beFalse

      // 他のフォームに影響がないか
      enc.EncForms.find(form_id1).get.status === enc.EncForms.STATUS_RUN
      enc.EncForms.find(form_id2).get.status === enc.EncForms.STATUS_EDIT
      enc.EncForms.find(form_id3).get.status === enc.EncForms.STATUS_CANCEL
      enc.EncForms.find(form_id4).get.status === enc.EncForms.STATUS_RUN
    }
  }

  "registReceipt" should {
    "入金登録できる" in testForms {
      // 入金登録
      Admin.registReceipt(form_id2, 2000) must beTrue

      // 料金を登録されているか
      enc.EncFormConfigs.findByFormid(form_id2).get.paymentConfirm.get === 2000

      // 他のフォームに影響がないか
      enc.EncFormConfigs.findByFormid(form_id1).get.paymentConfirm.get === 1000
    }

    "フォームIDが間違っていると登録できない" in testForms {
      // 入金登録
      Admin.registReceipt(-1, 2000) must beFalse

      // 他のフォームに影響がないか
      enc.EncFormConfigs.findByFormid(form_id1).get.paymentConfirm.get === 1000
    }
  }

  "paymentUpdate" should {
    "入金確認済みにできる" in testForms {
      // 登録
      Admin.paymentUpdate(form_id3)

      // 登録できているか
      enc.EncFormConfigs.findByFormid(form_id3).get.paymentConfirmDate must beSome

      // 他のフォームに影響がないか
      enc.EncFormConfigs.findByFormid(form_id2).get.paymentConfirmDate must beNone
    }
  }

  "getFormChargeOptionIdString" should {
    "フォームに紐づいたオプションプランのID一覧をカンマ区切りの文字列として取り出す" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // オプションプランをフォームに登録する
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(1, 1, 10))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(2, 1, 11))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(3, 1, 12))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(4, 2, 13))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(5, 2, 14))

      // フォーム1に紐づいているオプションプランを取り出す
      Admin.getFormChargeOptionIdString(1) === "10,11,12"
    }
  }

  "setFormChargeOptionByIdString" should {
    "カンマ区切りのオプションプランID一覧を受け取りDBに反映する" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // オプションプラン登録
      models.charge.ChargeOptionPlans.regist(models.charge.ChargeOptionPlans(10, "1", 1000, 1000, 50, "", now, now, now))
      models.charge.ChargeOptionPlans.regist(models.charge.ChargeOptionPlans(11, "2", 180, 2000, 100, "", now, now, now))
      models.charge.ChargeOptionPlans.regist(models.charge.ChargeOptionPlans(12, "3", 1, 3000, 150, "", now, now, now))
      models.charge.ChargeOptionPlans.regist(models.charge.ChargeOptionPlans(13, "4", 10, 4000, 200, "", now, now, now))
      models.charge.ChargeOptionPlans.regist(models.charge.ChargeOptionPlans(14, "5", 180, 5000, 250, "", now, now, now))
      // オプションプランをフォームに登録する
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(1, 1, 10))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(2, 1, 11))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(3, 1, 12))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(4, 2, 13))
      enc.EncFormChargeOptions.regist(enc.EncFormChargeOptions(5, 2, 14))

      // フォーム１のオプションプランを変更する
      Admin.setFormChargeOptionByIdString(1, "11,12,13") must beTrue

      // フォーム１のオプションプランを取得
      val list = enc.EncFormChargeOptions.findByFormId(1)
      list.length === 3
      list.foreach { chargeOptions =>
        List(11L, 12L, 13L).indexOf(chargeOptions.chargeOptionPlanId) must be_>= (0)
      }

      // 他のフォームに影響がないか
      val list2 = enc.EncFormChargeOptions.findByFormId(2)
      list2.length === 2
      list2.foreach { chargeOptions =>
        List(13L, 14L).indexOf(chargeOptions.chargeOptionPlanId) must be_>= (0)
      }
      true
    }
  }

  "searchContract" should {
    "契約者を検索して結果を(models.contract.SearchedContracts, String)のリストとして返す" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 契約者を登録する
      Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "石川五右衛門", "abc", now) must beTrue
      Contracts.directInsert("b@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "佐々木小次郎", "def", now) must beTrue
      Contracts.directInsert("c@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "ドラえもん", "ghi", now) must beTrue
      Contracts.directInsert("d@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "のびのびた", "jkl", now) must beTrue

      // 検索
      val list = Admin.searchContract(Admin.SearchContractContext("ドラ", "", "", "", "name", "asc"))
      list.length === 1
      list.head._1.mail === "c@van2k.com"
    }
  }

  "changeContractStatus" should {
    "契約者の状態を変更できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 契約者を登録する
      Contracts.directInsert("a@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "石川五右衛門", "abc", now) must beTrue
      Contracts.directInsert("b@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "佐々木小次郎", "def", now) must beTrue
      Contracts.directInsert("c@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "ドラえもん", "ghi", now) must beTrue
      Contracts.directInsert("d@van2k.com", contract.Contracts.STATUS_REGISTERED,
        "のびのびた", "jkl", now) must beTrue

      // 状態変更
      Admin.changeContractStatus(Contracts.findByMail("b@van2k.com").get.contractId.get,
        Contracts.STATUS_STOP) must beTrue

      // 変更を確認
      Contracts.findByMail("b@van2k.com").get.status === Contracts.STATUS_STOP

      // 他の契約者に影響がないか
      Contracts.findByMail("a@van2k.com").get.status === Contracts.STATUS_REGISTERED
      Contracts.findByMail("c@van2k.com").get.status === Contracts.STATUS_REGISTERED
      Contracts.findByMail("d@van2k.com").get.status === Contracts.STATUS_REGISTERED
    }
  }

  "administratorAdd" should {
    "管理者を追加できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      Admin.administratorAdd("admin2", "qwerty") must beTrue
      Admin.login("admin2", "qwerty") must beSome
    }
  }

  "administratorRemove" should {
    "管理者を削除できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // デフォルト管理者のidを取得
      val currentAdmin = Administrator.findByUseridAndPass("administrator", "van2kvan2k").get

      // 管理者追加
      Administrator.insert("admin2", "qwerty") must beTrue
      // 管理者削除
      val remove_id = Administrator.findByUseridAndPass("admin2", "qwerty").get.id
      Admin.administratorRemove(remove_id, currentAdmin) must beTrue

      // 削除されたか
      Administrator.findByUseridAndPass("admin2", "qwerty") must beNone
    }
  }

  "administratorChangePass" should {
    "管理者のパスワードを変更できる" in running(FakeApplication(additionalConfiguration = inMemoryDatabase())){
      // 管理者追加
      Administrator.insert("admin2", "qwerty") must beTrue
      // パスワード変更
      Admin.administratorChangePass("admin2", "qwerty", "zxcvbn") must beTrue
      // 変更されたか
      Administrator.findByUseridAndPass("admin2", "zxcvbn") must beSome
      Administrator.findByUseridAndPass("admin2", "qwerty") must beNone
      // 他の管理者に影響はないか
      Administrator.findByUseridAndPass("administrator", "van2kvan2k") must beSome
    }
  }
}
