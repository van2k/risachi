import play.api.GlobalSettings
import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent.Future

object ProductionGlobal extends GlobalSettings
{
  override def onHandlerNotFound(request: RequestHeader) = {
    Future.successful(NotFound(views.html.global_error(request, "NotFound")))
  }

  override def onError(request : RequestHeader, ex : scala.Throwable) = {
    Future.successful(InternalServerError(views.html.global_error(request, "InternalServerError")))
  }

  override def onBadRequest(request : RequestHeader, error : String) = {
    Future.successful(BadRequest(views.html.global_error(request, "BadRequest")))
  }
}
