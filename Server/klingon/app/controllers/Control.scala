package controllers

import play.api.mvc._
import models._
import models.contract.Contracts
import commons.Constants

class Control extends Controller {
	/*
	 * ログイン状態をセッションに登録する
	 * param contractId:契約者ID
	 */
	implicit def setAdminLogin(adminId: Option[Long]) = {
		//session.+(ADMIN_ID, contractId.get.toString)
		//if (session.get(CONTRACT_ID).isDefined){
		//  session.-(CONTRACT_ID)
		//}
		Constants.SESSION_ADMIN_ID -> adminId.get.toString
	}

	/*
	 * ログイン状態をセッションに登録する
	 * param contractId:契約者ID
	 */
	implicit def setContractLogin(contractId: Option[Long]) = {
		//session.+(CONTRACT_ID, contractId.get.toString)
		//if (session.get(ADMIN_ID).isDefined){
		//  session.-(ADMIN_ID)
		//}
		Constants.SESSION_CONTRACT_ID -> contractId.get.toString
	}

	/*
	 * 管理者がログインしているか確認する
	 * param req:Request
	 * return true ログイン中
	 */
	//def isSessionLogin(session:Session):Boolean = {
	//  return session.get(CONTRACT_ID).isDefined
	//}
	def isAdminLogin()(implicit request: RequestHeader): Boolean = {
		return session.get(Constants.SESSION_ADMIN_ID).isDefined
	}

	/*
	* 管理者ログインしている管理者の情報を取得する
	* param req:Reques
	* return 管理者レコード
	 */
	def getAdminLogin()(implicit request: RequestHeader): Option[Administrator] = {
		if (isAdminLogin()) {
			Administrator.findById(session.get(Constants.SESSION_ADMIN_ID).get.toLong)
		} else {
			Option.empty
		}
	}

	/*
	 * 契約者がログインしているか確認する
	 * param req:Request
	 * return true ログイン中
	 */
	//def isSessionLogin(session:Session):Boolean = {
	//  return session.get(CONTRACT_ID).isDefined
	//}
	def isContractLogin()(implicit request: RequestHeader): Boolean = {
		return session.get(Constants.SESSION_CONTRACT_ID).isDefined
	}

	/*
	* 契約者ログインしている契約者の情報を取得する
	* param req:Reques
	* return 契約者レコード
	 */
	def getContractLogin()(implicit request: RequestHeader): Option[Contracts] = {
		if (isContractLogin()) {
			Contracts.findById(session.get(Constants.SESSION_CONTRACT_ID).get.toLong)
		} else {
			Option.empty
		}
	}

	/*
	 * 契約者ログオフ
	 */
	def contractLogoff()(implicit request: RequestHeader) = {
		session.-(Constants.SESSION_CONTRACT_ID)
	}

	def withAuthAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
		Action{ request =>
			if(request.session.get(Constants.SESSION_CONTRACT_ID).isDefined){
				f(request)
			}else{
				Redirect(routes.Login.index())
			}
		}
	}

}
