package controllers

import play.api._
import data.Form
import data.Forms._
import mvc._
import views._
import scala.Some
import services.Admin.SearchContractContext
import models.Administrator
import models.charge.ChargePlans

object Admin extends Control {

	// 契約者検索用のフォーム
	val searchContractForm = Form(
		mapping(
			"name" -> text,
			"mail" -> text,
			"count_from" -> text,
			"count_to" -> text,
			"sort_column" -> text,
			"sort_desc" -> text
		)(SearchContractContext.apply)(SearchContractContext.unapply)
	)

	// 問い合わせ検索用のコンテキストとフォーム
	case class SearchInquiryContext(
		                               dateFrom: String,
		                               dateTo: String,
		                               name: String,
		                               mail: String,
		                               member: Boolean,
		                               nonmember: Boolean,
		                               unsupported: Boolean,
		                               supported: Boolean
		                               )

	val searchInquiryForm = Form(
		mapping(
			"date_from" -> text,
			"date_to" -> text,
			"name" -> text,
			"mail" -> text,
			"member" -> boolean,
			"nonmember" -> boolean,
			"unsupported" -> boolean,
			"supported" -> boolean
		)(SearchInquiryContext.apply)(SearchInquiryContext.unapply)
	)

	// 管理者追加フォーム
	val addAdministratorForm = Form(
		mapping(
			"userid" -> text(1, 32),
			"pass" -> tuple("main" -> text(minLength = 6), "confirm" -> text).verifying(
				// パスワードの入力ルール定義
				"パスワードが一致しません。", passwords => passwords._1 == passwords._2)
		)((userid, pass) => Administrator(0, userid))((a: Administrator) => Some(a.userId, ("", "")))
	)

	// 管理者パスワード変更フォーム
	val changeAdminPassForm = Form(
		mapping(
			"userid" -> text,
			"oldpass" -> text,
			"pass" -> tuple("main" -> text(minLength = 6), "confirm" -> text).verifying(
				// パスワードの入力ルール定義
				"パスワードが一致しません。", passwords => passwords._1 == passwords._2)
		)((userid, oldpass, pass) => Administrator(0, userid))((a: Administrator) => Some(a.userId, "", ("", "")))
	)

	// お知らせ追加フォーム
	val addInformationForm = Form(
		mapping(
			"id" -> text,
			"subject" -> nonEmptyText(maxLength = 128),
			"content" -> nonEmptyText
		)(models.addInformation.apply)(models.addInformation.unapply)
	)
	// todo content に入力できる最大文字数も設定した方が良いのではないか？

	// お知らせ検索フォーム
	val searchInformationForm = Form(
		mapping(
			"date_from" -> text,
			"date_to" -> text,
			"subject" -> text,
			"content" -> text
		)(models.SearchInformationContext.apply)(models.SearchInformationContext.unapply)
	)

	// アンケート検索フォーム
	val searchEncForm = Form(
		mapping(
			"contract_name" -> text,
			"start_st_from" -> optional(date("yyyy/MM/dd")),
			"start_st_to" -> optional(date("yyyy/MM/dd")),
			"end_st_from" -> optional(date("yyyy/MM/dd")),
			"end_st_to" -> optional(date("yyyy/MM/dd")),
			"payment_confirm_is_null" -> boolean,
			"payment_confirm_is_not_null" -> boolean,
			"is_displaying" -> boolean,
			"is_not_displaying" -> boolean
		)(services.Admin.SearchEncContext.apply)(services.Admin.SearchEncContext.unapply)
	)


	def login = Action {
		implicit request =>
			Ok(html.login.administrator()).withNewSession
	}

	def doLogin = Action {
		implicit request =>
		// リクエストパラメータ取得
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get

			val userid = params("userid").head
			val password = params("password").head

			services.Admin.login(userid, password) match {
				case Some(administrator) => {
					// ログイン成功
					Redirect(routes.Admin.enc).withSession(
						setAdminLogin(Option(administrator.id))
					)
				}
				case _ => {
					// ログイン失敗
					Ok(html.admin_error("ID、又は、パスワードが異なります", "/admin/login"))
				}
			}
	}

	def doLogout = Action {
		implicit request =>
			Ok(html.login.administrator()).withNewSession
	}

	// ログインしているか確認し、ログインしていなかったらエラーレスポンスを返すヘルパー関数
	def checkIfLogin(block: => play.api.mvc.Result)(implicit request: RequestHeader) = {
		if (isAdminLogin) {
			block
		} else {
			// ログインしていない場合、ログイン画面へリダイレクトする
			Redirect(routes.Admin.login())
		}
	}

	def enc = Action {
		implicit request =>
			checkIfLogin {
				val searchContext = services.Admin.SearchEncContext("", None, None, None, None, false, false, false, false)

				val forms = services.Admin.searchEnc(searchContext, List.empty[String])
				val chargeOptionList = models.charge.ChargeOptionPlans.findAll()
				val plans = ChargePlans.findAll()
				Ok(html.admin.enc(false, searchEncForm.fill(searchContext), forms, chargeOptionList, plans, List.empty[String]))
			}
	}

	def encSearch = Action {
		implicit request =>
			checkIfLogin {
				val plans = ChargePlans.findAll()
				val paramPlans = request.queryString.get("plans[]").getOrElse(Seq())
				val searchContext = searchEncForm.bindFromRequest().value.getOrElse(
					services.Admin.SearchEncContext("", None, None, None, None, false, false, false, false)
				)
				// アンケート（フォーム）検索
				val forms = services.Admin.searchEnc(searchContext, paramPlans.toList)
				val chargeOptionList = models.charge.ChargeOptionPlans.findAll()
				Ok(html.admin.enc(true, searchEncForm.fill(searchContext), forms, chargeOptionList, plans, paramPlans.toList))
			}
	}

	def encUpdatePaymentPrice(formId: Long, price: Int) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.registReceipt(formId, price) match {
					case true => Ok("ok")
					case _ => Ok("ng")
				}
			}
	}

	def encStop(formId: Long) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.changeEncStatus(formId, models.enc.EncForms.STATUS_END) match {
					case true => Ok("ok")
					case _ => Ok("ng")
				}
			}
	}

	def encGetPaymentOption(formId: Long) = Action {
		implicit request =>
			checkIfLogin {
				Ok(services.Admin.getFormChargeOptionIdString(formId))
			}
	}

	def encSetPaymentOption(formId: Long, chargeOptionId: String) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.setFormChargeOptionByIdString(formId, chargeOptionId) match {
					case true => Ok("ok")
					case _ => Ok("ng")
				}
			}
	}

	def paymentUpdate(formId: Long) = Action {
		implicit request =>
			checkIfLogin {
				val date = services.Admin.paymentUpdate(formId)
				val date_str = date match {
					case Some(d) => "%tY/%<tm/%<td %<tH:%<tM:%<tS" format d
					case _ => ""
				}
				Ok(date_str)
			}
	}

	def encDelete(formId: Long) = Action {
		implicit request =>
			checkIfLogin {
				if (services.Admin.changeEncStatus(formId, models.enc.EncForms.STATUS_DELETE)) {
					Ok("ok")
				} else {
					Ok("ng")
				}
			}
	}

	// 契約者管理
	def contract = Action {
		implicit request =>
			checkIfLogin {
				val searchContext = try {
					searchContractForm.bindFromRequest.get
				} catch {
					case e: Exception =>
						SearchContractContext("", "", "", "", "", "")
				}
				val contractAndRegDayStringList = services.Admin.searchContract(searchContext)

				Ok(html.admin.contract(searchContext, contractAndRegDayStringList))
			}
	}

	def contractStop(contractId: Long) = Action {
		implicit request =>
		// 契約者の停止
			checkIfLogin {
				services.Admin.changeContractStatus(contractId, models.contract.Contracts.STATUS_STOP)
				Ok("")
			}
	}

	def contractRegister(contractId: Long) = Action {
		implicit request =>
		// 契約者を契約状態にする
			checkIfLogin {
				services.Admin.changeContractStatus(contractId, models.contract.Contracts.STATUS_REGISTERED)
				Ok("")
			}
	}

	def contractResetPassword(contractId: Long) = Action {
		implicit request =>
		// 契約者を契約状態にする
			checkIfLogin {
				services.Admin.resetContractPassword(contractId, request)
				Ok("")
			}
	}

	// 問い合わせの一覧表示
	def inquiry = Action {
		implicit request =>
			checkIfLogin {
				val searchContext = try {
					searchInquiryForm.bindFromRequest.get
				} catch {
					case e: Exception =>
						SearchInquiryContext("", "", "", "", false, false, false, false)
				}
				Ok(html.admin.inquiry(searchContext, services.Admin.searchInquiry(searchContext)))
			}
	}

	// 問い合わせのステータスを変更する
	def inquiryStatusUpdate(inqId: Long, inqStatus: String) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.changeInquiry(inqId, inqStatus)
				Ok("")
			}
	}

	// お知らせの登録
	def notice = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.notice(addInformationForm, false))
			}
	}

	def notice_confirm = Action {
		implicit request =>
			checkIfLogin {
				addInformationForm.bindFromRequest.fold(
					formWithErrors => BadRequest(html.admin.notice(formWithErrors, false)),
					information => {
						Ok(html.admin.notice_confirm(information))
					}
				)
			}
	}

	def notice_submit = Action {
		implicit request =>
			checkIfLogin {
				addInformationForm.bindFromRequest.fold(
					formWithErrors => BadRequest(html.admin.notice(formWithErrors, false)),
					information => {
						val formbBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
						val params: Map[String, Seq[String]] = formbBody.get
						if (params.getOrElse("send", "") != "") {
							if (information.id == "") {
								services.Admin.registInfo(information.subject, information.content)
							} else {
								services.Admin.updateInfo(information.id.toLong, information.subject, information.content)
							}
							Ok(html.admin.notice(addInformationForm, true))
						} else {
							val filledForm = addInformationForm.fill(models.addInformation(information.id, information.subject, information.content))
							Ok(html.admin.notice(filledForm, false))
						}
					}
				)
			}
	}

	// お知らせの一覧表示
	def history = Action {
		implicit request =>
			checkIfLogin {
				val searchContext = try {
					searchInformationForm.bindFromRequest.get
				} catch {
					case e: Exception =>
						models.SearchInformationContext("", "", "", "")
				}
				Ok(html.admin.history(searchContext, services.Admin.searchInfo(searchContext)))
			}
	}

	// お知らせの削除
	def notice_delete(infId: Long) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.deleteInfo(infId)
				Ok("")
			}
	}

	// お知らせの修正
	def notice_update(infId: Long) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.searchByIdInfo(infId) match {
					case Some(information) => Ok(html.admin.notice(addInformationForm.fill(models.addInformation(infId.toString, information.subject, information.content)), false))
					case _ => Ok(html.admin_error("指定された管理者からの連絡が見つかりません。", "/admin/notice/update"))
				}
			}
	}

	// 料金プランの一覧表示
	def plan = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.plan(services.Admin.searchPlan()))
			}
	}

	def plan_add = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.plan_add())
			}
	}

	def plan_edit = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.plan_add())
			}
	}

	def plan_delete = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.plan(services.Admin.searchPlan()))
			}
	}

	def plan_confirm = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.plan_confirm())
			}
	}

	// 料金プランの詳細
	def plan_detail(chargePlansId: String) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.searchPlanById(chargePlansId.toLong) match {
					case Some(chargePlans) => Ok(html.admin.plan_detail(chargePlans))
					case _ => Ok(html.admin_error("指定された料金プランが見つかりません。", "/admin/plan/detail/"))
				}
			}
	}

	// オプションプランの一覧表示
	def opt_plan = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.opt_plan(services.Admin.searchOpitonPlan()))
			}
	}

	def opt_plan_add = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.opt_plan_add())
			}
	}

	def opt_plan_edit = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.opt_plan_add())
			}
	}

	def opt_plan_delete = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.opt_plan(services.Admin.searchOpitonPlan()))
			}
	}

	def opt_plan_confirm = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.opt_plan_confirm())
			}
	}

	// オプションプランの詳細
	def opt_plan_detail(optionPlansId: String) = Action {
		implicit request =>
			checkIfLogin {
				services.Admin.searchOptionPlanById(optionPlansId.toLong) match {
					case Some(optionPlans) => Ok(html.admin.opt_plan_detail(optionPlans))
					case _ => Ok(html.admin_error("指定されたオプションプランが見つかりません。", "/admin/opt_plan/detail/"))
				}
			}
	}

	def administrators = Action {
		implicit request =>
			checkIfLogin {
				Ok(html.admin.administrators(Administrator.getAllList(), addAdministratorForm.fill(Administrator(0, "")), getAdminLogin.get))
			}
	}

	def administratorAdd = Action {
		implicit request =>
			checkIfLogin {
				addAdministratorForm.bindFromRequest.fold(
					formWithErrors => BadRequest(html.admin.administrators(Administrator.getAllList(), formWithErrors, getAdminLogin.get)),
					administrator => {
						// パラメータ取得
						val params = request.body.asFormUrlEncoded.get
						val pass = params.get("pass.main").getOrElse(List("")).apply(0)

						// 管理者追加
						if (services.Admin.administratorAdd(administrator.userId, pass)) {
							// 管理者一覧画面へリダイレクト
							Redirect(routes.Admin.administrators)
						} else {
							Ok(html.admin_error("管理者を追加できません。同じuseridがすでに使われている可能性があります。", "/admin/administratorAdd"))
						}
					}
				)
			}
	}

	def administratorRemove(id: Long) = Action {
		implicit request =>
			checkIfLogin {
				// 管理者削除
				services.Admin.administratorRemove(id, getAdminLogin.get)
				// リダイレクト先のURLを返す
				Ok(routes.Admin.administrators.absoluteURL())
			}
	}

	def administratorChangePass(id: Long) = Action {
		implicit request =>
			checkIfLogin {
				Administrator.findById(id) match {
					case Some(administrator) => Ok(html.admin.administratorChangePass(changeAdminPassForm.fill(administrator)))
					case _ => Ok(html.admin_error("指定された管理者は削除されました。", "/admin/administratorChangePass"))
				}
			}
	}

	def administratorChangePassSubmit = Action {
		implicit request =>
			checkIfLogin {
				changeAdminPassForm.bindFromRequest.fold(
					formWithErrors => BadRequest(html.admin.administratorChangePass(formWithErrors)),
					administrator => {
						// パラメータ取得
						val params = request.body.asFormUrlEncoded.get
						val oldpass = params.get("oldpass").getOrElse(List("")).apply(0)
						val pass = params.get("pass.main").getOrElse(List("")).apply(0)

						// 現在のパスワードが一致するか？
						services.Admin.administratorChangePass(administrator.userId, oldpass, pass) match {
							case true => Ok(html.admin.administratorChangePassSubmit())
							case _ => Ok(html.admin_error("現在のパスワードが一致しません。", "/admin/administratorChangePassSubmit"))
						}
					}
				)
			}
	}
}
