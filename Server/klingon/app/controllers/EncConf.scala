package controllers

import play.api.mvc._
import models.enc._
import models.charge._
import java.util.Calendar
import play.api.Logger
import java.text.SimpleDateFormat
import models.{DbUpdateResult, QuestionType}
import models.validation.FormItemValueValidator
import models.QuestionType._
import models.exception.{DbExecuteFailException, QuestionTypeMismatchException}
import collection.mutable
import collection.SortedMap
import views._
import services.{EncFormItem, EncFormPage, User}
import commons.Constants
import java.util.Date
import play.api.Play.current
import play.api.libs.json._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.JsString
import scala.Some
import play.api.libs.json.JsObject
import play.api.templates.Html
import play.api.Play.configuration
import play.api.libs.json.JsString
import scala.Some
import play.api.libs.json.JsObject
import models.contract.Contracts
import scalikejdbc._

object EncConf extends Control {

	case class ChangePassContext(oldpass: String, newpass: String)

	val changePassForm = Form(
		mapping(
			"oldpass" -> text,
			"pass" -> tuple("main" -> text(minLength = 6), "confirm" -> text).verifying(
				// パスワードの入力ルール定義
				"パスワードが一致しません。", passwords => passwords._1 == passwords._2)
		)((oldpass, pass) => ChangePassContext(oldpass, pass._1))((a: ChangePassContext) => Some(a.oldpass, (a.newpass, "")))
	)

	/**
	 * アンケート設定の一覧を取得します。
	 *
	 * @return
	 */
	def list() = Action {
		implicit request => {
			val contract = getContractLogin()
			if (contract.isDefined) {
        val (encFormList, infoList, encFormConfigs, chargePlans) = services.User.dispEncList(contract.get.contractId.getOrElse(-1))
				Ok(views.html.encconf.list(encFormList, encFormConfigs, chargePlans, infoList))
			} else {
				Redirect(routes.Login.index())
			}
		}
	}

  /**
   * チュートリアル用画面を表示します。
   *
   */
  def tutorial() = Action {
    implicit request => {
      val contract = getContractLogin()
      if (contract.isDefined) {
        val (encFormList, infoList, encFormConfigs, chargePlans) = services.User.dispEncList(contract.get.contractId.getOrElse(-1))
        Ok(views.html.encconf.list(encFormList, encFormConfigs, chargePlans, infoList, "1"))
      } else {
        Redirect(routes.Login.index())
      }
    }
  }

  /**
  * アンケート料金選択画面
  * @return
  */
  def chargeselect(formId:Long, planChange:String, tutorialType:String) = Action {
    implicit request => {
      val contract = getContractLogin()
      if (contract.isDefined) {
        val form = EncForms.findByIdContractid(formId, contract.get.contractId.getOrElse(-1)).getOrElse(EncForms.getNewForm())
        val plans = ChargePlans.findAll()
        if (plans.length > 0) {
          Ok(html.encconf.chargeselect(form.formId, planChange, plans, EncFormConfigs.findByFormid(form.formId), ChargeOptionPlans.findAll(), EncFormConfigValues.findByFormId(form.formId), tutorialType))
        } else {
          Redirect(routes.EncConf.list()).flashing(
          "EncConf.list.message" -> "料金プランが登録されていません"
          )
        }
      } else {
        Redirect(routes.Login.index())
      }
    }
  }

	/**
	 * アンケート料金登録
	 *
	 * @return
	 */
  def chargeupdate() = Action {
    implicit request => {
      val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
      val params: Map[String, Seq[String]] = formBody.get
      val formId = params("formId").head
      val planId = params("planId").head
      val changePlan = params("changePlan").head
      val tutorialType = params("tutorialType").head
      var result: Option[Html] = Option(null)
      val plancharge = ChargePlans.findById(planId.toInt)

      val cal = Calendar.getInstance()
      val now = cal.getTime()

      Logger.debug("formId:" + formId)
      Logger.debug("planId:" + planId)
      val contract = getContractLogin()
      if (contract.isDefined) {
        if (plancharge.isDefined) {
          if (formId == "0") {
            Logger.debug("* 新規登録処理の開始")
            val (curForm, mFormItems, mFormItemValues, formConfigs, planCharges) = services.User.registEnc(contract.head.contractId.getOrElse(-1), planId.toInt)
            Ok(views.html.encconf.index(curForm, "", null, null, mFormItems, mFormItemValues, true, formConfigs, planCharges, tutorialType))
          } else {
            Logger.debug("* 更新処理の開始")
            val (curForm, formItems, formItemValues, mFormItems, mFormItemValues, formConfigs) = services.User.updateEnc(contract.head.contractId.getOrElse(-1), planId.toInt, formId.toLong, params)
            if (curForm.formId != -1) {
              val serial = "ENC" + curForm.formId + System.currentTimeMillis()
              if (changePlan == "0") {
                Ok(views.html.encconf.index(curForm, serial, formItems, formItemValues, mFormItems, mFormItemValues, false, formConfigs, plancharge.get, tutorialType))
              } else {
                Redirect(routes.EncConf.list())
              }
            } else {
              Redirect(routes.EncConf.list()).flashing(
                "EncConf.list.message" -> "該当するアンケートがありません"
              )
            }
          }
        } else {
          Redirect(routes.EncConf.list()).flashing(
            "EncConf.list.message" -> "料金プランが見つかりません"
          )
        }
      } else {
        Redirect(routes.Login.index())
      }
    }
  }

	/**
	 * アンケートの更新画面表示
	 * @param formId:アンケートID
	 * @return
	 */
	def formedit(formId: Long) = Action {
		implicit request => {
			val contract = getContractLogin()
			if (contract.isDefined) {
				val form = EncForms.findByIdContractid(formId, contract.get.contractId.getOrElse(-1)).getOrElse(EncForms.getBlankForm())
				if (form.formId != -1) {
					val encconfig = EncFormConfigs.findByFormid(form.formId).getOrElse(EncFormConfigs.getBlankEncFormConfig())
					val chargePlan = ChargePlans.findById(encconfig.chargePlanId.getOrElse(-1))

					if (chargePlan.isDefined) {
						Logger.debug("* 更新処理の開始")

						val formItems = EncFormItems.findByFormId(form.formId)
						val formItemValues = EncFormItemValues.findByFormId(form.formId)

						val serial = "ENC" + form.formId + System.currentTimeMillis()

						Logger.debug("serial:" + serial)
						Ok(views.html.encconf.index(form, serial, formItems, formItemValues, EncMFormItems.findAll(), EncMFormItemValues.findAll(), false, encconfig, chargePlan.get))
					} else {
						Redirect(routes.EncConf.list()).flashing(
							"EncConf.list.message" -> "料金プランが見つかりません"
						)
					}
				} else {
					Redirect(routes.EncConf.list()).flashing(
						"EncConf.list.message" -> "アンケート、又は、料金情報が見つかりません"
					)
				}
			} else {
				Redirect(routes.Login.index())
			}
		}
	}

	/**
	 * アンケートタイトルの更新を行います。
	 * @return
	 */
	def updateTitle = Action {
		implicit request => {

			// note:逆引きレシピ P.455参照
			// note:JSONデータとして取得することができないためPostパラメーターとして取得する
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get
			val formId = params("formId").head
			val contractId = params("contractId").head
			val content = params("value").head
			val dataDiv = params("dataDiv").head.toInt
			val cssId: Option[Long] = Option(0)
			val orgCss = Option("")
			val imageUrl = Option("")

			val resultMessage = ""
			var totalPageCount = EncFormItems.getMaxPageNo(formId.toLong)
			if(totalPageCount == 0) totalPageCount = 1

      // 受信データのデータ区分を取得
      val titleDataDiv = 0
      val detailDataDiv = 1
      val resultDataDiv = 2

			//region ** ログ出力
			Logger.debug("formId:" + formId)
			Logger.debug("contractId:" + contractId)
			Logger.debug("value:" + content)
			Logger.debug("dataDiv:" + dataDiv)
      Logger.debug("value length:" + content.length)
			//endregion

      var status = "0"
      var retMessage = ""
      // タイトル文字
      if (dataDiv == titleDataDiv && content.length > 100) {
        status = "-1"
        retMessage = "アンケートタイトルは100文字以内でご入力ください。"
      } else if (dataDiv == detailDataDiv && content.length > 2500) {
        status = "-1"
        retMessage = "アンケートの説明は2500文字以内でご入力ください。"
      } else if (dataDiv == resultDataDiv  && content.length > 1000) {
        status = "-1"
        retMessage = "アンケート回答後のメッセージは1000文字以内でご入力ください。"
      } else {
        val curForms = EncForms.find(formId.toLong)
        val currentDate = Calendar.getInstance().getTime
        val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

        if (curForms.isEmpty) {
          retMessage = "パラメータが不正です。"
        } else {

          Logger.debug("--> 更新処理")

          val forms = curForms.getOrElse(EncForms.getBlankForm())

          val formName = if (dataDiv == titleDataDiv) {
            retMessage = "アンケートタイトルを登録しました。"
            content
          } else {
            forms.formName
          }

          Logger.debug("formName:" + formName)

          val formDesc = if (dataDiv == detailDataDiv) {
            retMessage = "アンケート説明を登録しました。"
            content
          } else {
            forms.formDesc
          }

          Logger.debug("formDesc:" + formDesc)

          val resultMessage = if (dataDiv == resultDataDiv) {
            retMessage = "アンケート回答後メッセージを登録しました。"
            content
          } else {
            forms.resultMessage
          }

          Logger.debug("resultMessage:" + resultMessage)

          val updForms = EncForms(forms.formId, forms.contractId, formName, formDesc, resultMessage
            , totalPageCount, cssId, orgCss, imageUrl, EncForms.STATUS_EDIT, forms.urlKey, forms.createdAt, currentDate)

          EncForms.update(updForms, currentDate)
        }
      }
      Logger.debug("retMessage:" + retMessage)

      Ok(Json.toJson(Map("status" -> status, "message" -> retMessage)))

		}
	}

	/*
	 * フォームの複製作成
	 */
	def copyForm(formId: Long) = Action {
		implicit request => {
			try {
				val contract = getContractLogin()
				if (contract.isDefined) {
					val form = EncForms.findByIdContractid(formId, contract.get.contractId.getOrElse(-1)).getOrElse(EncForms.getBlankForm())
					if (form.formId != -1) {
						// 複製作成へ
						val ret = services.Contract.copyEnc(form.formId, contract.head.contractId.get)
						if (ret.isDefined) {
							Redirect(routes.EncConf.list())
						} else {
							Redirect(routes.EncConf.list()).flashing(
								"EncConf.list.message" -> "複製作成に失敗しました"
							)
						}
					} else {
						Redirect(routes.EncConf.list()).flashing(
							"EncConf.list.message" -> "アンケートが見つかりません"
						)
					}
				} else {
					Ok(html.error("ログインしてください"))
				}
			} catch {
				case e: Exception => Ok(html.error("内部エラー"))
			}
		}
	}

	/**
	 * formのステータスを更新します
	 * @return
	 */
	def updateStatus(formId: Long, status: String) = Action {
		implicit request => {
			val contract = getContractLogin()

			if (contract.isDefined) {

				// アンケートの設定状態確認およびステータス更新
				val message = services.Contract.checkRegistEnqAndUpdate(formId, contract.head.contractId.get, status)
				if (message != "") {
					Redirect(routes.EncConf.list()).flashing(
						"EncConf.list.message" -> message
					)
				} else {
					Redirect(routes.EncConf.list())
				}
			} else {
				Redirect(routes.Login.index())
			}
		}
	}

	/**
	 * データを削除します。
	 * @return
	 */
	def deleteFormItems = Action {
		implicit request => {
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get

			val formId = params("formId").head
			val contractId = params("contractId").head
			val formItemId = params("formItemId").head
			val updatedAtFormItemsString = params("updatedAtFormItems").head

			val allPageCount = params("allPageCount").head.toInt

      Logger.debug("deleteFormItems **")

			Logger.debug("formId:" + formId)
			Logger.debug("contractId:" + contractId)
			Logger.debug("formItemId:" + formItemId)
			Logger.debug("updatedAtFormItemsString:" + updatedAtFormItemsString)
			Logger.debug("allPageCount:" + allPageCount)

			if (updatedAtFormItemsString.size > 0) {
				val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
				val updatedAt = sdf.parse(updatedAtFormItemsString)
				val currentDate = Calendar.getInstance().getTime

				val ret = EncFormItem.delete(formItemId.toLong, formId.toLong, updatedAt, currentDate, allPageCount, currentDate)
				Ok(Json.toJson(Map("status" -> "Ok", "message" -> "質問を削除しました。", "formUpdateAt" -> sdf.format(currentDate))))
			} else {
				Ok(Json.toJson(Map("status" -> "Ok", "message" -> "質問を削除しました。")))
			}
			//if (ret){
			//Ok(Json.toJson(Map("status" -> "Ok", "message" -> "質問を削除しました。", "formUpdateAt" -> sdf.format(currentDate))))
			//}else{
			// エラーメッセージをセットする
			//Ok(Json.toJson(Map("status" -> "Conflict"
			//  , "message" -> "他のユーザーによりデータが更新されています。再度データを取得して処理を行ってください。")))
			//}
		}
	}

	/**
	 * ページ追加に伴い、総ページ数を更新します。
	 *
	 * @return
	 */
	def addPage = Action {
		implicit request => {
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get

			val formId = params("formId").head
			val allPageCount = params("allPageCount").head.toInt

			Logger.debug("formId:" + formId)
			Logger.debug("allPageCount:" + allPageCount)

			val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")

			val currentDate = Calendar.getInstance().getTime

			EncFormPage.add(formId.toLong, allPageCount, currentDate, currentDate)

			Ok(Json.toJson(Map("status" -> "Ok", "message" -> "ページの情報を追加しました。", "formUpdateAt" -> sdf.format(currentDate))))
		}
	}

	/**
	 * ページに関する情報を削除します。
	 * @return
	 */
	def deletePage = Action {
		implicit request => {
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get

			val formId = params("formId").head
			val contractId = params("contractId").head
			val pageNo = params("pageNum").head.toInt
			val allPageCount = params("allPageCount").head.toInt

			Logger.debug("formId:" + formId)
			Logger.debug("contractId:" + contractId)
			Logger.debug("pageNo:" + pageNo)
			Logger.debug("allPageCount:" + allPageCount)

			val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")

			val currentDate = Calendar.getInstance().getTime

			EncFormPage.delete(formId.toLong, pageNo, allPageCount, currentDate, currentDate)

			//      // ページデータの削除処理を行う
			//      EncFormItemValues.deleteByFormItemIdPageNo(formId, pageNo)
			//      EncFormItems.deleteByPageNo(formId, pageNo)

			Ok(Json.toJson(Map("status" -> "Ok", "message" -> "ページの情報を削除しました。", "formUpdateAt" -> sdf.format(currentDate))))
		}
	}

	/**
	 * アンケート項目の並び順を更新します
	 * @return
	 */
	case class updateDispOdrCase(formid: String, pageno: String, itemid: String, dispodr: String, status: String)

	implicit object updateDispOdrFormat extends Format[updateDispOdrCase] {
		def reads(json: JsValue): JsResult[updateDispOdrCase] = JsSuccess(updateDispOdrCase(
			(json \ "formid").as[String],
			(json \ "pageno").as[String],
			(json \ "itemid").as[String],
			(json \ "dispodr").as[String],
			(json \ "status").as[String]
		))

		// Writeは使わないので、適当に実装
		def writes(updatedispodr: updateDispOdrCase) = JsObject(Seq("formid" -> JsString(updatedispodr.formid)))
	}

	def updateDispOdr = Action {
		implicit request => {
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get
			val json = Json.parse(params("values").head)
			val formitems = Json.parse(params("values").head).as[List[updateDispOdrCase]]

			// TODO 以下の処理はService層に移動させたい
			DB localTx {
				implicit con => {
					for (formitem <- formitems) {
						if (formitem.status == "1") {
							Logger.debug(formitem.formid)
							EncFormItems.updatePagenoDispodr(formitem.itemid.toLong, formitem.pageno.toInt, formitem.dispodr.toInt)(con)
						}
					}
				}
			}
			Ok
		}
	}

	/**
	 * アンケート項目の更新処理を行います。
	 * @return
	 */
	def updateFormItems = Action {
		implicit request => {

			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get

			val formId = params("formId").head
			val contractId = params("contractId").head
			val formItemId = params("formItemId").head
			val pageNum = params("pageNum").head.toInt
			val dispOdr = params("dispOdr").head.toInt
			val updatedAtFormItemsString = params("updatedAtFormItems").head
			val questionTitleString = params("questionTitle").head
			val questionTypeNum = params("questionType").head.toInt
			val requiredString = params("required").head.toBoolean
			val mFormItemId = params("mFormItemId").head.toInt

			val required = if (requiredString) {
				"1"
			} else {
				"0"
			}

			val ansInputMaxLength = params("ansInputMaxLength").head.toInt
			val itemSelectCount = params("itemSelectCount").head.toInt
			val selectNumberConditionString = params("selectNumberCondition").head
			val allPageCount = params("allPageCount").head.toInt

			Logger.debug("formId:" + formId)
			Logger.debug("contractId:" + contractId)
			Logger.debug("formItemId:" + formItemId)
			Logger.debug("pageNum:" + pageNum)
			Logger.debug("dispOdr:" + dispOdr)
			Logger.debug("updatedAtFormItemsString:" + updatedAtFormItemsString)
			Logger.debug("questionTitleString:" + questionTitleString)
			Logger.debug("questionType:" + questionTypeNum)
			Logger.debug("requiredString:" + requiredString)
			Logger.debug("ansInputMaxLength:" + ansInputMaxLength)
			Logger.debug("itemSelectCount:" + itemSelectCount)
			Logger.debug("selectNumberConditionString:" + selectNumberConditionString)
			Logger.debug("allPageCount:" + allPageCount)
			Logger.debug("mFormItemId:" + mFormItemId)

			val questionItems = params.filter {
				case (key, value) => key.startsWith("questionItems")
			}

			// 質問番号と項目のマップ
			val questionItemMap: mutable.Map[String, String] = mutable.Map()
			questionItems.keys.toList.sorted.foreach {
				key => {
					Logger.debug("key:" + key.replace("questionItems[", "").replace("]", ""))
					Logger.debug("items:" + questionItems(key).head)
					questionItemMap.put(key.replace("questionItems[", "").replace("]", ""), questionItems(key).head)
				}
			}

			val questionType: QuestionType = getQuestionTypeEnum(questionTypeNum)
			Logger.debug("選択肢タイプ：" + questionType.toString)

			// その他はチェックボックス、ラジオボタン、ドロップダウンリストで入力文字数が1以上の場合チェックされている
			val isSelectElseChecked = (ansInputMaxLength > 0
				&& (questionType == QuestionType.Checkbox || questionType == QuestionType.Radio))

			// チェックロジックをコールする
			val sortedMap: SortedMap[String, String] = SortedMap[String, String]() ++ questionItemMap.toMap

			val chkRetValue = if (questionType == QuestionType.Checkbox) {
				FormItemValueValidator.createInstance(questionType, questionTitleString, ansInputMaxLength, itemSelectCount
					, selectNumberConditionString, isSelectElseChecked, requiredString, sortedMap)
			} else {
				FormItemValueValidator.createInstance(questionType, questionTitleString, ansInputMaxLength, requiredString, sortedMap)
			}

			// チェックロジックでエラーのため戻り値を返す
			if (!chkRetValue.executeCheck()) {
				Logger.debug("チェックロジックでエラー")

				val resultData = ResultErrorData(chkRetValue.getErrorMessage, chkRetValue.getSelectItemCountError
					, chkRetValue.getAnsInputMaxLength, chkRetValue.getItemErrorMessage)
				Ok(Json.toJson(resultData))
			} else {

				val currentDate = Calendar.getInstance().getTime
				val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")

				val updateAt = if (updatedAtFormItemsString == "") {
					currentDate
				} else {
					sdf.parse(updatedAtFormItemsString)
				}

				val categoryId = 1
				val formItems = EncFormItems(formItemId.toLong, formId.toLong, categoryId, questionTypeNum, questionTitleString, pageNum, itemSelectCount,
					selectNumberConditionString, required, dispOdr, currentDate, updateAt)

				val isRegist = (updatedAtFormItemsString == "")

				val result = EncFormItem.update(formItems, ansInputMaxLength, currentDate, currentDate, allPageCount
					, isRegist, questionItemMap)

				if (result == null) {
					// todo:競合の時のエラー処理を追加する
					Logger.debug("データベースの登録で競合が発生")
					throw new DbExecuteFailException
				}

				// 取得したIDと更新日時を返却する
				val resultData = ResultData(result.formItems.itemId.toString, sdf.format(currentDate), result.itemIdList)
				Ok(Json.toJson(resultData))
			}
		}
	}

	def getQuestionTypeEnum(questionTypeNum: Int): QuestionType.Value = {
		if (questionTypeNum == TextLine.id) {
			TextLine
		}
		else if (questionTypeNum == TextMultiLine.id) {
			TextMultiLine
		}
		else if (questionTypeNum == Checkbox.id) {
			Checkbox
		}
		else if (questionTypeNum == Radio.id) {
			Radio
		}
		else if (questionTypeNum == DropList.id) {
			DropList
		}
		else if (questionTypeNum == TextDate.id) {
			TextDate
		}
		else if (questionTypeNum == TextRegx.id) {
			TextRegx
		}
		else {
			throw new QuestionTypeMismatchException
		}
	}

	def passwd = Action {
		implicit request => {
			if (getContractLogin().isDefined) {
				Ok(html.encconf.passwd(changePassForm))
			} else {
				Ok(html.error("ログインしてください"))
			}
		}
	}

	def passwdSubmit = Action {
		implicit request => DB autoCommit {
			implicit con => {
				if (getContractLogin().isDefined) {
					changePassForm.bindFromRequest.fold(
						formWithErrors => BadRequest(html.encconf.passwd(formWithErrors)),
						changePassContext => {
							// 現在のパスワードが一致するか？
							services.Contract.changePassword(getContractLogin().get, changePassContext.oldpass, changePassContext.newpass) match {
								case true => Ok(html.encconf.passwdSubmit())
								case _ => Ok(html.error("現在のパスワードが一致しません。"))
							}
						}
					)
				} else {
					Ok(html.error("ログインしてください"))
				}
			}
		}
	}

	def deleteConfirm(formId: Long) = Action {
		implicit request => {
			if (getContractLogin().isDefined) {

				val form = EncForms.findByIdContractid(formId, getContractLogin().get.contractId.getOrElse(-1)).getOrElse(EncForms.getBlankForm())
				if (form.formId != -1) {
					Ok(html.encconf.delete_confirm(form.formId))
				} else {
					Redirect(routes.EncConf.list()).flashing(
						"EncConf.list.message" -> "アンケートが見つかりません"
					)
				}
			} else {
				Ok(html.error("ログインしてください"))
			}
		}
	}

	/**
	 * フォームを削除する
	 *
	 * @return
	 */
	def deleteForm = Action {
		implicit request => {
			if (getContractLogin().isDefined) {
				val params = request.body.asFormUrlEncoded.get
				val formId = params.get("form_id").getOrElse(Seq("")).head
				val deleteCheck = params.get("delete_check").getOrElse(Seq("")).head
				if (!EncForms.find(formId.toLong).isEmpty) {
					deleteCheck match {
						case "1" => {
							// 物理削除
							services.Contract.deleteEnc(formId.toLong)
							//val ret = EncForms.updateStatus(formId, EncForms.STATUS_DELETE, form.head.updatedAt, Calendar.getInstance().getTime)
							Redirect(routes.EncConf.list()).flashing(
                "EncConf.list.message" -> "アンケートフォームを削除しました。"
              )
						}
						case _ => {
							Ok(html.encconf.delete_confirm(formId.toLong, "アンケートフォーム削除のチェックボックスが未チェックです"))
						}
					}
				} else {
					Ok(html.error("フォームIDが不正です"))
				}
			} else {
				Ok(html.error("ログインしてください"))
			}
		}
	}

	/**
	 * 解約確認画面
	 *
	 * パスワード入力フォームを表示し、
	 * パスワードが入力されたら解約手続き画面に遷移する。
	 *
	 * @return
	 */
	def cancelConfirm = Action {
		implicit request => {
			if (getContractLogin().isDefined) {
				Ok(html.encconf.cancel_confirm())
			} else {
				Ok(html.error("ログインしてください"))
			}
		}
	}

	/**
	 * 解約手続き画面
	 *
	 * 解約の同意チェックボックスを表示し、
	 * 同意が得られた（チェック状態だった）場合は
	 * 契約者解約処理を実行する
	 *
	 * @return
	 */
	def cancel = Action {
		implicit request => {
			val params = request.body.asFormUrlEncoded.get
			val pass = params.get("password").getOrElse(Seq("")).head

			getContractLogin() match {
				case c if(c.isDefined) => {
					if (!Contracts.findByMailAndPass(c.get.mail, pass).isEmpty) {
						// CSRF対策用トークン作成
						val token = Contracts.md5(Constants.CSRF_SALT + Calendar.getInstance.getTime.toString)

						Ok(html.encconf.cancel(token)).withSession(session + ("token" -> token))
					} else {
						Ok(html.encconf.cancel_confirm("パスワードが正しくありません"))
					}
				}
				case _ => Ok(html.error("ログインしてください"))
			}
		}
	}

	/**
	 * 契約者解約処理の実行
	 *
	 * @return
	 */
	def doCancel = Action {
		implicit request => {
			val contract = getContractLogin()
			if (contract.isDefined) {
				val params = request.body.asFormUrlEncoded.get
				val isCancel = params.get("is_cancel").getOrElse(Seq("0")).head
				val paramToken = params.get("token").getOrElse(Seq("")).head
				val cancelCheck = params.get("cancel_check").getOrElse(Seq("")).head
				val sessionToken = session.get("token").getOrElse("session token")

				Logger.debug("paramToken[" + paramToken + "] sessionToken[" + sessionToken + "]")
				// CSRFチェック用tokenを削除
				session - "token"
				if(isCancel == "1") {
					Redirect(routes.EncConf.list())
				} else {
					cancelCheck match {
						case c if(c == "1") => {
							sessionToken match {
								case t if (paramToken == t) => {
									// CSRFチェック用tokenを削除
									session - "token"
									// 解約処理
									services.Contract.withdrawContract(contract.get.contractId.getOrElse(0))
									// 強制ログアウト
									Ok(html.encconf.canceled()).withNewSession
								}
								// パラメータのtokenが一致しない＝正常な画面遷移でない為エラー
								case _ => Ok(html.error("パラメータが不正です"))
							}
						}
						case _ => Ok(html.encconf.cancel(paramToken, "解約するには「解約により情報が削除されることに同意する」のチェックが必要です"))
					}
				}
			} else {
				Ok(html.error("ログインしてください"))
			}
		}
	}

	/**
	 * ファイルアップロード処理
	 *
	 * @return
	 */
	def uploadLogo = Action(parse.multipartFormData) {
		request =>
			request.body.file("logo_file").map {
				file =>
				// パラメータ取得
					val id = request.body.asFormUrlEncoded.get("form_id").getOrElse(List(-1)).apply(0)
					val contentType = file.contentType
					var imageFileName = EncForms.getHashCode()
					Logger.debug("contentType[" + contentType + "]")
					// ファイル名をランダム作成
					imageFileName = contentType match {
						case Some("image/jpeg") => imageFileName + ".jpg"
						case Some("image/png") => imageFileName + ".png"
						case Some("image/gif") => imageFileName + ".gif"
						case _ => ""
					}

					if (!imageFileName.equals("")) {
						val imageUrl = configuration.getString("upload.logo.path") match {
							case Some(path) => path + "/" + imageFileName
							case _ => ""
						}
						Logger.debug("imageUrl[" + imageUrl + "]")
						configuration.getString("upload.logo.dir") match {
							case Some(dir) => {
								Logger.debug("upload.logo.dir[" + dir + "]")
								val moveToFile = new java.io.File(dir, imageFileName)
								// ファイルをuploadsディレクトリに移動
								file.ref.moveTo(moveToFile)
								/*
								 * ファイルサイズチェックはmoveToにてファイルを移動しないと取得できなかったので
								 * ここでチェックする
								 */
								Logger.debug("tmp file size = " + moveToFile.length() + "byte")
								Logger.debug("tmp file width = " + javax.imageio.ImageIO.read(moveToFile).getWidth() + "pix")
								val response = moveToFile match {
									// ファイルのbyteサイズでのValidate
									case f if f.length() > Constants.logoFileLimitSize => {
										Ok(Json.toJson(Map("error" -> "1", "message" -> "ファイルサイズは%dKbyte以内としてください。(選択したファイルサイズ：%dKbyte)".format((Constants.logoFileLimitSize / 1024), (f.length() / 1024))))).as(HTML)
									}
									case i if javax.imageio.ImageIO.read(i).getWidth() > Constants.logoFileLimitWidth => {
										Ok(Json.toJson(Map("error" -> "1", "message" -> "ファイルの横幅は%dpx以内としてください。(選択したファイルの横幅：%dpx)".format(Constants.logoFileLimitWidth, javax.imageio.ImageIO.read(i).getWidth())))).as(HTML)
									}
									case _ => {
										Ok(Json.toJson(Map("success" -> "1", "url" -> imageUrl))).as(HTML)
									}
								}
								response
							}
							case _ => Ok(Json.toJson(Map("error" -> "1", "message" -> "設定ファイルの設定が不正"))).as(HTML)
						}
					} else {
						Ok(Json.toJson(Map("error" -> "1", "message" -> "ファイルはjpg,gif,pngのいずれかを指定してください。"))).as(HTML)
					}
			}.getOrElse {
				// fileパラメータが取得出来ない場合
				Ok(Json.toJson(Map("error" -> "1", "message" -> "ファイルを指定してください。"))).as(HTML)
			}
	}

	/**
	 * 画像のURLをDBに格納する
	 *
	 * @return
	 */
	def updateImage = Action {
		implicit request =>
		// パラメータ取得
			val params = request.queryString
			val formId = params.get("form_id").getOrElse(List(-1)).apply(0)
			val imageUrl = params.get("image_url").getOrElse(List("")).apply(0)
			val mode = params.get("mode").getOrElse(List("")).apply(0)

			// 画像を登録
			mode match {
				case "1" | "3" => {
					User.saveLogo(formId.asInstanceOf[Long], imageUrl)
					Ok(Json.toJson(Map("success" -> "1"))).as(HTML)
				}
				case "2" => {
					User.deleteLogo(formId.asInstanceOf[Long])
					Ok(Json.toJson(Map("success" -> "1"))).as(HTML)
				}
				case _ => Ok(Json.toJson(Map("success" -> "0", "message" -> "パラメータ(mode)が不正です")))
			}
	}

	/**
	 * CSSの設定情報を更新する
	 *
	 * @return
	 */
	def updateCss = Action {
		implicit request =>
		// パラメータ取得
			val formBody = request.body.asFormUrlEncoded
			val params = formBody.get
			val formId = params.get("form_id").getOrElse(List(-1)).apply(0).asInstanceOf[Long]
			val cssId = params.get("css_design").getOrElse(List("")).apply(0)
			val cssCode = if (cssId.toString == "99") params.get("css_code").getOrElse(List("")).apply(0) else null

			val record = EncAnswers(-1,
				formId,
				Calendar.getInstance.getTime,
				Option(null),
				Option(null),
				Option(null),
				Option(null),
				Option(null),
				Option(null),
				Option(null),
				"1",
				request.remoteAddress,
				request.headers.get("User-Agent").toString(),
				Calendar.getInstance.getTime,
				Calendar.getInstance.getTime)

			Logger.debug("formId:" + formId)
			Logger.debug("cssId:" + cssId.toString)

			User.updateCss(formId, cssId.toString().toLong, cssCode) match {
				case DbUpdateResult.success => {
					// 更新に成功したので、プレビュー画面を表示
					EncAnswers.regist(record) match {
						case Some(answerId) => Ok(views.html.enc.index(answerId,
							1,
							EncForms.find(formId).get,
							EncFormItems.findByFormidPageno(formId, 1),
							EncFormItemValues.findByFormidPageno(formId, 1),
							EncMFormItems.findAll(),
							EncAnswerDetails.findByAnsweridPageno(answerId, 1),
							Map[String, String](),
							true,
							"0",
							EncMCss.findAll()))
						case _ => Ok(html.error("CSSの設定更新に失敗しました。"))
					}
				}
				case _ => Ok(html.error("CSSの設定更新に失敗しました。"))
			}

	}
}
