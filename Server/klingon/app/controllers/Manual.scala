package controllers

import play.api.mvc._
import views._

object Manual extends Control {

	def howToRegist = Action {
		implicit request => {
			Ok(html.manual.how_to_regist("", (!getContractLogin().isDefined)))
		}
	}

	def useScene = Action {
		implicit request => {
			Ok(html.manual.use_scene("", (!getContractLogin().isDefined)))
		}
	}

}
