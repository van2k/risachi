package controllers

import play.api.mvc._
import models.enc._
import play.api.Logger
import java.util.Calendar
import play.api.templates._
import commons._
import models.charge.ChargePlans
import services.User
import scalikejdbc._

object Enc extends Controller {

	// foreignKey、及び、属性の許容する長さ
	def Key_Value_max_length = 20

	/**
	 * アンケート表示
	 * @param urlKey URLキー
	 * @param key 外部キー
	 * @param attribute1 属性1
	 * @param attribute2 属性2
	 * @param attribute3 属性3
	 * @param attribute4 属性4
	 * @param attribute5 属性5
	 * @param mode 表示モード
	 */
	def index(urlKey: String, mode: String, key: String, attribute1: String, attribute2: String, attribute3: String, attribute4: String, attribute5: String) = Action {
		implicit request => {
			val cal = Calendar.getInstance()
			val now = cal.getTime()
			var result: Option[Html] = Option(null)
			var answers: Option[List[EncAnswers]] = Option(null)
			var answerId: Long = -1
			val form = EncForms.findByUrlKey(urlKey).getOrElse(EncForms.getBlankForm())
			var chargePlan: ChargePlans = null

			// formIdが存在するかチェックする
			if (form.formId == -1) {
				result = Option(views.html.enc_error("アンケートがありません"))
			}
			var encConfig = EncFormConfigs.findByFormid(form.formId).getOrElse(EncFormConfigs.getBlankEncFormConfig())

			// アンケート設定情報があるか確認する
			if (result.isEmpty) {
				if (encConfig.id == -1) {
					result = Option(views.html.caution("アンケート設定情報がありません"))
				} else {
					chargePlan = ChargePlans.findById(encConfig.chargePlanId.getOrElse(-1)).getOrElse(ChargePlans.getBlankChargePlan())
				}
			}

			// アンケートのステータスをチェックする
			if (result.isEmpty && form.status != EncForms.STATUS_RUN) {
				result = Option(views.html.caution("アンケート受付中の状態ではありません"))
			}

			// アンケートの質問項目件数をチェックする
			if (result.isEmpty && EncFormItems.findByFormId(form.formId).length == 0) {
				result = Option(views.html.caution("アンケートの質問項目が存在しません"))
			}

			// アンケートの受付が終了しているか確認する
			if (result.isEmpty) {
				cal.setTime(encConfig.startSt)
				cal.add(Calendar.DAY_OF_MONTH, encConfig.term)
				if (now.after(cal.getTime())) {
					result = Option(views.html.caution("アンケートの受付期間が終了しています。"))
				}
			}

			// アンケートが受付上限に達しているか確認する
			if (result.isEmpty && EncAnswers.findByFormid(form.formId).filter(x => x.endAt.isDefined && x.isPreview == "0").length >= chargePlan.collectNumber) {
				result = Option(views.html.caution("アンケートは受付上限に達したため終了しています。"))
			}

			// Cookieが設定されている場合
			if (result.isEmpty && !request.cookies.get(form.formId.toString).isEmpty) {
				result = Option(views.html.caution("このアンケートには既に回答頂いています"))
			}

			// TODO 外部キー関連の機能はα版リリース時は塞いでおく
			if (result.isEmpty && (key != "" || attribute1 != "" || attribute2 != "" || attribute3 != "" || attribute4 != "" || attribute5 != "")) {
				result = Option(views.html.error("不正なパラメータが指定されました"))
			}
//			// foreignKey、及び、属性の長さをチェックする
//			if (result.isEmpty && key.length() > 0 && chargePlan.isForeignKey == "0") {
//				result = Option(views.html.enc_error("外部キーの設定は出来ません"))
//			}
//
//			// keyが設定されている場合は、二重投稿かチェックする
//			if (result.isEmpty && key.length() > 0) {
//				answers = Option(EncAnswers.findByFormidForeignkey(formId, key))
//				if (answers.get.length > 0 && answers.get(0).endAt.isDefined) {
//					result = Option(views.html.caution("このアンケートには既に回答頂いております(error = 2)"))
//				}
//			}
//
//			// foreignKey、及び、属性の長さをチェックする
//			if (result.isEmpty && (key.length() > Key_Value_max_length || attribute1.length() > Key_Value_max_length || attribute2.length() > Key_Value_max_length || attribute3.length() > Key_Value_max_length || attribute4.length() > Key_Value_max_length || attribute5.length() > Key_Value_max_length)) {
//				result = Option(views.html.enc_error("指定したパラメータにエラーがあります"))
//			}

			if (result.isEmpty) {
				if (answers.isEmpty || answers.get.length == 0) {
					val record = EncAnswers(-1, form.formId, now, Option(null), Option(key), Option(attribute1), Option(attribute2), Option(attribute3), Option(attribute4), Option(attribute5), "0", request.remoteAddress, request.headers.get("User-Agent").toString(), now, now)
					answerId = EncAnswers.regist(record).getOrElse(-1)
				} else {
					// 一度keyで投稿した場合は、更新はかけない(ユーザが故意にGET値を変更した事を考えて)
//					answerId = answers.get(0).id
				}
				Redirect("/_enc/" + answerId + "/" + mode)
			} else {
				Ok(result.getOrElse(views.html.enc_error("エラー")))
			}
		}
	}

	/*
	 * アンケート表示(リダイレクト用)
	 * @param answerId 回答者ID
	 * @param mode 表示モード
	 */
	def _index(answerId: Long, mode: String) = Action {
		implicit request => {
			val answer = EncAnswers.find(answerId)
			if (answer.isDefined) {
				this.encView(answerId, answer.get.formId, 1, Map[String, String](), false, mode)
			} else {
				Ok(views.html.enc_error("エラー"))
			}
		}
	}

	/*
	 * アンケートのプレビュー表示
	 * @param formId フォームID
	 */
	def preview(urlKey: String) = Action {
		implicit request => {
			val cal = Calendar.getInstance()
			val now = cal.getTime()

			val form = EncForms.findByUrlKey(urlKey).getOrElse(EncForms.getBlankForm())
			if (form.formId != -1) {
				val (result, message) = services.Contract.checkRegistEnqForPreview(form.formId)

				if (result == "caution") {
						Ok(views.html.caution(message))
				} else if (result == "error") {
						Ok(views.html.enc_error(message))
				} else {
					val record = EncAnswers(-1, form.formId, now, Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "1", request.remoteAddress, request.headers.get("User-Agent").toString(), now, now)
					val answerId = EncAnswers.regist(record)
					this.encView(answerId.get, form.formId, 1, Map[String, String](), true, "0")
				}
			} else {
				Ok(views.html.enc_error("アンケートがありません"))
			}
		}
	}

	/**
	 * 指定されたCSSを表示する。
	 *
	 * @param urlKey
	 * @param cssId
	 * @return
	 */
	def customCss(urlKey: String, cssId: Long) = Action {
		implicit request => {
			val form = EncForms.findByUrlKey(urlKey).getOrElse(EncForms.getBlankForm())

			Logger.debug("formId = " + form.formId)
			val cssCode = User.getCssCode(form.formId, cssId)
			Logger.debug("cssCode = " + cssCode)
			Ok(cssCode.toString).as("text/css")
		}
	}

	/**
	 * アンケート投稿
	 * TODO DB更新処理部分をService層に切り出すべき
	 */
	def post() = Action {
		implicit request => DB localTx {
			implicit con => {
				val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
				val params: Map[String, Seq[String]] = formBody.get

				val formId = getSeqValue(params.get("formId"))
				val pageNo = getSeqValue(params.get("pageNo"))
				val answerId = getSeqValue(params.get("answerId"))
				val ispreview = getSeqValue(params.get("ispreview")).toBoolean
				val mode = getSeqValue(params.get("mode"))
				val values = params -("answerId", "formId", "pageNo", "send", "next", "back", "ispreview", "mode")

				if (formId != null && pageNo != null && answerId != null) {
					if (params.contains("back")) {
						// 前へ戻る
						save(answerId.toLong, pageNo.toLong, values)(con)
						this.encView(answerId.toLong, formId.toLong, pageNo.toLong - 1, Map[String, String](), ispreview, mode)(con, session, request)
					} else {
						// 入力データの検証
						val errormsg = validation(formId.toLong, pageNo.toLong, values)(con)
						save(answerId.toLong, pageNo.toLong, values)(con)

						if (errormsg.size > 0) {
							this.encView(answerId.toLong, formId.toLong, pageNo.toLong, errormsg, ispreview, mode)(con, session, request)
						} else {
							if (params.contains("send")) {
								// 終了時間を打刻
								val cal = Calendar.getInstance()
								val answer = EncAnswers.find(answerId.toLong)(con).get
								var uprec = EncAnswers(answer.id, answer.formId, answer.startAt, Option(cal.getTime()),
									answer.foreignKey, answer.attribute1, answer.attribute2, answer.attribute3, answer.attribute4, answer.attribute5,
									answer.isPreview, answer.ipAddress, answer.webBrowser, answer.createdAt, answer.updatedAt)
								EncAnswers.update(uprec)(con)
								if (!ispreview) {
									encendView(formId.toLong, ispreview, mode, Option(new Cookie(name = formId.toString, value = "1", maxAge = Some(604800), domain = None)))(con)
								} else {
									encendView(formId.toLong, ispreview, mode, Option(null))(con)
								}
							} else {
								// 次のページへ
								this.encView(answerId.toLong, formId.toLong, pageNo.toLong + 1, errormsg, ispreview, mode)
							}
						}
					}
				} else {
					Logger.error("Parameter error")
					NotFound
				}
			}
		}
	}

	/*
	 * 明細解答時に登録
	 * @param answerid 回答者ID
	 * @param itemid 設問ID
	 */
	def answeritem(answerid: Long, itemid: Long) = Action {
		val cal = Calendar.getInstance()
		val now = cal.getTime()
		val details = EncAnswerDetails.findByAnsweridFormitemid(answerid, itemid)

		if (details.length == 0) {
			val item = EncAnswerDetails(-1, answerid, itemid, -1, "", Option(now), now, now)
			EncAnswerDetails.regist(item)
		} else if (details(0).answerAt.isEmpty) {
			val record = EncAnswerDetails(details(0).id, answerid, itemid, details(0).formItemValueId, details(0).answerValue, Option(now), details(0).createdAt, details(0).updatedAt)
			EncAnswerDetails.update(record)
		}
		Ok
	}

	/*
	 * アンケートリスト表示
	 */
	def list() = Action {
		implicit request => {

			// 一覧の取得
			val list = EncForms.findAll()
			Ok(views.html.enc.list(list))
		}
	}

	/**
	 * アンケート表示
	 *
	 * @param answerId
	 * @param formId
	 * @param pageNo
	 * @param errormsg
	 * @param ispreview
	 * @param mode
	 * @param session
	 * @param request
	 * @return
	 */
	def encView(answerId: Long, formId: Long, pageNo: Long, errormsg: Map[String, String], ispreview: Boolean, mode: String)(implicit dbSession: DBSession = AutoSession, session: play.api.mvc.Session, request: play.api.mvc.Request[Any]) = {
		// データを取得する
		val findForm = EncForms.find(formId)(dbSession)
		val findItem = EncFormItems.findByFormidPageno(formId, pageNo)(dbSession)
		val findValue = EncFormItemValues.findByFormidPageno(formId, pageNo)(dbSession)
		val findMItem = EncMFormItems.findAll()(dbSession)
		val findAnswers = EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)(dbSession)
		val mCss = EncMCss.findAll()(dbSession)

		if (!findForm.isEmpty && !findItem.isEmpty && !findValue.isEmpty) {
			// アンケート投稿画面へ
			val recForm = findForm.get
			if (mode == "0") {
				Ok(views.html.enc.index(answerId, pageNo, recForm, findItem, findValue, findMItem, findAnswers, errormsg, ispreview, "0", mCss))
			} else {
				Ok(views.html.enc.index_inner(answerId, pageNo, recForm, findItem, findValue, findMItem, findAnswers, errormsg, ispreview, mode, mCss))
			}
		} else {
			// エラー表示へ
			Ok(views.html.enc_error("アンケート表示に必要な質問項目が不正です"))
		}
	}

	/**
	 * アンケート終了表示
	 *
	 * @param formId
	 * @param ispreview
	 * @param mode
	 * @param cookie
	 * @return
	 */
	def encendView(formId: Long, ispreview: Boolean, mode: String, cookie: Option[Cookie])(implicit session: DBSession = AutoSession) = {
		var html = null

		if (mode == "0") {
			if (cookie.isEmpty) {
				Ok(views.html.enc.encend(EncForms.find(formId)(session).get, ispreview, mode))
			} else {
				Ok(views.html.enc.encend(EncForms.find(formId)(session).get, ispreview, mode)).withCookies(cookie.get)
			}
		} else {
			if (cookie.isEmpty) {
				Ok(views.html.enc.encend_inner(EncForms.find(formId)(session).get, ispreview, mode))
			} else {
				Ok(views.html.enc.encend_inner(EncForms.find(formId)(session).get, ispreview, mode)).withCookies(cookie.get)
			}
		}
	}

	/**
	 * 入力値の検証
	 *
	 * @param formId
	 * @param pageNo
	 * @param values
	 * @return エラーメッセージ
	 */
	def validation(formId: Long, pageNo: Long, values: Map[String, Seq[String]])(implicit session: DBSession = AutoSession): Map[String, String] = {
		val errormsg = scala.collection.mutable.Map[String, String]()
		val findItem = EncFormItems.findByFormidPageno(formId, pageNo)(session)
		val findItemValue = EncFormItemValues.findByFormidPageno(formId, pageNo)(session)
		var itemIdList = scala.collection.mutable.ListBuffer.empty[Long]

		// POSTデータからEncFormItems->idと、EncFormItemValuesの値を取得する
		for ((k, v) <- values) {
			Logger.debug(k + "=" + v)
			Logger.debug("v=[" + v.head + "]")
			Logger.debug("v.length=" + v.head.length)
			val itemId = k.split("-")(0).toLong
			itemIdList += itemId

			// 必須項目をチェックする
			if ((findItem.filter(x => x.itemId == itemId && x.required == "1").length > 0 && (v.length == 0 || v.count(x => x.trim().length() == 0) > 0))) {
				errormsg.put(itemId.toString, "必須項目になります")
			// 文字数チェック
			} else if (findItem.filter(x => x.itemId == itemId && (x.mItemId.toString == Constants.QuestionType.TextLine || x.mItemId.toString == Constants.QuestionType.TextMultiLine)).length > 0
				&& findItemValue.filter(x => x.formItemId == itemId && x.chLength < v.head.length).length > 0) {
				errormsg.put(itemId.toString, "文字数上限オーバーです")
			/*
			 * 質問項目(EncFormItems.mItemId)がラジオボタン、チェックボックス、プルダウンのいずれかで、
			 * 選択値(v.head)が選択項目(EncFormItemValues.itemId)のいずれでもない場合エラー
			 */
			} else if (findItem.filter(x => x.itemId == itemId && (x.mItemId.toString == Constants.QuestionType.Radio || x.mItemId.toString == Constants.QuestionType.Checkbox || x.mItemId.toString == Constants.QuestionType.DropList)).length > 0
				&& v.head.length > 0 && !(findItemValue.filter(x => x.formItemId == itemId).flatMap(item => List(item.itemId.toString)).contains(v.head))) {
					errormsg.put(itemId.toString, "不正な値が指定されました")
			} else {
				val checkmsg = checkSelectNumber(findItem.filter(x => x.itemId == itemId)(0), values(k).toList)
				if (checkmsg != "") {
					errormsg.put(itemId.toString, checkmsg)
				}
			}
		}
		// POSTデータに含まれない必須項目をチェックする
		for (t <- findItem) {
			if (t.required == "1" && itemIdList.count(x => x == t.itemId) == 0) {
				errormsg.put(t.itemId.toString, "必須項目になります")
			}
		}

		errormsg.toMap
	}

	/**
	 * 項目の選択チェック
	 * @param item フォームの項目
	 * @param values 項目の値
	 * @return エラーメッセージ
	 */
	def checkSelectNumber(item: EncFormItems, values: List[String]): String = {
		var message = ""

		if (item.selectNumber > 0 && item.condition != null) {
			item.condition match {
				case Constants.ConditionType.Equal => {
					if (item.selectNumber != values.length) {
						message = String.format("%d項目、選択（または、入力）してください", item.selectNumber.asInstanceOf[AnyRef])
					}
				}
				case Constants.ConditionType.MoreEqual => {
					if (item.selectNumber > values.length) {
						message = String.format("%d項目以上、選択（または、入力）してください", item.selectNumber.asInstanceOf[AnyRef])
					}
				}
				case Constants.ConditionType.LessEqual => {
					if (item.selectNumber < values.length) {
						message = String.format("%d項目以下、選択（または、入力）してください", item.selectNumber.asInstanceOf[AnyRef])
					}
				}
				case _ => {
					// 特に何もしない
				}
			}
		}

		message
	}

	/**
	 * アンケート内容保存
	 * TOOD この処理はService層に移すべき
	 *
	 * @param answerId
	 * @param pageNo
	 * @param values
	 */
	def save(answerId: Long, pageNo: Long, values: Map[String, Seq[String]])(implicit session: DBSession = AutoSession) = {
		if (EncAnswerDetails.findByAnsweridPageno(answerId, pageNo)(session).size > 0) {
			// ページ項目単位で回答データを削除
			EncAnswerDetails.removeByAnsweridPageno(answerId, pageNo)(session)
		}
		// POSTデータからEncFormItems->idと、EncFormItemValuesの値を取得する
		for ((k, v) <- values) {
			Logger.debug(k + "=" + v)
			val p = k.split("-")

			val itemId = p(0)
			if (p.length == 2) {
				val valueId = p(1)
				for (value <- v) {
					Logger.debug("pattern1 itemId:" + itemId + " valueId:" + valueId + " value:" + value)
					saveAnswerDetails(answerId, itemId.toLong, valueId.toLong, value)(session)
				}
			} else {
				for (value <- v) {
					Logger.debug("pattern2 itemId:" + itemId + " valueId:" + value + " value:" + value)
					val numberPattern = """\d+""".r
					val saveValue = numberPattern.findFirstIn(value)
					Logger.debug("saveValue:" + saveValue)
					saveAnswerDetails(answerId, itemId.toLong, saveValue.getOrElse("0").toLong, value)(session)
				}
			}
		}
	}

	/*
	 * アンケート明細内容保存
	 * @param itemId 項目ID
	 * @param valueId 値ID
	 * @param value 値
	 */
	def saveAnswerDetails(answerId: Long, itemId: Long, valueId: Long, value: String)(implicit session: DBSession = AutoSession) {
		val detail = EncAnswerDetails.findByAnsweridFormitemidFormitemvalueid(answerId, itemId, valueId)(session)
		val cal = Calendar.getInstance()

		if (detail.length == 0) {
			Logger.debug("add answerId:" + answerId + ",itemId:" + itemId)
			val cal = Calendar.getInstance()
			val record = EncAnswerDetails(-1, answerId, itemId, valueId, value, Option(null), cal.getTime(), cal.getTime())
			EncAnswerDetails.regist(record)(session)
		} else {
			Logger.debug("update answerId:" + answerId + ",itemId:" + itemId)
			val record = EncAnswerDetails(detail(0).id, detail(0).answerId, itemId, valueId, value, detail(0).answerAt, detail(0).createdAt, detail(0).updatedAt)
			EncAnswerDetails.update(record)(session)
		}
	}

	/**
	 * Seqから値取得
	 * @return 値
	 */
	def getSeqValue(value: Option[Seq[String]]): String = {
		if (value != None) {
			return value.get(0)
		} else {
			return null
		}
	}
}
