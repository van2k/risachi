package controllers

import play.api.mvc._
import java.util.Calendar
import models.enc._
import play.api.data._
import play.api.data.Forms._
import play.api.Logger
import play.api.libs.json._
import scala.util.Random
import java.util.Date
import commons._
import play.api.libs.iteratee.Enumerator
import collection.mutable
import scalikejdbc._

case class SearchItem(menuId: String, formId: String, formItemId: Option[List[String]], formItemValueId: Option[List[String]], andOr: Boolean, ispreview: Boolean)

object EncResult extends Control {
	val form = Form(mapping("menuId" -> text, "formId" -> text, "formItemId" -> optional((list(text))), "formItemValueId" -> optional(list(text)), "andOr" -> boolean, "ispreview" -> boolean)
		(SearchItem.apply)(SearchItem.unapply))

	/**
	 * アンケートレポート1表示
	 * @param menuId メニューID
	 * @param urlKey URLキー
	 * @param ispreview プレビューかどうか
	 */
	def report(menuId: String, urlKey: String, ispreview: Boolean) = withAuthAction {
		implicit request => {
			val reportForm = EncForms.findByUrlKey(urlKey).getOrElse(EncForms.getBlankForm())
			val findItem = EncFormItems.findByFormId(reportForm.formId)
			val findMItem = EncMFormItems.findAll()
			val answers: List[EncAnswers] = EncAnswers.findByFormid(reportForm.formId)
			val isPreviewCondition = if (ispreview) "1" else "0"
			val finishedAnswerCount = answers.count(x => x.isPreview == isPreviewCondition && x.endAt != None)
			val unfinishedAnswerCount = answers.count(x => x.isPreview == isPreviewCondition && x.endAt == None)

			menuId match {
				case "1" => Ok(views.html.encresult.report1(EncForms.find(reportForm.formId).getOrElse(EncForms.getBlankForm()), findItem, findMItem, form.fill(SearchItem(menuId, reportForm.formId.toString(), Option(null), Option(null), true, ispreview)), finishedAnswerCount, unfinishedAnswerCount))
//				case "2" => Ok(views.html.encresult.report2(EncForms.find(formId).get, findItem, findMItem, form.fill(SearchItem(menuId, formId.toString(), Option(null), Option(null), true, ispreview)), finishedAnswerCount, unfinishedAnswerCount))
				case _ => BadRequest
			}
		}
	}

	/**
	 * アンケート結果表示
	 */
	def search(): Action[AnyContent] = Action {
		implicit request => form.bindFromRequest.fold(
			hasErrors => BadRequest(views.html.error("ERROR!!")),
			value => {
				val findItem = EncFormItems.findByFormId(value.formId.toLong)
				//val findTotal = EncAnalyze.aggAnsByFormid(value.formId, form.fill(value).get.ispreview)
				val findMItem = EncMFormItems.findAll()
				val answers: List[EncAnswers] = EncAnswers.findByFormid(value.formId.toLong)
				val isPreviewCondition = if (value.ispreview) "1" else "0"
				val finishedAnswerCount = answers.count(x => x.isPreview == isPreviewCondition && x.endAt != None)
				val unfinishedAnswerCount = answers.count(x => x.isPreview == isPreviewCondition && x.endAt == None)

				value.menuId match {
					case "1" => Ok(views.html.encresult.report1(EncForms.find(value.formId.toLong).get, findItem, findMItem, form.fill(value), finishedAnswerCount, unfinishedAnswerCount))
					case "2" => Ok(views.html.encresult.report2(EncForms.find(value.formId.toLong).get, findItem, findMItem, form.fill(value), finishedAnswerCount, unfinishedAnswerCount))
					case "3" => Ok(views.html.encresult.report3(EncForms.find(value.formId.toLong).get, findItem, findMItem, form.fill(value), finishedAnswerCount, unfinishedAnswerCount))
					case "4" => Ok(views.html.encresult.report4(EncForms.find(value.formId.toLong).get, findItem, findMItem, form.fill(value), finishedAnswerCount, unfinishedAnswerCount))
					case _ => BadRequest
				}
			}
		)
	}

	/*
	 * 設問の値を取得する
	 * @param formId フォームID
	 * @param itemid 設問ID
	 */
	def itemvalues(formId: Long, itemid: Long) = Action {
		val random = new Random

		if (EncForms.find(formId).isDefined) {
			val values = EncFormItemValues.findByFormItemId(itemid)
			val jsondata = values.map {
				x => (x.itemId.toString -> x.formValue)
			}.toMap
			Logger.debug("itemvalues call.")
			Ok(Json.toJson(jsondata))
		} else {
			Logger.debug("itemvalues BadRequest.")
			BadRequest
		}
	}

	/*
	 * 回答の明細を表示する
	 * @itemid: 設問ID
	 */
	def showDetails(itemid: Long) = Action {
		val item = EncFormItems.find(itemid)

		if (item.isDefined) {
			val values = EncAnswerDetails.findByFormitemid(itemid)
			Ok(views.html.encresult.answer_details(item.get.itemId, item.get.name, values.filter(x => x.answerValue.length > 0).toList))
		} else {
			BadRequest
		}
	}

	/*
 * アンケート結果をデータ出力する
 * @param formId フォームID
 */
	def csvOutput = Action {
		implicit request => {
			val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
			val params: Map[String, Seq[String]] = formBody.get
			val formId = params.get("formId")
			val isPreview = params.get("ispreview")

			if (formId.isDefined) {
				val form = EncForms.find(formId.get(0).toLong)
				val config = EncFormConfigs.findByFormid(formId.get(0).toLong)

				if (form.isDefined && config.isDefined) {
					val csv = new StringBuilder()

					if (config.get.outputNumber < config.get.outputLimitNumber) {
						val items = EncFormItems.findByFormId(formId.get(0).toLong)
						val mitems = EncMFormItems.findAll()
						for (item <- items) {
							val values = EncAnalyze.aggAnsByFormidFormitemid(formId.get(0).toLong, item.itemId, isPreview.get(0).toBoolean)
							for (value <- values) {
								csv.append(item.name + "," + value.name + "," + value.total.toString() + "\n")
							}
							val mitem = mitems.filter(x => x.itemId == item.mItemId)
							// テキストボックスの場合は、入力された文字を出力する
							if (mitem.length == 1 && (mitem(0).kind == Constants.QuestionType.TextLine || mitem(0).kind == Constants.QuestionType.TextMultiLine || mitem(0).kind == Constants.QuestionType.TextRegx)) {
								val values = EncAnswerDetails.findByFormitemid(item.itemId)
								for (value <- values) {
									csv.append(",," + value.answerValue + "\n")
								}
							}
						}
						// 出力した回数を増やす
						EncFormConfigs.updateOutputnumber(config.get.id, config.get.outputNumber + 1)

						SimpleResult(
							header = ResponseHeader(200, Map(CONTENT_TYPE -> "text/csv", CONTENT_DISPOSITION -> ("attachment; filename=output.csv"))),
							body = Enumerator(Array(csv.toByte))
						)
					} else {
						Ok(views.html.error("アンケート出力上限に達しています"))
					}
				} else {
					Ok(views.html.error("アンケートが見つかりません"))
				}
			} else {
				Ok(views.html.error("パラメータエラー"))
			}
		}
	}

	/**
	 * 出欠確認票を表示する
	 *
	 * @param formId
	 * @return
	 */
	def showRecordOfAttendance(formId: Long) = Action {
		implicit request => {
			if (EncForms.find(formId).isDefined) {
				val attends = EncAttend.recordOfAttendance(formId)
				val attend_dates = EncAttend.attendDateItems(formId)
				val select_dates: mutable.MutableList[String] = new mutable.MutableList[String]

				for (attend_date <- attend_dates) {
					select_dates += attend_date.date
				}

				if (attends.size != 0) {
					val title = attends.head.title

					var answer_id: String = ""
					var nameTitle: String = ""
					var dateTitle: String = ""
					var answererMap: Map[String, String] = Map.empty[String, String]
					var participations: mutable.MutableList[String] = new mutable.MutableList[String]
					var participationDateMap: Map[String, List[String]] = Map.empty[String, List[String]]
					for (attend: EncAttend <- attends) {
						if (answer_id != attend.answerId.toString) {
							if (answer_id != "") {
								participationDateMap = participationDateMap + (answer_id -> participations.toList)
								participations = new mutable.MutableList[String]
							}
							answer_id = attend.answerId.toString
						}
						attend.nameTitle match {
							case Some(n) => {
								if (nameTitle == "") nameTitle = n
							}
							case _ =>
						}
						attend.dateTitle match {
							case Some(d) => {
								if (dateTitle == "") dateTitle = d
							}
							case _ =>
						}
						attend.answerName match {
							case Some(an) => answererMap = answererMap + (answer_id -> an)
							case _ =>
						}
						attend.answer match {
							case Some(a) => participations += a
							case _ =>
						}
					}
					if (!participationDateMap.contains(answer_id)) {
						participationDateMap = participationDateMap + (answer_id -> participations.toList)
					}

					Ok(views.html.encresult.attend(title, nameTitle, dateTitle, select_dates.toList, answererMap, participationDateMap))
				} else {
					Ok(views.html.caution("まだ回答がありません"))
				}
			} else {
				Ok(views.html.error("パラメータエラー"))
			}
		}
	}

	/*
	 * ダミーデータ作成
	 * TODO 以下のDB関連の処理はService層に移動させたい
	 *
	 * @param formId フォームID
	 * @param datanum データ数
	 */
	def createDummyData(formId: Long, datanum: Int) = withAuthAction {
		implicit request => scalikejdbc.DB localTx {
			implicit con => {
				if (EncForms.find(formId)(con).isDefined) {
					val cal = Calendar.getInstance()
					val formItems = EncFormItems.findByFormId(formId)(con)
					val findMItems = EncMFormItems.findAll()(con)
					val nowdate = cal.getTime

					Logger.debug("nowdate:" + cal.get(Calendar.YEAR).toString() + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DATE))
					for (i <- 1 to datanum) {

						// 回答情報を登録する
						val startAt = randomDate(nowdate, 1000000)
						val answer = EncAnswers(-1, formId, startAt, Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), Option(null), "1", "", "", cal.getTime(), cal.getTime())
						val answerId = EncAnswers.regist(answer)(con)
						var answerAt = startAt

						// 各設問ごとにダミーデータを作成する
						for (formItem <- formItems) {
							answerAt = randomDate(answerAt, 100)
							val findMItem = findMItems.filter(x => x.itemId == formItem.mItemId)

							findMItem(0).itemId.toString() match {
								// チェックボックスのデータを作成
								case Constants.QuestionType.Checkbox => createDummyDataCheckBox(answerId.get, formItem.itemId, answerAt)(con)
								// ラジオボックスのデータを作成
								case Constants.QuestionType.Radio => createDummyDataRadio(answerId.get, formItem.itemId, answerAt)(con)
								// ドロップリストのデータを作成
								case Constants.QuestionType.DropList => createDummyDataDroplist(answerId.get, formItem.itemId, answerAt)(con)
								// １行テキストデータの作成
								case Constants.QuestionType.TextLine => createDummyTextLine(answerId.get, formItem.itemId, answerAt)(con)
								// 複数行テキストデータの作成
								case Constants.QuestionType.TextMultiLine => createDummyTextMultiLine(answerId.get, formItem.itemId, answerAt)(con)
								case _ => Logger.error("dataType match error:" + findMItem(0).dataType)
							}
						}

						// 回答終了
						val updanswer1 = EncAnswers.find(answerId.get)(con)
						val updanswer2 = EncAnswers(answerId.get, formId, answer.startAt, Option(answerAt),
							answer.foreignKey, answer.attribute1, answer.attribute2, answer.attribute3, answer.attribute4, answer.attribute5,
							answer.isPreview, answer.ipAddress, answer.webBrowser, updanswer1.get.createdAt, updanswer1.get.updatedAt)
						EncAnswers.update(updanswer2)(con)
					}
					Ok
				} else {
					Logger.error("formId is not found :" + formId)
					BadRequest
				}
			}
		}
	}

	/*
	 * チェックボックス用のダミーデータを作成する
	 * @param answerid 回答ID
	 * @param itemid 項目ID
	 * @param answerat 回答日時
	 */
	def createDummyDataCheckBox(answerid: Long, itemid: Long, answerat: Date)(implicit con: DBSession = AutoSession) = {
		val random = new Random
		val itemValues = EncFormItemValues.findByFormItemId(itemid)(con).toBuffer

		val idx = random.nextInt(itemValues.length)
		val detail = EncAnswerDetails(-1, answerid, itemid, itemValues(idx).itemId, "", Option(answerat), answerat, answerat)
		EncAnswerDetails.regist(detail)(con)
	}

	/*
	 * ラジオボタン用のダミーデータを作成する
	 * @param answerid 回答ID
	 * @param itemid 項目ID
	 * @param answerat 回答日時
	 */
	def createDummyDataRadio(answerid: Long, itemid: Long, answerat: Date)(implicit con: DBSession = AutoSession) = {
		var random = new Random

		val itemValues = EncFormItemValues.findByFormItemId(itemid)(con)
		val idx = random.nextInt(itemValues.length)
		val detail = EncAnswerDetails(-1, answerid, itemid, itemValues(idx).itemId, "", Option(answerat), answerat, answerat)

		EncAnswerDetails.regist(detail)(con)
	}

	/*
	 * ドロップリスト用のダミーデータを作成する
	 * @param answerid 回答ID
	 * @param itemid 項目ID
	 * @param answerat 回答日時
	 */
	def createDummyDataDroplist(answerid: Long, itemid: Long, answerat: Date)(implicit con: DBSession = AutoSession) = {
		// ラジオボタンと同じ
		createDummyDataRadio(answerid, itemid, answerat)(con)
	}

	/*
	 * テキストのダミーデータを作成する
	 * @param answerid 回答ID
	 * @param itemid 項目ID
	 * @param answerat 回答日時
	 */
	def createDummyTextLine(answerid: Long, itemid: Long, answerat: Date)(implicit con: DBSession = AutoSession) = {
		createDummyTextMultiLine(answerid, itemid, answerat)(con)
	}

	/*
	 * 複数行テキストのダミーデータを作成する
	 * @param answerid 回答ID
	 * @param itemid 項目ID
	 * @param answerat 回答日時
	 * @param linenum 行数
	 */
	def createDummyTextMultiLine(answerid: Long, itemid: Long, answerat: Date)(implicit con: DBSession = AutoSession) = {
		val itemValues = EncFormItemValues.findByFormItemId(itemid)(con)
		val random = new Random
		var text = ""
		var code = 'あ'.toInt

		if (itemValues.length == 1) {
			// 文字数を決める
			val chlen = random.nextInt(itemValues(0).chLength.toInt)
			for (i <- 0 to chlen) {
				text += (code + random.nextInt(80)).toChar
			}
			val detail = EncAnswerDetails(-1, answerid, itemid, itemValues(0).itemId, text, Option(answerat), answerat, answerat)

			EncAnswerDetails.regist(detail)(con)
		}
	}

	/*
	 * ランダムな日付を取得
	 */
	def randomDate(dt: Date, randomind: Int): Date = {
		var random = new Random
		val cal = Calendar.getInstance()

		cal.setTime(dt)
		cal.add(Calendar.SECOND, random.nextInt(randomind))
		val ret = cal.getTime()

		return ret
	}
}
