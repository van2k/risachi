package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import views._
import models.contract._


object Contract extends Controller {

	// 仮登録フォーム
	val registForm = Form(
		mapping(
			"contractId" -> ignored(None: Option[Long]),
			"mail" -> email.verifying("指定されたメールアドレスは既に使用されています。", {
				mail =>
					Contracts.findByMail(mail) match {
						case Some(contracts) => contracts.status == Contracts.STATUS_INTERIM || contracts.status == Contracts.STATUS_WITHDRAWN
						case _ => true
					}
			}),
			"agree" -> checked("利用規約に同意しないと登録出来ません。")
		)((contractId, mail, _) => Contracts(contractId, mail, "1", ""))((c: Contracts) => Some(c.contractId, c.mail, false))
	)

	// 仮登録確認フォーム
	val confirmForm = Form(
		mapping(
			"contractId" -> ignored(None: Option[Long]),
			"mail" -> email.verifying("指定されたメールアドレスは既に使用されています。", {
				mail =>
					Contracts.findByMail(mail) match {
						case Some(contracts) => contracts.status == Contracts.STATUS_INTERIM || contracts.status == Contracts.STATUS_WITHDRAWN
						case _ => true
					}
			})
		)((contractId, mail) => Contracts(contractId, mail, "1", ""))((c: Contracts) => Some(c.contractId, c.mail))
	)

	// プロフィールフォーム
	val profileForm = Form(
		mapping(
			"contractId" -> ignored(None: Option[Long]),
			"mail" -> email,
			"name" -> nonEmptyText(1, 64),
			"pass" -> tuple("main" -> nonEmptyText(6, 64), "confirm" -> text).verifying(
				// パスワードの入力ルール定義
				"パスワードが一致しません。", passwords => passwords._1 == passwords._2)
		)((contractId, mail, name, pass) => Contracts(contractId, mail, "1", name))((c: Contracts) => Some(c.contractId, c.mail, c.name, ("", "")))
	)

	// パスワードを忘れたときに入力する新しいパスワードのフォーム
	val forgotPassNewPassForm = Form(
		mapping(
			"pass" -> tuple("main" -> text(minLength = 6), "confirm" -> text).verifying(
				// パスワードの入力ルール定義
				"パスワードが一致しません。", passwords => passwords._1 == passwords._2)
		)((pass) => pass._1)((p: String) => Some((p, "")))
	)

	def regist = Action {
		implicit request => {
			Ok(html.contract.regist(registForm))
		}
	}

	def confirm = Action {
		implicit request =>
			registForm.bindFromRequest.fold(
				// Form has errors, redisplay it
				formWithErrors => BadRequest(html.contract.regist(formWithErrors)),
				contracts => Ok(html.contract.confirm(contracts))
			)
	}

	def backRegist(mail: String) = Action {
		implicit request => {
			val filledForm = registForm.fill(Contracts(None, mail, Contracts.STATUS_INTERIM, ""))
			Ok(html.contract.regist(filledForm))
		}
	}

	def interimRegist = Action {
		implicit request =>
			confirmForm.bindFromRequest.fold(
				formWithErrors => Ok(html.contract.error("", "不明なエラーが発生しました。")),
				contracts => {
					if (services.Contract.tempRegist(contracts.mail, request)) Ok(html.contract.interimRegist())
					else Ok(html.contract.error(contracts.mail, "このメールアドレスはすでに使用されています。"))
				}
			)
	}

	def profileRegist(token: String) = Action {
		implicit request => {
			Logger.debug(token)

			// トークンからメールアドレスを取得する
			// もし有効なメールアドレスがなかったらエラー画面を表示する
			Contracts.findInterimContractByToken(token) match {
				case Some(contracts) => Ok(html.contract.profileRegist(profileForm.fill(Contracts(contracts.contractId, contracts.mail, contracts.status, "")), token))
				case _ => Ok(html.contract.error("", "このURLは有効期限が切れています。最初からやり直してください。"))
			}
		}
	}

	def backProfileRegist(mail: String, name: String, token: String) = Action {
		implicit request => {

			val filledForm = profileForm.fill(Contracts(None, mail, Contracts.STATUS_INTERIM, name))
			Ok(html.contract.profileRegist(filledForm, token))
		}
	}


	def profileConfirm = Action {
		implicit request =>
		// パラメータ取得
			val params = request.body.asFormUrlEncoded.get
			val token = params.get("token").getOrElse(List("")).apply(0)
			val pass = params.get("pass.main").getOrElse(List("")).apply(0)

			profileForm.bindFromRequest.fold(
				formWithErrors => BadRequest(html.contract.profileRegist(formWithErrors, token)),
				contracts => {
					// パスワードはこの段階でDBに保存する
					Contracts.setPassByMail(contracts.mail, pass)

					Ok(html.contract.profileConfirm(contracts, token))
				}
			)
	}

	def profileSubmit = Action {
		implicit request =>
		// パラメータ取得
			val params = request.body.asFormUrlEncoded.get
			val token = params.get("token").getOrElse(List("")).apply(0)
			val name = params.get("name").getOrElse(List("")).apply(0)

			services.Contract.Regist(token, name) match {
				// エラー
				case Some(message) => Ok(html.contract.error("", message))
				// 正常
				case _ => Ok(html.contract.profileSubmit())
			}
	}

	def newPassword(token: String) = Action {
		implicit request => {
			// パスワードに変更して、新しいパスワードを取得
			val password: String = services.Contract.changePassword(token)
			if (password == null) {
				Ok(html.contract.newPasswordError("ご指定のURLは無効です。"))
			} else {
				Ok(html.contract.newPassword(password))
			}
		}
	}

	def forgotPass = Action {
		implicit request =>
		// パスワードを忘れたときの画面
			Ok(html.contract.forgotPass())
	}

	def forgotPassMailSubmit = Action {
		implicit request =>
		// パラメータ取得
			val params = request.body.asFormUrlEncoded.get
			val mail = params.get("mail").getOrElse(List("")).apply(0)
			if (mail == "") {
				Ok(html.contract.forgotPass("メールアドレスを入力してください。"))
			} else {
				services.Contract.forgotPassSendMail(mail, request) match {
					case true => Ok(html.contract.forgotPassMailSubmit())
					case _ => Ok(html.contract.forgotPass("このメールアドレスは登録されていません。"))
				}
			}
	}

	def forgotPassNewPass(token: String) = Action {
		implicit request =>
		// 指定されたトークンに対応する契約者が存在するか
			Contracts.findByToken(token) match {
				case Some(contract) => Ok(html.contract.forgotPassNewPass(forgotPassNewPassForm, token))
				case _ => Ok(html.contract.forgotPassError("ご指定のURLは無効です。"))
			}
	}

	def forgotPassNewPassSubmit = Action {
		implicit request =>
		// パラメータ取得
			val params = request.body.asFormUrlEncoded.get
			val token = params.get("token").getOrElse(List("")).apply(0)

			forgotPassNewPassForm.bindFromRequest().fold(
				formWithErrors => BadRequest(html.contract.forgotPassNewPass(formWithErrors, token)),
				pass => {
					services.Contract.forgotPassChangePass(token, pass) match {
						case true => Ok(html.contract.forgotPassNewPassSubmit())
						case _ => Ok(html.contract.forgotPassError("ご指定のURLは無効です。"))
					}
				}
			)
	}
}
