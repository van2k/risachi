package controllers

import play.api._
import mvc._
import views._
import models.contract.Contracts
import java.util.Calendar

object Login extends Control {

	def index = Action {
		implicit request => {
			val contract = getContractLogin()
			if (contract.isDefined) {
        // ログイン済みなので、フォーム一覧画面にリダイレクト
        contract.get.lastLoginAt match {
          case Some(l) => Redirect(routes.EncConf.list)
          case None => Redirect(routes.EncConf.tutorial)
        }
			} else {
				Ok(html.login.index())
			}
		}
	}

	def login = Action {
    implicit request => {
      val formBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
      val params: Map[String, Seq[String]] = formBody.get

      val userid = params("userid").head
      val password = params("password").head

      val contract = Contracts.findByMailAndPass(userid, password)
      Logger.debug("contract:" + contract.toString)
      if (contract.isDefined && contract.head.status == Contracts.STATUS_REGISTERED) {
        // 最終ログイン日付を更新
        Contracts.updateLastLoginAt(contract.get.contractId.getOrElse(-1), Calendar.getInstance().getTime())
        contract.get.lastLoginAt match {
          case Some(l) => Redirect(routes.EncConf.list).withSession(
            setContractLogin(Option(contract.head.contractId.getOrElse(-1)))
          )
          case None => {
            // チュートリアル用にフォームを1件作成
            services.User.registEnc(contract.head.contractId.getOrElse(-1), 4, "チュートリアル用アンケートフォーム", "チュートリアル用のアンケートフォームです。このフォームはチュートリアル後削除してください。", "ここに回答後メッセージを入力してください。")
            Redirect(routes.EncConf.tutorial).withSession(
              setContractLogin(Option(contract.head.contractId.getOrElse(-1)))
            )
          }
        }
      } else {
        Ok(html.login.index("ID、又は、パスワードが異なります"))
      }
    }
  }

	def logout = Action {
		implicit request => {
			// withNewSessionでログオフする
			Redirect(routes.Application.index).withNewSession
		}
	}
}
