package controllers

import play.api.mvc._
import views._

object Application extends Control {

	def index = Action {
		implicit request => {
			Ok(html.index("", (!getContractLogin().isDefined)))
		}
	}

}
