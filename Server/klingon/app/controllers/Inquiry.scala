package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import views._
import commons.Constants
import models.contract.Contracts

object Inquiry extends Control {

	case class Inquiry(
		                  subject: String,
		                  mail: String,
		                  name: String,
		                  content: String
		                  )

	val inquiryForm = Form(
		mapping(
			"subject" -> nonEmptyText(maxLength = 128),
			"mail" -> email,
			"name" -> nonEmptyText(1, 64),
			"content" -> nonEmptyText(1, 2000)
		)(Inquiry.apply)(Inquiry.unapply)
	)

	def index = Action {
		implicit request => {
			val contract = getContractLogin()
			if (contract.isDefined) {
				val contractid = session.get(Constants.SESSION_CONTRACT_ID)
				val contract = Contracts.findById(contractid.head.toLong)
				val contract_mail = contract.head.mail
				val contract_name = contract.head.name
				Ok(html.inquiry.index(inquiryForm.fill(Inquiry("", contract_mail, contract_name, ""))))
			} else {
				Ok(html.inquiry.index(inquiryForm))
			}
		}
	}

	def confirm = Action {
		implicit request =>
			inquiryForm.bindFromRequest.fold(
				formWithErrors => BadRequest(html.inquiry.index(formWithErrors)),
				inquiry => {
					Ok(html.inquiry.confirm(inquiry))
				}
			)
	}

	def submit = Action {
		implicit request =>
			inquiryForm.bindFromRequest.fold(
				formWithErrors => BadRequest(html.inquiry.index(formWithErrors)),
				inquiry => {
					val formbBody: Option[Map[String, Seq[String]]] = request.body.asFormUrlEncoded
					val params: Map[String, Seq[String]] = formbBody.get
					if (params.getOrElse("send", "") != "") {
						val contract = getContractLogin()
						if (contract.isDefined) {
							val contractid = session.get(Constants.SESSION_CONTRACT_ID)
							val contract = Contracts.findById(contractid.head.toLong)
							val contract_id = contract.head.contractId.get.toString.toLong
							services.Contract.sendInquiry(inquiry.subject, inquiry.mail, inquiry.name,
								inquiry.content, models.Inquiry.InquiryStatus.UNSUPPORTED, contract_id, true)
						} else {
							services.Contract.sendInquiry(inquiry.subject, inquiry.mail, inquiry.name,
								inquiry.content, models.Inquiry.InquiryStatus.UNSUPPORTED, 0, false)
						}
						Ok(html.inquiry.submit())
					} else {
						val filledForm = inquiryForm.fill(Inquiry(inquiry.subject, inquiry.mail, inquiry.name, inquiry.content))
						Ok(html.inquiry.index(filledForm))
					}
				}
			)
	}

}