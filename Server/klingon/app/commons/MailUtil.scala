package commons

import org.apache.commons.mail.SimpleEmail

object MailUtil {

	def sendMail(message: String, subject: String, to: String, from: String = "no-reply@van2k.com") = {
		try {
			val mailer = new SimpleEmail
      // TODO 利用可能なメールサーバの設定を指定してください
			mailer.setHostName("mail.van2k.com")
			mailer.setAuthentication("contract@van2k.com", "************")
			mailer.setDebug(true)
			mailer.setSmtpPort(587)
			mailer.setCharset("ISO-2022-JP")
			mailer.addHeader("X-Mailer", "SimpleEmail")
			mailer.addTo(to)
			mailer.setFrom(from)
			mailer.setSubject(subject)
			mailer.setContent(message, "text/plain; charset=ISO-2022-JP")
			mailer.send()

			true
		} catch {
			case e: Exception => false
		}
	}
}
