package commons

/**
 * 定数クラス
 */
object Constants {
	// サービス名
	val SERVICE_NAME = "りさっち　α"

	// 契約者ID
	val SESSION_CONTRACT_ID = "contractid"

	// 管理者ID
	val SESSION_ADMIN_ID = "adminid"

	// アンケートタイトルの上限文字数
	val encTitleLength: Int = 100

	// アンケート説明の上限文字数
	val encDetailLength: Int = 2500

	// 質問の文字数
	val questionTitleLength: Int = 200

	// 質問項目の文字列長
	val questionItemLength: Int = 200

	// CSRF用SALT
	val CSRF_SALT = "r5B_%a-RMW(6"

	// 比較値区分
	object ConditionType {
		// a = b
		val Equal = "1"

		// a >= b
		val MoreEqual = "2"

		// a <= b
		val LessEqual = "3"
	}

	// アンケート項目区分
	object QuestionType {
		// テキストボックス定義値
		val TextLine = "1"

		// テキストエリア定義値
		val TextMultiLine = "2"

		// ラジオボタン定義値
		val Radio = "3"

		// チェックボックス定義値
		val Checkbox = "4"

		// ドロップダウンリスト定義値
		val DropList = "5"

		// 日付型定義値
		val TextDate = "6"

		// 正規表現型定義値
		val TextRegx = "7"
	}

	// ロゴファイルのファイルサイズ上限(1M)
	val logoFileLimitSize: Int = 1048576

	// ロゴファイルの横幅サイズ上限（500px）
	val logoFileLimitWidth = 500

	// 料金プラン名
	object ChargePlan {

		// 松
		val plan1 = "松"

		// 竹
		val plan2 = "竹"

		// 梅
		val plan3 = "梅"
	}

	// オプション料金プラン区分
	object ChargeOptionType {
		// 収集件数
		val collect_number = "1"
		// 回答期間
		val term = "2"
		// ページ数
		val page_num = "3"
		// 項目数
		val item_num = "4"
		// 保存期間
		val save_term = "5"
		//  CSV出力回数
		val output_number = "6"
	}

}