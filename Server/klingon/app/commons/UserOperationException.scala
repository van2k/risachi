package commons

/**
 * 同時実行例外
 */
class UserOperationException(msg: String) extends Exception {
	val _msg = msg
}
