package services

import play.api._
import play.api.mvc._
import java.util.{Date, Calendar}
import models.enc._
import play.api.Play.current
import scalikejdbc.{AutoSession, DB, DBSession}
import models.charge.{ChargeOptionPlans, ChargePlans}
import models.contract.Contracts
import models.Information

/*
 * 利用者サービス
 */
object User {
	/* アンケート投稿画面 */
	// アンケートを表示する
	def dispEnc() = {

	}

  /**
   * アンケートフォームを新規に登録する。
   *
   * @param contractId 契約者ID
   * @param planId プランID
   * @return
   */
  def registEnc(contractId: Long, planId: Int, formName: String = "", formDesc: String = "", resultMessage: String = "") = {
    DB localTx {
      implicit con => {
        val cal = Calendar.getInstance()
        val now = cal.getTime()
        val totalPageCount = 1

        val cssId: Option[Long] = Option(0)
        val orgCss = Option("")
        val imageUrl = Option("")
        var urlKey = EncForms.getNewUrlKey
        val plancharge = ChargePlans.findById(planId)

        val regForms = EncForms(-1, contractId, formName, formDesc, resultMessage, totalPageCount, cssId, orgCss, imageUrl, EncForms.STATUS_EDIT, urlKey, cal.getTime, cal.getTime)
        val newId = EncForms.regist(regForms)(con)
        val curForms = EncForms(newId.get, regForms.contractId, regForms.formName, regForms.formDesc, regForms.resultMessage
          , regForms.totalPageCount, regForms.cssId, regForms.orgCss, regForms.imageUrl, EncForms.STATUS_EDIT, regForms.urlKey, regForms.createdAt, regForms.updatedAt)

        cal.set(3000, 11, 31)
        val enddt = cal.getTime()
        val regConfig = EncFormConfigs(0, newId.get, 0, now, Option(enddt), "0", 0, plancharge.get.term.toInt, plancharge.get.outputNumber.toInt, Option(0), Option(plancharge.get.charge.toInt), Option(0), Option(null), Option(planId.toLong), "0", now, now)
        EncFormConfigs.regist(regConfig)(con)
        (curForms, EncMFormItems.findAll()(con), EncMFormItemValues.findAll()(con), regConfig, plancharge.get)
      }
    }
  }

  /**
   * アンケートフォーム情報を更新する。
   *
   * @param contractId 契約者ID
   * @param planId 料金プランID
   * @param formId フォームID
   * @param params URLパラメータ一覧
   * @return
   */
  def updateEnc(contractId: Long, planId: Int, formId: Long, params: Map[String, Seq[String]]): (EncForms, List[EncFormItems], List[EncFormItemValues], List[EncMFormItems], List[EncMFormItemValues], EncFormConfigs) = {
    DB localTx {
      implicit con => {
        val form = EncForms.findByIdContractid(formId, contractId)(con)
        val config = EncFormConfigs.findByFormid(formId)(con)
        val plancharge = ChargePlans.findById(planId)
        val now = Calendar.getInstance().getTime()

        if (form.isDefined && config.isDefined) {
          val formItems = EncFormItems.findByFormId(formId)(con)
          val formItemValues = EncFormItemValues.findByFormId(formId)(con)

          val regConfig = EncFormConfigs(config.get.id, config.get.formId, config.get.collectNumber,
            config.get.startSt, config.get.endSt, config.get.stop, config.get.outputNumber, config.get.term,
            plancharge.get.outputNumber.toInt, config.get.saveTerm, Option(plancharge.get.charge.toInt), config.get.paymentConfirm,
            config.get.paymentConfirmDate, Option(planId.toLong), config.get.encResultOpen, config.get.createdAt, config.get.updatedAt)
          EncFormConfigs.update(regConfig)(con)

          updateFormConfigValues(formId.toLong, params, now)(con)

          (form.getOrElse(EncForms.getBlankForm()), formItems, formItemValues, EncMFormItems.findAll()(con), EncMFormItemValues.findAll()(con), regConfig)
        } else {
          (EncForms.getBlankForm(), List.empty[EncFormItems], List.empty[EncFormItemValues], List.empty[EncMFormItems], List.empty[EncMFormItemValues], EncFormConfigs.getBlankEncFormConfig())
        }
      }
    }
  }

  /**
   * アンケート設定項目値を登録
   * @param formId:アンケートID
   * @param params:GET/POSTデータ
   * @param currentDate: Date
   * @return
   */
  def updateFormConfigValues(formId: Long, params: Map[String, Seq[String]], currentDate: Date)(con:DBSession = AutoSession) {
    val planoptions = ChargeOptionPlans.findAll()(con)
    // 選択されたオプションを登録する
    EncFormConfigValues.deleteByFormId(formId)(con)

    val currentDate = Calendar.getInstance().getTime

    for (record <- planoptions) {
      val num = params.getOrElse("voptionNum_" + record.id, List("0")).head
      if (num.toInt > 0) {
        val configval = EncFormConfigValues(-1, formId, record.id, num.toInt, record.charge, record.charge * num.toInt, currentDate, currentDate)
        EncFormConfigValues.regist(configval, currentDate)(con)
      }
    }
  }


  /**
   * アンケート一覧表示に必要な情報を取得します。
   *
   * @param contractId 契約者ID
   * @return
   */
  def dispEncList(contractId: Long): (List[EncForms], List[Information], List[EncFormConfigs], List[ChargePlans]) = {
    // 一覧の取得
    val list = EncForms.findByContractidNotstatus(contractId, EncForms.STATUS_DELETE)
    val infoList = models.Information.list()
    val encFormConfigs = EncFormConfigs.findByContractid(contractId)
    val chargePlans = ChargePlans.findAll()

    (list, infoList, encFormConfigs, chargePlans)
  }

	// アンケートを投稿する
	def entryEnc(answerId: Long, pageNo: Long, values: Map[String, Seq[String]]) = {
		DB localTx {
			implicit con => {

				// POSTデータからEncFormItems->idと、EncFormItemValuesの値を取得する
				for ((k, v) <- values) {
					Logger.debug(k + "=" + v)
					val p = k.split("-")

					val itemId = p(0)
					if (p.length == 2) {
						val valueId = p(1)
						for (value <- v) {
							Logger.debug("patter1 itemId:" + itemId + " valueId:" + valueId + " value:" + value)
							saveAnswerDetails(answerId, itemId.toLong, valueId.toLong, value)(con)
						}
					} else {
						for (value <- v) {
							Logger.debug("patter2 itemId:" + itemId + " valueId:" + value + " value:" + value)
							saveAnswerDetails(answerId, itemId.toLong, if (value.length() > 0) value.toLong else 0, value)(con)
						}
					}
				}
			}
		}
	}

	private def saveAnswerDetails(answerId: Long, itemId: Long, valueId: Long, value: String)(implicit connection: DBSession) {
		val detail = EncAnswerDetails.findByAnsweridFormitemid(answerId, itemId)(connection)
		val cal = Calendar.getInstance()

		if (detail.length == 0) {
			Logger.debug("add answerId:" + answerId + ",itemId:" + itemId)
			val cal = Calendar.getInstance()
			val record = EncAnswerDetails(-1, answerId, itemId, valueId, value, Option(null), cal.getTime(), cal.getTime())
			EncAnswerDetails.regist(record)(connection)
		} else {
			Logger.debug("update answerId:" + answerId + ",itemId:" + itemId)
			val record = EncAnswerDetails(detail(0).id, detail(0).answerId, itemId, valueId, value, detail(0).answerAt, detail(0).createdAt, detail(0).updatedAt)
			EncAnswerDetails.update(record)(connection)
		}
	}

	/**
	 * 画像のURL、料金プランをDBに追加する
	 *
	 * @param formId
	 * @param imageUrl
	 * @return
	 */
	def saveLogo(formId: Long, imageUrl: String) = {
		DB localTx {
			implicit con => {
				// DBに画像URLを保存
				EncForms.updateImage(formId, imageUrl)(con)
				// TODO 料金部分を変更する

			}
		}
	}

	/**
	 * 画像のURL、料金プランをDBから削除する
	 *
	 * @param formId
	 * @return
	 */
	def deleteLogo(formId: Long) = {
		DB localTx {
			implicit con => {
				// DBに画像URLを保存
				EncForms.updateImage(formId, "")(con)
				// TODO 料金部分を変更する
			}
		}
	}

	/**
	 * CSSの適用ID情報を更新する
	 *
	 * @param formId
	 * @param cssId
	 * @return
	 */
	def updateCss(formId: Long, cssId: Long, orgCss: String = null) = {
		// cssIDを更新
		EncForms.updateCss(formId, cssId, orgCss)
	}

	/**
	 * CSSのコードを取得する。
	 *
	 * @param cssId
	 * @return
	 */
	def getCssCode(formId: Long, cssId: Long) = {
		EncMCss.findById(cssId) match {
			case Some(css) => css.cssCode
			case _ => {
				if (cssId == 99) {
					EncForms.find(formId) match {
						case Some(form) => form.orgCss.get
						case _ =>
					}
				}
			}
		}
	}
}
