package services

import java.util.Date
import models.DbUpdateResult
import play.api.Logger
import play.api.Play.current
import models.exception.DbExecuteFailException
import scalikejdbc.DB
import models.enc._


/**
 * アンケートのページ処理に関するCRUDを管理するBizロジック
 */
object EncFormPage {

	/**
	 * ページを追加します。
	 * @param formId フォームID
	 * @param allPageCount 設定する総ページ数
	 * @param formUpdatedAt フォーム更新日
	 * @param currentDate 現在日付
	 */
	def add(formId: Long, allPageCount: Int, formUpdatedAt: Date, currentDate: Date): Boolean = {

		// ページの更新処理
		EncForms.updatePageNum(formId, formUpdatedAt, allPageCount, currentDate) match {
			case DbUpdateResult.dbConcurrency => {
				Logger.warn("updatePageNum.updateResult : dbConcurrency")
				return false
			}
			case DbUpdateResult.fail => {
				Logger.error("updatePageNum.updateResult : fail")
				throw new DbExecuteFailException
			}
			case _ => {
				Logger.info("updatePageNum.updateResult : success")
				true
			}
		}
	}

	/**
	 * ページを削除します。
	 * @param formId フォームID
	 * @param pageNo ページ番号
	 * @param allPageCount 設定するページ数
	 * @param formUpdatedAt フォームの更新日付
	 * @param currentDate 現在日付
	 * @return
	 */
	def delete(formId: Long, pageNo: Int, allPageCount: Int, formUpdatedAt: Date, currentDate: Date): Boolean = {
		DB localTx {
			implicit con => {

				// ページの更新処理
				EncForms.updatePageNum(formId, formUpdatedAt, allPageCount, currentDate)(con) match {
					case DbUpdateResult.dbConcurrency => {
						Logger.warn("updatePageNum.updateResult : dbConcurrency")
						return false
					}
					case DbUpdateResult.fail => {
						Logger.error("updatePageNum.updateResult : fail")
						throw new DbExecuteFailException
					}
					case _ => {
						Logger.info("updatePageNum.updateResult : success")
						true
					}
				}

				// 質問項目に変更があった為これまでの回答情報を削除する
				EncAnswers.findByFormid(formId)(con).foreach(
					encAnswer => {
						EncAnswers.deleteById(encAnswer.id)(con)
						EncAnswerDetails.deleteByAnswerId(encAnswer.id)(con)
					}
				)

				// ページ番号からformItemを削除
				EncFormItemValues.deleteByFormItemIdPageNo(formId, pageNo)(con) match {
					case DbUpdateResult.dbConcurrency => {
						Logger.warn("deleteByFormItemIdPageNo.updateResult : dbConcurrency")
						return false
					}
					case DbUpdateResult.fail => {
						Logger.error("deleteByFormItemIdPageNo.updateResult : fail")
						throw new DbExecuteFailException
					}
					case _ => {
						Logger.info("deleteByFormItemIdPageNo.updateResult : success")
						true
					}
				}

				EncFormItems.deleteByPageNo(formId, pageNo)(con) match {
					case DbUpdateResult.dbConcurrency => {
						Logger.warn("deleteByPageNo.updateResult : dbConcurrency")
						return false
					}
					case DbUpdateResult.fail => {
						Logger.error("deleteByPageNo.updateResult : fail")
						throw new DbExecuteFailException
					}
					case _ => {
						Logger.info("deleteByPageNo.updateResult : success")
						true
					}
				}
			}
		}
	}
}
