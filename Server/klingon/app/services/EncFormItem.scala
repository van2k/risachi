package services

import collection.mutable
import java.util.Date
import play.api.Logger
import play.api.Play.current
import collection.mutable.ListBuffer
import models.DbUpdateResult
import models.exception.DbExecuteFailException
import models.Exceptions.DbConcurrencyException
import java.sql.SQLException
import scalikejdbc.DB
import models.enc._
import models.enc.EncFormItemServiceUpdateResult


/**
 * アンケートの質問に関するCRUDを管理するBizロジッククラス
 */
object EncFormItem {

  /**
   * 質問について登録・更新を行います。
   * @param formItems 登録・更新対象の質問
   * @param isRegist 登録処理の場合はTrue
   * @param questionItemMap 質問の各項目
   * @return EncFormItemsのインスタンスと各項目Idのリストを保持するオブジェクトを返します。ただし、競合が発生した場合はNullを返します。
   */
  def update(formItems: EncFormItems, ansInputMaxLength: Int, formUpdatedAt: Date, currentDate: Date, allPageCount: Int, isRegist: Boolean
             , questionItemMap: mutable.Map[String, String]): EncFormItemServiceUpdateResult = {
    DB localTx {
      implicit con => {

        var updateResult = DbUpdateResult.success

        // ページ数を更新する
        val encFormRet = EncForms.updatePageNum(formItems.formId, formUpdatedAt, allPageCount, currentDate)(con)
        if (encFormRet == DbUpdateResult.dbConcurrency) {
          Logger.debug("updateResult : dbConcurrency")
          return null
        }

        if (encFormRet == DbUpdateResult.fail) {
          Logger.debug("updateResult : fail")
          throw new DbExecuteFailException
        }

        // 更新日時が設定されていない場合は新規登録
        val curEncFormItems = if (isRegist) {

          Logger.debug("EncFormItems.regist is start!")

          // 登録処理
          val ret = EncFormItems.regist(formItems, currentDate)(con)
          Logger.debug("ret:" + ret)

          val itemId = EncFormItems.findByFormIdCreateAt(formItems.formId, currentDate)(con).itemId
          Logger.debug("itemId:" + itemId)

          formItems.copy(itemId = itemId)
        } else {

          Logger.debug("EncFormItems.update is start!")

          // 更新処理
          updateResult = EncFormItems.update(formItems, currentDate)(con)
          formItems.copy(updatedAt = currentDate)
        }

        // 競合エラーの場合は戻り値をNullとする
        if (updateResult == DbUpdateResult.dbConcurrency) {
          Logger.debug("updateResult : dbConcurrency")
          return null
        }

        // エラーの場合は処理続行不可のため例外をスローする
        if (updateResult == DbUpdateResult.fail) {
          Logger.debug("updateResult : fail")
          throw new DbExecuteFailException
        }

        // 削除処理を行う
        if (!isRegist) {
          EncFormItemValues.deleteByFormItemId(curEncFormItems.itemId)(con)
        }
	      // 質問項目に変更があった為これまでの回答情報を削除する
	      EncAnswers.findByFormid(formItems.formId)(con).foreach(
		      encAnswer => {
			      EncAnswers.deleteById(encAnswer.id)(con)
			      EncAnswerDetails.deleteByAnswerId(encAnswer.id)(con)
		      }
	      )

	      var sortedkeys = questionItemMap.keys.toList.map(x => x.toInt).toList.sorted
        sortedkeys.foreach {
          key =>
            val formItemValue = EncFormItemValues(-1, curEncFormItems.itemId, questionItemMap(key.toString), ansInputMaxLength, currentDate, currentDate)
            Logger.debug("EncFormItemValues.regist is start! key:" + key + "," + questionItemMap(key.toString))
            EncFormItemValues.regist(formItemValue, curEncFormItems.createdAt)(con)
        }

        val formItemValues = EncFormItemValues.findByFormItemIdCreateAt(curEncFormItems.itemId, curEncFormItems.createdAt)(con)
        val itemListIds: ListBuffer[String] = ListBuffer()
        formItemValues.foreach {
          formItemValue => {
            Logger.debug("itemId2:" + formItemValue.itemId)
            itemListIds.append(formItemValue.itemId.toString)
          }
        }

        // 結果を返す
        EncFormItemServiceUpdateResult(curEncFormItems, itemListIds.toList)
      }
    }
  }

  /**
   * 質問を削除します。
   * @param formItemId 削除対象の質問ID
   * @param formId 質問のフォームID
   * @param updatedAt 削除対象の質問更新日付
   * @param formUpdatedAt 質問のフォーム更新日付
   * @param allPageCount 全体ページ数
   * @param currentDate 現在日付
   * @return 正常終了の場合はTrue
   */
  def delete(formItemId: Long, formId: Long, updatedAt: Date, formUpdatedAt: Date, allPageCount: Int, currentDate: Date): Boolean = {
    DB localTx {
      implicit con => {

        try {
          // ページの更新処理
          EncForms.updatePageNum(formId, formUpdatedAt, allPageCount, currentDate)(con) match {
            case DbUpdateResult.dbConcurrency => {
              Logger.warn("updatePageNum.updateResult : dbConcurrency")
              return false
            }
            case DbUpdateResult.fail => {
              Logger.error("updatePageNum.updateResult : fail")
              throw new DbExecuteFailException
            }
            case _ => {
              Logger.info("updatePageNum.updateResult : success")
              true
            }
          }
        } catch {
          case e: DbConcurrencyException => {

            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown DbConcurrencyException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            return false
          }
          case e: SQLException => {
            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown SQLException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            throw new DbExecuteFailException
          }
          //          case e:Exception =>{
          //            Logger.error("thrown Exception message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
          //            throw e
          //          }
        }

        try {
          // 質問の削除
          val items = EncFormItems.find(formItemId)
          EncFormItems.delete(formItemId, items.head.updatedAt)(con) match {
            case DbUpdateResult.dbConcurrency => {
              Logger.warn("delete.updateResult : dbConcurrency")
              return false
            }
            case DbUpdateResult.fail => {
              Logger.error("delete.updateResult : fail")
              throw new DbExecuteFailException
            }
            case _ => {
              Logger.info("delete.updateResult : success")
              true
            }
          }
        } catch {
          case e: DbConcurrencyException => {

            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown DbConcurrencyException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            return false
          }
          case e: SQLException => {
            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown SQLException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            throw new DbExecuteFailException
          }
          //          case e:Exception =>{
          //            Logger.error("thrown Exception message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
          //            throw e
          //          }
        }

        try {
          // 質問項目の削除
          EncFormItemValues.deleteByFormItemId(formItemId)(con) match {
            case DbUpdateResult.dbConcurrency => {
              Logger.warn("deleteByFormItemId.updateResult : dbConcurrency")
              return false
            }
            case DbUpdateResult.fail => {
              Logger.error("deleteByFormItemId.updateResult : fail")
              throw new DbExecuteFailException
            }
            case _ => {
              Logger.info("deleteByFormItemId.updateResult : success")
              true
            }
          }
        } catch {
          case e: DbConcurrencyException => {

            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown DbConcurrencyException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            return false
          }
          case e: SQLException => {
            // note:Rollbackは例外がスローされると自動的に実行される
            Logger.error("thrown SQLException message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
            throw new DbExecuteFailException
          }
          //          case e:Exception =>{
          //            Logger.error("thrown Exception message:" + e.getMessage + "\nstackTrace:" + e.getStackTrace)
          //            throw e
          //          }
        }

	      // 質問項目に変更があった為これまでの回答情報を削除する
	      EncAnswers.findByFormid(formId)(con).foreach(
	        encAnswer => {
		        EncAnswers.deleteById(encAnswer.id)(con)
		        EncAnswerDetails.deleteByAnswerId(encAnswer.id)(con)
	        }
	      )
	      true
      }
    }
  }
}
