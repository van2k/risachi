package services

import play.api._
import play.api.mvc._
import java.util.Calendar

import models.contract._
import models.enc._
import util.Random
import controllers.routes
import commons.MailUtil
import java.sql.Connection
import models.DbUpdateResult
import scalikejdbc.{DB, DBSession}
import scala.util.control.Breaks.{break, breakable}

/*
 * 契約者サービス
 */
object Contract {
	/**
	 * テスト実行時にメールが送信されないように制御する為のフラグ
	 * テスト実行時はtrueに変更した上で処理を行うこと。
	 */
	var testMode = false

	/**
	 * トークンを生成する
	 */
	def createRegistToken(): String = {
		val rand = new Random
		val token_bean = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXTZ"
		var regist_token = ""
		for (i <- 1 to 64) {
			regist_token = regist_token + token_bean(rand.nextInt(token_bean.length))
		}
		regist_token
	}

	/* 登録画面 */
	// 契約者を仮登録する
	def tempRegist(mail: String, request: play.api.mvc.RequestHeader): Boolean = {
    // 登録用トークンの生成
    val regist_token = createRegistToken()

		// トークンの有効期限はとりあえず1時間とする
		val cal = Calendar.getInstance()
		cal.add(Calendar.HOUR, 1)
		val token_limit = cal.getTime

		// DBに登録
		if (Contracts.tempRegist(mail, regist_token, token_limit)) {
			// メール送信
			val message = views.txt.contract.confirmMail(routes.Contract.profileRegist(regist_token).absoluteURL()(request), routes.Inquiry.index.absoluteURL()(request)).toString()
			if (!testMode)
				MailUtil.sendMail(message, commons.Constants.SERVICE_NAME + " 登録確認メール", mail)
			true
		} else {
			false
		}
	}

	// 契約者の本登録する
	def Regist(token: String, name: String): Option[String] = {
		// まず、トークンから契約者情報を取得する
		Contracts.findInterimContractByToken(token) match {
			case Some(contract) => {
				// 取得に成功した
				Contracts.regist(contract.contractId.get, name) match {
					case true => Option(null)
					case _ => Option("正常に登録できませんでした。最初からやり直してください。")
				}
			}
			case _ => {
				// 取得に失敗した
				Option("仮登録から時間が経っています。最初からやり直してください。")
			}
		}
	}

	/* パスワード変更画面 */
	// 契約者のパスワードを変更する
	def changePassword(token: String): String = {
		// 新しいパスワードをランダムに生成
		val rand = new Random
		val bean = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXTZ"
		var pass = ""
		for (i <- 1 to 10) {
			pass = pass + bean(rand.nextInt(bean.length))
		}

		// DBに保存
		if (Contracts.setPassByToken(token, pass)) {
			// トークンを無効にする
			Contracts.invalidateRegistToken(token)
			pass
		} else {
			null
		}
	}

	/**
	 * 現在のパスワードを指定してパスワードを変更する
	 * 現在のパスワードが一致しなければ失敗する
	 * @param contract 契約者
	 * @param oldpass 現在のパスワード
	 * @param newpass 新しいパスワード
	 * @return 成功したらtrue
	 */
	def changePassword(contract: Contracts, oldpass: String, newpass: String)(implicit con: DBSession): Boolean = {
		// パスワードが一致するか確認
		Contracts.findByMailAndPass(contract.mail, oldpass) match {
			// 一致したらパスワード変更
			case Some(c) => Contracts.setPass(contract.contractId.get, newpass)
			case _ => false
		}
	}

	/**
	 * 解約処理
	 *
	 * 実際にはステータス変更のみでレコードの物理削除は行わない。
	 * 参考：https://van2k.com/redmine/issues/97
	 *
	 * @param contractId 契約者ID
	 */
  def withdrawContract(contractId: Long) = {
    DB localTx {
      implicit con => {
        // 契約者に関連するフォームを削除状態に変更する
        EncForms.updateStatusByContractId(contractId, EncForms.STATUS_DELETE)(con) == DbUpdateResult.success
        // 最終ログイン日付をnullとする
        Contracts.updateLastLoginAt(contractId, null)(con)
        // 契約者のステータスを変更
        Contracts.changeStatus(contractId, Contracts.STATUS_WITHDRAWN)(con)
      }
    }
  }

	/* 問い合わせ画面 */
	// 問い合わせ画面を送信する
	def sendInquiry(subject: String, mail: String, name: String, content: String, status: String, contract_id: Long, login: Boolean) = {
		models.Inquiry.insert(subject, mail, name, content, status, contract_id, login)
	}

	/* アンケート一覧画面 */
	// アンケートを検索する
	def searchEnc() = {

	}

	// アンケートのステータスを変更する
	def changeEncStatus() = {

	}

	/* アンケート編集画面 */
	// アンケート項目を登録する
	def registEncItem() = {

	}

	// 設定されたアンケート情報がステータス開始変更時に有効かどうかを確認する
	def checkRegistEnqAndUpdate(formId: Long, contractId: Long, status: String) = {

		var message = ""
		var curForm = EncForms.findByIdContractid(formId, contractId).getOrElse(EncForms.getBlankForm())
		if (curForm.formId != -1) {
			val config = EncFormConfigs.findByFormid(curForm.formId).getOrElse(EncFormConfigs.getBlankEncFormConfig())
			val totalPageCount = curForm.totalPageCount

			// ステータスをチェックする
			if (status == EncForms.STATUS_RUN && config.id == -1) {
				// 料金プランが選択されていない
				message = "料金プランが選択されていないため、開始する事ができません"
			} else if(status == EncForms.STATUS_RUN && EncFormItems.findByFormId(curForm.formId).length == 0){
				message = "アンケート項目が1件もない為、ステータスを「開始」にする事ができません"
			} else if(curForm.formName == ""){
				message = "アンケートタイトルが設定されていません。アンケートタイトルを設定してください。"
			} else {
				breakable {
					for(pageNo <- 1 to (totalPageCount)) {
						val findItem = EncFormItems.findByFormidPageno(curForm.formId, pageNo)
						val findValue = EncFormItemValues.findByFormidPageno(curForm.formId, pageNo)
						Logger.debug("pageNo[" + pageNo + "] totalPageCount[" + totalPageCount + "] findItem[" + findItem.toString + "] findValue[" + findValue.toString + "]")
						if (status == EncForms.STATUS_RUN && (findItem.length == 0 || findValue.length == 0)) {
							message = pageNo + "ページ目のアンケートの質問項目が存在しない為、ステータスを「開始」にすることが出来ません"
							break
						}
					}
				}
			}
		} else {
			message = "アンケートが見つかりません"
		}
		if (message == "") {
			// ステータスの更新
			EncForms.updateStatus(curForm.formId, status, curForm.updatedAt, Calendar.getInstance().getTime)
		}
		message
	}


	// 設定されたアンケート情報がプレビュー表示する際に有効かどうかを確認する
	def checkRegistEnqForPreview(formId: Long) = {
		// データを取得する
		val findForms = EncForms.find(formId)
		val totalPageCount = findForms.getOrElse(EncForms(formId, 0, "", "", "", 1, Option(0), Option(null), Option(null), "1", "", Calendar.getInstance().getTime(), Calendar.getInstance().getTime())).totalPageCount

		var message = ("", "")
		if (!findForms.isDefined) {
			message = ("error", "アンケートがありません")
		} else if (EncFormItems.findByFormId(formId).length == 0) {
			message = ("caution", "アンケートの質問項目が存在しません")
		} else {
			breakable {
				for(pageNo <- 1 to (totalPageCount)) {
					val findItem = EncFormItems.findByFormidPageno(formId, pageNo)
					val findValue = EncFormItemValues.findByFormidPageno(formId, pageNo)
					Logger.debug("pageNo[" + pageNo + "] totalPageCount[" + totalPageCount + "] findItem[" + findItem.toString + "] findValue[" + findValue.toString + "]")
					if (findItem.length == 0 || findValue.length == 0) {
						message = ("caution", pageNo + "ページ目のアンケートの質問項目が存在しません。")
						break
					}
				}
			}
		}
		message
	}

	// アンケートのソート順を変更する
	def changeEndDispodr() {

	}

	// 料金プラン（オプション含む）を登録する
	def registPlan(formId: Long, forms: EncForms, planId: Int) = {
	}

	/*
	 * アンケートを削除する
	 * @ param formid 削除するフォームID
	 */
	def deleteEnc(formid: Long) = {
		DB localTx {
			implicit con => {
				// アンケート回答明細
				EncAnswerDetails.deleteByFormid(formid)(con)

				// アンケート回答
				EncAnswers.deleteByFormid(formid)(con)

				// アンケート項目値を削除する
				EncFormItemValues.deleteByFormid(formid)(con)

				// アンケート項目を削除する
				EncFormItems.deleteByFormid(formid)(con)

				// アンケート設定情報を削除する
				EncFormConfigs.deleteByFormid(formid)(con)

				// アンケートを削除する
				EncForms.deleteById(formid)(con)
			}
		}
	}

	/*
	*アンケートを複製する
	* @param formid フォームID
	* @param contractid 契約者ID
	* @return 新しく作成したフォームID
	*/
	def copyEnc(formid: Long, contractid: Long): Option[Long] = {
		val form = EncForms.findByIdContractid(formid, contractid)
		var newid: Option[Long] = Option(-1)

		scalikejdbc.DB localTx {
			implicit con => {
				if (form.isDefined) {
					// フォームをコピーする
					val newformid = EncForms.copyForm(formid, EncForms.STATUS_EDIT)(con)

					// form_idで各項目を取得する
					val items = EncFormItems.findByFormId(formid)(con)
					val cal = Calendar.getInstance()

					// 各項目分ループする
					for (item <- items) {
						Logger.debug("item->itemid:" + item.itemId)
						// 新規に登録する
						val formitems = EncFormItems(-1, newformid, item.categoryId, item.mItemId, item.name, item.pageNo, item.selectNumber, item.condition, item.required, item.dispodr, cal.getTime, cal.getTime)
						val newitemid = EncFormItems.regist(formitems, cal.getTime)(con)
						Logger.debug("newitemid:" + newitemid.get)

						// 項目値をコピーする
						EncFormItemValues.copyFormItemValues(item.itemId, newitemid.get)(con)
					}
					newid = Option(newformid)
				}
			}
		}

		return newid
	}

	// パスワードを忘れた場合に、指定されたメールアドレスにトークン付きのメールを送る
	def forgotPassSendMail(mail: String, request: play.api.mvc.RequestHeader): Boolean = {
		// まず指定されたメールアドレスのユーザーを探す
		Contracts.findByMail(mail) match {
			case Some(contract) => {
        // 存在するので、トークンを生成する
        val token = createRegistToken()

				// トークンの有効期限はとりあえず1時間とする
				val cal = Calendar.getInstance()
				cal.add(Calendar.HOUR, 1)
				val token_limit = cal.getTime

				// DBに登録
				if (Contracts.changeRegistToken(contract.contractId.get, token, token_limit)) {
					// メール送信
					val message = views.txt.contract.forgotPassMail(routes.Contract.forgotPassNewPass(token).absoluteURL()(request)).toString()
					if (!testMode)
						MailUtil.sendMail(message, "アンケート利用者パスワード変更確認メール", mail)
					true
				} else {
					false
				}
			}
			case _ => false
		}
	}

	// パスワードを忘れた人のパスワードを変更する
	def forgotPassChangePass(token: String, pass: String): Boolean = {
		if (Contracts.setPassByToken(token, pass)) {
			// トークンを無効にする
			Contracts.invalidateRegistToken(token)
			true
		} else {
			false
		}
	}
}
