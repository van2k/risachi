package services

import play.api.mvc._
import java.util.{Date, Calendar}
import models.contract.SearchedContracts
import collection.mutable.ListBuffer
import controllers.routes
import commons.MailUtil
import models.Administrator
import scalikejdbc.DB
import models.charge.ChargePlans

/*
 * 管理者サービス
 */
object Admin {

	// 契約者検索用のコンテキスト
	case class SearchContractContext(
		                                name: String,
		                                mail: String,
		                                count_from: String,
		                                count_to: String,
		                                sort_column: String,
		                                sort_desc: String
		                                )

	// フォーム検索の検索条件
	case class SearchEncContext(
		                           contract_name: String,
		                           start_st_from: Option[Date],
		                           start_st_to: Option[Date],
		                           end_st_from: Option[Date],
		                           end_st_to: Option[Date],
		                           payment_confirm_is_null: Boolean,
		                           payment_confirm_is_not_null: Boolean,
		                           is_displaying: Boolean,
		                           is_not_displaying: Boolean
		                           )


	// フォーム検索結果１行を表すクラス
	case class SearchEncRow(
		                       form_id: Long,
		                       contract_name: String,
		                       start_st: String,
		                       end_st: String,
		                       plan_name: String,
		                       status: String,
		                       price: Int,
		                       payment_confirm: Int,
		                       payment_confirm_date: String
		                       )

	object SearchEncRow {
		// ファクトリメソッド
		def apply(form: models.enc.EncFormsSearch): SearchEncRow = {
			val dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd")
			val dateTimeFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
			// 支払い確認日
			val paymentConfirmDate: String = form.payment_confirm_date match {
				case Some(date) => dateTimeFormat.format(date)
				case _ => ""
			}

			SearchEncRow(form.form_id, form.contract_name, dateFormat.format(form.start_st),
				dateFormat.format(form.end_st), form.plan_name,
				models.enc.EncForms.STATUS_LIST(form.status), form.price.getOrElse(0),
				form.payment_confirm.getOrElse(0), paymentConfirmDate)
		}
	}

	/* ログイン画面 */
	// ログインする
	def login(userid: String, password: String): Option[Administrator] = {
		Administrator.findByUseridAndPass(userid, password)
	}

	/* アンケート管理画面 */
	// アンケートを検索する
	def searchEnc(searchContext: SearchEncContext, selectedPlans: List[String]): List[SearchEncRow] = {

		// フォームのリストを取得する
		val form_list = models.enc.EncFormsSearch.search(
			searchContext.contract_name, searchContext.start_st_from, searchContext.start_st_to,
			searchContext.end_st_from, searchContext.end_st_to, selectedPlans,
			searchContext.payment_confirm_is_null, searchContext.payment_confirm_is_not_null,
			searchContext.is_displaying, searchContext.is_not_displaying)

		// 画面表示用のフォームデータのリストを作る
		form_list.map {
			form =>
				SearchEncRow(form)
		}
	}

	// アンケートのステータスを変更する
	def changeEncStatus(formId: Long, status: String): Boolean = {
		models.enc.EncForms.find(formId) match {
			case Some(form) => {
				val now = Calendar.getInstance().getTime
				models.enc.EncForms.updateStatus(formId, status, form.updatedAt, now)
				true
			}
			case _ => false
		}
	}

	// 入金登録する
	def registReceipt(formId: Long, price: Int): Boolean = {
		models.enc.EncFormConfigs.setPaymentConfirm(formId, price)
	}

	// 入金確認済みにする
	def paymentUpdate(formId: Long): Option[Date] = {
		models.enc.EncFormConfigs.setPaymentConfirmDate(formId)
	}

	// 特定のフォームのオプション料金プランを取得して、IDをカンマ区切りの文字列にする
	def getFormChargeOptionIdString(formId: Long): String = {
		var ret = ""
		models.enc.EncFormChargeOptions.findByFormId(formId).foreach {
			chargeOption =>
				if (ret != "") ret += ","
				ret += chargeOption.chargeOptionPlanId
		}
		ret
	}

	// カンマ区切りのIDの羅列を受け取り、含まれるオプション料金プランを登録し、含まれないオプション料金プランを外す
	def setFormChargeOptionByIdString(formId: Long, chargeOptionIds: String): Boolean = {
		// カンマ区切りをパースして配列にする
		val id_array = chargeOptionIds.split(",").map {
			id =>
				try {
					id.toLong
				} catch {
					case e: Exception => -1
				}
		}

		DB localTx {
			implicit con => {
				models.charge.ChargeOptionPlans.findAll().foreach {
					chargeOption =>
						models.enc.EncFormChargeOptions.updateFormChargeOption(formId, chargeOption.id,
							id_array.indexOf(chargeOption.id) >= 0)
				}
			}
		}
		true
	}

	/* 契約者管理画面*/
	// 契約者を検索する
	def searchContract(searchContext: SearchContractContext): List[(models.contract.SearchedContracts, String)] = {
		val count_from = try {
			Integer.parseInt(searchContext.count_from)
		} catch {
			case e: Exception => 0
		}
		val count_to = try {
			Integer.parseInt(searchContext.count_to)
		} catch {
			case e: Exception => 0
		}

		val contractList = SearchedContracts.search(searchContext.name, searchContext.mail, count_from, count_to,
			"name", false)
		// 登録日を文字列に変換する
		val dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

		contractList.map {
			cont => Pair(cont, dateFormat.format(cont.created_at))
		}
	}

	// 契約者の状態を変更する
	def changeContractStatus(contractId: Long, status: String): Boolean = {
		models.contract.Contracts.changeStatus(contractId, status)
	}

	// 契約者のパスワードをリセットする
	def resetContractPassword(contractId: Long, request: play.api.mvc.RequestHeader): Boolean = {
		// 契約者取得
		models.contract.Contracts.findById(contractId) match {
			case Some(contract) => {
				// トークン生成
				val token = Contract.createRegistToken()

				// トークンの有効期限はとりあえず1時間とする
				val cal = Calendar.getInstance()
				cal.add(Calendar.HOUR, 1)
				val token_limit = cal.getTime

				// DBに登録
				if (models.contract.Contracts.changeRegistToken(contractId, token, token_limit)) {
					// メール送信
					val message = views.txt.contract.confirmResetPasswordMail(routes.Contract.newPassword(token).absoluteURL()(request)).toString()
					MailUtil.sendMail(message, "パスワードリセット確認メール", contract.mail)
				} else false
			}
			case _ => {
				false
			}
		}
	}

	/* 管理者通知画面 */
	// 問い合わせの検索をする
	def searchInquiry(searchContext: controllers.Admin.SearchInquiryContext): List[models.Inquiry] = {
		models.Inquiry.search(searchContext)
	}

	// 問い合わせのステータスを変更する
	def changeInquiry(inqId: Long, inqStatus: String) = {
		models.Inquiry.changeStatus(inqId, inqStatus)
	}

	/* お知らせ一覧*/
	// お知らせを検索する
	def searchInfo(searchContext: models.SearchInformationContext): List[models.Information] = {
		models.Information.search(searchContext)
	}

	/* お知らせ一覧*/
	// お知らせを id で検索する
	def searchByIdInfo(infId: Long): Option[models.Information] = {
		models.Information.findById(infId)
	}

	/* お知らせ一覧 */
	// お知らせを削除する
	def deleteInfo(infId: Long) = {
		models.Information.delete(infId)
	}

	/* お知らせ一覧 */
	// お知らせを更新する
	def updateInfo(infId: Long, subject: String, content: String) = {
		models.Information.update(infId, subject, content)
	}

	/* お知らせ画面 */
	// お知らせを送信する
	def registInfo(subject: String, content: String) = {
		models.Information.insert(subject, content)
	}

	/* 料金プラン画面 */
	// 料金プランを検索する
	def searchPlan(): List[models.charge.ChargePlans] = {
		models.charge.ChargePlans.findAll()
	}

	/* 料金プラン画面 */
	// id を検索条件として料金プランを検索する
	def searchPlanById(id: Long): Option[models.charge.ChargePlans] = {
		models.charge.ChargePlans.findById(id)
	}

	/* オプションプラン画面*/
	// オプションプランを検索する
	def searchOpitonPlan(): List[models.charge.ChargeOptionPlans] = {
		models.charge.ChargeOptionPlans.findAll()
	}

	/* オプションプラン画面 */
	// id を検索条件としてオプションプランを検索する
	def searchOptionPlanById(id: Long): Option[models.charge.ChargeOptionPlans] = {
		models.charge.ChargeOptionPlans.findById(id)
	}

	/**
	 * 管理者の追加
	 * @param userid ユーザID
	 * @param pass パスワード
	 * @return 成功したらtrue
	 */
	def administratorAdd(userid: String, pass: String): Boolean = {
		Administrator.insert(userid, pass)
	}

	/**
	 * 管理者の削除
	 * @param id 管理者ID
	 * @param currentAdmin 現在ログイン中の管理者
	 * @return 成功したらtrue
	 */
	def administratorRemove(id: Long, currentAdmin: Administrator): Boolean = {
		if (id == currentAdmin.id) false
		else Administrator.remove(id)
	}

	/**
	 * 管理者のパスワードを変更する
	 * @param userid ユーザーID
	 * @param oldpass 古いパスワード
	 * @param newpass 新しいパスワード
	 * @return 成功したらtrue
	 */
	def administratorChangePass(userid: String, oldpass: String, newpass: String): Boolean = {
		Administrator.findByUseridAndPass(userid, oldpass) match {
			case Some(administrator) => Administrator.changePass(administrator.id, newpass)
			case _ => false
		}
	}
}
