package models.contract


import java.util.Date
import java.util.Calendar
import play.api.Logger
import java.sql.{SQLException}
import scalikejdbc._

case class Contracts(
                      contractId: Option[Long] = None,
                      mail: String,
                      status: String,
                      name: String,
                      lastLoginAt: Option[Date] = None
                      )

object Contracts extends models.Model {
	// ステータスの定義
	val STATUS_INTERIM = "1"
	// 仮登録状態
	val STATUS_REGISTERED = "2"
	// 登録状態
	val STATUS_STOP = "3"
	// 停止状態
	val STATUS_WITHDRAWN = "9" // 解約状態

  val tableName = "contracts"

  object columnNames {
    val contractId = "id"
    val mail = "mail"
    val status = "status"
    val name = "name"
    val lastLoginAt = "last_login_at"
    val all = Seq(contractId, mail, status, name)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Contracts(
      contractId = rs.longOpt(contractId),
      mail = rs.string(mail),
      status = rs.string(status),
      name = rs.string(name),
      lastLoginAt = Option(rs.timestamp(lastLoginAt)))
  }

  val autoSession = AutoSession

	/**
	 * 仮契約
	 *
	 * @param mail 契約者メールアドレス
	 * @param regist_token 登録用トークン
	 * @param token_limit トークン有効期限
	 */
	def tempRegist(mail: String, regist_token: String, token_limit: Date)(implicit session: DBSession = autoSession): Boolean = {
		// すでにこのメールアドレスのレコードが存在し、かつ、そのレコードのstatusが同じだったら、データを書き換える
		val result = SQL(
			"UPDATE contracts SET status='1', regist_token={regist_token}, token_limit={token_limit}, updated_at = {updated_at} WHERE mail={mail} AND (status='1' OR status='9')"
		).bindByName(
			'mail -> mail,
			'regist_token -> regist_token,
			'token_limit -> token_limit,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply()

    result match {
			case 1 => true
			case _ => {
				// 更新しなかった＝対象のレコードがなかった場合は、挿入する
				try {
					SQL(
						"""
						  |INSERT INTO contracts (mail, status, regist_token, token_limit, created_at, updated_at) values (
						  |  {mail}, '1', {regist_token}, {token_limit}, {created_at}, {updated_at}
						  |);
						""".stripMargin
					).bindByName(
						'mail -> mail,
						'regist_token -> regist_token,
						'token_limit -> token_limit,
						'created_at -> Calendar.getInstance().getTime(),
						'updated_at -> Calendar.getInstance().getTime()
					).update.apply()
					true
				} catch {
					case e: SQLException => false
				}
			}
		}
	}

	/**
	 * 名前を含めてすべてのデータを指定して挿入する（テストで使う）
	 * @param mail メールアドレス
	 * @param status 契約状態
	 * @param name 名前
	 * @param regist_token 登録トークン
	 * @param token_limit 登録トークンの期限
	 * @return 成功したらtrue, 失敗したらfalse
	 */
	def directInsert(mail: String, status: String, name: String, regist_token: String, token_limit: Date)(implicit session: DBSession = autoSession): Boolean = {
		try {
			SQL(
				"""
				  |INSERT INTO contracts (mail, status, name, regist_token, token_limit, created_at, updated_at) values (
				  |  {mail}, {status}, {name}, {regist_token}, {token_limit}, NOW(), NOW()
				  |);
				""".stripMargin
			).bindByName(
				'mail -> mail,
				'status -> status,
				'name -> name,
				'regist_token -> regist_token,
				'token_limit -> token_limit
			).update.apply()
			true
		} catch {
			case e: SQLException => false
		}
	}

	def changeStatus(contractId: Long, status: String)(implicit session: DBSession = autoSession): Boolean = {
		SQL(
			"UPDATE contracts SET status={status}, updated_at={updated_at} WHERE id={id};"
		).bindByName(
			'status -> status,
			'id -> contractId,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

	def changeRegistToken(contractId: Long, token: String, token_limit: Date)(implicit session: DBSession = autoSession): Boolean = {
		SQL(
			"UPDATE contracts SET regist_token={token}, updated_at={updated_at}, token_limit={token_limit} WHERE id={id};"
		).bindByName(
			'token -> token,
			'token_limit -> token_limit,
			'id -> contractId,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

  def updateLastLoginAt(contractId: Long, lastLoginAt: Date)(implicit session: DBSession = autoSession): Boolean = {
    SQL(
      "UPDATE contracts SET last_login_at={last_login_at}, updated_at={updated_at} WHERE id={id};"
    ).bindByName(
      'last_login_at -> lastLoginAt,
      'id -> contractId,
      'updated_at -> Calendar.getInstance().getTime()
    ).update.apply() > 0

  }

	/**
	 * 登録処理
	 * 具体的には、名前をセットしてstatusを「登録済み」にする
	 */
	def regist(contractId: Long, name: String)(implicit session: DBSession = autoSession): Boolean = {

		SQL(
			"UPDATE contracts SET status={status}, name={name}, updated_at={updated_at} WHERE id={id};"
		).bindByName(
			'status -> Contracts.STATUS_REGISTERED,
			'name -> name,
			'id -> contractId,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

	/**
	 * パスワードをMD5ハッシュにして保存する。
	 */
	def setPass(contractId: Long, pass: String)(implicit session: DBSession = autoSession): Boolean = {
		val hashedPass = md5(pass)

		SQL(
			"UPDATE contracts SET pass={pass}, updated_at={updated_at} WHERE id={id};"
		).bindByName(
			'pass -> hashedPass,
			'id -> contractId,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

	/**
	 * パスワードをMD5ハッシュにして保存する。
	 */
	def setPassByMail(mail: String, pass: String)(implicit session: DBSession = autoSession): Boolean = {
		val hashedPass = md5(pass)

		SQL(
			"UPDATE contracts SET pass={pass}, updated_at={updated_at} WHERE mail={mail};"
		).bindByName(
			'pass -> hashedPass,
			'mail -> mail,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

	/**
	 * パスワードをMD5ハッシュにして保存する。
	 */
	def setPassByToken(token: String, pass: String)(implicit session: DBSession = autoSession): Boolean = {
		val hashedPass = md5(pass)

		SQL(
			"UPDATE contracts SET pass={pass}, updated_at={updated_at} WHERE regist_token={token} AND token_limit > NOW();"
		).bindByName(
			'pass -> hashedPass,
			'token -> token,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}

	/**
	 * メールアドレスが存在するかチェックする。
	 * @param mail メールアドレス
	 * @return 取得したContractsテーブルのデータ
	 */
	def findByMail(mail: String)(implicit session: DBSession = autoSession): Option[Contracts] = {
		val sql =
			"""
			  |SELECT id, mail, status, name, last_login_at
			  |  FROM contracts
			  | WHERE mail = {mail}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'mail -> mail
		).map(*).single.apply()
	}

	/**
	 * メールアドレスとパスワードが一致する契約者を返す
	 * @param mail メールアドレス
	 * @param pass パスワード
	 * @return 取得したContractsテーブルのデータ
	 */
	def findByMailAndPass(mail: String, pass: String)(implicit session: DBSession = autoSession): Option[Contracts] = {
		val hashedPass = md5(pass)

		val sql =
			"""
			  |SELECT id, mail, status, name, last_login_at
			  |  FROM contracts
			  | WHERE mail = {mail} AND pass = {pass}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'mail -> mail,
			'pass -> hashedPass
		).map(*).single.apply()
	}

	/**
	 * 契約者IDから契約者データを取り出す
	 * @param id 契約者ID
	 * @return 取得したContractsテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[Contracts] = {
		val sql =
			"""
			  |SELECT id, mail, status, name, pass, last_login_at
			  |  FROM contracts
			  | WHERE id = {id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply()
	}

	/**
	 * 登録用トークンから契約者を捜して返す
	 * @param token トークン
	 * @return 取得したContractsテーブルのデータ
	 */
	def findByToken(token: String)(implicit session: DBSession = autoSession): Option[Contracts] = {
		val sql =
			"""
			  |SELECT id, mail, status, name, last_login_at
			  |  FROM contracts
			  | WHERE regist_token={token} AND token_limit >= NOW()
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'token -> token
		).map(*).single.apply()
	}

	/**
	 * 仮登録中の契約者を捜して返す
	 * @param token トークン
	 * @return 取得したContractsテーブルのデータ
	 */
	def findInterimContractByToken(token: String)(implicit session: DBSession = autoSession): Option[Contracts] = {
		val sql =
			"""
			  |SELECT id, mail, status, name, last_login_at
			  |  FROM contracts
			  | WHERE regist_token={token} AND status='1' AND token_limit>=NOW()
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'token -> token
		).map(*).single.apply()
	}

	/**
	 * 登録トークンを無効にする
	 * @param token トークン
	 */
	def invalidateRegistToken(token: String)(implicit session: DBSession = autoSession) = {
		SQL(
			"UPDATE contracts SET regist_token=NULL, updated_at={updated_at} WHERE regist_token={regist_token};"
		).bindByName(
			'regist_token -> token,
			'updated_at -> Calendar.getInstance().getTime()
		).update.apply() > 0
	}
}
