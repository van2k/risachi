package models.contract

import collection.mutable.ListBuffer
import play.api.Logger
import java.util.Date
import scalikejdbc._
import scala.collection.mutable

// 契約者検索で取得される契約者情報
case class SearchedContracts(
	                            contractId: Option[Long] = None,
	                            mail: String,
	                            status: String,
	                            name: String,
	                            formNum: Long,
	                            created_at: Date
	                            )

object SearchedContracts extends models.Model {

  val tableName = "contracts"

  object columnNames {
    val contractId = "id"
    val mail = "mail"
    val createdAt = "created_at"
    val status = "status"
    val name = "name"
    val formNum = "form_num"
    val all = Seq(contractId, mail, createdAt, status, name)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => SearchedContracts(
      contractId = rs.longOpt(contractId),
      mail = rs.string(mail),
      created_at = rs.timestamp(createdAt).toJavaUtilDate,
      status = rs.string(status),
      formNum = rs.long(formNum),
      name = rs.string(name))
  }

  val autoSession = AutoSession

	def search(name: String, mail: String, formsMin: Int, formsMax: Int, sort_col: String, sortDesc: Boolean)
            (implicit session: DBSession = autoSession): List[SearchedContracts] = {
		// WHERE句を作る
		val whereList = ListBuffer[String]()
    var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (name != null && name != "") {
      whereList += "name LIKE {name}"
      params = params ++ Seq('name -> (if (name == null) "" else  ("%" + name + "%")))
    }

    if (mail != null && mail != "") {
      whereList += "mail LIKE {mail}"
      params = params ++ Seq('mail -> (if (mail == null) "" else  ("%" + mail + "%")))
    }

		var where = ""
		whereList.toList.foreach {
			where_str =>
				where += {
					if (where == "") " WHERE " else " AND "
				}
				where += where_str
		}

		// HAVING句を作る
		val havingList = ListBuffer[String]()
		if (formsMin > 0) havingList += "count(forms.id) >= " + formsMin
		if (formsMax > 0) havingList += "count(forms.id) <= " + formsMax
		var having = ""
		havingList.toList.foreach {
			havingStr =>
				having += {
					if (having == "") " HAVING " else " AND "
				}
				having += havingStr
		}

		val sql =
			"""
			  |SELECT contracts.id, contracts.mail, contracts.status, contracts.name, count(forms.id) AS form_num, contracts.created_at
			  |  FROM contracts LEFT JOIN forms ON contracts.id=forms.contract_id
			""".stripMargin + {
				where + " GROUP BY contracts.id" + having + " ORDER BY " + sort_col + {
					if (sortDesc) " DESC" else " ASC"
				} + " LIMIT 50"
			}

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(params:_*).map(*).list.apply()
	}

}