package models.charge

import java.util.Date
import play.api.Logger
import java.sql.SQLException
import scalikejdbc._
import commons.Constants.ChargeOptionType

case class ChargeOptionPlans(
                              id: Long,
                              optionType: String,
                              optionValue: Int,
                              charge: Int,
                              tax: Int,
                              description: String,
                              applyStartDate: Date,
                              createdAt: Date,
                              updatedAt: Date
                              ) {
  override def toString = {
    "itemId:%d".format(id)
  }
}

object ChargeOptionPlans extends models.Model {

  val tableName = "charge_option_plans"

  object columnNames {
    val id = "id"
    val charge = "charge"
    val tax = "tax"
    val description = "description"
    val applyStartDate = "apply_start_date"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val optionType = "option_type"
    val optionValue = "option_value"
    val all = Seq(id, charge, tax, description, applyStartDate, createdAt, updatedAt, optionType, optionValue)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => ChargeOptionPlans(
      id = rs.long(id),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      applyStartDate = rs.timestamp(applyStartDate).toJavaUtilDate,
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate,
      optionType = rs.string(optionType),
      optionValue = rs.int(optionValue))
  }

  val autoSession = AutoSession

  /**
   * 全てのデータ取得
   * @return 取得したChargeOptionPlansテーブルのデータ
   */
  def findAll()(implicit session: DBSession = autoSession): List[ChargeOptionPlans] = {
    val sql =
      """
        |SELECT id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at
        | FROM charge_option_plans
        | ORDER BY id
      """.stripMargin

    Logger.debug("sql:" + sql)

    SQL(sql).bindByName().map(*).list.apply
  }

	/**
	 * オプションプランIDからオプションプランデータを取り出す
	 * @param id オプションプランID
	 * @return 取得したcharge_option_plansテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[ChargeOptionPlans] = {
		val sql =
			"""
			  |SELECT id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at
			  | FROM charge_option_plans
			  | WHERE id = {id}
			""".stripMargin
		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply()
	}

	def regist(plan: ChargeOptionPlans)(implicit session: DBSession = autoSession) = {
		val sql =
			"""
			  |INSERT INTO charge_option_plans (id,option_type,option_value,charge,tax,description,apply_start_date,created_at,updated_at)
			  | VALUES ({id},{option_type},{option_value},{charge},{tax},{description},{apply_start_date},{created_at},{updated_at})
			""".stripMargin

		try {
			SQL(sql).bindByName(
				'id -> plan.id,
				'option_type -> plan.optionType,
				'option_value -> plan.optionValue,
				'charge -> plan.charge,
				'tax -> plan.tax,
				'description -> plan.description,
				'apply_start_date -> plan.applyStartDate,
				'created_at -> plan.createdAt,
				'updated_at -> plan.updatedAt
			).update.apply()
			true
		} catch {
			case e: SQLException => false
		}
	}

  /**
   * オプション区分に対応するオプション料金プラン名
   * @param optionType オプション区分
   * @return オプション料金プラン名
   */
  def name(optionType: String): String = optionType match {
    case ChargeOptionType.collect_number => "収集件数"
    case ChargeOptionType.term => "回答期間"
    case ChargeOptionType.page_num => "ページ数"
    case ChargeOptionType.item_num => "項目数"
    case ChargeOptionType.save_term => "保存期間"
    case ChargeOptionType.output_number => "CSV出力回数"
    case _ => ""
  }

}
