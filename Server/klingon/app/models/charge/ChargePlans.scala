package models.charge

import java.util.{Calendar, Date}
import play.api.Logger
import java.sql.SQLException
import scalikejdbc._

case class ChargePlans(
                        id: Long,
                        name: String,
                        charge: Long,
                        tax: Long,
                        description: String,
                        collectNumber: Long,
                        pageNum: Long,
                        itemNum: Long,
                        term: Long,
                        outputNumber: Long,
                        saveTerm: Long,
                        applyStartDate: Date,
                        csvOutputNum: Long,
                        answersDispType: Long,
                        isStartSet: String,
                        isLogoSet: String,
                        isTemplate: String,
                        isCss: String,
                        isInnerHtml: String,
                        isForeignKey: String,
                        createdAt: Date,
                        updatedAt: Date
                        ) {
  override def toString = {
    "itemId:%d".format(id)
  }
}

object ChargePlans extends models.Model {

  val tableName = "charge_plans"

  object columnNames {
    val id = "id"
    val charge = "charge"
    val tax = "tax"
    val description = "description"
    val collectNumber = "collect_number"
    val term = "term"
    val outputNumber = "output_number"
    val saveTerm = "save_term"
    val applyStartDate = "apply_start_date"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val csvOutputNum = "csv_output_num"
    val answersDispType = "answers_disp_type"
    val isStartSet = "is_start_set"
    val isLogoSet = "is_logo_set"
    val isTemplate = "is_template"
    val isCss = "is_css"
    val isInnerHtml = "is_inner_html"
    val isForeignKey = "is_foreign_key"
    val name = "name"
    val pageNum = "page_num"
    val itemNum = "item_num"
    val all = Seq(id, name, charge, tax, description, collectNumber, pageNum, itemNum, term, outputNumber, saveTerm, applyStartDate
      , csvOutputNum, answersDispType, isStartSet, isLogoSet, isTemplate, isCss, isInnerHtml, isForeignKey, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => ChargePlans(
      id = rs.long(id),
      name = rs.string(name),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      collectNumber = rs.long(collectNumber),
      pageNum = rs.int(pageNum),
      itemNum = rs.int(itemNum),
      term = rs.long(term),
      outputNumber = rs.int(outputNumber),
      saveTerm = rs.int(saveTerm),
      applyStartDate = rs.timestamp(applyStartDate).toJavaUtilDate,
      csvOutputNum = rs.long(csvOutputNum),
      answersDispType = rs.long(answersDispType),
      isStartSet = rs.string(isStartSet),
      isLogoSet = rs.string(isLogoSet),
      isTemplate = rs.string(isTemplate),
      isCss = rs.string(isCss),
      isInnerHtml = rs.string(isInnerHtml),
      isForeignKey = rs.string(isForeignKey),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

	def getBlankChargePlan(): ChargePlans = {
		val cal = Calendar.getInstance
		val now = cal.getTime
		val oldDate = Calendar.getInstance
		oldDate.add(Calendar.MONTH, -2)

		ChargePlans(-1, "", -1, 0, "", 0, 0, 0, 0, 0, 0, oldDate.getTime, 0, 0, "", "", "", "", "", "", now, now)
	}

  /**
	 * 全てのデータ取得
	 * @return 取得したChargePlansテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[ChargePlans] = {
		val sql =
			"""
			  |SELECT id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term, apply_start_date,csv_output_num, answers_disp_type, is_start_set, is_logo_set, is_template, is_css, is_inner_html, is_foreign_key, created_at, updated_at
			  | FROM charge_plans
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
		).map(*).list.apply()
	}

	/**
	 * 料金プランIDから料金プランデータを取り出す
	 * @param id 料金プランID
	 * @return 取得したcharge_plansテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[ChargePlans] = {
		val sql =
			"""
			  |SELECT id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term, apply_start_date,csv_output_num, answers_disp_type, is_start_set, is_logo_set, is_template, is_css, is_inner_html, is_foreign_key, created_at, updated_at
			  | FROM charge_plans
			  | WHERE id = {id}
			""".stripMargin

		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply()
	}

	def insert(plan: ChargePlans)(implicit session: DBSession = autoSession): Boolean = {
		val sql =
			"""
			  |INSERT INTO charge_plans (id,charge,tax,description,collect_number,term,output_number,save_term,
			  | apply_start_date,created_at,updated_at,csv_output_num,answers_disp_type,is_start_set,
			  | is_logo_set,is_template,is_css,is_inner_html,is_foreign_key,name,page_num,item_num)
			  | VALUES ({id},{charge},{tax},{description},{collect_number},{term},{output_number},{save_term},
			  | {apply_start_date},{created_at},{updated_at},{csv_output_num},{answers_disp_type},{is_start_set},
			  | {is_logo_set},{is_template},{is_css},{is_inner_html},{is_foreign_key},{name},{page_num},{item_num})
			""".stripMargin

		try {
			SQL(sql).bindByName(
				'id -> plan.id,
				'charge -> plan.charge,
				'tax -> plan.tax,
				'description -> plan.description,
				'collect_number -> plan.collectNumber,
				'term -> plan.term,
				'output_number -> plan.outputNumber,
				'save_term -> plan.saveTerm,
				'apply_start_date -> plan.applyStartDate,
				'created_at -> plan.createdAt,
				'updated_at -> plan.updatedAt,
				'csv_output_num -> plan.csvOutputNum,
				'answers_disp_type -> plan.answersDispType,
				'is_start_set -> plan.isStartSet,
				'is_logo_set -> plan.isLogoSet,
				'is_template -> plan.isTemplate,
				'is_css -> plan.isCss,
				'is_inner_html -> plan.isInnerHtml,
				'is_foreign_key -> plan.isForeignKey,
				'name -> plan.name,
				'page_num -> plan.pageNum,
				'item_num -> plan.itemNum
			).update.apply()
			true
		} catch {
			case e: SQLException => false
		}

	}
}
