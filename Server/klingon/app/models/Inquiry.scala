package models

import java.util.{Date, Calendar}
import java.sql.SQLException
import scalikejdbc._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import java.text.SimpleDateFormat
import play.Logger

case class Inquiry(
	                  id: Long,
	                  sendDate: Date,
	                  subject: String,
	                  mail: String,
	                  content: String,
	                  status: String,
	                  contractId: Option[Long],
	                  name: Option[String]
	                  )

object Inquiry extends models.Model {

	// 問い合わせへの対応状況
	object InquiryStatus {
		val UNSUPPORTED = "0"
		// 未対応
		val SUPPORTED = "1" // 対応済み
	}

  val tableName = "inquiry"

  object columnNames {
    val id = "id"
    val sendDate = "send_date"
    val subject = "subject"
    val mail = "mail"
    val content = "content"
    val status = "status"
    val contractId = "contract_id"
    val name = "name"
    val all = Seq(id, sendDate, subject, mail, content, status, contractId, name)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Inquiry(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toJavaUtilDate,
      subject = rs.string(subject),
      mail = rs.string(mail),
      content = rs.string(content),
      status = rs.string(status),
      contractId = rs.longOpt(contractId),
      name = rs.stringOpt(name))
  }

  val autoSession = AutoSession

  /**
	 * 問い合わせ情報を登録する。
	 *
	 * @param subject 件名
	 * @param mail 契約者メールアドレス
	 * @param name 契約者名前
	 * @param content 本文
	 * @param status 対応状況
	 * @param contractId 契約者ID
	 * @param login ログイン中であるか否か
	 */
	def insert(subject: String, mail: String, name: String, content: String, status: String, contractId: Long, login: Boolean)
            (implicit session: DBSession = autoSession)= {
		if (login) {
			val sql =
				"""
				  |INSERT INTO inquiry (send_date, subject, mail, name, content, status, contract_id) values (
				  |  {send_date}, {subject}, {mail}, {name}, {content}, {status}, {contract_id}
				  |);
				""".stripMargin
			try {
				SQL(sql).bindByName(
					'send_date -> Calendar.getInstance().getTime(),
					'subject -> subject,
					'mail -> mail,
					'name -> name,
					'content -> content,
					'status -> status,
					'contract_id -> contractId
				).update().apply()
				true
			} catch {
				case e: SQLException => false
			}
		} else {
			val sql =
				"""
				  |INSERT INTO inquiry (send_date, subject, mail, name, content, status) values (
				  |  {send_date}, {subject}, {mail}, {name}, {content}, {status}
				  |);
				""".stripMargin
			try {
				SQL(sql).bindByName(
					'send_date -> Calendar.getInstance().getTime(),
					'subject -> subject,
					'mail -> mail,
					'name -> name,
					'content -> content,
					'status -> status
				).update.apply()
				true
			} catch {
				case e: SQLException => false
			}
		}
	}

	def search(searchContext: controllers.Admin.SearchInquiryContext)(implicit session: DBSession = autoSession): List[Inquiry] = {

		val whereList = ListBuffer[String]()
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()
		val dateFormat = new SimpleDateFormat("yyyy/MM/dd")

		if (searchContext.dateFrom != "") {
			whereList += "send_date >= {send_date_from}"
			val dateFrom = Calendar.getInstance()
			dateFrom.setTime(dateFormat.parse(searchContext.dateFrom))
			params = params ++ Seq('send_date_from -> dateFrom.getTime)
		}
		if (searchContext.dateTo != "") {
			whereList += "send_date < {send_date_to}"
			val dateTo = Calendar.getInstance()
			dateTo.setTime(dateFormat.parse(searchContext.dateTo))
			dateTo.add(Calendar.DATE, 1)
			params = params ++ Seq('send_date_to -> dateTo.getTime)
		}
		if (searchContext.name != "") {
			whereList += "name LIKE {inquiry_name}"
			params = params ++ Seq('inquiry_name -> ("%" + searchContext.name + "%"))
		}
		if (searchContext.mail != "") {
			whereList += "mail LIKE {inquiry_mail}"
			params = params ++ Seq('inquiry_mail -> ("%" + searchContext.mail + "%"))
		}
		if (searchContext.member && !searchContext.nonmember) {
			whereList += "contract_id IS NOT NULL"
		}
		if (searchContext.nonmember && !searchContext.member) {
			whereList += "contract_id IS NULL"
		}
		if (searchContext.unsupported && !searchContext.supported) {
			whereList += "status = '" + InquiryStatus.UNSUPPORTED + "'"
		}
		if (searchContext.supported && !searchContext.unsupported) {
			whereList += "status = '" + InquiryStatus.SUPPORTED + "'"
		}
		var where = ""
		whereList.toList.foreach {
			where_str =>
				where += {
					if (where == "") " WHERE " else " AND "
				}
				where += where_str
		}

		SQL(
      "SELECT * FROM inquiry " + where + " ORDER BY id"
    ).bindByName(params:_*).map(*).list.apply()
	}

	def changeStatus(inquiryId: Long, status: String)(implicit session: DBSession = autoSession) = {
		val retCount = SQL(
			"UPDATE inquiry SET status={status} WHERE id={id};"
		).bindByName(
			'status -> status,
			'id -> inquiryId
		).update.apply()

    retCount > 0
	}

	// テスト用に問い合わせ日時を設定する
	def changeSendDate(inquiryId: Long, sendDate: String)(implicit session: DBSession = autoSession) = {
		val dateFrom = Calendar.getInstance()
		dateFrom.setTime(new SimpleDateFormat("yyyy/MM/dd").parse(sendDate))

		val retCount = SQL(
			"UPDATE inquiry SET send_date={send_date} WHERE id={id};"
		).bindByName(
			'send_date -> dateFrom.getTime,
			'id -> inquiryId
		).update.apply()

    retCount > 0
	}

}
