package models

/**
 * 質問形式の列挙体
 */
object QuestionType extends Enumeration {

	type QuestionType = Value

	/**
	 * テキスト行
	 */
	val TextLine = Value(1)

	/**
	 * テキスト複数行
	 */
	val TextMultiLine = Value(2)

	/**
	 * ラジオボタン
	 */
	val Radio = Value(3)

	/**
	 * チェックボックス
	 */
	val Checkbox = Value(4)

	/**
	 * Dropリスト
	 */
	val DropList = Value(5)

	/**
	 * Dropリスト
	 */
	val TextDate = Value(6)

	/**
	 * Dropリスト
	 */
	val TextRegx = Value(7)
}
