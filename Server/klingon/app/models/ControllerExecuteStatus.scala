package models

/**
 * コントローラーの処理結果ステータス
 */
object ControllerExecuteStatus extends Enumeration {

	type ControllerExecuteStatus = Value

	val success = Value(0)
	val fail = Value(-1)
}
