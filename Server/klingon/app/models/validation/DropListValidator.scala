package models.validation

import collection.SortedMap


/**
 * ドロップダウンリストのチェックロジック
 * @param formItemName 質問のタイトル
 * @param questionItems 質問の項目
 */
class DropListValidator(formItemName: String, questionItems: SortedMap[String, String])
	extends FormItemValueValidator(formItemName: String, questionItems: SortedMap[String, String]) {

	def executeCheck(): Boolean = {

		// 文字列長のチェックと同値チェックを行う
		questionTitleLengthCheck & (questionItemLengthCheck && notExistsSameData)
	}
}
