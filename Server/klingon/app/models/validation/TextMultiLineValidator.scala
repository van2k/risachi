package models.validation

import collection.SortedMap

class TextMultiLineValidator(formItemName: String, questionItems: SortedMap[String, String])
	extends FormItemValueValidator(formItemName: String, questionItems: SortedMap[String, String]) {

	def executeCheck(): Boolean = {
		questionTitleLengthCheck & ansInputMaxLengthCheck
	}

}
