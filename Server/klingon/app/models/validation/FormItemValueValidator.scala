package models.validation

import models.QuestionType
import models.QuestionType.QuestionType
import collection.{SortedMap, mutable}
import play.api.Logger
import commons._


/**
 * フォーム項目のチェックロジック
 * @param questionTitle 質問のタイトル
 * @param questionItems 質問の項目
 */
abstract class FormItemValueValidator(questionTitle: String, questionItems: SortedMap[String, String]) {

	protected var selectItemCount: Int = 0
	protected var selectItemCountOp: String = ""
	protected var isElseDataChecked: Boolean = false
	protected var ansInputMaxLength: Int = 0
	protected var isRequired: Boolean = true

	/**
	 * タイトルについてエラーチェックした結果を保持します。
	 */
	protected var titleErrorMessage: String = ""

	/**
	 * 項目のエラーを項目の番号(順序)とエラーメッセージのリストで保持します。
	 * 1項目に複数のエラーが登録されることを想定しています。
	 */
	protected val itemErrorMessage: mutable.Map[Int, mutable.Set[String]] = mutable.Map()

	/**
	 * 選択するアイテムの個数が正しい関係にあるか確認した結果のエラーメッセージを保持します。
	 */
	protected var selectItemCountError: String = ""

	/**
	 * 入力文字数の最大値を確認した結果のエラーメッセージを保持します。
	 */
	protected var ansInputMaxLengthError: String = ""

	/**
	 * チェックを実行します。
	 * @return 正常の場合はTrue
	 */
	def executeCheck(): Boolean

	/**
	 * 項目の文字数が上限以下かどうかをチェックします。
	 * @return 正常の場合はTrue
	 */
	def questionTitleLengthCheck(): Boolean = {

		if (questionTitle.trim.length == 0 || questionTitle == "　") {
			Logger.error("質問の長さ -> 質問は必ず入力してください。")
			titleErrorMessage = "質問は必ず入力してください。"
			false
		} else {
			// 質問件名の長さが200文字以内の場合はTrue
			if (questionTitle.length <= Constants.questionTitleLength) {
				Logger.debug("質問タイトルの長さ(%d) <= ".format(questionTitle.length) + Constants.questionTitleLength)
				true
			} else {
				Logger.error("質問の長さ -> %d文字内で入力してください。".format(Constants.questionTitleLength))
				titleErrorMessage = "%d文字内で入力してください。".format(Constants.questionTitleLength)
				false
			}
		}
	}

	/**
	 * 項目の文字数が上限以下かどうかをチェックします。
	 * @return 正常の場合はTrue
	 */
	def questionItemLengthCheck(): Boolean = {

		val errMessage = "%d文字以内で入力してください。"

		var retValue: Boolean = true
		var orderNum: Int = 1
		questionItems.foreach {
			keyValue => {
				val itemRetValue: Boolean = if (keyValue._2.length <= Constants.questionItemLength && keyValue._2.trim.length > 0) {

					Logger.debug("質問項目の長さNo.%d => ".format(orderNum) + questionTitle.length)
					true
				} else {

					val message = if (keyValue._2.trim.length == 0) {
						"質問の項目は必ず入力してください。"
					} else {
						errMessage.format(Constants.questionTitleLength)
					}

					Logger.error("質問項目の長さ No.%d".format(orderNum) + " -> " + errMessage.format(orderNum, Constants.questionTitleLength))

					SetItemErrorMessage(orderNum, message)

					false
				}

				orderNum += 1
				retValue = itemRetValue && retValue
			}
		}

		Logger.debug("retValue:" + retValue)
		retValue
	}

	/**
	 * 入力可能文字列長が正しい値かをチェックします。
	 * @return
	 */
	def ansInputMaxLengthCheck(): Boolean = {

		val retValue: Boolean = if (ansInputMaxLength <= 0) {
			ansInputMaxLengthError = "入力可能文字数は1以上を入力してください。"
			false
		} else if (ansInputMaxLength > 4000) {
			ansInputMaxLengthError = "入力可能文字数は4000文字以内で入力してください。"
			false
		} else {
			true
		}

		retValue
	}

	private def SetItemErrorMessage(orderNum: Int, message: String) {
		itemErrorMessage.contains(orderNum) match {
			case true => itemErrorMessage.get(orderNum).get += message
			case false => itemErrorMessage.put(orderNum, mutable.Set(message))
		}
	}

	/**
	 * 同一のデータを保持しているか確認します。
	 * @return 正常の場合はTrue
	 */
	def notExistsSameData(): Boolean = {

		// 重複チェックを行うための Map[String, Set[Key]]
		val duplicateCheckMap: mutable.Map[String, mutable.Set[String]] = mutable.Map()
		val orderItemKeyMap: mutable.Map[Int, String] = mutable.Map()

		var orderNum: Int = 1
		questionItems.foreach {
			keyValue => {

				// 質問項目をキーとして質問番号を値にセットするMAPを作成する
				if (!duplicateCheckMap.contains(keyValue._2)) {
					val set: mutable.Set[String] = mutable.Set(keyValue._1)
					duplicateCheckMap += {
						keyValue._2 -> set
					}
				} else {
					duplicateCheckMap.get(keyValue._2).get += keyValue._1
				}

				// 連番と質問番号のMAPを作成する
				orderItemKeyMap += {
					orderNum -> keyValue._1
				}
				orderNum += 1
			}
		}

		// 質問項目に対して質問番号が複数存在するものは重複のためエラーをセットする
		var retValue: Boolean = true
		duplicateCheckMap.filter {
			kv => kv._2.size > 1
		}.foreach {
			keyValue =>
				keyValue._2.foreach {
					keys =>
						val o = orderItemKeyMap.filter {
							kv => kv._2 == keys
						}.head._1
						val message = "同一の値が設定されています。"
						SetItemErrorMessage(o, message)

						// 戻り値にFalseをセット
						retValue = false
				}
		}

		Logger.debug("retValue:" + retValue)
		retValue
	}

	/**
	 * 選択個数と選択対象個数が正しい関係かチェックします。
	 * @return 正常の場合はTrue
	 */
	def selectItemCountCheck(): Boolean = {

		val invalidCountMessage = "チェック個数は1以上項目数未満の値を設定してください。"

		// 項目個数 + その他
		val elseCount = if (isElseDataChecked) 1 else 0
		val itemSize = questionItems.size + elseCount

		// 設定値が0または項目数以上の場合はエラーとする
		// note:演算子はチェック対象ではない…？
		val retValue = if (isRequired && (selectItemCount <= 0 || selectItemCount >= itemSize)) {
			selectItemCountError = invalidCountMessage
			false
		} else {
			true
		}

		retValue
	}

	/**
	 * エラーメッセージを取得します。
	 * @return チェック内容とエラー内容のMap
	 */
	def getErrorMessage: String = {
		titleErrorMessage
	}

	/**
	 * 各項目のエラーメッセージを取得します。
	 * @return
	 */
	def getItemErrorMessage: Map[Int, mutable.Set[String]] = {
		itemErrorMessage.toMap
	}

	/**
	 * 選択個数チェックのエラーメッセージを取得します。
	 * @return
	 */
	def getSelectItemCountError: String = {
		selectItemCountError
	}

	/**
	 * 入力可能文字数のチェックエラーメッセージを取得します。
	 * @return
	 */
	def getAnsInputMaxLength: String = {
		ansInputMaxLengthError
	}
}

object FormItemValueValidator {

	/**
	 * インスタンスを生成します。
	 * @param questionType 質問の形式
	 * @param questionItems 質問項目
	 * @return 生成したインスタンス
	 */
	def createInstance(questionType: QuestionType, questionTitle: String, ansInputMaxLength: Int, isRequired: Boolean, questionItems: SortedMap[String, String])
	: FormItemValueValidator = {
		createInstance(questionType, questionTitle, ansInputMaxLength, 0, "", false, isRequired, questionItems)
	}

	/**
	 * インスタンスを生成します。
	 * @param questionType 質問の形式
	 * @param questionItems 質問項目
	 * @return 生成したインスタンス
	 */
	def createInstance(questionType: QuestionType, questionTitle: String, ansInputMaxLength: Int, isElseDataChecked: Boolean
	                   , isRequired: Boolean, questionItems: SortedMap[String, String]): FormItemValueValidator = {
		createInstance(questionType, questionTitle, ansInputMaxLength, 0, "", isElseDataChecked, isRequired, questionItems)
	}

	/**
	 * インスタンスを生成します。
	 * @param questionType 質問の形式
	 * @param questionTitle 質問タイトル
	 * @param ansInputMaxLength 回答文字数
	 * @param selectItemCount 選択個数
	 * @param selectItemCountOp 選択個数演算子
	 * @param isElseDataChecked その他チェックフラグ
	 * @param isRequired 必須項目(true:必須 false:必須でない)
	 * @param questionItems 質問項目
	 * @return 生成したインスタンス
	 */
	def createInstance(questionType: QuestionType, questionTitle: String, ansInputMaxLength: Int, selectItemCount: Int = 0, selectItemCountOp: String = ""
	                   , isElseDataChecked: Boolean = false, isRequired: Boolean, questionItems: SortedMap[String, String]): FormItemValueValidator = {

		val instance: FormItemValueValidator = questionType match {
			case QuestionType.TextLine => {
				val instance = new TextLineValidator(questionTitle, questionItems)
				instance.ansInputMaxLength = ansInputMaxLength
				return instance
			}
			case QuestionType.TextMultiLine => {
				val instance = new TextMultiLineValidator(questionTitle, questionItems)
				instance.ansInputMaxLength = ansInputMaxLength
				return instance
			}
			case QuestionType.Radio => new RadioButtonValidator(questionTitle, questionItems)
			case QuestionType.Checkbox => {
				val instance = new CheckBoxValidator(questionTitle, questionItems)
				instance.selectItemCount = selectItemCount
				instance.selectItemCountOp = selectItemCountOp
				instance.isElseDataChecked = isElseDataChecked
				instance.isRequired = isRequired
				return instance
			}
			case QuestionType.DropList => new DropListValidator(questionTitle, questionItems)
			case QuestionType.TextDate => new DateValidator(questionTitle, questionItems)
			case QuestionType.TextRegx => new RegxValidator(questionTitle, questionItems)
		}

		return instance
	}
}
