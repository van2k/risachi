package models.validation

import collection.SortedMap

/**
 * テキスト行チェックロジック
 * @param formItemName 質問のタイトル
 * @param questionItems 質問の項目
 */
class DateValidator(formItemName: String, questionItems: SortedMap[String, String])
	extends FormItemValueValidator(formItemName: String, questionItems: SortedMap[String, String]) {

	def executeCheck(): Boolean = {
		questionTitleLengthCheck
	}
}
