package models

import java.sql.SQLException
import scalikejdbc._


case class Administrator(
	                        id: Long,
	                        userId: String
	                        )

object Administrator extends models.Model {

  val tableName = "administrator"

  object columnNames {
    val id = "id"
    val userId = "userid"
    val all = Seq(id, userId)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Administrator(
      id = rs.long(id),
      userId = rs.string(userId))
  }


  val autoSession = AutoSession

	def insert(userId: String, pass: String)(implicit session: DBSession = autoSession) = {
		val sql = "INSERT INTO administrator (userid, password) VALUES ({userid}, {pass})"

		try {
			SQL(sql).bindByName('userid -> userId, 'pass -> md5(pass)).update.apply()
			true
		} catch {
			case e: SQLException => false
		}
	}

	def getAllList()(implicit session: DBSession = autoSession):List[Administrator] = {
		val sql = "SELECT id, userid FROM administrator ORDER BY id"

		SQL(sql).map(*).list.apply()
	}

	def changePass(id: Long, pass: String)(implicit session: DBSession = autoSession) = {
		val sql = "UPDATE administrator SET password={pass} WHERE id={id}"

		try {
			val retCount = SQL(sql).bindByName(
				'id -> id,
				'pass -> md5(pass)
			).update.apply
      retCount > 0
		} catch {
			case e: SQLException => false
		}
	}

	def remove(id: Long)(implicit session: DBSession = autoSession) = {
		val sql = "DELETE FROM administrator WHERE id={id}";

		try {
			val retCount = SQL(sql).bindByName(
				'id -> id
			).update.apply()

      retCount > 0
		} catch {
			case e: SQLException => false
		}
	}

	def findByUseridAndPass(userId: String, pass: String)(implicit session: DBSession = autoSession): Option[Administrator] = {
		val sql = "SELECT id, userid FROM administrator WHERE userid = {userid} AND password = {pass}"

		SQL(sql).bindByName('userid -> userId, 'pass -> md5(pass)).map(*).single.apply
	}

	def findById(id: Long)(implicit session: DBSession = autoSession): Option[Administrator] = {
		val sql = "SELECT id,userid FROM administrator WHERE id={id}"
		SQL(sql).bindByName('id -> id).map(*).single().apply()
	}
}
