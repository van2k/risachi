package models.exception


/**
 * データベースへの登録・更新でエラーとなり処理続行が不可能な場合にスローする例外
 */
class DbExecuteFailException extends Exception {

}
