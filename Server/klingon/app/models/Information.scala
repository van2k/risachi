package models

import java.util.{Date, Calendar}
import java.sql.SQLException
import scalikejdbc._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import java.text.SimpleDateFormat


case class Information(
	                      id: Long,
	                      sendDate: Date,
	                      subject: String,
	                      content: String
	                      )

// お知らせ追加用
case class addInformation(
	                         id: String,
	                         subject: String,
	                         content: String
	                         )

// お知らせ検索用
case class SearchInformationContext(
	                                   date_from: String,
	                                   date_to: String,
	                                   subject: String,
	                                   content: String
	                                   )

object Information extends models.Model {

  val tableName = "information"

  object columnNames {
    val id = "id"
    val sendDate = "send_date"
    val subject = "subject"
    val content = "content"
    val all = Seq(id, sendDate, subject, content)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Information(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toJavaUtilDate,
      subject = rs.string(subject),
      content = rs.string(content))
  }

  val autoSession = AutoSession

  /**
	 * お知らせを登録する。
	 *
	 * @param subject 件名
	 * @param content 本文
	 */
	def insert(subject: String, content: String)(implicit session: DBSession = autoSession) = {
		SQL(
			"""
			  |INSERT INTO information (send_date, subject, content) values (
			  |  {send_date}, {subject}, {content}
			  |);
			""".stripMargin
		).bindByName(
			'send_date -> Calendar.getInstance().getTime(),
			'subject -> subject,
			'content -> content
		).update.apply()
	}

	def search(searchContext: SearchInformationContext)(implicit session: DBSession = autoSession): List[Information] = {

		val whereList = ListBuffer[String]()
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()
		val dateFormat = new SimpleDateFormat("yyyy/MM/dd")

		if (searchContext.date_from != "") {
			whereList += "send_date >= {send_date_from}"
			val dateFrom = Calendar.getInstance()
			dateFrom.setTime(dateFormat.parse(searchContext.date_from))
			params = params ++ Seq('send_date_from -> dateFrom.getTime)
		}
		if (searchContext.date_to != "") {
			whereList += "send_date < {send_date_to}"
			val dateTo = Calendar.getInstance()
			dateTo.setTime(dateFormat.parse(searchContext.date_to))
			dateTo.add(Calendar.DATE, 1)
			params = params ++ Seq('send_date_to -> dateTo.getTime)
		}
		if (searchContext.subject != "") {
			whereList += "subject LIKE {subject}"
			params = params ++ Seq('subject -> ("%" + searchContext.subject + "%"))
		}
		if (searchContext.content != "") {
			whereList += "content LIKE {content}"
			params = params ++ Seq('content -> ("%" + searchContext.content + "%"))
		}
		var where = ""
		whereList.toList.foreach {
			where_str =>
				where += {
					if (where == "") " WHERE " else " AND "
				}
				where += where_str
		}

		val sql = "SELECT * FROM information " + where + " ORDER BY id"

		SQL(sql).bindByName(params:_*).map(*).list.apply()
	}

	def list()(implicit session: DBSession = autoSession): List[Information] = {
    SQL(
      "SELECT * FROM information ORDER BY id DESC LIMIT 3"
    ).map(*).list.apply()
	}

	/**
	 * お知らせIDからお知らせデータを取り出す
	 * @param id お知らせID
	 * @return 取得したinformationテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[Information] = {
		val sql =
			"""
			  |SELECT id, send_date, subject, content
			  | FROM information
			  | WHERE id = {id}
			""".stripMargin

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	def delete(id: Long)(implicit session: DBSession = autoSession) = {
		val sql = "DELETE FROM information WHERE id={id}"
		try {
      val retCount = SQL(sql).bindByName(
				'id -> id
			).update.apply()

      retCount > 0

		} catch {
			case e: SQLException => false
		}
	}

	def update(id: Long, subject: String, content: String)(implicit session: DBSession = autoSession) = {
		val retCount = SQL(
			"UPDATE information SET send_date={send_date}, subject={subject}, content={content} WHERE id={id};"
		).bindByName(
			'send_date -> Calendar.getInstance().getTime(),
			'subject -> subject,
			'content -> content,
			'id -> id
		).update.apply()

    retCount > 0
	}

}
