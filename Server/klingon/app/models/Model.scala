package models

import play.api.Logger
import java.text.SimpleDateFormat
import java.security.MessageDigest
import java.util.Calendar
import java.math.BigInteger

class Model {

	/*
	 * ハッシュ値を取得する
	 * SHA-1＋時間
	 */
	def getHashCode(): String = {
		val cal = Calendar.getInstance()
		val df2 = new SimpleDateFormat("yyyyMMddHHmmssSSS")
		val md = MessageDigest.getInstance("SHA-1")

		md.update(System.nanoTime().toString.getBytes)

		val code = md.digest.foldLeft("") {
			(s, b) => s + "%02x".format(if (b < 0) b + 256 else b)
		}
		val hash = code + new BigInteger(df2.format(cal.getTime())).toString(36)
		Logger.debug(hash)

		return hash
	}

	/*
	 * dataをMD5に変換する
	 * @data 変換する文字列
	 * @return MD5変換後の文字列
	 */
	def md5(data: String): String = {
		val digestedBytes = MessageDigest.getInstance("MD5").digest(data.getBytes)
		digestedBytes.map("%02x".format(_)).mkString
	}
}