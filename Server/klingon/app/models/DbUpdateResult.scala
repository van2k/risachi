package models

object DbUpdateResult extends Enumeration {
	val success, dbConcurrency, fail = Value
}
