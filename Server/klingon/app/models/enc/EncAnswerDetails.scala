package models.enc


import java.util.{Calendar, Date}
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._

case class EncAnswerDetails(
	                           id: Long,
	                           answerId: Long,
	                           formItemId: Long,
	                           formItemValueId: Long,
	                           answerValue: String,
	                           answerAt: Option[Date],
	                           createdAt: Date,
	                           updatedAt: Date
	                           ) {
	override def toString = {
		"id:%s answerId:%s formItemId:%s formItemValueId:%s answerValue:%s ".format(
			id, answerId, formItemId, formItemValueId, answerValue)
	}
}

object EncAnswerDetails extends models.Model {


  val tableName = "answer_details"

  object columnNames {
    val id = "id"
    val answerId = "answer_id"
    val formItemId = "form_item_id"
    val formItemValueId = "form_item_value_id"
    val answerValue = "answer_value"
    val answerAt = "answer_at"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, answerId, formItemId, formItemValueId, answerValue, answerAt, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncAnswerDetails(
      id = rs.long(id),
      answerId = rs.long(answerId),
      formItemId = rs.long(formItemId),
      formItemValueId = rs.long(formItemValueId),
      answerValue = rs.string(answerValue),
      answerAt = rs.dateOpt(answerAt),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

	def getBrankAnswerDetail(): EncAnswerDetails = {
		val cal = Calendar.getInstance()
		val now = cal.getTime
		return EncAnswerDetails(-1, -1, -1, -1, "0", Option(now), now, now)
	}

  /**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncAnswerDetails] = {
		val sql =
			"""
			  |SELECT id, answer_id, form_item_id, form_item_value_id, answer_value, answer_at, created_at, updated_at
			  |  FROM answer_details
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * idからデータを取得します。
	 *
	 * @param formitemid
	 * @return 取得したFormsテーブルのデータ
	 */
	def findByFormitemid(formitemid: Long)(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
		val sql =
			"""
			  |SELECT id, answer_id, form_item_id, form_item_value_id, answer_value, answer_at, created_at, updated_at
			  |  FROM answer_details
			  | WHERE form_item_id = {form_item_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('form_item_id -> formitemid).map(*).list.apply()
	}

	/**
	 * idからデータを取得します。
	 *
	 * @param answerid
	 * @param formitemid
	 * @return
	 */
	def findByAnsweridFormitemid(answerid: Long, formitemid: Long)(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
		val sql =
			"""
			  |SELECT id, answer_id, form_item_id, form_item_value_id, answer_value, answer_at, created_at, updated_at
			  |  FROM answer_details
			  | WHERE answer_id = {answer_id} and form_item_id = {form_item_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('answer_id -> answerid, 'form_item_id -> formitemid).map(*).list.apply()
	}

	/**
	 * idからデータを取得します。
	 *
	 * @param answerid
	 * @param formitemid
	 * @param formitemvalueid
	 * @return
	 */
	def findByAnsweridFormitemidFormitemvalueid(answerid: Long, formitemid: Long, formitemvalueid: Long
                                              )(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
		val sql =
			"""
			  |SELECT id, answer_id, form_item_id, form_item_value_id, answer_value, answer_at, created_at, updated_at
			  |  FROM answer_details
			  | WHERE answer_id = {answer_id} and form_item_id = {form_item_id} and form_item_value_id = {form_item_value_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'answer_id -> answerid,
			'form_item_id -> formitemid,
			'form_item_value_id -> formitemvalueid
		).map(*).list.apply()
	}

	/**
	 * idからデータを取得します。
	 *
	 * @param answerid
	 * @param pageNo
	 * @return
	 */
	def findByAnsweridPageno(answerid: Long, pageNo: Long)(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
		val sql =
			"""
			  |SELECT a.id, a.answer_id, a.form_item_id, a.form_item_value_id, a.answer_value, a.answer_at, a.created_at, a.updated_at
			  |  FROM answer_details a left join form_items b on a.form_item_id = b.id
			  | WHERE a.answer_id = {answer_id} and b.page_no = {page_no}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'answer_id -> answerid,
			'page_no -> pageNo
		).map(*).list.apply()
	}

	/**
	 * データを登録します。
	 *
	 * @param encAnswerDetails
	 * @return
	 */
	def regist(encAnswerDetails: EncAnswerDetails)(implicit session: DBSession = autoSession) = {

		Logger.debug("データの登録開始")

		val sql =
			"""
			  |INSERT INTO answer_details(
			  |            answer_id, form_item_id, form_item_value_id, answer_value, answer_at, created_at, updated_at)
			  |    VALUES ({answer_id}, {form_item_id}, {form_item_value_id}, {answer_value}, {answer_at},
			  |            current_timestamp, current_timestamp);
			""".stripMargin

		Logger.debug("sql:" + sql)

		var retValue = DbUpdateResult.success

		val newAnswerDetailId = SQL(sql).bindByName(
			'answer_id -> encAnswerDetails.answerId,
			'form_item_id -> encAnswerDetails.formItemId,
			'form_item_value_id -> encAnswerDetails.formItemValueId,
			'answer_value -> encAnswerDetails.answerValue,
			'answer_at -> encAnswerDetails.answerAt
		).updateAndReturnGeneratedKey.apply()

		Logger.debug("newAnswerDetailId:" + newAnswerDetailId)

		retValue = DbUpdateResult.success


		// 戻り値を返す
		retValue
	}

	def removeByAnsweridPageno(answerId: Long, pageNo: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM answer_details a
			  | WHERE answer_id={answer_id} and EXISTS(select * from form_items b where a.form_item_id = b.id and b.page_no = {page_no}) ;
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'answer_id -> answerId,
			'page_no -> pageNo
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	def removeById(id: Long)(implicit session: DBSession = autoSession) = {

		Logger.debug("データの削除開始")

		val sql =
			"""
			  |DELETE answer_details
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName('id -> id).update.apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	def update(encAnswerDetails: EncAnswerDetails)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE answer_details
			  |   SET answer_id = {answer_id}, form_item_id = {form_item_id}, form_item_value_id = {form_item_value_id}, answer_value = {answer_value}, answer_at = {answer_at}
			  |     , updated_at=current_timestamp
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success


		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS")
		Logger.debug("日付の確認" + sdf.format(encAnswerDetails.updatedAt))

		val retCount = SQL(sql).bindByName(
			'answer_id -> encAnswerDetails.answerId,
			'form_item_id -> encAnswerDetails.formItemId,
			'form_item_value_id -> encAnswerDetails.formItemValueId,
			'answer_value -> encAnswerDetails.answerValue,
			'answer_at -> encAnswerDetails.answerAt,
			'id -> encAnswerDetails.id
		).update.apply()

		Logger.debug("処理件数:" + retCount)
		Logger.debug("value:" + encAnswerDetails.answerValue)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	def deleteByAnswerId(answerId: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  |DELETE FROM answer_details a
			  | WHERE answer_id = /*'answer_id*/-1;
			""".stripMargin

		val retCount = SQL(sql).bindByName(
			'answer_id -> answerId
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}

	/*
	* FormIDによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  |DELETE FROM answer_details a
			  | WHERE EXISTS (select * from answers b where a.answer_id = b.id and b.form_id={form_id});
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> formid
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
