package models.enc

import play.api.Logger
import scalikejdbc.{DBSession, SQL, AutoSession, WrappedResultSet}
import scala.collection.mutable

case class EncAnalyze(
	                     name: String,
	                     total: java.math.BigDecimal,
	                     value: String
	                     ) {}

object EncAnalyze extends models.Model {

  object columnNames {
    val name = "name"
    val total = "total"
    val value = "value"
    val all = Seq(name, total, value)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncAnalyze(
      name = rs.string(name),
      total = rs.bigDecimal(total),
      value = rs.string(value))
  }

  val autoSession = AutoSession


  /*
		* 最後までアンケートをした人の数を取得
		*/
	def getEncEndCount(formId: Long, formItemValueIds: Option[List[String]], isAnd: Boolean, isPreview: Boolean)(implicit session: DBSession = autoSession): Long = {
    val sql =
      """
        	 		  | SELECT count(*) as cnt
        	 		  | FROM answers a
        	 		  | WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is not null and
        	 		  | exists (select * from answer_details b where a.id = b.answer_id {addwhere})
        	 		  | and a.is_preview = {ispreview}
      			""".stripMargin

		var addwhere = ""
	  var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()
		if (formItemValueIds.isDefined) {
			addwhere += " and ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}

		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('ispreview -> isPreviewStr):_*).map(_.long(1)).single().apply().get

	}

	/*
	* 途中までアンケートをした人の数を取得
	*/
	def getEncPartwayCount(formId: Long, formItemValueIds: Option[List[String]], isAnd: Boolean, isPreview: Boolean
                         )(implicit session: DBSession = autoSession): Long = {
		val sql =
			"""
			  | SELECT count(*) as cnt
			  | FROM answers a
			  | WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is null
			  | and a.is_preview = {ispreview}
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			addwhere += " add ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}

		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('ispreview -> isPreviewStr):_*).map(_.long(1)).single.apply.get
	}

	/*
	 * 未回答者から途中まで回答した設問を分析
	 */
	def aggNonansByFormid(formId: Long, formItemValueIds: Option[List[String]], isAnd: Boolean, isPreview: Boolean
                        )(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT name, sum(total) as total, '' as value
			  | FROM (
			  |  SELECT a.name,cast(0 as numeric) as total
			  |  FROM form_items a
			  |  WHERE form_id = {form_id}
			  |   UNION
			  |  SELECT d.name,cast(count(*) as numeric) as total
			  |  FROM answers a, answer_details b,
			  |       (select min(id) as id from answer_details where answer_at is not null group by answer_id,form_item_id) c,
			  |       form_items d
			  |  WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is null and
			  |        a.id = b.answer_id and b.id = c.id and
			  |        exists (select * from answer_details b where a.id = b.answer_id {addwhere}) and
			  |        d.id = b.form_item_id and
			  |        a.is_preview = {ispreview}
			  |  GROUP BY name
			  | ) a
			  | GROUP by name
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			addwhere += " and ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}
		Logger.debug("addwhere:" + addwhere)
		Logger.debug("sql:" + sql)
		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}

	/**
	 * formIdから回答時間を分析
	 * @param formId formsテーブルのID
	 * @return 回答集計データ
	 */
	def aggAnsMinuteByFormid(formId: Long, formItemValueIds: Option[List[String]], minute: Long, isAnd: Boolean
                           , isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT to_char(total * {min},'999') || '分～' as name , cast(count(*) as numeric) as total, '' as value
			  | FROM (
			  |  SELECT trunc(date_part('minute', end_at-start_at) / {min}) as total
			  |  FROM answers a
			  |  WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is not null and
			  |  exists (select * from answer_details b where a.id = b.answer_id {addwhere}) and
			  |  a.is_preview = {ispreview}
			  |  ) as x
			  | group by total
			  | order by name
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			addwhere += " and ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}

		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('min -> minute) ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}

	/**
	 * formIdから回答時間帯を分析
	 * @param formId formsテーブルのID
	 * @return 回答集計データ
	 */
	def aggAnsHourByFormid(formId: Long, formItemValueIds: Option[List[String]], isAnd: Boolean, isPreview: Boolean
                         )(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT to_char(hour, '99') || '時台' as name, cast(count(*) as numeric) as total, '' as value
			  |  FROM (
			  |  SELECT date_part('hour', start_at) as hour
			  |  FROM answers a
			  |  WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is not null and
			  |   exists (select * from answer_details b where a.id = b.answer_id {addwhere}) and
			  |   a.is_preview = {ispreview}
			  |  ) as x
			  | group by hour
			  | order by name
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			addwhere += " and ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}

		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}

	/**
	 * formIdから回答日を分析
	 * @param formId formsテーブルのID
	 * @return 回答集計データ
	 */
	def aggAnsDateByFormid(formId: Long, formItemValueIds: Option[List[String]], dateFormat: String, isAnd: Boolean
                         , isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT dt as name,cast(count(*) as numeric) as total, '' as value
			  | FROM (
			  |  SELECT to_char(start_at,{dateformat}) as dt
			  |  FROM answers a
			  |  WHERE a.form_id = {form_id} and a.start_at is not null and a.end_at is not null and
			  |        a.is_preview = {ispreview} and
			  |  exists (select * from answer_details b where a.id = b.answer_id {addwhere}) and
			  |  is_preview = {ispreview}
			  | ) as x
			  | group by dt
			  | order by name
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			addwhere += " and ( "
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += " (b.form_item_value_id = {item_value_id" + i + "})"
				params = params ++ Seq(Symbol("item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
			addwhere += ")"
		}

		val isPreviewStr = if (isPreview) "1" else "0"
		val sql1 = sql.replace("{addwhere}", addwhere)
		SQL(sql1).bindByName(params ++ Seq('form_id -> formId) ++ Seq('dateformat -> dateFormat) ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}

	/**
	 * formIdから回答集計データを取得します。
	 *
	 * MEMO:テスト時にh2DBを利用すると、何故か最終的なSELECT句
	 *   SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 * の「value_name as name」という別名定義部分がnameとして取得できない為、
	 *   SELECT form_id,form_item_id,item_name,name,total,value
	 *   FROM (
	 *     SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 *     ---省略---
	 *     GROUP BY form_id,form_item_id,item_name,value_name
	 *   ) as T
	 * のようにテーブル名を付与し、そのテーブルから別名定義したカラム名を表示するようにしています。
	 *
	 * @param formId formsテーブルのID
	 * @return 回答集計データ
	 */
	def aggAnsByFormid(formId: Long, isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT form_id,form_item_id,item_name,name,total,value
			  | FROM (
			  |  SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
			  |    SELECT a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,0 as cnt
			  |    FROM form_items a left join form_item_values b on a.id = b.form_item_id
			  |    WHERE form_id = {form_id}
			  |  UNION
			  |    SELECT a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,count(*) as cnt
			  |    FROM form_items a
			  |      LEFT JOIN form_item_values b on a.id = b.form_item_id
			  |      INNER JOIN answer_details c on b.id = c.form_item_value_id,answers d
			  |    WHERE a.form_id = {form_id} and
			  |      d.id = c.answer_id and
			  |      d.end_at is not null and
			  |      d.is_preview = {ispreview}
			  |    GROUP BY a.form_id,b.form_item_id,a.name,b.form_item_id,b.form_value) a
			  |  GROUP BY form_id,form_item_id,item_name,value_name
			  | ) as T
			  | ORDER BY form_id,form_item_id
			""".stripMargin

		Logger.debug("sql:" + sql)

		val isPreviewStr = if (isPreview) "1" else "0"
		SQL(sql).bindByName('form_id -> formId, 'ispreview -> isPreviewStr).map(*).list.apply()
	}

	/**
	 * formIdとformItemIdから回答集計データを取得します。
	 *
	 * MEMO:テスト時にh2DBを利用すると、何故か最終的なSELECT句
	 *   SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 * の「value_name as name」という別名定義部分がnameとして取得できない為、
	 *   SELECT form_id,form_item_id,item_name,name,total,value
	 *   FROM (
	 *     SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 *     ---省略---
	 *     GROUP BY form_id,form_item_id,item_name,value_name
	 *   ) as T
	 * のようにテーブル名を付与し、そのテーブルから別名定義したカラム名を表示するようにしています。
	 *
	 *
	 * @param formId
	 * @param formItemId
	 * @param isPreview
	 * @param session
	 * @return 回答集計データ
	 */
	def aggAnsByFormidFormitemid(formId: Long, formItemId: Long, isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  |SELECT form_id,form_item_id,item_name,name,total,value
			  | FROM (
			  |  SELECT id,form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
			  |    SELECT b.id,a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,0 as cnt
			  |    FROM form_items a
			  |      LEFT JOIN form_item_values b
			  |      ON a.id = b.form_item_id
			  |    WHERE a.form_id = {form_id}
			  |      and b.form_item_id = {form_item_id}
			  |  UNION
			  |    SELECT b.id,a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,count(*) as cnt
			  |    FROM form_items a
			  |      LEFT JOIN form_item_values b
			  |      ON a.id = b.form_item_id
			  |      INNER JOIN answer_details c
			  |      ON b.id = c.form_item_value_id
			  |      INNER JOIN answers d
			  |      ON d.id = c.answer_id
			  |    WHERE a.form_id = {form_id}
			  |      and b.form_item_id = {form_item_id}
			  |      and d.end_at is not null
			  |      and d.is_preview = {ispreview}
			  |    GROUP BY b.id,a.form_id,b.form_item_id,a.name,b.form_item_id,b.form_value) a
			  |  GROUP BY id,form_id,form_item_id,item_name,value_name
			  |) as T
			  | ORDER BY id,form_id,form_item_id
			""".stripMargin

		Logger.debug("sql:" + sql)

		val isPreviewStr = if (isPreview) "1" else "0"
		SQL(sql).bindByName('form_id -> formId, 'form_item_id -> formItemId, 'ispreview -> isPreviewStr).map(*).list.apply()
	}

	/**
	 * formIdとformItemValueIdから回答集計データを取得します。
	 *
	 * MEMO:テスト時にh2DBを利用すると、何故か最終的なSELECT句
	 *   SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 * の「value_name as name」という別名定義部分がnameとして取得できない為、
	 *   SELECT form_id,form_item_id,item_name,name,total,value
	 *   FROM (
	 *     SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 *     ---省略---
	 *     GROUP BY form_id,form_item_id,item_name,value_name
	 *   ) as T
	 * のようにテーブル名を付与し、そのテーブルから別名定義したカラム名を表示するようにしています。
	 *
	 *
	 * @param formId formsテーブルのID
	 * @param formItemValueIds 回答IDList
	 * @param isAnd true:AND集計 or:OR集計
	 * @return 回答集計データ
	 */
	def aggAnsByFormidFormitemidFormitemvalueid(formId: Long, formItemId: Long, formItemValueIds: Option[List[String]]
                                              , isAnd: Boolean, isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT form_id,form_item_id,item_name,name,total,value
			  | FROM (
			  |   SELECT id,form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
			  |     SELECT b.id,a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,0 as cnt
			  |     FROM form_items a left join form_item_values b on a.id = b.form_item_id
			  |     WHERE a.form_id = {form_id} and b.form_item_id = {form_item_id}
			  |   UNION
			  |     SELECT b.id,a.form_id,b.form_item_id,a.name as item_name,b.form_value as value_name,count(*) as cnt
			  |     FROM form_items a
			  |       LEFT JOIN form_item_values b on a.id = b.form_item_id
			  |       INNER JOIN answer_details c on b.id = c.form_item_value_id,
			  |       answers d
			  |     WHERE a.form_id = {form_id} and
			  |       b.form_item_id = {form_item_id} and
			  |       exists (select * from answer_details e where d.id = e.answer_id and {addwhere}) and
			  |       d.id = c.answer_id and
			  |       d.end_at is not null and
			  |       d.is_preview = {ispreview}
			  |     GROUP BY b.id,a.form_id,b.form_item_id,a.name,b.form_item_id,b.form_value) a
			  |   GROUP BY id,form_id,form_item_id,item_name,value_name
			  | ) AS T
			  | ORDER BY id,form_id,form_item_id
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += "e.form_item_value_id = {form_item_value_id" + i + "}"
				params = params ++ Seq(Symbol("form_item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
		}

		Logger.debug("sql:" + sql)
		Logger.debug("formId:" + formId)
		Logger.debug("formItemId:" + formItemId)
		Logger.debug("addwhere:" + addwhere)

		val isPreviewStr = if (isPreview) "1" else "0"
		SQL(sql.replace("{addwhere}", addwhere)).bindByName(params ++ Seq('form_id -> formId) ++ Seq('form_item_id -> formItemId)
       ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}

	/**
	 * formIdとformItemValueIdから回答集計データを取得します。
	 *
	 * MEMO:テスト時にh2DBを利用すると、何故か最終的なSELECT句
	 *   SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 * の「value_name as name」という別名定義部分がnameとして取得できない為、
	 *   SELECT form_id,form_item_id,item_name,name,total,value
	 *   FROM (
	 *     SELECT form_id,form_item_id,item_name,value_name as name,sum(cnt) as total, '' as value from(
	 *     ---省略---
	 *     GROUP BY form_id,form_item_id,item_name,value_name
	 *   ) as T
	 * のようにテーブル名を付与し、そのテーブルから別名定義したカラム名を表示するようにしています。
	 *
	 *
	 * @param formId formsテーブルのID
	 * @param formItemValueIds 回答IDList
	 * @param isAnd true:AND集計 or:OR集計
	 * @return 回答集計データ
	 */
	def aggAnsValueByFormidFormitemidFormitemvalueid(formId: Long, formItemId: Long
                                                   , formItemValueIds: Option[List[String]], isAnd: Boolean
                                                   , isPreview: Boolean)(implicit session: DBSession = autoSession): List[EncAnalyze] = {
		val sql =
			"""
			  | SELECT name,total,value
			  | FROM (
			  |   SELECT a.name as name,cast(0 as numeric) as total,c.answer_value as value,a.form_id,b.form_item_id
			  |   FROM form_items a
			  |     LEFT JOIN form_item_values b on a.id = b.form_item_id
			  |     INNER JOIN answer_details c on b.id = c.form_item_value_id
			  |     LEFT JOIN answers d on d.id = c.answer_id and d.is_preview = {ispreview} and d.end_at is not null
			  |   WHERE a.form_id = {form_id} and b.form_item_id = {form_item_id} and
			  |     exists (select * from answer_details e where d.id = e.answer_id and {addwhere})
			  | ) AS T
			  | ORDER BY form_id,form_item_id
			""".stripMargin

		var addwhere = ""
		var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()

		if (formItemValueIds.isDefined) {
			for (i <- 0 until formItemValueIds.get.length) {
				addwhere += "e.form_item_value_id = {form_item_value_id" + i + "}"
				params = params ++ Seq(Symbol("form_item_value_id" + i) -> formItemValueIds.get(i).toString())
				if (i < formItemValueIds.get.length - 1) {
					addwhere += (if (isAnd) " and " else " or ")
				}
			}
		} else {
			addwhere += " 1 = 1 "
		}

		Logger.debug("sql:" + sql)
		Logger.debug("formId:" + formId)
		Logger.debug("formItemId:" + formItemId)
		Logger.debug("addwhere:" + addwhere)

		val isPreviewStr = if (isPreview) "1" else "0"
		SQL(sql.replace("{addwhere}", addwhere)).bindByName(params ++ Seq('form_id -> formId) ++ Seq('form_item_id -> formItemId)
       ++ Seq('ispreview -> isPreviewStr):_*).map(*).list.apply()
	}
}
