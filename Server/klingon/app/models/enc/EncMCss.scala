package models.enc

import java.util.Date
import play.api.Logger
import scalikejdbc._

case class EncMCss(
	                  id: Long,
	                  name: String,
	                  cssCode: String,
	                  dispImageUrl: String,
	                  createdAt: Date,
	                  updatedAt: Date
	                  ) {
	override def toString = {
		"id:%d name:%s cssCode:%s dispImageUrl:%s".format(
			id, name, cssCode, dispImageUrl)
	}
}

object EncMCss extends models.Model {

  val tableName = "m_css"

  object columnNames {
    val id = "id"
    val name = "name"
    val cssCode = "css_code"
    val dispImageUrl = "disp_image_url"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, name, cssCode, dispImageUrl, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMCss(
      id = rs.long(id),
      name = rs.string(name),
      cssCode = rs.string(cssCode),
      dispImageUrl = rs.string(dispImageUrl),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

  /**
	 * 全てのデータ取得
	 * @return 取得したm_cssテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncMCss] = {
		val sql =
			"""
			  | SELECT id, name, css_code, disp_image_url, created_at, updated_at
			  |  FROM m_css
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
		).map(*).list.apply()
	}

	/**
	 * 適用CSSマスタIDからCSS情報を取り出す
	 * @param id 適用CSSマスタID
	 * @return 取得したm_cssテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[EncMCss] = {
		val sql =
			"""
			  | SELECT id, name, css_code, disp_image_url, created_at, updated_at
			  |  FROM m_css
			  | WHERE id = {id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

}
