package models.enc

import java.util.Date
import play.api.Logger
import java.sql.Connection
import scalikejdbc._

case class EncMFormItems(
	                        itemId: Long,
	                        name: String,
	                        kind: String,
	                        dataType: String,
	                        graphKind: String,
	                        regex: Option[String],
	                        createdAt: Date,
	                        updatedAt: Date
	                        ) {
	override def toString = {
		"itemId:%s name:%s kind:%s dataType:%s graphKind:%s".format(
			itemId, name, kind, dataType, graphKind)
	}
}

object EncMFormItems extends models.Model {

  val tableName = "m_form_items"

  object columnNames {
    val itemId = "id"
    val name = "name"
    val kind = "kind"
    val dataType = "data_type"
    val graphKind = "graph_kind"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val regex = "regex"
    val all = Seq(itemId, name, kind, dataType, graphKind, createdAt, updatedAt, regex)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMFormItems(
      itemId = rs.long(itemId),
      name = rs.string(name),
      kind = rs.string(kind),
      dataType = rs.string(dataType),
      graphKind = rs.string(graphKind),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate,
      regex = rs.stringOpt(regex))
  }

  val autoSession = AutoSession

  /**
	 * 全てのデータ取得
	 *
	 * @return 取得したFormItemValuesテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncMFormItems] = {
		val sql =
			"""
			  |SELECT id, name, kind, data_type, graph_kind, regex, created_at, updated_at
			  |  FROM m_form_items
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).map(*).list().apply()
	}

	/**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncMFormItems] = {
		val sql =
			"""
			  |SELECT id, name, kind, data_type, graph_kind, regex, created_at, updated_at
			  |  FROM m_form_items
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}
}
