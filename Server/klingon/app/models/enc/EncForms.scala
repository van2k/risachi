package models.enc

import java.util.Date
import java.util.Calendar
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._


case class EncForms(
	                   formId: Long,
	                   contractId: Long,
	                   formName: String,
	                   formDesc: String,
	                   resultMessage: String,
	                   totalPageCount: Int,
	                   cssId: Option[Long],
	                   orgCss: Option[String],
	                   imageUrl: Option[String],
	                   status: String,
										 urlKey: String,
	                   createdAt: Date,
	                   updatedAt: Date
	                   ) {
	override def toString = {
		"formId:%s contractId:%s formName:%s totalPageCount:%d imageusr:%s status:%s urlKey:%s ".format(
			formId, contractId, formName, totalPageCount, imageUrl, status, urlKey)
	}
}

object EncForms extends models.Model {

  val tableName = "forms"

  object columnNames {
    val formId = "id"
    val contractId = "contract_id"
    val formName = "form_name"
    val formDesc = "form_desc"
    val resultMessage = "result_message"
    val totalPageCount = "total_page_count"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val cssId = "css_id"
    val orgCss = "org_css"
    val imageUrl = "image_url"
    val status = "status"
	  val urlKey = "url_key"
    val all = Seq(formId, contractId, formName, formDesc, resultMessage, totalPageCount, createdAt, updatedAt, cssId, orgCss, imageUrl, status, urlKey)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => (EncForms)(
      formId = rs.long(formId),
      contractId = rs.long(contractId),
      formName = rs.string(formName),
      formDesc = rs.string(formDesc),
      resultMessage = rs.string(resultMessage),
      totalPageCount = rs.int(totalPageCount),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate,
      cssId = rs.longOpt(cssId),
      orgCss = rs.stringOpt(orgCss),
      imageUrl = rs.stringOpt(imageUrl),
      status = rs.string(status),
		  urlKey = rs.string(urlKey))
  }

  val autoSession = AutoSession

  // ステータスの定義
	val STATUS_EDIT = "1"
	// 編集中
	val STATUS_RUN = "2"
	// 開始中
	val STATUS_END = "3"
	// 終了
	val STATUS_CANCEL = "4"
	// 中止
	val STATUS_DELETE = "9"
	// 削除
	val STATUS_LIST = Map(STATUS_EDIT -> "編集中", STATUS_RUN -> "開始中", STATUS_END -> "終了", STATUS_CANCEL -> "中止", STATUS_DELETE -> "削除")

	def getBlankForm(): EncForms = {
		val now = Calendar.getInstance().getTime()
		EncForms(-1, -1, "", "", "", 1, Option(1), Option(""),	Option(""), EncForms.STATUS_DELETE, "", now, now)
	}

  def getNewForm(): EncForms = {
    val now = Calendar.getInstance().getTime()
    EncForms(0, -1, "", "", "", 1, Option(1), Option(""),	Option(""), EncForms.STATUS_DELETE, "", now, now)
  }

	def getNewUrlKey(): String = {
		var urlKey = this.getHashCode()
		while(!EncForms.findByUrlKey(urlKey).isEmpty) {
			urlKey = this.getHashCode()
		}
		urlKey
	}

	/**
	 * idからデータを取得します。
	 * @return 取得したFormsテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)
		SQL(sql).map(*).list.apply()
	}

	/**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession):  Option[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE id = /*'id*/1
			  | ORDER BY id
			""".stripMargin
		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * URLキーからデータを取得します。
	 * @param urlKey URLキー
	 * @param session
	 * @return
	 */
	def findByUrlKey(urlKey: String)(implicit session: DBSession = autoSession):  Option[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE url_key = /*'urlKey*/'1'
			  | ORDER BY id
			""".stripMargin
		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('urlKey -> urlKey).map(*).single.apply()
	}

	/**
	 * statusからデータを取得します。
	 *
	 * @param id
	 * @param contractid
	 * @return
	 */
	def findByIdContractid(id: Long, contractid: Long)(implicit session: DBSession = autoSession):  Option[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE id = /*'id*/1 and contract_id = /*'contractId*/1
			  | ORDER BY id
			""".stripMargin
		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('id -> id, 'contractId -> contractid).map(*).single.apply()
	}

	/**
	 * statusからデータを取得します。
	 * @param status ステータス
	 * @return 取得したFormsテーブルのデータ
	 */
	def findByContractidStatus(contractid: Long, status: String)(implicit session: DBSession = autoSession):  Option[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE contract_id = /*'contractId*/1 and status = /*'status*/'status'
			  | ORDER BY id
			""".stripMargin
		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('contractId -> contractid, 'status -> status).map(*).single.apply()
	}


	/**
	 * 指定したステータス以外のデータを取得します。
	 * @param contractid 契約者ID
	 * @param status ステータス
	 * @return 取得したFormsテーブルのデータ
	 */
	def findByContractidNotstatus(contractid: Long, status: String)
                               (implicit session: DBSession = autoSession):  List[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE contract_id = /*'contractId*/1 and status <> /*'status*/'status'
			  | ORDER BY updated_at desc
			""".stripMargin
		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('contractId -> contractid, 'status -> status).map(*).list.apply()
	}

	/**
	 * statusからデータを取得します。
	 * @param status ステータス
	 * @return 取得したFormsテーブルのデータ
	 */
	def findByStatus(status: String)
                  (implicit session: DBSession = autoSession):  Option[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count,css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE status = /*'status*/'status'
			  | ORDER BY id
			""".stripMargin
		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('status -> status).map(*).single.apply()
	}

	/**
	 * idからデータを取得します。
	 * @param contractId formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def findByContractId(contractId: Long)
                      (implicit session: DBSession = autoSession):List[EncForms] = {
		val sql =
			"""
			  |SELECT id, contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, url_key,
			  |       created_at, updated_at
			  |  FROM forms
			  | WHERE contract_id = /*'contractId*/1
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('contractId -> contractId).map(*).list.apply()
	}

	/**
	 * データを登録します。
	 * @param encForms 登録対象のデータ
	 * @return 新しいId、エラーの場合は
	 */
	def regist(encForms: EncForms)(implicit session: DBSession = autoSession):  Option[Long] = {
		Logger.debug("データの登録開始")

    val newId = SQL("""
      INSERT INTO forms (
        contract_id,
        form_name,
        form_desc,
        result_message,
        total_page_count,
        created_at,
        updated_at,
        css_id,
        org_css,
        image_url,
        status,
		    url_key
      ) values (
        /*'contractId*/1,
        /*'formName*/'abc',
        /*'formDesc*/'abc',
        /*'resultMessage*/'abc',
        /*'totalPageCount*/1,
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'cssId*/1,
        /*'orgCss*/'abc',
        /*'imageUrl*/'abc',
        /*'status*/'abc',
        /*'urlKey */'abc'
      )
        """)
      .bindByName(
      'contractId -> encForms.contractId,
      'formName -> encForms.formName,
      'formDesc -> encForms.formDesc,
      'resultMessage -> encForms.resultMessage,
      'totalPageCount -> encForms.totalPageCount,
      'createdAt -> encForms.createdAt,
      'updatedAt -> encForms.updatedAt,
      'cssId -> encForms.cssId,
      'orgCss -> encForms.orgCss,
      'imageUrl -> encForms.imageUrl,
      'status -> encForms.status,
      'urlKey -> encForms.urlKey
    ).updateAndReturnGeneratedKey.apply()

    (EncForms)(
      formId = encForms.formId,
      contractId = encForms.contractId,
      formName = encForms.formName,
      formDesc = encForms.formDesc,
      resultMessage = encForms.resultMessage,
      totalPageCount = encForms.totalPageCount,
      createdAt = encForms.createdAt,
      updatedAt = encForms.updatedAt,
      cssId = encForms.cssId,
      orgCss = encForms.orgCss,
      imageUrl = encForms.imageUrl,
      status = encForms.status,
      urlKey = encForms.urlKey)

		Option(newId)
	}

	/**
	 * ページ数を更新します。
	 * @param id 更新対象のID
	 * @param updatedAt 更新日時
	 * @param pageNum ページ数
	 * @param curDate 設定する更新日時
	 * @return
	 */
	def updatePageNum(id: Long, updatedAt: Date, pageNum: Int, curDate: Date)
                   (implicit session: DBSession = autoSession) = {
		val sql =
			"""
			  |UPDATE forms
			  |   SET total_page_count=/*'totalPageCount*/3
			  |     , updated_at=/*'curDate*/'2013-06-06 10:00:00'
			  | WHERE id=/*'id*/-1;
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName('totalPageCount -> pageNum ,'curDate -> curDate, 'id -> id).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
    DbUpdateResult.success
	}

	/**
	 * ステータスを更新
	 * @param id 更新対象のID
	 * @param updatedAt 更新日時
	 * @param status ステータス
	 * @param curDate 設定する更新日時
	 * @param session 接続
	 * @return
	 */
	def updateStatus(id: Long, status: String, updatedAt: Date, curDate: Date)(implicit session: DBSession = autoSession) = {
		val sql =
			"""
			  |UPDATE forms
			  |   SET status=/*'status*/'status'
			  |     , updated_at=/*'curDate*/'2013-06-06 10:00'
			  | WHERE id=/*'id*/-1;
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName('status -> status, 'curDate -> curDate, 'id -> id).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
    DbUpdateResult.success
	}

	/**
	 * 契約者IDに紐づくフォーム情報のステータスを更新する。
	 *
	 * @param contractId 契約者ID
	 * @param status 更新するステータス
	 * @param session コネクション
	 * @return
	 */
	def updateStatusByContractId(contractId: Long, status: String)
                              (implicit session: DBSession = autoSession)= {
		val sql =
			"""
			  |UPDATE forms
			  |   SET status=/*'status*/'status'
			  |     , updated_at=NOW()
			  | WHERE contract_id=/*'contractId*/-1;
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName('status -> status, 'contractId -> contractId).update().apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
    DbUpdateResult.success
	}

	/**
	 * データを更新します。
	 * @param encForms 更新対象のデータ
	 * @param updatedAt 更新日時
	 * @return 更新結果
	 */
	def update(encForms: EncForms, updatedAt: Date) (implicit session: DBSession = autoSession)= {
		Logger.debug("データの更新開始")

    val retCount = SQL("""
      update
        forms
      set
        contract_id = /*'contractId*/1,
        form_name = /*'formName*/'abc',
        form_desc = /*'formDesc*/'abc',
        result_message = /*'resultMessage*/'abc',
        total_page_count = /*'totalPageCount*/1,
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        css_id = /*'cssId*/1,
        org_css = /*'orgCss*/'abc',
        image_url = /*'imageUrl*/'abc',
        status = /*'status*/'abc',
				url_key = /*'urlKey*/'abc'
      where
        id = /*'id*/-1
                       """)
      .bindByName(
      'id -> encForms.formId,
      'contractId -> encForms.contractId,
      'formName -> encForms.formName,
      'formDesc -> encForms.formDesc,
      'resultMessage -> encForms.resultMessage,
      'totalPageCount -> encForms.totalPageCount,
      'updatedAt -> encForms.updatedAt,
      'cssId -> encForms.cssId,
      'orgCss -> encForms.orgCss,
      'imageUrl -> encForms.imageUrl,
      'status -> encForms.status,
      'urlKey -> encForms.urlKey
    ).update().apply()

		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS")
		Logger.debug("日付の確認" + sdf.format(encForms.updatedAt))
		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
    DbUpdateResult.success
	}

	/**
	 * 画像URLを更新する
	 *
	 * @param id
	 * @param imageUrl
	 * @param session
	 * @return
	 */
	def updateImage(id: Long, imageUrl: String) (implicit session: DBSession = autoSession)= {

		val sql =
			"""
			  |UPDATE forms
			  |   SET image_url=/*'imageUrl*/'hoge', updated_at=/*'updateAt*/'2013-06-03 10:00'
			  | WHERE id=/*'id*/1;
			""".stripMargin

		Logger.debug("sql:" + sql)

		val uploadUrl = if (imageUrl == "") null else imageUrl

		var retValue = DbUpdateResult.success

    val retCount = SQL(sql).bindByName('imageUrl -> uploadUrl
      , 'updatedAt -> Calendar.getInstance().getTime(), 'id -> id).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/**
	 * CSSの設定を更新する。
	 *
	 * @param id
	 * @param cssId
	 * @return
	 */
	def updateCss(id: Long, cssId: Long, orgCss: String) (implicit session: DBSession = autoSession)= {
		val sql =
			"""
			  |UPDATE forms
			  |   SET css_id=/*'cssId*/1, updated_at=/*'updatedAt*/'2013-06-10 10:00:00', org_css=/*'orgCss*/'orgCss'
			  | WHERE id=/*'id*/1;
			""".stripMargin

		Logger.debug("sql:" + sql)

		var retValue = DbUpdateResult.success

    val retCount = SQL(sql).bindByName('cssId -> cssId, 'updatedAt -> Calendar.getInstance().getTime()
      , 'orgCss -> orgCss, 'id -> id).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue

	}

	/**
	 * 既存のform情報を元に新たにフォームを作成する（コピー新規）
	 *
	 * @param id
	 * @param status
	 * @return
	 */
	def copyForm(id: Long, status: String)(implicit session: DBSession = autoSession) = {

		val sql =
			"""
			  |INSERT INTO forms(
			  |            contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, status, urlKey, created_at, updated_at)
			  | SELECT contract_id, form_name, form_desc, result_message, total_page_count, css_id, org_css, image_url, {status}, {urlKey}, current_timestamp, current_timestamp
			  | FROM forms
			  | WHERE id = /*'id*/1
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName(
	    'id -> id,
      'status -> status,
      'urlKey -> this.getNewUrlKey()
    ).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		retCount
	}

	/*
	* IDによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteById(id: Long) (implicit session: DBSession = autoSession)= {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM forms
			  | WHERE id=/*'id*/-1
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName('id -> id).update().apply()

//		val retCount = executeUpdate(SQL(sql).on(
//			'id -> id
//		), con)

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
