package models.enc

import java.util.{Calendar, Date}
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._

case class EncFormConfigs(
                           id: Long,
                           formId: Long,
                           collectNumber: Int,
                           startSt: Date,
                           endSt: Option[Date],
                           stop: String,
                           outputNumber: Int,
                           term: Int,
                           outputLimitNumber: Int,
                           saveTerm: Option[Int],
                           price: Option[Int],
                           paymentConfirm: Option[Int],
                           paymentConfirmDate: Option[Date],
                           chargePlanId: Option[Long],
                           encResultOpen: String,
                           createdAt: Date,
                           updatedAt: Date
                           ) {
	override def toString = {
		"itemId:%d".format(id)
	}
}

object EncFormConfigs extends models.Model {

  val tableName = "form_configs"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val collectNumber = "collect_number"
    val startSt = "start_st"
    val endSt = "end_st"
    val stop = "stop"
    val outputNumber = "output_number"
    val term = "term"
    val outputLimitNumber = "output_limit_number"
    val saveTerm = "save_term"
    val price = "price"
    val paymentConfirm = "payment_confirm"
    val paymentConfirmDate = "payment_confirm_date"
    val chargePlanId = "charge_plan_id"
    val encResultOpen = "enc_result_open"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, collectNumber, startSt, endSt, stop, outputNumber, term, outputLimitNumber, saveTerm, price, paymentConfirm, paymentConfirmDate, chargePlanId, encResultOpen, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormConfigs(
      id = rs.long(id),
      formId = rs.long(formId),
      collectNumber = rs.int(collectNumber),
      startSt = rs.date(startSt),
      endSt = rs.dateOpt(endSt),
      stop = rs.string(stop),
      outputNumber = rs.int(outputNumber),
      term = rs.int(term),
      outputLimitNumber = rs.int(outputLimitNumber),
      saveTerm = rs.intOpt(saveTerm),
      price = rs.intOpt(price),
      paymentConfirm = rs.intOpt(paymentConfirm),
      paymentConfirmDate = rs.dateOpt(paymentConfirmDate),
      chargePlanId = rs.longOpt(chargePlanId),
      encResultOpen = rs.string(encResultOpen),
      createdAt = rs.date(createdAt).toJavaUtilDate,
      updatedAt = rs.date(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

	def getBlankEncFormConfig(): EncFormConfigs = {
		val cal = Calendar.getInstance
		val now = cal.getTime
		val startSt = Calendar.getInstance
		startSt.add(Calendar.MONTH, -2)
		val endSt = Calendar.getInstance
		endSt.add(Calendar.MONTH, -1)
		EncFormConfigs(-1, -1, 0, startSt.getTime, Option(endSt.getTime), "", 0, 0, 0, Option(0), Option(0), Option(0), Option(now), Option(-1), "", now, now)
	}

	/**
	 * 全てのデータ取得
	 * @return 取得したEncEncConfigsテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncFormConfigs] = {
		val sql =
			"""
			  |SELECT id, form_id, collect_number, start_st, end_st, stop, output_number, term, output_limit_number,
			  |       save_term, price, payment_confirm, payment_confirm_date, charge_plan_id, enc_result_open, created_at, updated_at
			  | FROM form_configs
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
		).map(*).list.apply()
	}

	/**
	 * IDに取得する
	 * @return 取得したEncFormConfigsテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[EncFormConfigs] = {
		val sql =
			"""
			  |SELECT id, form_id, collect_number, start_st, end_st, stop, output_number, term, output_limit_number,
			  |       save_term, price, payment_confirm, payment_confirm_date, charge_plan_id, enc_result_open, created_at, updated_at
			  | FROM form_configs
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply()
	}

	/**
	 * IDに取得する
	 * @return 取得したEncFormConfigsテーブルのデータ
	 */
	def findByFormid(formid: Long)(implicit session: DBSession = autoSession): Option[EncFormConfigs] = {
		val sql =
			"""
			  |SELECT id, form_id, collect_number, start_st, end_st, stop, output_number, term, output_limit_number,
			  |       save_term, price, payment_confirm, payment_confirm_date, charge_plan_id, enc_result_open, created_at, updated_at
			  | FROM form_configs
			  | WHERE form_id = {formid}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'formid -> formid
		).map(*).single().apply
	}

	/**
	 * ContractIdで取得する
	 * @param contractid 契約者ID
	 * @return 取得したEncFormConfigsテーブルのデータ
	 */
	def findByContractid(contractid: Long)(implicit session: DBSession = autoSession): List[EncFormConfigs] = {
		val sql =
			"""
			  |SELECT a.id, a.form_id, a.collect_number, a.start_st, a.end_st, a.stop, a.output_number, a.term, a.output_limit_number,
			  |       a.save_term, a.price, a.payment_confirm, a.payment_confirm_date, a.charge_plan_id, a.enc_result_open, a.created_at, a.updated_at
			  | FROM form_configs a left join forms b on a.form_id = b.id
			  | WHERE b.contract_id = {contract_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'contract_id -> contractid
		).map(*).list.apply()
	}

	/**
	 * データを登録します。
	 * @param encconfig 登録対象のデータ
	 * @return 登録件数
	 */
	def regist(encconfig: EncFormConfigs)(implicit session: DBSession = autoSession): Boolean = {

		Logger.debug("データの登録開始")

    val sql =
      """
        	 		  |INSERT INTO form_configs(
        	 		  |            form_id, collect_number, start_st, end_st, stop, output_number, term, output_limit_number,
        	 		  |             save_term, price, payment_confirm, payment_confirm_date, charge_plan_id, enc_result_open, created_at, updated_at)
        	 		  |    VALUES ({form_id}, {collect_number}, {start_st}, {end_st}, {stop}, {output_number}, {term}, {output_limit_number},
        	 		  |            {save_term}, {price}, {payment_confirm}, {payment_confirm_date}, {charge_plan_id}, {enc_result_open}, current_timestamp, current_timestamp);
      			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> encconfig.formId,
			'collect_number -> encconfig.collectNumber,
			'start_st -> encconfig.startSt,
			'end_st -> encconfig.endSt,
			'stop -> encconfig.stop,
			'output_number -> encconfig.outputNumber,
			'term -> encconfig.term,
			'output_limit_number -> encconfig.outputLimitNumber,
			'save_term -> encconfig.saveTerm,
			'price -> encconfig.price,
			'payment_confirm -> encconfig.paymentConfirm,
			'payment_confirm_date -> encconfig.paymentConfirmDate,
			'charge_plan_id -> encconfig.chargePlanId,
			'enc_result_open -> encconfig.encResultOpen
		).update.apply

		// 戻り値を返す
//		retCount match {
//			case Some(cnt) => cnt > 0
//			case _ => false
//		}

    retCount > 0
	}

	def update(encconfigs: EncFormConfigs)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_configs
			  |   SET form_id = {form_id}, collect_number = {collect_number}, start_st = {start_st}, end_st = {end_st}, stop ={stop},
			  |       output_number = {output_number}, term = {term}, output_limit_number = {output_limit_number},
			  |       save_term = {save_term}, price = {price}, payment_confirm = {payment_confirm},
			  |       payment_confirm_date = {payment_confirm_date}, charge_plan_id = {charge_plan_id}, enc_result_open = {enc_result_open}
			  |     , updated_at=current_timestamp
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_id -> encconfigs.formId,
			'collect_number -> encconfigs.collectNumber,
			'start_st -> encconfigs.startSt,
			'end_st -> encconfigs.endSt,
			'stop -> encconfigs.stop,
			'output_number -> encconfigs.outputNumber,
			'term -> encconfigs.term,
			'output_limit_number -> encconfigs.outputLimitNumber,
			'save_term -> encconfigs.saveTerm,
			'price -> encconfigs.price,
			'payment_confirm -> encconfigs.paymentConfirm,
			'payment_confirm_date -> encconfigs.paymentConfirmDate,
			'charge_plan_id -> encconfigs.chargePlanId,
			'enc_result_open -> encconfigs.encResultOpen,
			'id -> encconfigs.id
		).update.apply

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}


	def updateOutputnumber(id: Long, outputnum: Int)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_configs
			  |   SET output_number = {output_number}
			  | WHERE id={id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		var retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'output_number -> outputnum,
			'id -> id
		).update.apply

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/*
	* FormIdによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM form_configs
			  | WHERE form_id={form_id};
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName('form_id -> formid).update.apply

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}

	// 支払い確認時刻を現在時刻にセットする
	def setPaymentConfirmDate(formid: Long)(implicit session: DBSession = autoSession): Option[Date] = {
		val sql =
			"""
			  |UPDATE form_configs SET payment_confirm_date={now}, updated_at=current_timestamp
			  | WHERE form_id={form_id};
			""".stripMargin

		Logger.debug("sql:" + sql)

		val now = Calendar.getInstance().getTime
		val retCount = SQL(sql).bindByName(
			'form_id -> formid,
			'now -> now
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		if (retCount > 0) {
			Option(now)
		} else None
	}

	// 支払い確認の金額をセットする
	def setPaymentConfirm(formid: Long, price: Int)(implicit session: DBSession = autoSession): Boolean = {
		val sql =
			"""
			  |UPDATE form_configs SET payment_confirm={price}, updated_at=current_timestamp
			  | WHERE form_id={form_id};
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName('form_id -> formid,'price -> price).update.apply
    retCount > 0
	}
}
