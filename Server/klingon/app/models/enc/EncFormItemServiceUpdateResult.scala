package models.enc



/**
 * EncFormItemService.updateメソッドの戻り値クラス
 * @param formItems 質問オブジェクト
 * @param itemIdList IDリスト
 */
case class EncFormItemServiceUpdateResult(formItems: EncFormItems, itemIdList: List[String]) {
}
