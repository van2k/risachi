package models.enc

import java.util.Date
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._

case class EncFormItems(
	                       itemId: Long,
	                       formId: Long,
	                       categoryId: Long,
	                       mItemId: Long,
	                       name: String,
	                       pageNo: Long,
	                       selectNumber: Long,
	                       condition: String,
	                       required: String,
	                       dispodr: Long,
	                       createdAt: Date,
	                       updatedAt: Date
	                       ) {
	override def toString = {
		"itemId:%s formId:%s categoryId:%d mItemId:%d name:%s pageNo:%d selectNumber:%d condition:%s required:%s dispodr:%d".format(
			itemId, formId, categoryId, mItemId, name, pageNo, selectNumber, condition, required, dispodr)
	}
}

object EncFormItems extends models.Model {

  val tableName = "form_items"

  object columnNames {
    val itemId = "id"
    val formId = "form_id"
    val categoryId = "category_id"
    val mItemId = "m_form_item_id"
    val name = "name"
    val pageNo = "page_no"
    val required = "required"
    val selectNumber = "select_number"
    val condition = "condition"
    val dispodr = "dispodr"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(itemId, formId, categoryId, mItemId, name, pageNo, required, selectNumber, condition, dispodr, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormItems(
      itemId = rs.long(itemId),
      formId = rs.long(formId),
      categoryId = rs.long(categoryId),
      mItemId = rs.long(mItemId),
      name = rs.string(name),
      pageNo = rs.int(pageNo),
      required = rs.string(required),
      selectNumber = rs.int(selectNumber),
      condition = rs.string(condition),
      dispodr = rs.int(dispodr),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

	/**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormItems] = {
		val sql =
			"""
			  |SELECT id, form_id, category_id, m_form_item_id, name, page_no, select_number, condition, required, dispodr
			  |     , created_at, updated_at
			  |  FROM form_items
			  | WHERE id = {id}
			  | ORDER BY dispodr
			""".stripMargin

		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * formidからデータを取得します。
	 * @param formId formのID
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormId(formId: Long)(implicit session: DBSession = autoSession): List[EncFormItems] = {
		val sql =
			"""
			  |SELECT id, form_id, category_id, m_form_item_id, name, page_no, select_number, condition, required, dispodr
			  |     , created_at, updated_at
			  | FROM form_items
			  | WHERE form_id = {form_id}
			  | ORDER BY dispodr
			""".stripMargin

		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('form_id -> formId).map(*).list.apply()
	}

	/**
	 * formId、pageNoからデータを取得します。
	 * @param formId formのID
	 * @param pageNo ページID
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormidPageno(formId: Long, pageNo: Long)(implicit session: DBSession = autoSession): List[EncFormItems] = {
		val sql =
			"""
			  |SELECT id, form_id, category_id, m_form_item_id, name, page_no, select_number, condition, required, dispodr
			  |     , created_at, updated_at
			  | FROM form_items
			  | WHERE form_id = {form_id} and page_no = {page_no}
			  | ORDER BY dispodr
			""".stripMargin

		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('form_id -> formId,
      'page_no -> pageNo).map(*).list().apply()
	}

	/**
	 * formId、createAtからデータを取得します。
	 * @param formId formID
	 * @param createAt 生成時間
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormIdCreateAt(formId: Long, createAt: Date)(implicit session: DBSession = autoSession): EncFormItems = {
    val sql =
      """
          |SELECT id, form_id, category_id, m_form_item_id, name, page_no, select_number, condition, required, dispodr
          |     , created_at, updated_at
          | FROM form_items
          | WHERE form_id = {form_id} and created_at = {create_at}
          | ORDER BY dispodr
      """.stripMargin

    Logger.debug("sql:" + sql)

    SQL(sql).bindByName('form_id -> formId,
      'create_at -> createAt).map(*).single.apply().get
	}

	/**
	 * データを登録します。
	 * @param encFormItems 登録対象のデータ
	 * @param currentDate 現在時刻
	 * @return 正常に登録された場合はTrue
	 */
	def regist(encFormItems: EncFormItems, currentDate: Date)(implicit session: DBSession = autoSession): Option[Long] = {
		Logger.debug("データの登録開始")

		val regSql =
			"""
			  |INSERT INTO form_items(
			  |            form_id, category_id, m_form_item_id, name, page_no, select_number, condition, required, dispodr
			  |            , created_at, updated_at)
			  |    VALUES ({form_id}, {category_id}, {m_form_item_id}, {name}, {page_no}, {select_number}, {condition}
			  |           , {required}, {dispodr}, {current_date}, {current_date});
			""".stripMargin

		Logger.debug("sql:" + regSql)

		val newId = SQL(regSql).bindByName(
			'form_id -> encFormItems.formId,
			'category_id -> encFormItems.categoryId,
			'm_form_item_id -> encFormItems.mItemId,
			'name -> encFormItems.name,
			'page_no -> encFormItems.pageNo,
			'required -> encFormItems.required,
			'select_number -> encFormItems.selectNumber,
			'dispodr -> encFormItems.dispodr,
			'condition -> encFormItems.condition,
			'current_date -> currentDate
		).updateAndReturnGeneratedKey.apply()

		// 戻り値を返す
		Option(newId)
	}

	/**
	 * データを更新します。
	 * @param encFormItems 更新対象のデータ
	 * @param currentDate 現在時刻
	 * @return
	 */
	def updateWithTran(encFormItems: EncFormItems, currentDate: Date)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_items
			  |   SET form_id = {form_id}, category_id = {category_id}, m_form_item_id = {m_form_item_id}, name = {name}
			  |   , page_no = {page_no}, select_number = {select_number}, condition = {condition}, required = {required}, dispodr = {dispodr}
			  |     , updated_at={current_date}
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS")
		Logger.debug("日付の確認:" + sdf.format(encFormItems.updatedAt))

		val retCount = SQL(sql).bindByName(
			'form_id -> encFormItems.formId,
			'category_id -> encFormItems.categoryId,
			'm_form_item_id -> encFormItems.mItemId,
			'name -> encFormItems.name,
			'page_no -> encFormItems.pageNo,
			'select_number -> encFormItems.selectNumber,
			'condition -> encFormItems.condition,
			'required -> encFormItems.required,
			'dispodr -> encFormItems.dispodr,
			'current_date -> currentDate,
			'id -> encFormItems.itemId
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/**
	 * データを更新します。
	 * @param encFormItems 更新対象のデータ
	 * @param currentDate 現在時刻
	 * @return
	 */
	def update(encFormItems: EncFormItems, currentDate: Date)(implicit session: DBSession = autoSession) = {

		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_items
			  |   SET form_id = {form_id}, category_id = {category_id}, m_form_item_id = {m_form_item_id}, name = {name}
			  |   , page_no = {page_no}, select_number = {select_number}, condition = {condition}, required = {required}, dispodr = {dispodr}
			  |     , updated_at={current_date}
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success
		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS")
		Logger.debug("日付の確認" + sdf.format(encFormItems.updatedAt))

		val retCount = SQL(sql).bindByName(
			'form_id -> encFormItems.formId,
			'category_id -> encFormItems.categoryId,
			'm_form_item_id -> encFormItems.mItemId,
			'name -> encFormItems.name,
			'page_no -> encFormItems.pageNo,
			'select_number -> encFormItems.selectNumber,
			'condition -> encFormItems.condition,
			'required -> encFormItems.required,
			'dispodr -> encFormItems.dispodr,
			'current_date -> currentDate,
			'id -> encFormItems.itemId
		).update.apply

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/**
	 * データを更新します。
	 *
	 * @param itemid
	 * @param pageno
	 * @param dispodr
	 * @return
	 */
	def updatePagenoDispodr(itemid: Long, pageno: Int, dispodr: Int)(implicit session: DBSession = autoSession) = {

		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_items
			  |   SET page_no = {page_no}, dispodr={dispodr}
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'page_no -> pageno,
			'dispodr -> dispodr,
			'id -> itemid
		).update.apply

		// 戻り値を返す
		retValue
	}

	/**
	 * FormItemIdからデータを物理削除します。
	 * @param formItemId 削除対象のFormItemId
	 * @param updatedAt 更新日時
	 * @return 削除結果
	 */
	def delete(formItemId: Long, updatedAt: Date)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  | DELETE FROM form_items WHERE id = {form_item_id} AND updated_at = {updated_at};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_item_id -> formItemId,
			'updated_at -> updatedAt
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		retValue
	}

	/**
	 * FormIdから該当するページ番号のデータを物理削除します。
	 * @param formId 削除対象のFormId
	 * @param pageNo ページ番号
	 * @return 削除結果
	 */
	def deleteByPageNo(formId: Long, pageNo: Int)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  | DELETE FROM form_items
			  |  WHERE form_id = {form_id}
			  |    AND page_no = {page_no};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_id -> formId,
			'page_no -> pageNo
		).update.apply

		val sql2 =
			"""
			  |UPDATE form_items SET page_no = page_no - 1
			  |WHERE form_id = {form_id} and page_no > {page_no}
			""".stripMargin

		Logger.debug("sql2:" + sql2)

		val retCount2 = SQL(sql2).bindByName(
			'form_id -> formId,
			'page_no -> pageNo
		).update.apply

		retValue
	}

	/*
	* FormIdによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM form_items
			  | WHERE form_id={form_id};
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> formid
		).update.apply

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}

	/*
		* 最大ページNoを取得する
		* ・ページがない場合は、0を返す
		*/
	def getMaxPageNo(formId: Long)(implicit session: DBSession = autoSession): Int = {
		val sql =
			"""
			 | SELECT coalesce(max(page_no), 0) as cnt
			 | FROM form_items
			 | WHERE form_id = {form_id}
			""".stripMargin

		SQL(sql).bindByName('form_id -> formId).map(_.int(1)).single().apply().get
	}
}
