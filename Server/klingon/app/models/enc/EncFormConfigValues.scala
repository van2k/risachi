package models.enc

import java.util.Date
import play.api.Logger
import scalikejdbc._

case class EncFormConfigValues(
	                              id: Long,
	                              formId: Long,
	                              optionId: Long,
	                              optionNumber: Long,
	                              charge: Long,
	                              price: Long,
	                              createdAt: Date,
	                              updatedAt: Date
	                              ) {
	override def toString = {
		"id:%d formId:%s optionId:%d optionNumber:%d charge:%d price:%d".format(
			id, formId, optionId, optionNumber, charge, price)
	}
}

object EncFormConfigValues extends models.Model {


  val tableName = "form_config_values"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val optionId = "option_id"
    val optionNumber = "option_number"
    val charge = "charge"
    val price = "price"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, optionId, optionNumber, charge, price, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormConfigValues(
      id = rs.long(id),
      formId = rs.long(formId),
      optionId = rs.long(optionId),
      optionNumber = rs.long(optionNumber),
      charge = rs.long(charge),
      price = rs.long(price),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

  /**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormConfigValues] = {
		val sql =
			"""
			  |SELECT id, form_id, option_id, option_number, charge, price, created_at, updated_at
			  |  FROM form_config_values
			  | WHERE id = {id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply()
	}

	/**
	 * formidからデータを取得します。
	 * @param formId formのID
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormId(formId: Long)(implicit session: DBSession = autoSession): List[EncFormConfigValues] = {
		val sql =
			"""
			  |SELECT id, form_id, option_id, option_number, charge, price, created_at, updated_at
			  | FROM form_config_values
			  | WHERE form_id = {form_id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_id -> formId
		).map(*).list.apply()
	}

	/**
	 * データを登録します。
	 * @param encFormConfigValues 登録対象のデータ
	 * @param currentDate 現在時刻
	 * @return 正常に登録された場合はTrue
	 */
	def regist(encFormConfigValues: EncFormConfigValues, currentDate: Date)
            (implicit session: DBSession = autoSession): Boolean = {
		Logger.debug("データの登録開始")

		val regSql =
			"""
			  |INSERT INTO form_config_values(
			  |            form_id, option_id, option_number, charge, price, created_at, updated_at)
			  |    VALUES ({form_id}, {option_id}, {option_number}, {charge}, {price}, {created_at}, {updated_at});
			""".stripMargin

		Logger.debug("sql:" + regSql)

		val retCount = SQL(regSql).bindByName(
			'form_id -> encFormConfigValues.formId,
			'option_id -> encFormConfigValues.optionId,
			'option_number -> encFormConfigValues.optionNumber,
			'charge -> encFormConfigValues.charge,
			'price -> encFormConfigValues.price,
			'created_at -> currentDate,
			'updated_at -> currentDate
		).update.apply()

		retCount > 0
	}

	/*
	* FormIdによるデータの削除
	* @param formId アンケートID
	* @return 更新結果
	*/
	def deleteByFormId(formId: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM form_config_values
			  | WHERE form_id={form_id};
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> formId
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
