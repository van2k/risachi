package models.enc

import java.util.Date
import collection.mutable.ListBuffer
import scalikejdbc._
import scala.collection.mutable


case class EncFormsSearch(
	                         form_id: Long,
	                         contract_name: String,
	                         start_st: Date,
	                         end_st: Date,
	                         plan_name: String,
	                         status: String,
	                         price: Option[Int],
	                         payment_confirm: Option[Int],
	                         payment_confirm_date: Option[Date]
	                         )

object EncFormsSearch extends models.Model {

  val tableName = "contracts"

  object columnNames {
    val form_id = "id"
    val contract_name = "contract_name"
    val start_st = "start_st"
    val end_st = "end_st"
    val plan_name = "plan_name"
    val status = "status"
    val price = "price"
    val payment_confirm = "payment_confirm"
    val payment_confirm_date = "payment_confirm_date"
    val all = Seq(form_id, contract_name, start_st, end_st, plan_name, status, price, payment_confirm, payment_confirm_date)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormsSearch(
      form_id = rs.long(form_id),
      contract_name = rs.string(contract_name),
      start_st = rs.timestamp(start_st).toJavaUtilDate,
      end_st = rs.timestamp(end_st).toJavaUtilDate,
      plan_name = rs.string(plan_name),
      status = rs.string(status),
      price = rs.intOpt(price),
      payment_confirm = rs.intOpt(payment_confirm),
      payment_confirm_date = rs.timestampOpt(payment_confirm_date))
  }

  val autoSession = AutoSession

	def search(contractName: String, startStFrom: Option[Date], startStTo: Option[Date],
	           endStFrom: Option[Date], endStTo: Option[Date], plans: List[String],
	           chargeConfirmIsNull: Boolean, chargeConfirmIsNotNull: Boolean,
	           isDisplaying: Boolean, isNotDisplaying: Boolean)(implicit session: DBSession = autoSession): List[EncFormsSearch] = {
		// WHERE句を作る
		val where_list = ListBuffer[String]()

		if (contractName != null && contractName != "") {
      where_list += "contracts.name LIKE {contract_name}"
    }
		if (startStFrom.isDefined) where_list += "form_configs.start_st >= {start_st_from}"
		if (startStTo.isDefined) where_list += "form_configs.start_st <= {start_st_to}"
		if (endStFrom.isDefined) where_list += "form_configs.end_st >= {end_st_from}"
		if (endStTo.isDefined) where_list += "form_configs.end_st <= {end_st_to}"
		if (plans != null) {
			if (plans.length > 0) {
				// ORで結合する
				var plans_where = ""
				var i = 1
				plans.foreach {
					plan_id =>
						if (plans_where != "") plans_where += " OR "
						plans_where += "form_configs.charge_plan_id = {plan_id_" + i + "}"
						i = i + 1
				}
				if (plans_where != "") where_list += "(" + plans_where + ")"
			}
		}
		if (isDisplaying) where_list += "forms.status='2'"
		if (isNotDisplaying) where_list += "forms.status<>'2'"
		if (chargeConfirmIsNull) where_list += "form_configs.payment_confirm IS NULL"
		if (chargeConfirmIsNotNull) where_list += "form_configs.payment_confirm IS NOT NULL"
		var where = ""
		where_list.toList.foreach {
			where_str =>
				where += " AND "
				where += where_str
		}

		// nullを""にする
		val likeContractName = if (contractName == null) ""
		else {
			"%" + contractName + "%"
		}

		val sql =
			"""
			  |SELECT forms.id, contracts.name AS contract_name, forms.status, form_configs.start_st,
			  | form_configs.end_st, charge_plans.name AS plan_name, form_configs.price,
			  | form_configs.payment_confirm, form_configs.payment_confirm_date
			  | FROM forms,form_configs,contracts,charge_plans
			  | WHERE forms.contract_id=contracts.id AND forms.id=form_configs.form_id
			  | AND charge_plans.id=form_configs.charge_plan_id AND forms.status<>'9'
			""".stripMargin + {
				where + " ORDER BY contracts.name LIMIT 50"
			}


    var params: mutable.Seq[(Symbol, Any)]= mutable.Seq()
    if(where.contains("{contract_name}")){
      params = params ++ Seq('contract_name -> likeContractName)
    }

    if(where.contains("{start_st_from}")){
      params = params ++ Seq('start_st_from -> startStFrom)
    }

    if(where.contains("{start_st_to}")){
      params = params ++ Seq('start_st_to -> startStTo)
    }

    if(where.contains("{end_st_from}")){
      params = params ++ Seq('end_st_from -> endStFrom)
    }

    if(where.contains("{end_st_to}")){
      params = params ++ Seq('end_st_to -> endStTo)
    }
    if(where.contains("{plan_id_")){
      var j = 1
      plans.foreach {
        plan_id =>
          params = params ++ Seq(Symbol("plan_id_" + j) -> plan_id.toLong)
          j = j + 1
      }
    }

    SQL(sql).bindByName(params:_*).map(*).list.apply()
  }

}
