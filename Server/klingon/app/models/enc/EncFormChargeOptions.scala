package models.enc

import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import scalikejdbc.{DBSession, SQL, AutoSession, WrappedResultSet}

case class EncFormChargeOptions(
	                               id: Long,
	                               formId: Long,
	                               chargeOptionPlanId: Long
	                               ) {
	override def toString = {
		"itemId:%d".format(id)
	}
}

object EncFormChargeOptions extends models.Model {

  val tableName = "form_charge_options"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val chargeOptionPlanId = "charge_option_plan_id"
    val all = Seq(id, formId, chargeOptionPlanId)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormChargeOptions(
      id = rs.long(id),
      formId = rs.long(formId),
      chargeOptionPlanId = rs.long(chargeOptionPlanId))
  }

  val autoSession = AutoSession

	/**
	 * 全てのデータ取得
	 * @return 取得したEncEncConfigsテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncFormChargeOptions] = {
		val sql =
			"""
			  |SELECT id, form_id, charge_option_plan_id
			  | FROM form_charge_options
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).map(*).list.apply()
	}

	/**
	 * IDに取得する
	 * @return 取得したEncFormChargeOptionsテーブルのデータ
	 */
	def findById(id: Long)(implicit session: DBSession = autoSession): Option[EncFormChargeOptions] = {
		val sql =
			"""
			  |SELECT id, form_id, charge_option_plan_id
			  | FROM form_charge_options
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * FormIdに取得する
	 * @return 取得したEncFormChargeOptionsテーブルのデータ
	 */
	def findByFormId(formId: Long)(implicit session: DBSession = autoSession): List[EncFormChargeOptions] = {
		val sql =
			"""
			  |SELECT id, form_id, charge_option_plan_id
			  | FROM form_charge_options
			  | WHERE form_id = {formId}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('formId -> formId).map(*).list.apply()
	}

	/**
	 * 指定のフォームにオプションを付与、または剥奪する
	 * @param formId フォームID
	 * @param chargeOptionPlanId オプション料金ID
	 * @param enable 付与する場合はtrue, 剥奪する場合はfalse
	 */
	def updateFormChargeOption(formId: Long, chargeOptionPlanId: Long, enable: Boolean)(implicit session: DBSession = autoSession) = {
		if (enable) {
			// 付与
			// 同じ情報がすでにないか確認する
			val sql =
				"""
				  |SELECT id, form_id, charge_option_plan_id FROM form_charge_options
				  | WHERE form_id={form_id} AND charge_option_plan_id={charge_option_plan_id}
				  | LIMIT 1
				""".stripMargin
			Logger.debug("sql:" + sql)
			val result = SQL(sql).bindByName(
				'form_id -> formId,
				'charge_option_plan_id -> chargeOptionPlanId
			).map(*).list.apply()

			// 同じ情報がなければ挿入
			if (result.length == 0) {
				val sql =
					"""
					  |INSERT INTO form_charge_options(form_id, charge_option_plan_id)
					  |    VALUES ({form_id}, {charge_option_plan_id})
					""".stripMargin
				Logger.debug("sql:" + sql)
				SQL(sql).bindByName('form_id -> formId,	'charge_option_plan_id -> chargeOptionPlanId).update.apply()
			} else 0
		} else {

			// 剥奪
			val sql =
				"""
				  |DELETE FROM form_charge_options
				  | WHERE form_id={form_id} AND charge_option_plan_id={charge_option_plan_id}
				""".stripMargin

			Logger.debug("sql:" + sql)

      SQL(sql).bindByName('form_id -> formId, 'charge_option_plan_id -> chargeOptionPlanId).update.apply()
		}
	}

	/**
	 * データを登録します。
	 * @param options 登録対象のデータ
	 * @return 成功したらtrue
	 */
	def regist(options: EncFormChargeOptions)(implicit session: DBSession = autoSession): Boolean = {

		Logger.debug("データの登録開始")

    val sql =
      """
        	 		  |INSERT INTO form_charge_options(form_id, charge_option_plan_id)
        	 		  |    VALUES ({form_id}, {charge_option_plan_id});
      			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> options.formId,
			'charge_option_plan_id -> options.chargeOptionPlanId
		).update.apply()

//		// 戻り値を返す
//		retCount match {
//			case Some(n) => n > 0
//			case _ => false
//		}

    retCount > 0
	}

	def update(options: EncFormChargeOptions)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_charge_options
			  |   SET form_id = {form_id}, charge_option_plan_id = {charge_option_plan_id}, updated_at=current_timestamp
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_id -> options.formId,
			'charge_option_plan_id -> options.chargeOptionPlanId,
			'id -> options.id
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}
}
