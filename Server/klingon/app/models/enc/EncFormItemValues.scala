package models.enc

import java.util.Date
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._

case class EncFormItemValues(
	                            itemId: Long,
	                            formItemId: Long,
	                            formValue: String,
	                            chLength: Long,
	                            createdAt: Date,
	                            updatedAt: Date
	                            ) {
	override def toString = {
		"itemId:%s formItemId:%s formValue:%s chLength:%d".format(
			itemId, formItemId, formValue, chLength)
	}
}

object EncFormItemValues extends models.Model {

  val tableName = "form_item_values"

  object columnNames {
    val itemId = "id"
    val formItemId = "form_item_id"
    val formValue = "form_value"
    val chLength = "ch_length"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(itemId, formItemId, formValue, chLength, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormItemValues(
      itemId = rs.long(itemId),
      formItemId = rs.long(formItemId),
      formValue = rs.string(formValue),
      chLength = rs.int(chLength),
      createdAt = rs.date(createdAt).toJavaUtilDate,
      updatedAt = rs.date(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession


  /**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormItemValues] = {
		val sql =
			"""
			  |SELECT id, form_item_id, form_value, ch_length, created_at, updated_at
			  |  FROM form_item_values
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)
		SQL(sql).bindByName(
			'id -> id
		).map(*).single().apply
	}

	/**
	 * formItemIdからデータを取得します。
	 * @param formItemId form項目のID
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormItemId(formItemId: Long)(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
		val sql =
			"""
			  |SELECT id, form_item_id, form_value, ch_length, created_at, updated_at
			  | FROM form_item_values
			  | WHERE form_item_id = {form_item_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_item_id -> formItemId
		).map(*).list.apply()
	}

	/**
	 * formId,PageNoからデータを取得します。
	 * @param formId formのID
	 * @return 取得したFormItemValuesテーブルのデータ
	 */
	def findByFormId(formId: Long)(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
		val sql =
			"""
			  |   SELECT b.id, b.form_item_id,b.form_value, b.ch_length, b.created_at, b.updated_at
			  |     FROM form_items a INNER JOIN form_item_values b ON a.id = b.form_item_id
			  |    WHERE a.form_id = {form_id}
			  | ORDER BY b.form_item_id,b.id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_id -> formId
		).map(*).list().apply()
	}

	/**
	 * formId,PageNoからデータを取得します。
	 * @param formId formのID
	 * @param pageNo ページ番号
	 * @return 取得したFormItemValuesテーブルのデータ
	 */
	def findByFormidPageno(formId: Long, pageNo: Long)(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
		val sql =
			"""
			  |SELECT b.id, b.form_item_id, b.form_value, b.ch_length, b.created_at, b.updated_at
			  | FROM form_items a left join form_item_values b on a.id = b.form_item_id
			  | where a.form_id = {form_id} and a.page_no = {page_no}
			  | ORDER BY b.form_item_id,b.id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_id -> formId,
			'page_no -> pageNo
		).map(*).list.apply()
	}

	/**
	 * formId,PageNoからデータを取得します。
	 * @param formItemId formItemsのID
	 * @param createAt データ作成日時
	 * @return 取得したFormItemValuesテーブルのデータ
	 */
	def findByFormItemIdCreateAt(formItemId: Long, createAt: Date)(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
		val sql =
			"""
        | SELECT id, form_item_id, form_value, ch_length, created_at, updated_at
			  | FROM form_item_values
			  | WHERE form_item_id = {form_item_id} AND created_at = {created_at}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_item_id -> formItemId,
			'created_at -> createAt
		).map(*).list.apply()
	}

	/**
	 * データを登録します。
	 * @param encFormItemValues 登録対象のデータ
	 * @param currentDate 現在時刻
	 * @return 正常に登録された場合はTrue
	 */
	def regist(encFormItemValues: EncFormItemValues, currentDate: Date)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの登録開始")

		val sql =
			"""
			  |INSERT INTO form_item_values(
			  |            form_item_id, form_value, ch_length, created_at, updated_at)
			  |    VALUES ({form_item_id}, {form_value}, {ch_length}, {currentDate}, {currentDate});
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val newFormItemValueId = SQL(sql).bindByName(
			'form_item_id -> encFormItemValues.formItemId,
			'form_value -> encFormItemValues.formValue,
			'ch_length -> encFormItemValues.chLength,
			'currentDate -> currentDate
		).updateAndReturnGeneratedKey.apply()

		Logger.debug("newFormItemValueId:" + newFormItemValueId)

		// 戻り値を返す
		retValue
	}

	/**
	 * データを更新します。
	 * @param encFormItemValues 更新対象のデータ
	 * @param currentDate 現在時刻
	 * @return
	 */
	def update(encFormItemValues: EncFormItemValues, currentDate: Date)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_item_values
			  |   SET form_item_id = {form_item_id}, form_value = {form_value}, ch_length = {ch_length}
			  |     , updated_at={currentDate}
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS");
		Logger.debug("日付の確認" + sdf.format(encFormItemValues.updatedAt))

		val retCount = SQL(sql).bindByName(
			'form_item_id -> encFormItemValues.formItemId,
			'form_value -> encFormItemValues.formValue,
			'ch_length -> encFormItemValues.chLength,
			'currentDate -> currentDate,
			'id -> encFormItemValues.itemId
		).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/**
	 * FormItemIdからデータを物理削除します。
	 * @param formItemId 削除対象のFormItemId
	 * @return 削除結果
	 */
	def deleteByFormItemId(formItemId: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  | DELETE FROM form_item_values WHERE form_item_id = {form_item_id};
			""".stripMargin

		Logger.debug(sql)

		var retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_item_id -> formItemId
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		retValue
	}

	/**
	 * FormItemIdからデータを物理削除します。
	 * @param formItemId 削除対象のFormItemId
	 * @param pageNo ページ番号
	 * @return 削除結果
	 */
	def deleteByFormItemIdPageNo(formItemId: Long, pageNo: Int)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  | DELETE FROM form_item_values v
			  |  WHERE exists(select *
			  |                 FROM form_items i
			  |                WHERE i.form_id = {form_item_id}
			  |                  AND i.page_no = {page_no}
			  |                  AND i.id = v.form_item_id)
			""".stripMargin

		Logger.debug(sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_item_id -> formItemId,
			'page_no -> pageNo
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		retValue
	}


	/*
	* アンケートIDで項目値を複製する
	* @param formid 複製するアンケートID
	* @param newformid 新しいID
	* @param con コネクション
	*/
	def copyFormItemValues(formitemid: Long, newformitemid: Long)(implicit session: DBSession = autoSession) = {

		val sql =
			"""
			  |INSERT INTO form_item_values(
			  |            form_item_id, form_value, ch_length, created_at, updated_at)
			  | SELECT {newformitemid}, form_value, ch_length, created_at, updated_at
			  | FROM form_item_values
			  | WHERE form_item_id = {form_item_id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'newformitemid -> newformitemid,
			'form_item_id -> formitemid
		).update().apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}
	}

	/*
	* FormIdによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM form_item_values a
			  | WHERE EXISTS (select * from form_items b where a.form_item_id = b.id and b.form_id={form_id});
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> formid
		).update().apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
