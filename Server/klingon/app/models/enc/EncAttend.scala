package models.enc

import play.api.Logger
import scalikejdbc._

case class EncAttend(
	                    answerId: Long,
	                    title: String,
	                    nameTitle: Option[String],
	                    dateTitle: Option[String],
	                    answerName: Option[String],
	                    answer: Option[String]
	                    )

case class EncAttendDate(date: String)

object EncAttend extends models.Model {

  object columnNames {
    val answerId = "id"
    val title = "form_name"
    val nameTitle = "name_title"
    val dateTitle = "date_title"
    val answerName = "answer_name"
    val answer = "answer_value"
    val all = Seq(answerId, title, nameTitle, dateTitle, answerName, answer)
    val inSQL = all.mkString(", ")
  }

  val mapForAttend = {
    import columnNames._
    (rs: WrappedResultSet) => (EncAttend)(
      answerId = rs.long(answerId),
      title = rs.string(title),
      nameTitle = rs.stringOpt(nameTitle),
      dateTitle = rs.stringOpt(dateTitle),
      answerName = rs.stringOpt(answerName),
      answer = rs.stringOpt(answer))
  }

  val autoSession = AutoSession

  val mapForAttendData = {
    (rs:WrappedResultSet) => (EncAttendDate)(date = rs.string("attend_date"))
  }

	/**
	 * アンケート回答結果を出欠表形式で返す。
	 *
	 * @param formId
	 * @param session
	 * @return
	 */
	def recordOfAttendance(formId: Long)(implicit session: DBSession = autoSession):List[EncAttend] = {
		val sql =
			"""
			  |SELECT a1.id,
			  |  f.form_name,
			  |  fi.name AS question,
			  |  CASE WHEN fi.m_form_item_id = /*'nameKeyNameTitle*/1 THEN fi.name ELSE NULL END AS name_title,
			  |  CASE WHEN fi.m_form_item_id = /*'dateKeyDateTitle*/4 THEN fi.name ELSE NULL END AS date_title,
			  |  CASE WHEN fi.m_form_item_id = /*'nameKeyAnswerName*/1 THEN ad.answer_value ELSE NULL END AS answer_name,
			  |  CASE WHEN fi.m_form_item_id = /*'dateKeyAnswerValue*/4 THEN fiv.form_value ELSE NULL END AS answer_value
			  | FROM
			  |  answers AS a1
			  | INNER JOIN forms AS f
			  |  ON a1.form_id = f.id
			  | INNER JOIN answer_details AS ad
			  |  ON a1.id = ad.answer_id
			  | INNER JOIN form_items AS fi
			  |  ON ad.form_item_id = fi.id
			  | INNER JOIN form_item_values AS fiv
			  |  ON ad.form_item_value_id = fiv.id
			  | WHERE a1.form_id =/*'formId*/'form1'
			  |   AND a1.is_preview = /*'isPreview*/'0'
			  |   AND a1.end_at IS NOT NULL
			  | ORDER BY a1.start_at, ad.answer_id, ad.form_item_id, answer_value
			""".stripMargin

		Logger.debug("sql:" + sql)

    val textBoxNameId = 1
    val checkboxItemId = 4
    val previewOffFlg = "0"
    SQL(sql).bindByName('nameKeyNameTitle -> textBoxNameId, 'dateKeyDateTitle -> checkboxItemId, 'nameKeyAnswerName -> textBoxNameId
      , 'dateKeyAnswerValue -> checkboxItemId, 'formId -> formId, 'isPreview -> previewOffFlg).map(mapForAttend).list().apply()

	}

	def attendDateItems(formId: Long)(implicit session: DBSession = autoSession):List[EncAttendDate] = {
		val sql =
			"""
			  |  SELECT
			  |    fiv.form_value AS attend_date
			  |  FROM
			  |    form_item_values fiv
			  |  INNER JOIN form_items fi
			  |    ON fi.id = fiv.form_item_id
			  |  INNER JOIN forms f
			  |    ON f.id = fi.form_id
			  |  INNER JOIN m_form_items mfi
			  |    ON mfi.id = fi.m_form_item_id
			  |  WHERE
			  |    f.id = /*'formId*/'formid'
			  |    AND mfi.id = /*'formItemId*/4
			  |  ORDER BY fiv.id
			""".stripMargin

		Logger.debug("sql:" + sql)

    val formItemCheckbox = 4
    SQL(sql).bindByName('formId -> formId, 'formItemId -> formItemCheckbox).map(mapForAttendData).list().apply()
	}
}
