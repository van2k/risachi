package models.enc

import collection.mutable
import play.api.libs.json._
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsNumber
import models.ControllerExecuteStatus


/**
 * コントローラーの処理結果オブジェクト
 * @param titleErrMessage 質問タイトルのエラーメッセージ
 * @param selectItemCountErrMessage 選択する個数のエラーメッセージ
 * @param maxLengthErrMessage 入力文字数最大値エラーメッセージ
 * @param itemErrorMessage 各項目に設定するエラーメッセージ
 */
case class ResultErrorData(titleErrMessage: String, selectItemCountErrMessage: String
                           , maxLengthErrMessage: String, itemErrorMessage: Map[Int, mutable.Set[String]]) {
}

/**
 * JSONフォーマットへ変換するコンパニオンクラス
 */
object ResultErrorData {

	implicit object ResultErrorDataFormat extends Format[ResultErrorData] {

		// note:読み込みは行わない
		def reads(json: JsValue): JsResult[ResultErrorData] = null

		def writes(p: ResultErrorData): JsValue = JsObject(List(
			"status" -> JsNumber(ControllerExecuteStatus.fail.id)
			, "titleErrMessage" -> JsString(p.titleErrMessage)
			, "selectItemCountErrMessage" -> JsString(p.selectItemCountErrMessage)
			, "maxLengthErrMessage" -> JsString(p.maxLengthErrMessage)
			// リストからJsObjectにタプルをもつリストを生成している
			, "itemErrorMessages" -> JsArray(p.itemErrorMessage.keys.toSeq.map(
				key => JsObject(List("itemNumber" -> JsNumber(key)
					, "messages" -> JsArray(p.itemErrorMessage(key).toSeq.map(
						message => JsObject(List("message" -> JsString(message)))
					))))
			)
			)))
	}

}
