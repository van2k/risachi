package models.enc

import java.util.Date
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import scalikejdbc._

case class EncDispSchedule(
	                          id: Long,
	                          formId: Long,
	                          kind: String,
	                          settingValue: String,
	                          createdAt: Date,
	                          updatedAt: Date
	                          ) {
	override def toString = {
		"itemId:%d".format(id)
	}
}

object EncDispSchedule extends models.Model {

  val tableName = "disp_schedule"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val kind = "kind"
    val settingValue = "setting_value"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, kind, settingValue, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncDispSchedule(
      id = rs.long(id),
      formId = rs.long(formId),
      kind = rs.string(kind),
      settingValue = rs.string(settingValue),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession


  /**
	 * 全てのデータ取得
	 * @return 取得したEncEncConfigsテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncDispSchedule] = {
		val sql =
			"""
			  |SELECT id, form_id, kind, setting_value, created_at, updated_at
			  | FROM disp_schedule
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).map(*).list.apply()
	}

	/**
	 * IDに取得する
	 * @return 取得したEncDispScheduleテーブルのデータ
	 */
	def findById(id: String)(implicit session: DBSession = autoSession): Option[EncDispSchedule] = {
		val sql =
			"""
			  |SELECT id, form_id, kind, setting_value, created_at, updated_at
			  | FROM disp_schedule
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * データを登録します。
	 *
	 * @param schedule
	 * @param session
	 * @return
	 */
	def regist(schedule: EncDispSchedule)(implicit session: DBSession = autoSession): Boolean = {

		Logger.debug("データの登録開始")

    val sql =
      """
        	 		  |INSERT INTO disp_schedule(form_id, kind, setting_value, created_at, updated_at)
        	 		  |    VALUES ({form_id}, {kind}, {setting_value},current_timestamp, current_timestamp);
      			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName('form_id -> schedule.formId, 'kind -> schedule.kind
      , 'setting_value -> schedule.settingValue).update.apply()

		// 戻り値を返す
		retCount > 0
	}

	def update(schedule: EncDispSchedule)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE disp_schedule
			  |   SET form_id = {form_id}, kind = {kind}, setting_value = {setting_value}, updated_at=current_timestamp
			  | WHERE id={id};
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val retCount = SQL(sql).bindByName(
			'form_id -> schedule.formId,
			'kind -> schedule.kind,
			'setting_value -> schedule.settingValue,
			'id -> schedule.id
		).update.apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}
}
