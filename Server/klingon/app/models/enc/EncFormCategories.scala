package models.enc

import java.util.Date
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc.{DBSession, SQL, WrappedResultSet, AutoSession}

case class EncFormCategories(
	                            categoryId: Long,
	                            formId: Long,
	                            name: String,
	                            description: String,
	                            createdAt: Date,
	                            updatedAt: Date
	                            ) {
	override def toString = {
		"categoryId:%s formId:%s name:%s description:%d".format(
			categoryId, formId, name, description)
	}
}

object EncFormCategories extends models.Model {

  val tableName = "form_categories"

  object columnNames {
    val categoryId = "id"
    val formId = "form_id"
    val name = "name"
    val description = "description"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(categoryId, formId, name, description, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormCategories(
      categoryId = rs.long(categoryId),
      formId = rs.long(formId),
      name = rs.string(name),
      description = rs.string(description),
      createdAt = rs.date(createdAt),
      updatedAt = rs.date(updatedAt))
  }

  val autoSession = AutoSession

	/**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormCategories] = {
		val sql =
			"""
			  |SELECT id, form_id, name, description, created_at, updated_at
			  |  FROM form_categories
			  | WHERE id = /*'id*/0
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

    SQL(sql).bindByName('id -> id).map(*).single.apply

	}

	/**
	 * formidからデータを取得します。
	 * @param formId formのID
	 * @return 取得したFormCategoriesテーブルのデータ
	 */
	def findByFormId(formId: Long)(implicit session: DBSession = autoSession): List[EncFormCategories] = {
		val sql =
			"""
			  |SELECT id, form_id, name, description,
			  |       created_at, updated_at
			  | FROM form_categories
			  | WHERE form_id = /*'form_id*/-1
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)
    SQL(sql).bindByName('form_id -> formId).map(*).list.apply
	}

	/**
	 * データを登録します。
	 * @param encFormCategories 登録対象のデータ
	 * @return 正常に登録された場合はTrue
	 */
	def regist(encFormCategories: EncFormCategories)(implicit session: DBSession = autoSession) = {

		Logger.debug("データの登録開始")

		val sql =
			"""
			  |INSERT INTO form_categories(
			  |            form_id, name, description, created_at, updated_at)
			  |    VALUES (/*'form_id*/-1, /*'name*/'name', /*'description*/'descriprion',
			  |            current_timestamp, current_timestamp);
			""".stripMargin

		Logger.debug("sql:" + sql)

		var retValue = DbUpdateResult.success

    val retCount = SQL(sql).bindByName(
      'form_id -> encFormCategories.formId,
      'name -> encFormCategories.name,
      'description -> encFormCategories.description).update.apply

		retValue = DbUpdateResult.success

		// 戻り値を返す
		retValue
	}

	def update(encFormCategories: EncFormCategories)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE form_categories
			  |   SET form_id=/*'form_id*/-1, name=/*'name*/'name', description=/*'description*/'description_aaa'
			  |     , updated_at=current_timestamp
			  | WHERE id=/*'id*/1;
			""".stripMargin

		Logger.debug("sql:" + sql)

		var retValue = DbUpdateResult.success

		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS");
		Logger.debug("日付の確認" + sdf.format(encFormCategories.updatedAt))

    val retCount = SQL(sql).bindByName('form_id -> encFormCategories.formId,
      'name -> encFormCategories.name,
      'description -> encFormCategories.description,
      'id -> encFormCategories.categoryId).update().apply

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	/*
	* FormIdによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: String)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |DELETE FROM form_categories
			  | WHERE form_id=/*'form_id*/-1
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retCount = SQL(sql).bindByName('form_id -> formid).update().apply

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
