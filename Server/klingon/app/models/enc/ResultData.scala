package models.enc

import play.api.libs.json._
import models.ControllerExecuteStatus


/**
 * EncConf画面の処理結果クラス
 * @param parentSerial 親項目のID
 * @param updatedAtFormItemsString 更新日付
 * @param questionItemIds 登録データ項目IDリスト
 */
case class ResultData(parentSerial: String, updatedAtFormItemsString: String, questionItemIds: List[String])

/**
 * JSONフォーマットへ変換するコンパニオンクラス
 */
object ResultData {

	implicit object ResultDataFormat extends Format[ResultData] {

		// note:読み込みは行わない
		def reads(json: JsValue): JsResult[ResultData] = null

		def writes(p: ResultData): JsValue = JsObject(List(
			"status" -> JsNumber(ControllerExecuteStatus.success.id)
			, "parentSerial" -> JsString(p.parentSerial)
			, "updatedAtFormItemsString" -> JsString(p.updatedAtFormItemsString)
			// リストからJsObjectにタプルをもつリストを生成している
			, "questionItemIds" -> JsArray(p.questionItemIds.map(
				questionItemId => JsObject(List("id" -> JsString(questionItemId))))
			)))
	}

}
