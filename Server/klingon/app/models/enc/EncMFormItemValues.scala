package models.enc

import java.util.Date
import play.api.Logger
import scalikejdbc._

case class EncMFormItemValues(
	                             id: Long,
	                             mFormItemsId: Long,
	                             itemValue: String,
	                             createdAt: Date,
	                             updatedAt: Date
	                             ) {
	override def toString = {
		"id:%d mFormItemId:%d itemValue:%s".format(
			id, mFormItemsId, itemValue)
	}
}

object EncMFormItemValues extends models.Model {


  val tableName = "m_form_item_values"

  object columnNames {
    val id = "id"
    val mFormItemsId = "m_form_items_id"
    val itemValue = "item_value"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, mFormItemsId, itemValue, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMFormItemValues(
      id = rs.long(id),
      mFormItemsId = rs.long(mFormItemsId),
      itemValue = rs.string(itemValue),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

  /**
	 * 全てのデータ取得
	 *
	 * @return 取得したFormItemValuesテーブルのデータ
	 */
	def findAll()(implicit session: DBSession = autoSession): List[EncMFormItemValues] = {
		val sql =
			"""
			  |SELECT id, m_form_items_id, item_value, created_at, updated_at
			  |  FROM m_form_item_values
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).map(*).list.apply
	}

	/**
	 * idからデータを取得します。
	 * @param id formsテーブルのID
	 * @return 取得したFormsテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncMFormItemValues] = {
		val sql =
			"""
			  |SELECT id, m_form_items_id, item_value, created_at, updated_at
			  |  FROM m_form_item_values
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('id -> id).map(*).single.apply()
	}

	/**
	 * m_form_item_idからデータを取得します。
	 * @param mformitemid m_form_item_valuesテーブルのm_form_item_id
	 * @return 取得したm_form_item_valuesテーブルのデータ
	 */
	def findBymformitemid(mformitemid: Long)(implicit session: DBSession = autoSession): List[EncMFormItemValues] = {
		val sql =
			"""
			  |SELECT id, m_form_items_id, item_value, created_at, updated_at
			  |  FROM m_form_item_values
			  | WHERE m_form_items_id = {m_form_items_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName('m_form_items_id -> mformitemid).map(*).list.apply()
	}
}
