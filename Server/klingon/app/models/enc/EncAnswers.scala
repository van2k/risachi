package models.enc

import java.util.{Calendar, Date}
import play.api.Logger
import models.Exceptions.DbConcurrencyException
import models.DbUpdateResult
import java.text.SimpleDateFormat
import scalikejdbc._

case class EncAnswers(
	                     id: Long,
	                     formId: Long,
	                     startAt: Date,
	                     endAt: Option[Date],
	                     foreignKey: Option[String],
	                     attribute1: Option[String],
	                     attribute2: Option[String],
	                     attribute3: Option[String],
	                     attribute4: Option[String],
	                     attribute5: Option[String],
	                     isPreview: String,
	                     ipAddress: String,
	                     webBrowser: String,
	                     createdAt: Date,
	                     updatedAt: Date
	                     ) {
	override def toString = {
		"id:%s formId:%s startAt:%s endAt:%s isPreview:%s ipAddress:%s webBrowser:%s".format(
			id, formId, startAt, endAt, isPreview, ipAddress, webBrowser)
	}
}

object EncAnswers extends models.Model {

  val tableName = "answers"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val startAt = "start_at"
    val endAt = "end_at"
    val foreignKey = "foreign_key"
    val attribute1 = "attribute1"
    val attribute2 = "attribute2"
    val attribute3 = "attribute3"
    val attribute4 = "attribute4"
    val attribute5 = "attribute5"
    val isPreview = "is_preview"
    val ipAddress = "ip_address"
    val webBrowser = "web_browser"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, startAt, endAt, foreignKey, attribute1, attribute2, attribute3, attribute4, attribute5, isPreview, ipAddress, webBrowser, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }

  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncAnswers(
      id = rs.long(id),
      formId = rs.long(formId),
      startAt = rs.date(startAt),
      endAt = rs.dateOpt(endAt),
      foreignKey = rs.stringOpt(foreignKey),
      attribute1 = rs.stringOpt(attribute1),
      attribute2 = rs.stringOpt(attribute2),
      attribute3 = rs.stringOpt(attribute3),
      attribute4 = rs.stringOpt(attribute4),
      attribute5 = rs.stringOpt(attribute5),
      isPreview = rs.string(isPreview),
      ipAddress = rs.string(ipAddress),
      webBrowser = rs.string(webBrowser),
      createdAt = rs.timestamp(createdAt).toJavaUtilDate,
      updatedAt = rs.timestamp(updatedAt).toJavaUtilDate)
  }

  val autoSession = AutoSession

	def getBlankAnswer(): EncAnswers = {
		val now = Calendar.getInstance().getTime()
		EncAnswers(-1, -1, now, Option(now), Option(""), Option(""), Option(""), Option(""), Option(""), Option(""), "0", "127.0.0.1", "mozila", now, now)
	}

  /**
	 * idからデータを取得します。
	 * @param id answersテーブルのID
	 * @return 取得したanswersテーブルのデータ
	 */
	def find(id: Long)(implicit session: DBSession = autoSession): Option[EncAnswers] = {
		val sql =
			"""
			  |SELECT id, form_id, start_at, end_at, foreign_key, attribute1, attribute2, attribute3, attribute4, attribute5, is_preview, ip_address, web_browser, created_at, updated_at
			  |  FROM answers
			  | WHERE id = {id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'id -> id
		).map(*).single.apply
	}

	/**
	 * idからデータを取得します。
	 * @param formid answersテーブルのformid
	 * @return 取得したanswersテーブルのデータ
	 */
	def findByFormid(formid: Long)(implicit session: DBSession = autoSession): List[EncAnswers] = {
		val sql =
			"""
			  |SELECT id, form_id, start_at, end_at, foreign_key, attribute1, attribute2, attribute3, attribute4, attribute5, is_preview, ip_address, web_browser, created_at, updated_at
			  |  FROM answers
			  | WHERE form_id = {form_id}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_id -> formid
		).map(*).list.apply
	}

	/**
	 * idからデータを取得します。
	 *
	 * @param formId
	 * @param foreignKey
	 * @return
	 */
	def findByFormidForeignkey(formId: Long, foreignKey: String)(implicit session: DBSession = autoSession): List[EncAnswers] = {
		val sql =
			"""
			  |SELECT id, form_id, start_at, end_at, foreign_key, attribute1, attribute2, attribute3, attribute4, attribute5, is_preview, ip_address, web_browser, created_at, updated_at
			  |  FROM answers
			  | WHERE form_id = {form_id} and foreign_key = {foreign_key}
			  | ORDER BY id
			""".stripMargin

		Logger.debug("sql:" + sql)

		SQL(sql).bindByName(
			'form_id -> formId,
			'foreign_key -> foreignKey
		).map(*).list.apply
	}

	/**
	 * データを登録します。
	 * @param encAnswers 登録対象のデータ
	 * @return 正常に登録された場合はTrue
	 */
	def regist(encAnswers: EncAnswers)(implicit session: DBSession = autoSession): Option[Long] = {

		Logger.debug("データの登録開始")

    val sql =
      """
        |INSERT INTO answers(
        |            form_id, start_at, end_at, foreign_key, attribute1, attribute2, attribute3, attribute4, attribute5, is_preview, ip_address, web_browser, created_at, updated_at)
        |    VALUES ({form_id}, {start_at}, {end_at}, {foreign_key}, {attribute1}, {attribute2}, {attribute3}, {attribute4}, {attribute5}, {is_preview}, {ip_address}, {web_browser},
        |            current_timestamp, current_timestamp)
      """.stripMargin

		Logger.debug("sql:" + sql)

		val newId = SQL(sql).bindByName(
			'form_id -> encAnswers.formId,
			'start_at -> encAnswers.startAt,
			'end_at -> encAnswers.endAt,
			'foreign_key -> encAnswers.foreignKey,
			'attribute1 -> encAnswers.attribute1,
			'attribute2 -> encAnswers.attribute2,
			'attribute3 -> encAnswers.attribute3,
			'attribute4 -> encAnswers.attribute4,
			'attribute5 -> encAnswers.attribute5,
			'is_preview -> encAnswers.isPreview,
			'ip_address -> encAnswers.ipAddress,
			'web_browser -> encAnswers.webBrowser
		).updateAndReturnGeneratedKey.apply()

		// 戻り値を返す
		Option(newId)
	}

	/*
	 * データの更新
	 * @param encAnswers 更新対象のデータ
	 * @return 更新結果
	 */
	def update(encAnswers: EncAnswers)(implicit session: DBSession = autoSession)= {
		Logger.debug("データの更新開始")

		val sql =
			"""
			  |UPDATE answers
			  |   SET form_id = {form_id}, start_at = {start_at}, end_at = {end_at}, is_preview = {is_preview}, ip_address = {ip_address}, web_browser = {web_browser},
			  |       foreign_key = {foreign_key}, attribute1 = {attribute1}, attribute2 = {attribute2}, attribute3 = {attribute3}, attribute4 = {attribute4}, attribute5 = {attribute5}
			  |     , updated_at=current_timestamp
			  | WHERE id={id}
			""".stripMargin

		Logger.debug("sql:" + sql)

    val retValue = DbUpdateResult.success

		val sdf = new SimpleDateFormat("yyyy-MM-dd HH:kk:ss.SS");
		Logger.debug("日付の確認" + sdf.format(encAnswers.updatedAt))

		val retCount = SQL(sql).bindByName(
			'form_id -> encAnswers.formId,
			'start_at -> encAnswers.startAt,
			'end_at -> encAnswers.endAt,
			'is_preview -> encAnswers.isPreview,
			'ip_address -> encAnswers.ipAddress,
			'web_browser -> encAnswers.webBrowser,
			'foreign_key -> encAnswers.foreignKey,
			'attribute1 -> encAnswers.attribute1,
			'attribute2 -> encAnswers.attribute2,
			'attribute3 -> encAnswers.attribute3,
			'attribute4 -> encAnswers.attribute4,
			'attribute5 -> encAnswers.attribute5,
			'id -> encAnswers.id
		).update()apply()

		Logger.debug("処理件数:" + retCount)

		if (retCount == 0) {
			throw new DbConcurrencyException
		}

		// 戻り値を返す
		retValue
	}

	def deleteById(answerId: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  |DELETE FROM answers
			  | WHERE id=/*'answer_id*/-1
			""".stripMargin

		val retCount = SQL(sql).bindByName(
			'answer_id -> answerId
		).update()apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}

	/*
	* FormIDによるデータの削除
	* @param formid アンケートID
	* @return 更新結果
	*/
	def deleteByFormid(formid: Long)(implicit session: DBSession = autoSession) = {
		Logger.debug("データの削除開始")

		val sql =
			"""
			  |DELETE FROM answers
			  | WHERE form_id={form_id}
			""".stripMargin

		Logger.debug("sql:" + sql)

		val retCount = SQL(sql).bindByName(
			'form_id -> formid
		).update()apply()

		Logger.debug("処理件数:" + retCount)

		// 戻り値を返す
		retCount
	}
}
