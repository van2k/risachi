var ansitemid = new Array();

/**
 * アンケート入力時を投稿
 */
function sendAnswer(itemid){
  var answerid, url, request;

  if(ansitemid[itemid] != true){
	// JQueryは使用しない
	ansitemid[itemid] = true;
	answerid = document.getElementById("answerId");
    url = "/encai/" + answerid.value + "/" + itemid;
    request = createXMLHttpRequest();
    request.open("GET", url, true);
    request.send("");
  }
}

/**
 * XMLHttpReques作成
 * @return XMLHttpReques
 */
function createXMLHttpRequest() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
	  return new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
	  try {
	    return new ActiveXObject("Microsoft.XMLHTTP");
	  } catch (e2) {
	    return null;
	  }
    }
  } else {
    return null;
  }
}