
var itemCnt;
var itemId;
var itemName;
var itemKind;
var DUMMY_NUMBER = 100;

/*
 * 初期化
 */
function init(itemcnt, itemid, itemname, itemkind){
  this.itemCnt = itemcnt;
  this.itemId = itemid;
  this.itemName = itemname;
  this.itemKind = itemkind;
}

/**
 * アンケート結果の検索
 * @param menuid メニューID
 */
function searchEnc(menuid){
//   var post = document.getElementById('searchsubmit');
//   $("#menuId").val(menuid);
//   post.submit();
  location.reload();
}

/**
 * 検索項目の追加
 */
function addSearchItem(){
  var selectform = '',i;
  //var dispkind = new Array("3","4","5");

  selectform += '<div class="control-group">';
  selectform += '<select id="formItemId' + itemCnt + '" name="formItemId[' + itemCnt + ']" class="span6" onChange="getItemValue(' + itemCnt + ')">';
  selectform += '<option class="blank" value="">--項目 未選択--</option>';

  var cnt = this.itemKind.length;
  for(i = 0;i < itemId.length;i++){
      //if(Array.indexOf(dispkind, this.itemkind[i]) >= 0){
          selectform += '<option value="' + this.itemId[i] + '" >' + this.itemName[i] + '</option>';
      //}
  }
  selectform += '</select>\n';
  selectform += '<select id="formItemValueId' + itemCnt + '" name="formItemValueId[' + itemCnt + ']" class="span6">';
  selectform += '<option class="blank" value="">--値 未選択--</option>';
  selectform += '</select></div>';
  $("#searchitems").append(selectform);
  itemCnt = itemCnt + 1;
  $("#rdoAndOr").show();
}

/**
 * 検索項目の削除
 */
function removeSearchItem(){
	if(itemCnt > 0){
		$("#formItemId" + (itemCnt - 1)).remove();
		$("#formItemValueId" + (itemCnt - 1)).remove();
		itemCnt = itemCnt - 1;
	}
	if(itemCnt == 0){
		$("#rdoAndOr").hide();
	}
}

/**
 * 選択した検索項目の値を取得
 * @param itemidx 設問idx
 */
function getItemValue(itemidx){
  var itemid;

  itemid = $("#formItemId" + itemidx + "").children(':selected').val();
  $.getJSON("/reportitemvalues/" + getFormId() + "/" + itemid, function(json_obj){
	  var json = JSON.stringify(json_obj);
	  $('#formItemValueId' + itemidx + ' > option').remove();
	  $('#formItemValueId' + itemidx + '').append($('<option>').html('--値 未選択--').val(''));
	  for (var key in json_obj){
        $('#formItemValueId' + itemidx + '').append($('<option>').html(json_obj[key]).val(key));
	  }
	});
}

/**
 * ダミーデータ作成
 */
function createDummy(menuid){
  var itemid;

  $.getJSON("/reportdummy/" + getFormId() + "/" + DUMMY_NUMBER, function(json_obj){
	  searchEnc(menuid);
	});
}

/**
 * 回答情報詳細を表示する
 * @param itemid 設問id
 */
function answerPopup(itemid){
  window.open('/answerdetails/' + itemid, 'details', 'width=1000,height=600,toolbar=false,location=false,scrollbars=yes');
}

/**
 * フォームIDの取得
 * return フォームID
 */
function getFormId(){
  return $("#formId").val();
}