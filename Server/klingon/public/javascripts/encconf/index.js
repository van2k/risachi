
// m_from_itemsのデータを保持
var MFormItems;
var MFromItemValues;

//region *** タイトルデータをXHRで送信する際のデータ区分
var TitleDiv = 0;
var DetailDiv = 1;
var ResultMessageDiv = 2;
//endregion

// region *** 選択項目数条件(aが基準)

// a = b
var SelectNumberConditionEqual = 1;

// a >= b
var SelectNumberConditionMoreEqual = 2;

// a <= b
var SelectNumberConditionLessEqual = 3;
// endregion

//region *** 生成する質問形式の定義
var QuestionTypeTextLine = "1";
var QuestionTypeTextMultiLine="2";
var QuestionTypeRadio = "3";
var QuestionTypeChckbox = "4";
var QuestionTypeDropList = "5";
var QuestionTypeDate = "6";
var QuersionTypeRegex = "7";
//endregion

/**
 * 画面ロード時の処理
 */
$(function () {

    $("#enc_title").blur(function () {

        // XHRでコントローラへデータを登録する
        var item = $("#enc_title").val();

        execTitleUpdate(item, TitleDiv);
    });

    $("#enc_detail").blur(function () {
        // XHRでコントローラへデータを登録する
        var item = $("#enc_detail").val();

        execTitleUpdate(item, DetailDiv);
    });

    $("#enc_result_message").blur(function () {
        // XHRでコントローラへデータを登録する
        var item = $("#enc_result_message").val();

        execTitleUpdate(item, ResultMessageDiv);
    });

    // すべての選択タグに変更時処理を追加する
    //$("#enc_q_conf .q_q_type").live('change',onSelectChange);
});

/**
 * タイトルへフォーカスをセットする
 * @param tagId タグ
 * @param message メッセージ
 */
function setTitleOnFocus(tagId, message){

    var item = $(tagId).val();
    if(item === ''){
        $(tagId).val(message);
    }

    $(tagId).focus(function(){
        if(this.value === message){
            $(this).val("");
        }
    }).blur(function(){
        if($(this).val() === ''){
            $(this).val(message);
        }
    });
}

/**
 * コントローラーへXHR通信を行う
 * @param content 更新内容
 * @param dataDiv コンテンツデータ区分(0:enc_title / 1:enc_detail / 2:result_message)
 */
function execTitleUpdate(content, dataDiv) {

    // キー項目と更新日付を取得する
    var formId = $("#formId").val();
    var contractId = $('#contractId').val();
    var sendData = {formId:formId, contractId:contractId, value:content, dataDiv:dataDiv};

    $.ajax({
        type:'POST',
        url:'/conf/updatetitle',
        data:sendData,
        dataType:'json',
        success:function (data) {
            if (data.status == "0") {
                if (dataDiv === TitleDiv && $('#enc_title_error')[0]) {
                    $('#enc_title_error').remove();
                } else if (dataDiv === DetailDiv && $('#enc_detail_error')[0]) {
                    $('#enc_detail_error').remove();
                } else if (dataDiv === ResultMessageDiv && $('#enc_result_message_error')[0]) {
                    $('#enc_result_message_error').remove();
                }
                $().toastmessage('showSuccessToast', data.message);
            } else {
                if (dataDiv === TitleDiv && !$('#enc_title_error')[0]) {
                    $('#enc_title').after('<div id="enc_title_error" class="alert alert-error">' + data.message + '</div>');
                } else if (dataDiv === DetailDiv && !$('#enc_detail_error')[0]) {
                    $('#enc_detail').after('<div id="enc_detail_error" class="alert alert-error">' + data.message + '</div>');
                } else if (dataDiv === ResultMessageDiv && !$('#enc_result_message_error')[0]) {
                    $('#enc_result_message').after('<div id="enc_result_message_error" class="alert alert-error">' + data.message + '</div>');
                }
            }
        }
    });
}

/**
 * コントローラーへXHR通信を行う
 * @param content 更新内容
 * @param encId
 */
function execFormItemUpdate(sendData, encId) {

    $.ajax({
        type:'POST',
        url:'/conf/updateformitems',
        data:sendData,
        dataType:'json',
        success:function (data) {

            var parentDivId = '#enc_q_' + encId;
            var parentDiv = $(parentDivId);

            // エラー内容の初期化
            parentDiv.find('.text-error').remove();

            // 正常終了の場合
            if(data.status == 0){

                // エラーメッセージを削除する
                parentDiv.children('.q_div_title').children('.err_message').remove();

                // 更新時刻を設定する
                parentDiv.children('.updatedAtFormItems').val(data.updatedAtFormItemsString);

                // DBで登録したIDを更新する
                parentDiv.children('.enq_q_db_form_id').val(data.parentSerial);

                $().toastmessage('showNoticeToast', 'アンケートの質問を登録しました。');

                // ステータスを変更する
                ItemsManage.chgStatus(encId, 1);

                // タイトル以外を非表示とする
                parentDiv.children('.q_div_q_body').slideUp('slow');

                setButtonMode(encId, false);
            }else{
                var titleErrMessage = data.titleErrMessage;
                if(titleErrMessage !== ""){
                    parentDiv.children('.q_div_title').after(getErrorMessageTag(titleErrMessage));
                }

                var selectItemCountErrMessage = data.selectItemCountErrMessage;
                if(selectItemCountErrMessage !== ""){
                    parentDiv.children('.q_div_q_body').children('.q_div_answer').children('.q_answer_div_char_length').after(getErrorMessageTag(selectItemCountErrMessage));
                }

                var maxLengthErrMessage = data.maxLengthErrMessage;
                if(maxLengthErrMessage !== ""){
                    parentDiv.children('.q_div_q_body').children('.q_div_answer').children('.ans_input_max_length').after(getErrorMessageTag(maxLengthErrMessage));
                }

                var items = data.itemErrorMessages.sort(function(x, y){
                    return (x.itemNumber - y.itemNumber);
                });

                items.forEach(function(item){
                    item.messages.forEach(function(m){
                        parentDiv.children('.q_div_q_body').children('.q_div_answer').children('.q_answer_item_div')
                            .eq((Number(item.itemNumber) -1)).append(getErrorMessageTag(m.message));
                    });
                });
            }
        }
    });
}

function getErrorMessageTag(message){
    return '<p class="text-error">*' + message + '</p>';
}

/**
 * コントローラーへXHR通信を行う
 * @param content 更新内容
 * @param encId
 */
function execFormItemDelete(sendData, encId) {

    $.ajax({
        type:'POST',
        url:'/conf/deleteformitems',
        data:sendData,
        dataType:'json',
        success:function (data) {

            $().toastmessage('showNoticeToast', '質問を削除しました。');

            // ステータスがOKの場合は質問を削除する
            if(data.status === "Ok"){
                var parentDivId = '#enc_q_' + encId;
                var parentDiv = $(parentDivId);

                parentDiv.remove();
                // 管理クラスから項目を削除する
                ItemsManage.delItem(encId);
            }
        }
    });
}

/**
 * コントローラへXHR通信を行う
 *
 * @param sendData
 *
 */
function execPageAdd(sendData) {
	$.ajax({
		type:'POST',
		url:'/conf/addpage',
		data:sendData,
		dataType:'json',
		success:function (data) {
			$().toastmessage('showNoticeToast', data.message);
		}
	});

}

/**
 * コントローラーへXHR通信を行う
 * @param content 更新内容
 * @param serial
 */
function execPageDelete(sendData, pageDivId, pageNum) {

    $.ajax({
        type:'POST',
        url:'/conf/deletepage',
        data:sendData,
        dataType:'json',
        success:function (data) {

            $().toastmessage('showNoticeToast', data.message);

            // ステータスがOKの場合はページを削除する
            if(data.status === "Ok"){
                var pageDiv = $("#" + pageDivId);
                pageDiv.remove();
                // 管理クラスからも削除する
                ItemsManage.delPage(pageNum);
            }
        }
    });
}

/**
 * 一番下のノードにアンケートを追加する
 */
function addQuestionFormat(pageIndex){
   if(ItemNumTotal < ItemNum){
    var serial = getDateSerial();
    var nodeTag = getNodeTag(serial);

	  var pageNum = getCurrentPageNumber(pageIndex);
    var currentPageDivId = 'page_' + pageIndex;
    var addPageBtnDiv = $("#" + currentPageDivId).children('.add_page_btn_div');
    addPageBtnDiv.before(nodeTag);
    //addPageBtnDiv.append(nodeTag).SortableAddItem();

    var selectorTagName = 'q_q_type_' + serial;
    $("#" + selectorTagName).live('change',onSelectChange);

    // 編集ボタンは追加時は非活性とする
    $('#enc_q_' + serial).children('.q_div_button').children('.q_edit').attr('disabled', true);

    // 追加したアンケートへフォーカスする
    $('#enc_q_' + serial).children('.q_div_title').children('.q_title').focus();

    // 設問を管理クラスに追加する
    ItemsManage.addItem(pageNum, serial);
    ItemNumTotal++;
  }else{
    alert('項目が上限に達しました');
  }
}

/**
 * ページを追加します。
 */
function addPage(){
		var currentPageNum = getAllPageCount();
    if(currentPageNum < PageNum){
	    PageCounter++;
      var nextPageNum = PageCounter;

      var pageTag = getAddPageTag(nextPageNum, currentPageNum + 1);
      var parentDivId = 'enc_q_conf';
      $('#' + parentDivId).append(pageTag);
	    // 新しくページ要素が追加された為、sortable設定を再度行う
	    $(".enc_q_page").sortable({
		    connectWith: ".enc_q_page",
		    opacity: 0.7,
		    items: '.q_div',
		    handle: 'span',
		    cursor: "move",
		    update: function(event, ui) {
			    var idx, pageidx;
			    // 別ページに移動した場合は、2回コールされるので、event.target.idと比較する
			    if(ui.item.length == 1 && ui.item[0].parentNode.id == event.target.id){
				    encId = ui.item[0].id.split('_')[2];
				    pageIndex = ui.item[0].parentNode.id.split('_')[1];
				    pageNum = getCurrentPageNumber(pageIndex);
				    // divの何番目か確認する
				    pageidx = 0;
				    for(idx = 0;idx < ui.item[0].parentNode.children.length;idx++){
					    if(ui.item[0].parentNode.children[idx].id == ui.item[0].id){
						    break;
					    }
					    if(ui.item[0].parentNode.children[idx].id.substring(0,6) == "enc_q_"){
						    pageidx++;
					    }
				    }
				    ItemsManage.chgItemIndex(encId, String(pageNum), pageidx);
				    ItemsManage.sendPost();
			    }
		    }
	    });
	    $(".q_div span").disableSelection();

	    // アンケートフォームの総ページ数更新
	    var formId = $("#formId").val();
	    var allPageCount = getAllPageCount();

	    var sendData = {formId:formId, allPageCount:allPageCount};
	    execPageAdd(sendData);

    }else{
      // ページ数オーバー
      alert('ページを追加することはできません');
    }
}

/**
 * ページを削除します。
 * @param currentPageIndex
 */
function onDeletePage(currentPageIndex){

		if (!confirm("このページを削除してもよろしいですか？\n（※ページを削除した場合は、対象ページおよびページ内のアンケート項目を元に戻すことは出来ません。）")) {
			return;
		}
    // すべてのページを削除することはできない
    var pageCount = $('.enc_q_page').size();
    if(pageCount === 1){
        alert('すべてのページを削除することはできません。');
        return;
    }

		var currentPageNum = getCurrentPageNumber(currentPageIndex);
    var formId = $("#formId").val();
    var contractId = $('#contractId').val();
    var pageDivId = "page_" + currentPageIndex;
    var allPageCount = getAllPageCount() - 1;

    var sendData = {formId:formId, contractId:contractId, pageNum:currentPageNum, allPageCount:allPageCount};
    execPageDelete(sendData, pageDivId, currentPageIndex);
}

/**
 * 回答方法の選択変更時
 */
function onSelectChange(){
    onSelectChangeById($(this).attr('id'));
}

/**
 * 回答方法の選択変更時
 */
function onSelectChangeById(inencId){
    var i;
    var encId = inencId.split('_')[3];
    var parentDivId = 'enc_q_' + encId;
    var childDivAnswer = $('#' + parentDivId).children('.q_div_q_body').children('.q_div_answer');
    var questionType = $('#' + 'q_q_type_' + encId + ' option:selected').val();
    var selectedIndex = $('#' + 'q_q_type_' + encId).get(0).selectedIndex;
    var mitemid = MFormItems[selectedIndex].id;
    var mitemvalue = MFormItemValues[selectedIndex].values;

    //alert(mitemvalue);
    childDivAnswer.empty();

    var appendTag = '<div class="q_div_answer">';
    var questionId = getDateSerial();
    //var questionType = $('#' + 'q_q_type_' + questionId + ' option:selected').val();

    switch (questionType){
        case QuestionTypeTextLine:
            appendTag = '入力文字列の長さ：<input type="text" class="ans_input_max_length" value="100" maxlength="4">';
            break;

        case QuestionTypeTextMultiLine:
            appendTag = '入力文字列の長さ：<input type="text" class="ans_input_max_length" value="300" maxlength="4">';
            break;

        case QuestionTypeRadio:
            var questionId1 = questionId;
            var questionId2 = questionId + 1;
            var questionId3 = questionId + 2;

            if(mitemvalue == null){
              appendTag =
                  '<input type="button" value="項目追加" class="q_item_add btn btn-info" onclick="onAddItemClick(\'' + encId + '\')" /> ' +
                  '<div id="q_answer_item_div_' + questionId1 + '" class="q_answer_item_div"><input type="radio" checked="" name="q_answer_rd_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId1) + '</div>'+
                  '<div id="q_answer_item_div_' + questionId2 + '" class="q_answer_item_div"><input type="radio" checked="" name="q_answer_rd_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId2) + '</div>'+
                  '<div id="q_answer_item_div_' + questionId3 +'" class="q_answer_item_div"><input type="radio" checked="" name="q_answer_rd_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId3) + '</div>'+
                  getElseTag(encId);
            }else{
              appendTag = fixedAppendTag(questionId, mitemvalue);
            }
            break;

        case QuestionTypeChckbox:

            var questionId1 = questionId;
            var questionId2 = questionId + 1;
            var questionId3 = questionId + 2;

            if(mitemvalue == null){
              appendTag =
                  '<input type="button" value="項目追加" class="q_item_add btn btn-info" onclick="onAddItemClick(\'' + encId + '\')" /> ' +
                  '<div id="q_answer_item_div_' + questionId1 + '" class="q_answer_item_div"><input type="checkbox" name="q_answer_chk_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId1) + '</div>'+
                  '<div id="q_answer_item_div_' + questionId2 + '" class="q_answer_item_div"><input type="checkbox" name="q_answer_chk_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId2) + '</div>'+
                  '<div id="q_answer_item_div_' + questionId3 + '" class="q_answer_item_div"><input type="checkbox" name="q_answer_chk_' + questionId + '" class="enc_q_answer" /> '
                      + getInputQuestionTag(questionId3) + '</div>' +
                  getElseTag(encId) + getSelCountConditionTag(encId);
            }else{
              appendTag = fixedAppendTag(questionId, mitemvalue);
            }
            break;

        case QuestionTypeDropList:

            var questionId1 = questionId;
            var questionId2 = questionId + 1;
            var questionId3 = questionId + 2;

            if(mitemvalue == null){
              appendTag =
                  '<input type="button" value="項目追加" class="q_item_add btn btn-info" onclick="onAddItemClick(\'' + encId + '\')" /> ' +
                  '<div id="q_answer_item_div_' + questionId1 + '" class="q_answer_item_div">' + getInputQuestionTag(questionId1) +'</div>' +
                  '<div id="q_answer_item_div_' + questionId2 + '" class="q_answer_item_div">' + getInputQuestionTag(questionId2) +'</div>' +
                  '<div id="q_answer_item_div_' + questionId3 + '" class="q_answer_item_div">' + getInputQuestionTag(questionId3) +'</div>';
            }else{
              appendTag = fixedAppendTag(questionId, mitemvalue);
            }
            break;

        case QuestionTypeDate:
            //appendTag = '<input type="text" id="datepicker">';
            break;

        case QuersionTypeRegex:
            break;

        default:
            break;

    }
    appendTag += "</div>";
    childDivAnswer.append(appendTag);

    //appendTag += "</div>";
    //childDivAnswer.append(appendTag);
}

function fixedAppendTag(questionId, mitemvalue){
  var appendTag;

  appendTag = '定型:<SELECT>';
  for(i = 0;i < mitemvalue.length;i++){
    appendTag += '<OPTION value="' +  mitemvalue[i].id+ '">' +  mitemvalue[i].itemValue + '</OPTION>';
  }
  appendTag += '</SELECT>';
  for(i = 0;i < mitemvalue.length;i++){
      appendTag += '<div id="q_answer_item_div_' + (questionId + i) + '" class="q_answer_item_div"><input type="hidden" class="q_answer" value="' + mitemvalue[i].itemValue + '"></div>';
  }

  return appendTag;
}

/**
 * 選択した検索項目の値を取得
 * @param itemidx 設問idx
 */
function createQuestion(mitemid, questionType){
  $.getJSON("/conf/mitemvalues/" + mitemid, function(json_obj){

	  var json = JSON.stringify(json_obj);
	  //$('#formItemValueId' + itemidx + ' > option').remove();
	  //$('#formItemValueId' + itemidx + '').append($('<option>').html('--値 未選択--').val(''));
	  for (var key in json_obj){
        $('#formItemValueId' + itemidx + '').append($('<option>').html(json_obj[key]).val(key));
	  }
	});
}


/**
 * 項目追加ボタンクリック時
 * @param serial 現在のシリアル値
 */
function onAddItemClick(encId){
    // q_answer_divの最後に項目を追加する
    var parentDivId = '#enc_q_' + encId;

    var childDivAnswer = $(parentDivId).children("div.q_div_q_body").children("div.q_div_answer");
    var questionType = $(parentDivId + ' option:selected').val();
    var questionId = getDateSerial();

    // 一つ目の項目を取得する
    // 質問項目は最低一つは存在するため
    var answerItemDivTag = childDivAnswer.children('.q_answer_item_div :first').clone();

    // すでに設定されている項目を削除する
    answerItemDivTag.children('.q_answer').val('');

    // divタグのidを変更する
    var newId = 'q_answer_item_div_' + questionId;
    answerItemDivTag.attr('id', newId);

    // deleteイベントの追加
    var onDeleteClickEvent = 'onDeleteClick(' + questionId + ')';
    answerItemDivTag.children(':button').attr('onClick', onDeleteClickEvent);

    // 入力項目を追加
    childDivAnswer.children('.q_answer_item_div :last').after(answerItemDivTag);

    // 追加項目へフォーカスをセットする
    answerItemDivTag.children('.q_answer').focus();
    ItemNumTotal++;
}

function getAllPageCount() {
// ページ数
    var allPageCount = $('#enc_q_conf .enc_q_page').size();
    return allPageCount;
}
/**
 * 完了ボタンクリック時にコントローラへ該当する質問内容を登録します。
 * JSON形式
 * [formId:フォームID,
 *  contractId:契約ID,
 *  updatedAtFormItems:フォーム項目更新日時,
 *  questionTitle:質問タイトル,
 *  questionType:質問形式,
 *  required:必須項目チェック,
 *  ansInputMaxLength:回答内容の長さ,
 *  questionItems:質問内容(複数項目の場合は各項目),
 *  itemSelectCount:質問選択個数,
 *  selectNumberCondition:選択個数条件]
 */
function onUpdateClick(encId){

    // アンケート全体のdivを取得
    var parentDivId = '#enc_q_' + encId;
    var parentDiv = $(parentDivId);

    // タイトル
    var inputTitle = parentDiv.children('.q_div_title').children('.q_title');
    var inputTitleValue = inputTitle.val();

    var qustionBodyDiv = parentDiv.children('.q_div_q_body');

    // 項目マスタID
    var questionType = qustionBodyDiv.children('.q_div_q_type').children('.q_q_type');
    var qTypeValue = questionType.val();

    // 必須の有無
    var required = qustionBodyDiv.children('.q_div_must').children('.checkbox').children('.q_must').attr('checked');
    var requiredValue = false;
    if(required === 'checked'){
        requiredValue = true;
    }

    // アンケート更新日付
    var updatedAtFormItems = parentDiv.children('.updatedAtFormItems').val();
    if(updatedAtFormItems === undefined){
        updatedAtFormItems = "";
    }

    // ページ番号
    var pageNumDivId = parentDiv.parent(".enc_q_page").attr('id');
    var pageIndex = pageNumDivId.split('_')[1];
		var pageNum = getCurrentPageNumber(pageIndex);

    var allPageCount = getAllPageCount();

    // 回答入力最大長
    var ansInputMaxLength = 0;

    // 回答選択個数
    var itemSelectCount = 150;
    var selectNumberCondition = "0";

    // チェックボックスの場合
    if(qTypeValue === QuestionTypeChckbox){
        var answerDivCharLength = qustionBodyDiv.children('.q_div_answer').children('.q_answer_div_char_length');
        itemSelectCount = answerDivCharLength.children('#q_answer_char_length_' + encId).val();
        selectNumberCondition = answerDivCharLength.children('#q_answer_char_length_condition_' + encId).val();
    }

    var question={};

    if(qTypeValue === QuestionTypeTextLine || qTypeValue === QuestionTypeTextMultiLine){
        // テキストを取得する
        question[0] ="dummy";
        ansInputMaxLength = qustionBodyDiv.children('.q_div_answer').children('.ans_input_max_length').val();
	      if(!jQuery.isNumeric(ansInputMaxLength)){
		      ansInputMaxLength = 0;
	      }
    }else{
        // 質問内容を取得
        var questionDivAnswer = qustionBodyDiv.children('.q_div_answer');
        var qustionItems = questionDivAnswer.children('.q_answer_item_div').children('.q_answer');
        $.each(qustionItems, function(index, qItem){
            question[index] = qItem.value;
        });

        // その他が存在する場合は回答文字数の最大長を変更する
        var selectElseValue = questionDivAnswer.children('.q_answer_div_else').children('input:checkbox').attr('checked');
        if(selectElseValue === 'checked'){
            ansInputMaxLength = questionDivAnswer.children('.q_answer_div_else').children('input:checkbox').val();
        }
    }

    // キー項目と更新日付を取得する
    var formId = $("#formId").val();
    var contractId = $('#contractId').val();
    var formItemId = parentDiv.children('.enq_q_db_form_id').val();
    var selectedIndex = $('#' + 'q_q_type_' + encId).get(0).selectedIndex;
    var mFormItemId = MFormItems[selectedIndex].id;

    // JSONの生成
    var sendData = {formId:formId,contractId:contractId, formItemId:formItemId ,updatedAtFormItems:updatedAtFormItems
        , questionTitle:inputTitleValue, pageNum:pageNum, questionType:qTypeValue, required:requiredValue
        , ansInputMaxLength:ansInputMaxLength, questionItems:question, itemSelectCount:itemSelectCount, dispOdr: ItemsManage.getDispOdr(encId)
        , selectNumberCondition:selectNumberCondition, allPageCount:allPageCount, mFormItemId:mFormItemId  };

    // JSONデータをコントローラーへ送信する
    execFormItemUpdate(sendData, encId);
}

/**
 * 編集ボタンクリック時
 */
function onEditClick(encId){
    var parentDiv = $("#enc_q_" + encId);
    var qustionBodyDiv = parentDiv.children('.q_div_q_body');
    qustionBodyDiv.slideDown('slow');

    setButtonMode(encId, true);
}


/**
 * 編集ボタンクリック時
 */
function onDeleteQuestionClick(encId){

    // アンケート全体のdivを取得
    var parentDivId = '#enc_q_' + encId;
    var parentDiv = $(parentDivId);

    // アンケート更新日付
    var updatedAtFormItems = parentDiv.children('.updatedAtFormItems').val();
    var formItemId = parentDiv.children('.enq_q_db_form_id').val();

    var formId = $("#formId").val();
    var contractId = $('#contractId').val();

    var allPageCount = getAllPageCount();

    var sendData = {formId:formId, contractId:contractId, updatedAtFormItems:updatedAtFormItems
        , formItemId:formItemId, allPageCount:allPageCount};
    execFormItemDelete(sendData, encId);
    ItemNumTotal--;
}

/**
 * 削除ボタンをクリックしたdiv領域を削除します。
 * @param btnDivId 該当するDivのid
 */
function onDeleteClick(btnDivId){

    // すべてのdivは削除できない
    var curDivId = '#q_answer_item_div_' + btnDivId;
    var parentDiv = $(curDivId).parent().parent();
    var items = $(parentDiv).find('.q_answer_item_div');
    if(items.size() === 1){
        alert('すべての質問を削除することはできません。');
        return;
    }

    // 該当するdivを削除する
    $(curDivId).remove();
}

/**
 * 現在日付からシリアル値を取得します。
 * @return {Date}
 */
function getDateSerial(){
    var curDate = new Date();
    var curDateSerial = curDate.valueOf();
    return curDateSerial;
}

/**
 * アンケートの質問部分を追加します。
 * @param serial 設定するシリアル値
 * @return {String} 生成したタグ
 */
function getNodeTag(serial){
    var i;
    var nodeTags =
        '<div id="enc_q_' + serial + '" class="q_div"> '+

        '    <div class="q_div_title"> '+
	      '        <span class="ui-icon ui-icon-arrowthick-2-n-s ui-corner-all ui-state-hover"></span>'+
        '        タイトル:<input class="q_title" type="text" value=""> '+
        '    </div> '+
        '    <div class="q_div_q_body"> '+
        '    <div class="q_div_q_type"> '+
        '        質問形式: '+
        '        <select class="q_q_type" id="q_q_type_' + serial + '"> ';
        for(i = 0;i < MFormItems.length;i++){
            nodeTags += '<option value="' + MFormItems[i].kind + '">' + MFormItems[i].name + '</option>';
        }
     nodeTags +=
        '        </select> '+
        '    </div> '+
        '    <div class="q_div_answer"> '+
        '        入力文字列の長さ：<input class="ans_input_max_length" value="100" maxlength="4">'+
        '    </div> '+
        '    <div class="q_div_must"> '+
        '        <label class="checkbox inline"><input type="checkbox" class="q_must" value="true" checked />この質問を必須にする</lavel> '+
        '    </div> '+
        '    </div> '+
        '    <div class="q_div_button"> '+
        '        <input type="button" value="完了" class="q_complete btn btn-primary" onclick="onUpdateClick(' + serial + ')" /> '+
        '        <input type="button" value="編集" class="q_edit btn btn-inverse" onclick="onEditClick(' + serial + ')" /> '+
        '        <input type="button" value="削除" class="q_delete btn btn-danger" onclick="onDeleteQuestionClick(' + serial + ')" /> '+
        '    </div> '+
        '    <input type="hidden" class="updatedAtFormItems">' +
        '    <input type="hidden" value="-1" class="enq_q_db_form_id" /> '+
        '</div> ';

    return nodeTags;
}

/**
 * 入力内容の共通タグ生成処理
 * @param questionId タグ識別数値
 * @return {String} 生成したタグ
 */
function getInputQuestionTag(questionId){
    var s = '<input type="text" class="q_answer" />' +
        '<input type="button" class="item_delete_btn btn btn-danger" value="削除" onclick="onDeleteClick('+questionId+')">';
    return s;
}

/**
 * 選択個数設定タグを生成します。
 * @param encId 親Divのシリアル値
 * @return {String} 生成したタグ
 */
function getSelCountConditionTag(encId){
    var tag =
        '<div id="q_answer_div_char_length_' + encId + '" class="q_answer_div_char_length">' +
            '選択個数：' + '<input type="text" id="q_answer_char_length_' + encId + '" value="1" class="q_answer_div_char_length_input">' +
            '<select class="q_answer_char_length_condition" id="q_answer_char_length_condition_' + encId + '">' +
            '<option value="'+SelectNumberConditionEqual+'">個</option> ' +
            '<option value="'+SelectNumberConditionMoreEqual+'">個以上</option> '+
            '<option value="'+SelectNumberConditionLessEqual+'">個以下</option> '+
            '</select>' +
            '</div>';
    return tag;
}

/**
 * その他を追加するタグを生成します。
 * @param encId 親Divのシリアル値
 * @return {String} 生成したタグ
 */
function getElseTag(encId){
    //var tag = '<div id="q_answer_div_else_' + encId + '" class="q_answer_div_else">' +
    //    '<label class="checkbox inline"><input type="checkbox" value="150" name="add_else_check_' + encId + '">その他を追加する(100文字以内)</label></div>';
    var tag = "";
    return tag;
}

/**
 * ページ追加ボタン押下時のタグを生成します。
 * @param pageIndex 次ページインデックス
 * @param currentPageNum 現在のページ数
 * @return {String}
 */
function getAddPageTag(pageIndex, currentPageNum) {
    var pageTag =
        '<div id="page_' + pageIndex + '" class="enc_q_page well ui-sortable">' +
        '    <div class="add_question_btn_div">' +
        '        <input type="button" class="btn btn-primary" value="質問の追加" onclick="addQuestionFormat(' + pageIndex + ')">' +
        '    </div>' +
        '    <div class="add_page_btn_div">'
    pageTag +=
        '        <input type="button" class="btn btn-warning" value="ページの追加" onclick="addPage()">' +
        '        <input type="button" class="btn btn-danger" value="ページの削除" onclick="onDeletePage(' + pageIndex + ')">' +
        '    </div>' +
        '</div>';

    return pageTag;
}

/**
 * ボタンモードを指定します。
 * @param encId アンケートID
 * @param isEditMode 編集モードの場合はTrue
 */
function setButtonMode(encId, isEditMode){

    var editClass = '#enc_q_' + encId + ' .q_edit';
    $(editClass).prop('disabled', isEditMode);

    var completeClass = '#enc_q_' + encId + ' .q_complete';
    $(completeClass).prop('disabled', !isEditMode);
}

/**
 * 指定されたページインデックスが何ページ目なのかを返す。
 *
 * @param currentPageIndex ページインデックス
 * @returns ページ番号
 */
function getCurrentPageNumber(currentPageIndex) {
	var currentPageNum = 0;
	for (i = 1; i <= PageCounter; i++) {
		if ($('#page_' + i).size() > 0) {
			currentPageNum++;
			if (i == currentPageIndex) {
				break;
			}
		}
	}
	console.log("currentPageNum:" + currentPageNum)
	return currentPageNum;
}


/*
* 設問の管理関数
* ・ページ番号
* ・並び順
* {formid:フォームID, pageno:ページ番号, itemid:設問ID, dispodr:表示順, status:ステータス(0:未送信,1:送信済み)}
*/
var FormItemsManage = function(formid){
	this.formId = formid;
	this.formItems = new Array();

	/**
	* 設問項目初期化
	* @param items array
	*/
	this.initItems = function(items){
		var i = 0;
		this.formItems = items;
		for(i = 0;i < this.formItems.length;i++){
			this.formItems[i].formid = this.formId;
		}
	}

	/**
	* 設問項目追加
	* @param itemid 設問ID
	*/
	this.addItem = function(pageno, itemid){
		var i, maxid, jsn;

		pageLastIdx = -1;
		for(i = 0;i < this.formItems.length;i++){
			if(this.formItems[i].pageno == pageno){
				pageLastIdx = i;
			}
		}
		jsn = {formid:this.formId, pageno:pageno.toString(), itemid:itemid.toString(), dispodr:this.formItems.length.toString, status:"0"};
		if(pageLastIdx == -1){
			this.formItems.push(jsn);
		}else{
			this.formItems.splice(pageLastIdx + 1, 0, jsn);
		}
		this.setDispOdr();
	}

	/**
	* ステータス変更
	* @param itemid 設問ID
	* @param status ステータス
	*/
	this.chgStatus = function(itemid, status){
		idx = this.itemIndex(itemid);
		if(idx == -1) return;
		if(status != 0 || status != 1) return;

		this.formItems[idx].status = status.toString();
	}

	/**
	* 設問項目削除
	* @param itemid 設問ID
	*/
	this.delItem = function(itemid){
		var idx;

		idx = this.itemIndex(itemid);
		if(idx != -1){
			this.formItems.splice(idx ,1);
		}
		this.setDispOdr();
	}

	/**
	* ページNoによる削除
	* @param pageno
	*/
	this.delPage = function(pageno){
		var i;

		for(i = this.formItems.length - 1;i >= 0;i--){
			if(this.formItems[i].pageno == pageno){
				this.formItems.splice(i ,1);
			}
		}
		this.setDispOdr();
	}

	/**
	* 表示順取得
	*
	*/
	this.getDispOdr = function(itemid){
		var idx = this.itemIndex(itemid);

		if(idx < 0) return;
		return this.formItems[idx].dispodr;
	}

	/*
	* 表示順最大番号
	*/
	this.maxDispOdr = function(){
		return this.formItems.length;
	}

	/**
	* 表示順再セット
	*/
	this.setDispOdr = function(){
		for(i = 0;i < this.formItems.length;i++){
			this.formItems[i].dispodr = i.toString();
		}
	}

	/*
	* 設問の配列Idx取得
	* @param itemid 設問ID
	* @return this.formItemsの配列Index(not found -1)
	*/
	this.itemIndex = function(itemid){
		var i,ret;

		ret = -1;
		for(i = 0;i < this.formItems.length;i++){
			if(this.formItems[i].itemid == itemid){
				ret = i;
				break;
			}
		}

		return ret;
	}

	/**
	* 設問項目並び順変更
	* @param itemid 設問ID
	* @param pageno ページ番号
	* @param pageidx 並び順(0～)
	*/
	this.chgItemIndex = function(itemid, pageno, pageidx){
        console.debug("chgItemIndex itemid:" + itemid + ",pageno:" + pageno + ",pageidx:" + pageidx)
		if(pageidx < 0 || pageidx >= this.formItems.length || this.itemIndex[itemid] == -1){
            console.error("chgItemIndex Parameter error.")
			return;
		}
		var insertidx, tmp, idx, tmpidx;

		idx = this.itemIndex(itemid);
        console.debug("chgItemIndex idx:" + idx);
		if(idx != -1){
			tmpidx = 0;
			for(insertidx = 0;insertidx < this.formItems.length;insertidx++){
				if(this.formItems[insertidx].pageno == pageno){
					if(pageidx == tmpidx){
						break;
					}
                    tmpidx++;
				}
                /*
				if(this.formItems[insertidx].pageno > pageno){
					tmpidx++;
					if(pageidx == tmpidx) break;
				}
				*/
			}
            console.debug("chgItemIndex insertidx:" + insertidx);
			if(insertidx <= this.formItems.length){
				tmp = this.formItems[idx];
				tmp.pageno = pageno;
				// 一度削除
				this.formItems.splice(idx, 1);
				if(idx < insertidx && insertidx > 0){
					// 指定した並び順に移動する
					this.formItems.splice(insertidx - 1, 0, tmp);
				}else{
					this.formItems.splice(insertidx, 0, tmp);
				}

				this.setDispOdr();
				onUpdateClick(tmp.itemid);
			}
		}
	}

	/**
		* オブジェクトの内容表示
		* @return 文字列
		*/
	this.toString = function(){
		var str,i;

		str = "";
		for(i = 0;i < this.formItems.length;i++){
			str += "{formid:" + this.formItems[i].formid + ",pageno:" + this.formItems[i].pageno + ",itemid:" + this.formItems[i].itemid + ",dispodr" + this.formItems[i].dispodr + ",status:" + this.formItems[i].status + ",name:" + this.formItems[i].name + "} ";
		}

		return str;
	}

	/**
	* データ送信へ
	* Ajaxにより、並び順を送信する
	*/
	this.sendPost = function(){
	  var jsn, i;

	  jsn = {values : JSON.stringify(this.formItems)}
      console.debug(this.toString());

    //jsn = {formid:111};

		 $.ajax({
				type:'POST',
				url:'/conf/updatedispodr',
				data:jsn,
				dataType:'json',
				success:function (data) {
						// ステータスがOKの場合はページを削除する
						//if(data.status === "Ok"){
						//		$().toastmessage('showNoticeToast', data.message);
						//}
                        $().toastmessage('showNoticeToast', '並べ替え更新しました')
				}
		});
	}
}
