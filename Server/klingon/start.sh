#!/bin/bash

# start.sh
#
# playframework capistrano用起動シェル
# capistrano内部の処理で/target/startコマンドを実行してもハングアップしたり、
# コマンドは実行されるがうまくアプリが起動しないといった事象が発生する為、
# capistrano経由で実行する為のシェルとなります。
#
# ■参考URL
# https://groups.google.com/forum/?fromgroups#!topic/play-framework/BlV4c3q8hxE
# http://stackoverflow.com/questions/10582515/how-to-start-a-play2-application-on-a-remote-machine-using-capistrano
#
# 引数について
# $1 playframeworkアプリルートパス(ex. /van2k/klingon/current/Server/klingon)
# $2 playframework実行時に読み込む設定ファイル(ex. production)
# $3 実行時のポート番号(ex. 9001)
# $4 ログ、PIDファイル出力先パス(ex. /van2k/klingon/shared)
# $5 javaHeapSize(ex.512)
# $6 sudo実行ユーザー名

if [ $# -eq 6 ]; then
  cmd="$1/target/start -Dsbt.log.noformat=true -Dconfig.resource=$2.conf -Dhttp.port=$3 -Dpidfile.path=$4/pids/RUNNING_PID -Xms$5m -Xmx$5m -server >> $4/log/play_start.log 2>&1"
  echo $cmd
  sudo -u $6 bash -c $cmd
fi
