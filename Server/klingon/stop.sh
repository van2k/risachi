#!/bin/bash

# stop.sh
#
# playframework capistrano用終了シェル
# 既にplayアプリケーションが起動していた場合は、
# 起動しているプロセスを終了させます。
#
# 引数について
# $1 playframeworkアプリ RUNNING_PIDファイルパス(ex. /van2k/dev_klingon/shared/pids/RUNNING_PID)
# $2 sudo実行ユーザー名(ex. van2k)

if [ -e $1 ]; then
  sudo -u $2 kill -15 `cat $1`
fi
