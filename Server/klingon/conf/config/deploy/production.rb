# 環境固有設定
set :repository,  "git@bitbucket.org:van2k/klingon.git"
role :server, "van2k.cloudapp.net"
set :branch, fetch(:branch, "master")
set :application, "klingon"
set :app_root, "Server/klingon"
set :app_port, "9001"
set :deploy_to, "/van2k/#{application}"
set :java_heap_size, 512

set :user, "van2k"
set :password, "van2k"
set :use_sudo, false
set :ssh_port, 22
set :ssh_key, "~/.ssh/van2k_ppk"

# SSH認証用秘密鍵
ssh_options[:keys] = "#{ssh_key}"
ssh_options[:port] = "#{ssh_port}"
