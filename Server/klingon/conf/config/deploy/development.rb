# 環境固有設定
set :repository,  "git@bitbucket.org:van2k/klingon.git"
role :server, "dev.van2k.com"
set :branch, "develop"
set :application, "dev_klingon"
set :app_root, "Server/klingon"
set :app_port, "9000"
set :deploy_to, "/van2k/#{application}"
set :java_heap_size, 256

set :user, "van2k_deploy"
set :password, "van2k_deploy"
set :use_sudo, false
set :ssh_port, 64153
set :ssh_key, "~/.ssh/van2k_deploy_ppk"

# SSH認証用秘密鍵
ssh_options[:keys] = "#{ssh_key}"
ssh_options[:port] = "#{ssh_port}"
