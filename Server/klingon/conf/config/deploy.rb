require "capistrano/ext/multistage"

set :default_stage, "development"

set :scm, :git

ssh_options[:auth_methods] = %w(publickey)
set :default_shell, "bash -l"
default_run_options[:pty] = true

set :deploy_via, :copy

# deploy:cleanupタスクで<deploy_to>release以下に残す世代数の指定
set :keep_releases, 5
set :play, "/usr/local/play"

# publicフォルダ以下のimages、javascripts、stylesheetsフォルダに格納されてる
# リソースファイルに対しtouchコマンドを実行しないようにする
set :normalize_asset_timestamps, false

# finalize_updateを無効化(pulic以下のディレクトリ作成処理を削除)
namespace :deploy do
  task :finalize_update do;end
end

# ディレクトリ作成およびオーナー変更
namespace :setup do
  task :make_release_dir do
    run "mkdir -p /van2k/#{application}/releases"
  end
  task :change_app_dir_owner do
    run "#{sudo} chown van2k:van2k -R /van2k/#{application}"
  end
  task :change_release_owner do
    run "#{sudo} chown van2k:van2k -R #{current_path}"
    run "#{sudo} chown van2k:van2k -R #{release_path}"
  end
  task :create_symlink do
    run "ln -s #{shared_path}/log #{current_path}/#{app_root}/logs"
  end
end

# playframework起動・停止処理
namespace :play_command do
  task :compile_stage do
    run "cd #{current_path}/#{app_root} && #{sudo :as => 'van2k'} #{play} -Dsbt.log.noformat=true clean compile stage"
  end
  task :start do
    targets = find_servers_for_task(current_task)
    failed_targets = targets.map do |target|
      cmd = "nohup ssh #{user}@#{target.host} -p #{ssh_port} -i #{ssh_key} -t -t \"sudo chmod 774 #{current_path}/#{app_root}/start.sh && #{sudo :as => 'van2k'} #{current_path}/#{app_root}/start.sh #{current_path}/#{app_root} #{stage} #{app_port} #{shared_path} #{java_heap_size} van2k \" > /dev/null 2>&1 &"
      puts cmd
      target.host unless system cmd
    end.compact

    raise "starting play2 app failed on #{failed_targets.join(',')}" if failed_targets.any?
  end
  task :stop do
    targets = find_servers_for_task(current_task)
    failed_targets = targets.map do |target|
      # 既にアプリが起動していた場合のみ停止する
      cmd = "ssh #{user}@#{target.host} -p #{ssh_port} -i #{ssh_key} -t -t \"sudo chmod 774 #{current_path}/#{app_root}/stop.sh && #{sudo :as => 'van2k'} #{current_path}/#{app_root}/stop.sh #{shared_path}/pids/RUNNING_PID van2k \""
      puts cmd
      target.host unless system cmd
    end.compact

    raise "stopping play2 app failed on #{failed_targets.join(',')}" if failed_targets.any?
  end
end

after "deploy:create_symlink", "setup:create_symlink", "setup:change_release_owner"
after "deploy:setup", "setup:make_release_dir", "setup:change_app_dir_owner"
#after "deploy", "play_command:compile_stage", "play_command:stop", "play_command:start"
after "deploy", "play_command:compile_stage"

