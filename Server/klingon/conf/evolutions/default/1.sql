# --- First database schema

# --- !Ups

CREATE SEQUENCE forms_id_seq;
CREATE TABLE forms
(
	id bigint NOT NULL DEFAULT nextval('forms_id_seq'),
	contract_id bigint NOT NULL,
	form_name text NOT NULL,
	form_desc text NOT NULL,
	result_message text NOT NULL,
	total_page_count int NOT NULL,
  css_id bigint,
  org_css text,
  image_url varchar(256),
  status char(1) NOT NULL default (1),
	url_key varchar(200) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id),
  CONSTRAINT url_key_unique_key UNIQUE (url_key)

);

CREATE SEQUENCE form_categories_id_seq;
CREATE TABLE form_categories
(
	id bigint NOT NULL DEFAULT nextval('form_categories_id_seq'),
	form_id bigint NOT NULL,
	name varchar(200) NOT NULL,
	description text NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

CREATE SEQUENCE form_items_id_seq;
CREATE TABLE form_items
(
	id bigint NOT NULL DEFAULT nextval('form_items_id_seq'),
	form_id bigint NOT NULL,
	category_id bigint NOT NULL,
	m_form_item_id bigint NOT NULL,
	name varchar(200) NOT NULL,
	page_no int NOT NULL,
	required char(1) NOT NULL,
	select_number int DEFAULT 0,
	condition char(1),
	dispodr int NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE m_form_items
(
	id bigint NOT NULL,
	name varchar(200) NOT NULL,
	kind char(1) NOT NULL,
	data_type char(1) NOT NULL,
	graph_kind char(1) NOT NULL,
	regex varchar(100),
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO m_form_items(id, name, kind, data_type, graph_kind, regex, created_at, updated_at)
VALUES(1, 'テキストボックス', '1', '','4',  null, now(), now());
INSERT INTO m_form_items(id, name, kind, data_type, graph_kind, regex, created_at, updated_at)
VALUES(2, 'テキストエリア', '2', '','4', null,now(), now());
INSERT INTO m_form_items(id, name, kind, data_type, graph_kind, regex, created_at, updated_at)
VALUES(3, 'ラジオボタン', '3', '','2', null,now(), now());
INSERT INTO m_form_items(id, name, kind, data_type, graph_kind, regex, created_at, updated_at)
VALUES(4, 'チェックボックス', '4', '','2', null,now(), now());
INSERT INTO m_form_items(id, name, kind, data_type, graph_kind, regex, created_at, updated_at)
VALUES(5, 'ドロップダウンリスト', '5', '','2', null,now(), now());


CREATE SEQUENCE form_item_values_id_seq;
CREATE TABLE form_item_values
(
	id bigint NOT NULL DEFAULT nextval('form_item_values_id_seq'),
	form_item_id bigint NOT NULL,
	form_value varchar(200) NOT NULL,
	ch_length int NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

CREATE SEQUENCE answers_id_seq;
CREATE TABLE answers
(
  id bigint NOT NULL DEFAULT nextval('answers_id_seq'),
  form_id  bigint NOT NULL,
  start_at timestamp NOT NULL,
  end_at timestamp NULL,
  foreign_key varchar(100) NULL,
  attribute1 varchar(100) NULL,
  attribute2 varchar(100) NULL,
  attribute3 varchar(100) NULL,
  attribute4 varchar(100) NULL,
  attribute5 varchar(100) NULL,
  is_preview char(1) NOT NULL,
  ip_address varchar(100) NOT NULL,
  web_browser varchar(200) NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE answer_details_id_seq;
CREATE TABLE answer_details
(
  id bigint NOT NULL DEFAULT nextval('answer_details_id_seq'),
  answer_id bigint NOT NULL,
  form_item_id bigint NOT NULL,
  form_item_value_id bigint NOT NULL,
  answer_value text NOT NULL,
  answer_at timestamp,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE contracts_id_seq;
CREATE TABLE contracts
(
  id bigint NOT NULL DEFAULT nextval('contracts_id_seq'),
  mail text NOT NULL,
  status char(1) NOT NULL DEFAULT '1',
  regist_token varchar(64),
  token_limit timestamp NOT NULL DEFAULT '1900-01-01 00:00:00',
  name varchar(128),
  pass varchar(64),
  cancel_date timestamp,
  last_login_at timestamp,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT mail_unique_key UNIQUE (mail),
  CONSTRAINT regist_token_unique_key UNIQUE (regist_token)
);

CREATE SEQUENCE inquiry_id_seq;
CREATE TABLE inquiry
(
  id bigint NOT NULL DEFAULT nextval('inquiry_id_seq'),
  send_date timestamp NOT NULL,
  name varchar(128) NOT NULL,
  subject varchar(128) NOT NULL,
  mail varchar(256) NOT NULL,
  content text NOT NULL,
  status char(1) NOT NULL,
  contract_id bigint,
  PRIMARY KEY (id)
);

CREATE SEQUENCE m_form_item_values_id_seq;
CREATE TABLE m_form_item_values
(
	id bigint NOT NULL DEFAULT nextval('m_form_item_values_id_seq'),
	m_form_items_id bigint NOT NULL,
	item_value varchar(200) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '北海道', now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '青森県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '岩手県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '宮城県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '秋田県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '山形県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '福島県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '茨城県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '栃木県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '群馬県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '埼玉県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '千葉県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '東京都',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '神奈川県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '新潟県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '富山県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '石川県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '福井県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '山梨県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '長野県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '岐阜県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '静岡県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '愛知県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '三重県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '滋賀県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '京都府',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '大阪府',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '兵庫県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '奈良県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '和歌山県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '鳥取県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '島根県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '岡山県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '広島県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '山口県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '徳島県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '香川県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '愛媛県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '高知県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '福岡県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '佐賀県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '長崎県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '熊本県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '大分県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '宮崎県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '鹿児島県',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(8, '沖縄県',now(),now());

/* 性別（ラジオボックス）*/
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(9, '男',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(9, '女',now(),now());

/* 年代（プルダウン）*/
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(10, '10代',now(),now());
INSERT INTO m_form_item_values(m_form_items_id,item_value,created_at,updated_at)VALUES(10, '20代',now(),now());


CREATE SEQUENCE form_configs_id_seq;
CREATE TABLE form_configs
(
	id bigint NOT NULL DEFAULT nextval('form_configs_id_seq'),
	form_id bigint NOT NULL,
	collect_number int NOT NULL,
	start_st date NOT NULL,
	end_st date,
	stop char(1) NOT NULL,
	output_number int NOT NULL,
	term int NOT NULL,
	output_limit_number int NOT NULL,
	save_term int,
	price int,
	payment_confirm int,
	payment_confirm_date timestamp,
	charge_plan_id bigint,
	enc_result_open char(1) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	PRIMARY KEY (id)
);

CREATE SEQUENCE form_charge_options_id_seq;
CREATE TABLE form_charge_options(
  id bigint NOT NULL DEFAULT nextval('form_charge_options_id_seq'),
  form_id bigint NOT NULL,
  charge_option_plan_id bigint NOT NULL
);

CREATE TABLE charge_option_plans
(
  id bigint NOT NULL,
  charge int NOT NULL,
  option_type char(1) NOT NULL,
  option_value int NOT NULL,
  tax int NOT NULL,
  description text NOT NULL,
  apply_start_date timestamp NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(1, '1', 100, 1050, 50, '収集件数100件追加', '2012-01-01', '2012-01-01', '2012-01-01');
INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(2, '2', 1, 2100, 100, '1ヶ月期間追加', '2012-01-01', '2012-01-01', '2012-01-01');
INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(3, '3', 1, 3150, 150, '1ページ追加', '2012-01-01', '2012-01-01', '2012-01-01');
INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(4, '4', 1, 4200, 200, '設問1項目追加', '2012-01-01', '2012-01-01', '2012-01-01');
INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(5, '5', 1, 5250, 250, '保存期間1ヶ月追加', '2012-01-01', '2012-01-01', '2012-01-01');
INSERT INTO charge_option_plans(
	id, option_type, option_value, charge, tax, description, apply_start_date, created_at, updated_at)
VALUES(6, '6', 1, 6300, 300, 'CSV出力回数1回追加', '2012-01-01', '2012-01-01', '2012-01-01');


CREATE TABLE charge_plans(
  id bigint NOT NULL,
  name varchar(100) NOT NULL,
  charge int NOT NULL,
  tax int NOT NULL,
  description text NOT NULL,
  collect_number int,
  term int,
  output_number int NOT NULL,
  save_term int NOT NULL,
  apply_start_date timestamp NOT NULL,
  csv_output_num bigint NOT NULL default (0),
  answers_disp_type bigint NOT NULL default (0),
  is_start_set char(1)  NOT NULL default (0),
  is_logo_set char(1) NOT NULL default (0),
  is_template char(1) NOT NULL default (0),
  is_css char(1) NOT NULL default (0),
  is_inner_html char(1) NOT NULL default (0),
  is_foreign_key char(1) NOT NULL default (0),
  page_num int NOT NULL,
  item_num int NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO charge_plans(
 id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term,
 apply_start_date,csv_output_num, answers_disp_type,is_start_set, is_logo_set, is_template,
 is_css, is_inner_html, is_foreign_key, created_at, updated_at)
VALUES( 1,'無料',0,0,'無料版',10,1,10,30,0,30,
 '2000-01-01',0,0,0,0,0,
 0,0,0,'2012-01-01','2012-01-01'
);

INSERT INTO charge_plans(
 id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term,
 apply_start_date,csv_output_num, answers_disp_type,is_start_set, is_logo_set, is_template,
 is_css, is_inner_html, is_foreign_key, created_at, updated_at)
VALUES(2,'梅',3150,150,'梅',50,2,20,60,10,60,
 '2000-01-01',1,1,1,0,0,
 0,0,0,'2012-01-01','2012-01-01'
);

INSERT INTO charge_plans(
 id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term,
 apply_start_date,csv_output_num, answers_disp_type,is_start_set, is_logo_set, is_template,
 is_css, is_inner_html, is_foreign_key, created_at, updated_at)
VALUES(3,'竹',5250,250,'竹',100,3,30,90,10,100,
 '2000-01-01',1,1,1,1,1,
 0,0,0,'2012-01-01','2012-01-01'
);

INSERT INTO charge_plans(
 id, name, charge, tax, description, collect_number, page_num, item_num, term, output_number, save_term,
 apply_start_date,csv_output_num, answers_disp_type,is_start_set, is_logo_set, is_template,
 is_css, is_inner_html, is_foreign_key, created_at, updated_at)
VALUES(4,'松',10500,500,'松',200,10,100,120,100,100,
 '2000-01-01',1,1,1,1,1,
 1,1,1,'2012-01-01','2012-01-01'
);


CREATE TABLE disp_schedule(
  id bigint NOT NULL,
  form_id bigint NOT NULL,
  kind char(1) NOT NULL,
  setting_value text NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE administrator_id_seq;
CREATE TABLE administrator
(
	id bigint NOT NULL DEFAULT nextval('administrator_id_seq'),
	userid varchar(64) NOT NULL UNIQUE,
	password varchar(64) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO administrator (userid, password) VALUES ('administrator', 'ddb179cc61eae19d242208044fd458d3');

CREATE SEQUENCE information_id_seq;
CREATE TABLE information
(
  id bigint NOT NULL DEFAULT nextval('information_id_seq'),
  send_date timestamp NOT NULL,
  subject varchar(128) NOT NULL,
  content text NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE m_css
(
  id bigint NOT NULL,
  name varchar(64) NOT NULL,
  css_code text NOT NULL,
  disp_image_url varchar(256) NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  CONSTRAINT m_css_pkey PRIMARY KEY (id )
);

CREATE SEQUENCE form_config_values_id_seq;
CREATE TABLE form_config_values
(
  id bigint NOT NULL DEFAULT nextval('form_config_values_id_seq'),
  form_id bigint NOT NULL,
  option_id bigint NOT NULL,
  option_number bigint NOT NULL,
  charge bigint NOT NULL,
  price bigint NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  CONSTRAINT form_config_values_key PRIMARY KEY (id )
);


# --- !Downs

DROP TABLE if EXISTS forms;
DROP SEQUENCE forms_id_seq;

DROP TABLE if EXISTS form_categories;
DROP SEQUENCE form_categories_id_seq;

DROP TABLE if EXISTS form_items;
DROP SEQUENCE form_items_id_seq;

DROP TABLE if EXISTS m_form_items;

DROP TABLE if EXISTS form_item_values;
DROP SEQUENCE form_item_values_id_seq;

DROP TABLE if EXISTS answers;
DROP SEQUENCE answers_id_seq;

DROP TABLE if EXISTS answer_details;
DROP SEQUENCE answer_details_id_seq;

DROP TABLE if EXISTS contracts;
DROP SEQUENCE contracts_id_seq;

DROP TABLE if EXISTS inquiry;
DROP SEQUENCE inquiry_id_seq;

DROP TABLE if EXISTS m_form_item_values;
DROP SEQUENCE m_form_item_values_id_seq;

DROP TABLE if EXISTS form_configs;
DROP SEQUENCE form_configs_id_seq;

DROP TABLE if EXISTS form_charge_options;
DROP SEQUENCE form_charge_options_id_seq;

DROP TABLE if EXISTS charge_plans;

DROP TABLE if EXISTS charge_option_plans;

DROP TABLE if EXISTS disp_schedule;

DROP TABLE if EXISTS administrator;
DROP SEQUENCE administrator_id_seq;

DROP TABLE if EXISTS information;
DROP SEQUENCE information_id_seq;

DROP TABLE IF EXISTS m_css;

DROP TABLE IF EXISTS form_config_values;
DROP SEQUENCE form_config_values_id_seq;
