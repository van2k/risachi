import sbt._
import Keys._
import play.Project._
import de.johoop.jacoco4sbt.JacocoPlugin._
import de.johoop.jacoco4sbt.XMLReport
import de.johoop.jacoco4sbt.HTMLReport
import de.johoop.findbugs4sbt.FindBugs._

object ApplicationBuild extends Build {

	val appName         = "klingon"
	val appVersion      = "1.0-SNAPSHOT"

	lazy val s = playScalaSettings ++ Seq(jacoco.settings:_*) ++ Seq(findbugsSettings : _*)

	val appDependencies = Seq(
		"com.github.seratch" %% "scalikejdbc"             % "[1.6,)",
		"com.github.seratch" %% "scalikejdbc-play-plugin" % "[1.6,)",
		"com.github.seratch" %% "scalikejdbc-config" % "[1.6,)",
		"com.github.seratch" %% "scalikejdbc-test" % "[1.6,)"  % "test",
		"com.github.seratch" %% "scalikejdbc-interpolation" % "[1.6,)",
		"postgresql" % "postgresql" % "9.1-901.jdbc4",
		jdbc,
		"org.slf4j" % "slf4j-simple" % "[1.7,)"         // slf4j implementation
	)

	val main = play.Project(appName, appVersion, appDependencies, settings = s).settings(
		// Add your own project settings here
		javaOptions ++= sys.process.javaVmArguments.filter(
			a => Seq("-Xmx","-Xms","-XX").exists(a.startsWith)
		),
		//      parallelExecution in jacoco.Config := false,
		jacoco.reportFormats in jacoco.Config := Seq(XMLReport("utf-8"), HTMLReport("utf-8")),
		jacoco.outputDirectory in jacoco.Config := new File("target/coverage/")

	).settings(
		testOptions in Test += Tests.Argument("exclude integration"),
		testOptions in Test += Tests.Argument(TestFrameworks.Specs2, "console", "junitxml")
	)
}
