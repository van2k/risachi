package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncDispSchedule(
  id: Long, 
  formId: String, 
  kind: String, 
  settingValue: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncDispSchedule.autoSession): EncDispSchedule = EncDispSchedule.save(this)(session)

  def destroy()(implicit session: DBSession = EncDispSchedule.autoSession): Unit = EncDispSchedule.destroy(this)(session)

}
      

object EncDispSchedule {

  val tableName = "disp_schedule"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val kind = "kind"
    val settingValue = "setting_value"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, kind, settingValue, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncDispSchedule(
      id = rs.long(id),
      formId = rs.string(formId),
      kind = rs.string(kind),
      settingValue = rs.string(settingValue),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val kind = as(columnNames.kind)
    val settingValue = as(columnNames.settingValue)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, kind, settingValue, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncDispSchedule(
      id = rs.long(id),
      formId = rs.string(formId),
      kind = rs.string(kind),
      settingValue = rs.string(settingValue),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncDispSchedule] = {
    SQL("""select * from disp_schedule where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncDispSchedule] = {
    SQL("""select * from disp_schedule""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from disp_schedule""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncDispSchedule] = {
    SQL("""select * from disp_schedule where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from disp_schedule where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: Long,
    formId: String,
    kind: String,
    settingValue: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncDispSchedule = {
    SQL("""
      insert into disp_schedule (
        id,
        form_id,
        kind,
        setting_value,
        created_at,
        updated_at
      ) values (
        /*'id*/1,
        /*'formId*/'abc',
        /*'kind*/'abc',
        /*'settingValue*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'id -> id,
        'formId -> formId,
        'kind -> kind,
        'settingValue -> settingValue,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).update.apply()

    EncDispSchedule(
      id = id,
      formId = formId,
      kind = kind,
      settingValue = settingValue,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncDispSchedule)(implicit session: DBSession = autoSession): EncDispSchedule = {
    SQL("""
      update
        disp_schedule
      set
        id = /*'id*/1,
        form_id = /*'formId*/'abc',
        kind = /*'kind*/'abc',
        setting_value = /*'settingValue*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'kind -> m.kind,
        'settingValue -> m.settingValue,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncDispSchedule)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from disp_schedule where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
