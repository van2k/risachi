package models

import scalikejdbc._

case class EncFormChargeOptions(
  id: Long, 
  formId: String, 
  chargeOptionPlanId: Long) {

  def save()(implicit session: DBSession = EncFormChargeOptions.autoSession): EncFormChargeOptions = EncFormChargeOptions.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormChargeOptions.autoSession): Unit = EncFormChargeOptions.destroy(this)(session)

}
      

object EncFormChargeOptions {

  val tableName = "form_charge_options"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val chargeOptionPlanId = "charge_option_plan_id"
    val all = Seq(id, formId, chargeOptionPlanId)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormChargeOptions(
      id = rs.long(id),
      formId = rs.string(formId),
      chargeOptionPlanId = rs.long(chargeOptionPlanId))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val chargeOptionPlanId = as(columnNames.chargeOptionPlanId)
    val all = Seq(id, formId, chargeOptionPlanId)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormChargeOptions(
      id = rs.long(id),
      formId = rs.string(formId),
      chargeOptionPlanId = rs.long(chargeOptionPlanId))
  }
      
  val autoSession = AutoSession

  def find(id: Long, formId: String, chargeOptionPlanId: Long)(implicit session: DBSession = autoSession): Option[EncFormChargeOptions] = {
    SQL("""select * from form_charge_options where id = /*'id*/1 and form_id = /*'formId*/'abc' and charge_option_plan_id = /*'chargeOptionPlanId*/1""")
      .bindByName('id -> id, 'formId -> formId, 'chargeOptionPlanId -> chargeOptionPlanId).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormChargeOptions] = {
    SQL("""select * from form_charge_options""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_charge_options""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormChargeOptions] = {
    SQL("""select * from form_charge_options where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_charge_options where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    formId: String,
    chargeOptionPlanId: Long)(implicit session: DBSession = autoSession): EncFormChargeOptions = {
    val generatedKey = SQL("""
      insert into form_charge_options (
        form_id,
        charge_option_plan_id
      ) values (
        /*'formId*/'abc',
        /*'chargeOptionPlanId*/1
      )
      """)
      .bindByName(
        'formId -> formId,
        'chargeOptionPlanId -> chargeOptionPlanId
      ).updateAndReturnGeneratedKey.apply()

    EncFormChargeOptions(
      id = generatedKey, 
      formId = formId,
      chargeOptionPlanId = chargeOptionPlanId)
  }

  def save(m: EncFormChargeOptions)(implicit session: DBSession = autoSession): EncFormChargeOptions = {
    SQL("""
      update
        form_charge_options
      set
        id = /*'id*/1,
        form_id = /*'formId*/'abc',
        charge_option_plan_id = /*'chargeOptionPlanId*/1
      where
        id = /*'id*/1 and form_id = /*'formId*/'abc' and charge_option_plan_id = /*'chargeOptionPlanId*/1
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'chargeOptionPlanId -> m.chargeOptionPlanId
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormChargeOptions)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_charge_options where id = /*'id*/1 and form_id = /*'formId*/'abc' and charge_option_plan_id = /*'chargeOptionPlanId*/1""")
      .bindByName('id -> m.id, 'formId -> m.formId, 'chargeOptionPlanId -> m.chargeOptionPlanId).update.apply()
  }
          
}
