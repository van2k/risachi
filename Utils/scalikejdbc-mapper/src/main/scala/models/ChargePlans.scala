package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class ChargePlans(
  id: Long, 
  charge: Int, 
  tax: Int, 
  description: String, 
  collectNumber: Option[Int] = None, 
  term: Option[Int] = None, 
  outputNumber: Int, 
  saveTerm: Int, 
  applyStartDate: DateTime, 
  createdAt: DateTime, 
  updatedAt: DateTime, 
  csvOutputNum: Long, 
  answersDispType: Long, 
  isStartSet: String, 
  isLogoSet: String, 
  isTemplate: String, 
  isCss: String, 
  isInnerHtml: String, 
  isForeignKey: String, 
  name: String, 
  pageNum: Int, 
  itemNum: Int) {

  def save()(implicit session: DBSession = ChargePlans.autoSession): ChargePlans = ChargePlans.save(this)(session)

  def destroy()(implicit session: DBSession = ChargePlans.autoSession): Unit = ChargePlans.destroy(this)(session)

}
      

object ChargePlans {

  val tableName = "charge_plans"

  object columnNames {
    val id = "id"
    val charge = "charge"
    val tax = "tax"
    val description = "description"
    val collectNumber = "collect_number"
    val term = "term"
    val outputNumber = "output_number"
    val saveTerm = "save_term"
    val applyStartDate = "apply_start_date"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val csvOutputNum = "csv_output_num"
    val answersDispType = "answers_disp_type"
    val isStartSet = "is_start_set"
    val isLogoSet = "is_logo_set"
    val isTemplate = "is_template"
    val isCss = "is_css"
    val isInnerHtml = "is_inner_html"
    val isForeignKey = "is_foreign_key"
    val name = "name"
    val pageNum = "page_num"
    val itemNum = "item_num"
    val all = Seq(id, charge, tax, description, collectNumber, term, outputNumber, saveTerm, applyStartDate, createdAt, updatedAt, csvOutputNum, answersDispType, isStartSet, isLogoSet, isTemplate, isCss, isInnerHtml, isForeignKey, name, pageNum, itemNum)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => ChargePlans(
      id = rs.long(id),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      collectNumber = rs.intOpt(collectNumber),
      term = rs.intOpt(term),
      outputNumber = rs.int(outputNumber),
      saveTerm = rs.int(saveTerm),
      applyStartDate = rs.timestamp(applyStartDate).toDateTime,
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      csvOutputNum = rs.long(csvOutputNum),
      answersDispType = rs.long(answersDispType),
      isStartSet = rs.string(isStartSet),
      isLogoSet = rs.string(isLogoSet),
      isTemplate = rs.string(isTemplate),
      isCss = rs.string(isCss),
      isInnerHtml = rs.string(isInnerHtml),
      isForeignKey = rs.string(isForeignKey),
      name = rs.string(name),
      pageNum = rs.int(pageNum),
      itemNum = rs.int(itemNum))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val charge = as(columnNames.charge)
    val tax = as(columnNames.tax)
    val description = as(columnNames.description)
    val collectNumber = as(columnNames.collectNumber)
    val term = as(columnNames.term)
    val outputNumber = as(columnNames.outputNumber)
    val saveTerm = as(columnNames.saveTerm)
    val applyStartDate = as(columnNames.applyStartDate)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val csvOutputNum = as(columnNames.csvOutputNum)
    val answersDispType = as(columnNames.answersDispType)
    val isStartSet = as(columnNames.isStartSet)
    val isLogoSet = as(columnNames.isLogoSet)
    val isTemplate = as(columnNames.isTemplate)
    val isCss = as(columnNames.isCss)
    val isInnerHtml = as(columnNames.isInnerHtml)
    val isForeignKey = as(columnNames.isForeignKey)
    val name = as(columnNames.name)
    val pageNum = as(columnNames.pageNum)
    val itemNum = as(columnNames.itemNum)
    val all = Seq(id, charge, tax, description, collectNumber, term, outputNumber, saveTerm, applyStartDate, createdAt, updatedAt, csvOutputNum, answersDispType, isStartSet, isLogoSet, isTemplate, isCss, isInnerHtml, isForeignKey, name, pageNum, itemNum)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => ChargePlans(
      id = rs.long(id),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      collectNumber = rs.intOpt(collectNumber),
      term = rs.intOpt(term),
      outputNumber = rs.int(outputNumber),
      saveTerm = rs.int(saveTerm),
      applyStartDate = rs.timestamp(applyStartDate).toDateTime,
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      csvOutputNum = rs.long(csvOutputNum),
      answersDispType = rs.long(answersDispType),
      isStartSet = rs.string(isStartSet),
      isLogoSet = rs.string(isLogoSet),
      isTemplate = rs.string(isTemplate),
      isCss = rs.string(isCss),
      isInnerHtml = rs.string(isInnerHtml),
      isForeignKey = rs.string(isForeignKey),
      name = rs.string(name),
      pageNum = rs.int(pageNum),
      itemNum = rs.int(itemNum))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[ChargePlans] = {
    SQL("""select * from charge_plans where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[ChargePlans] = {
    SQL("""select * from charge_plans""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from charge_plans""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[ChargePlans] = {
    SQL("""select * from charge_plans where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from charge_plans where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: Long,
    charge: Int,
    tax: Int,
    description: String,
    collectNumber: Option[Int] = None,
    term: Option[Int] = None,
    outputNumber: Int,
    saveTerm: Int,
    applyStartDate: DateTime,
    createdAt: DateTime,
    updatedAt: DateTime,
    csvOutputNum: Long,
    answersDispType: Long,
    isStartSet: String,
    isLogoSet: String,
    isTemplate: String,
    isCss: String,
    isInnerHtml: String,
    isForeignKey: String,
    name: String,
    pageNum: Int,
    itemNum: Int)(implicit session: DBSession = autoSession): ChargePlans = {
    SQL("""
      insert into charge_plans (
        id,
        charge,
        tax,
        description,
        collect_number,
        term,
        output_number,
        save_term,
        apply_start_date,
        created_at,
        updated_at,
        csv_output_num,
        answers_disp_type,
        is_start_set,
        is_logo_set,
        is_template,
        is_css,
        is_inner_html,
        is_foreign_key,
        name,
        page_num,
        item_num
      ) values (
        /*'id*/1,
        /*'charge*/1,
        /*'tax*/1,
        /*'description*/'abc',
        /*'collectNumber*/1,
        /*'term*/1,
        /*'outputNumber*/1,
        /*'saveTerm*/1,
        /*'applyStartDate*/'1958-09-06 12:00:00',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'csvOutputNum*/1,
        /*'answersDispType*/1,
        /*'isStartSet*/'abc',
        /*'isLogoSet*/'abc',
        /*'isTemplate*/'abc',
        /*'isCss*/'abc',
        /*'isInnerHtml*/'abc',
        /*'isForeignKey*/'abc',
        /*'name*/'abc',
        /*'pageNum*/1,
        /*'itemNum*/1
      )
      """)
      .bindByName(
        'id -> id,
        'charge -> charge,
        'tax -> tax,
        'description -> description,
        'collectNumber -> collectNumber,
        'term -> term,
        'outputNumber -> outputNumber,
        'saveTerm -> saveTerm,
        'applyStartDate -> applyStartDate,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt,
        'csvOutputNum -> csvOutputNum,
        'answersDispType -> answersDispType,
        'isStartSet -> isStartSet,
        'isLogoSet -> isLogoSet,
        'isTemplate -> isTemplate,
        'isCss -> isCss,
        'isInnerHtml -> isInnerHtml,
        'isForeignKey -> isForeignKey,
        'name -> name,
        'pageNum -> pageNum,
        'itemNum -> itemNum
      ).update.apply()

    ChargePlans(
      id = id,
      charge = charge,
      tax = tax,
      description = description,
      collectNumber = collectNumber,
      term = term,
      outputNumber = outputNumber,
      saveTerm = saveTerm,
      applyStartDate = applyStartDate,
      createdAt = createdAt,
      updatedAt = updatedAt,
      csvOutputNum = csvOutputNum,
      answersDispType = answersDispType,
      isStartSet = isStartSet,
      isLogoSet = isLogoSet,
      isTemplate = isTemplate,
      isCss = isCss,
      isInnerHtml = isInnerHtml,
      isForeignKey = isForeignKey,
      name = name,
      pageNum = pageNum,
      itemNum = itemNum)
  }

  def save(m: ChargePlans)(implicit session: DBSession = autoSession): ChargePlans = {
    SQL("""
      update
        charge_plans
      set
        id = /*'id*/1,
        charge = /*'charge*/1,
        tax = /*'tax*/1,
        description = /*'description*/'abc',
        collect_number = /*'collectNumber*/1,
        term = /*'term*/1,
        output_number = /*'outputNumber*/1,
        save_term = /*'saveTerm*/1,
        apply_start_date = /*'applyStartDate*/'1958-09-06 12:00:00',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        csv_output_num = /*'csvOutputNum*/1,
        answers_disp_type = /*'answersDispType*/1,
        is_start_set = /*'isStartSet*/'abc',
        is_logo_set = /*'isLogoSet*/'abc',
        is_template = /*'isTemplate*/'abc',
        is_css = /*'isCss*/'abc',
        is_inner_html = /*'isInnerHtml*/'abc',
        is_foreign_key = /*'isForeignKey*/'abc',
        name = /*'name*/'abc',
        page_num = /*'pageNum*/1,
        item_num = /*'itemNum*/1
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'charge -> m.charge,
        'tax -> m.tax,
        'description -> m.description,
        'collectNumber -> m.collectNumber,
        'term -> m.term,
        'outputNumber -> m.outputNumber,
        'saveTerm -> m.saveTerm,
        'applyStartDate -> m.applyStartDate,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt,
        'csvOutputNum -> m.csvOutputNum,
        'answersDispType -> m.answersDispType,
        'isStartSet -> m.isStartSet,
        'isLogoSet -> m.isLogoSet,
        'isTemplate -> m.isTemplate,
        'isCss -> m.isCss,
        'isInnerHtml -> m.isInnerHtml,
        'isForeignKey -> m.isForeignKey,
        'name -> m.name,
        'pageNum -> m.pageNum,
        'itemNum -> m.itemNum
      ).update.apply()
    m
  }
      
  def destroy(m: ChargePlans)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from charge_plans where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
