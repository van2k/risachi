package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class (EncForms)(
  id: String, 
  contractId: Long, 
  formName: String, 
  formDesc: String, 
  resultMessage: String, 
  totalPageCount: Int, 
  createdAt: DateTime, 
  updatedAt: DateTime, 
  cssId: Option[Long] = None, 
  orgCss: Option[String] = None, 
  imageUrl: Option[String] = None, 
  status: String) {

  def save()(implicit session: DBSession = (EncForms).autoSession): (EncForms) = (EncForms).save(this)(session)

  def destroy()(implicit session: DBSession = (EncForms).autoSession): Unit = (EncForms).destroy(this)(session)

}
      

object (EncForms) {

  val tableName = "forms"

  object columnNames {
    val id = "id"
    val contractId = "contract_id"
    val formName = "form_name"
    val formDesc = "form_desc"
    val resultMessage = "result_message"
    val totalPageCount = "total_page_count"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val cssId = "css_id"
    val orgCss = "org_css"
    val imageUrl = "image_url"
    val status = "status"
    val all = Seq(id, contractId, formName, formDesc, resultMessage, totalPageCount, createdAt, updatedAt, cssId, orgCss, imageUrl, status)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => (EncForms)(
      id = rs.string(id),
      contractId = rs.long(contractId),
      formName = rs.string(formName),
      formDesc = rs.string(formDesc),
      resultMessage = rs.string(resultMessage),
      totalPageCount = rs.int(totalPageCount),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      cssId = rs.longOpt(cssId),
      orgCss = rs.stringOpt(orgCss),
      imageUrl = rs.stringOpt(imageUrl),
      status = rs.string(status))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val contractId = as(columnNames.contractId)
    val formName = as(columnNames.formName)
    val formDesc = as(columnNames.formDesc)
    val resultMessage = as(columnNames.resultMessage)
    val totalPageCount = as(columnNames.totalPageCount)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val cssId = as(columnNames.cssId)
    val orgCss = as(columnNames.orgCss)
    val imageUrl = as(columnNames.imageUrl)
    val status = as(columnNames.status)
    val all = Seq(id, contractId, formName, formDesc, resultMessage, totalPageCount, createdAt, updatedAt, cssId, orgCss, imageUrl, status)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => (EncForms)(
      id = rs.string(id),
      contractId = rs.long(contractId),
      formName = rs.string(formName),
      formDesc = rs.string(formDesc),
      resultMessage = rs.string(resultMessage),
      totalPageCount = rs.int(totalPageCount),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      cssId = rs.longOpt(cssId),
      orgCss = rs.stringOpt(orgCss),
      imageUrl = rs.stringOpt(imageUrl),
      status = rs.string(status))
  }
      
  val autoSession = AutoSession

  def find(id: String)(implicit session: DBSession = autoSession): Option[(EncForms)] = {
    SQL("""select * from forms where id = /*'id*/'abc'""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[(EncForms)] = {
    SQL("""select * from forms""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from forms""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[(EncForms)] = {
    SQL("""select * from forms where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from forms where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: String,
    contractId: Long,
    formName: String,
    formDesc: String,
    resultMessage: String,
    totalPageCount: Int,
    createdAt: DateTime,
    updatedAt: DateTime,
    cssId: Option[Long] = None,
    orgCss: Option[String] = None,
    imageUrl: Option[String] = None,
    status: String)(implicit session: DBSession = autoSession): (EncForms) = {
    SQL("""
      insert into forms (
        id,
        contract_id,
        form_name,
        form_desc,
        result_message,
        total_page_count,
        created_at,
        updated_at,
        css_id,
        org_css,
        image_url,
        status
      ) values (
        /*'id*/'abc',
        /*'contractId*/1,
        /*'formName*/'abc',
        /*'formDesc*/'abc',
        /*'resultMessage*/'abc',
        /*'totalPageCount*/1,
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'cssId*/1,
        /*'orgCss*/'abc',
        /*'imageUrl*/'abc',
        /*'status*/'abc'
      )
      """)
      .bindByName(
        'id -> id,
        'contractId -> contractId,
        'formName -> formName,
        'formDesc -> formDesc,
        'resultMessage -> resultMessage,
        'totalPageCount -> totalPageCount,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt,
        'cssId -> cssId,
        'orgCss -> orgCss,
        'imageUrl -> imageUrl,
        'status -> status
      ).update.apply()

    (EncForms)(
      id = id,
      contractId = contractId,
      formName = formName,
      formDesc = formDesc,
      resultMessage = resultMessage,
      totalPageCount = totalPageCount,
      createdAt = createdAt,
      updatedAt = updatedAt,
      cssId = cssId,
      orgCss = orgCss,
      imageUrl = imageUrl,
      status = status)
  }

  def save(m: (EncForms))(implicit session: DBSession = autoSession): (EncForms) = {
    SQL("""
      update
        forms
      set
        id = /*'id*/'abc',
        contract_id = /*'contractId*/1,
        form_name = /*'formName*/'abc',
        form_desc = /*'formDesc*/'abc',
        result_message = /*'resultMessage*/'abc',
        total_page_count = /*'totalPageCount*/1,
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        css_id = /*'cssId*/1,
        org_css = /*'orgCss*/'abc',
        image_url = /*'imageUrl*/'abc',
        status = /*'status*/'abc'
      where
        id = /*'id*/'abc'
      """)
      .bindByName(
        'id -> m.id,
        'contractId -> m.contractId,
        'formName -> m.formName,
        'formDesc -> m.formDesc,
        'resultMessage -> m.resultMessage,
        'totalPageCount -> m.totalPageCount,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt,
        'cssId -> m.cssId,
        'orgCss -> m.orgCss,
        'imageUrl -> m.imageUrl,
        'status -> m.status
      ).update.apply()
    m
  }
      
  def destroy(m: (EncForms))(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from forms where id = /*'id*/'abc'""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
