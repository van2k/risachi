package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncAnswerDetails(
  id: Long, 
  answerId: String, 
  formItemId: String, 
  formItemValueId: Long, 
  answerValue: String, 
  answerAt: Option[DateTime] = None, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncAnswerDetails.autoSession): EncAnswerDetails = EncAnswerDetails.save(this)(session)

  def destroy()(implicit session: DBSession = EncAnswerDetails.autoSession): Unit = EncAnswerDetails.destroy(this)(session)

}
      

object EncAnswerDetails {

  val tableName = "answer_details"

  object columnNames {
    val id = "id"
    val answerId = "answer_id"
    val formItemId = "form_item_id"
    val formItemValueId = "form_item_value_id"
    val answerValue = "answer_value"
    val answerAt = "answer_at"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, answerId, formItemId, formItemValueId, answerValue, answerAt, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncAnswerDetails(
      id = rs.long(id),
      answerId = rs.string(answerId),
      formItemId = rs.string(formItemId),
      formItemValueId = rs.long(formItemValueId),
      answerValue = rs.string(answerValue),
      answerAt = rs.timestampOpt(answerAt).map(_.toDateTime),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val answerId = as(columnNames.answerId)
    val formItemId = as(columnNames.formItemId)
    val formItemValueId = as(columnNames.formItemValueId)
    val answerValue = as(columnNames.answerValue)
    val answerAt = as(columnNames.answerAt)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, answerId, formItemId, formItemValueId, answerValue, answerAt, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncAnswerDetails(
      id = rs.long(id),
      answerId = rs.string(answerId),
      formItemId = rs.string(formItemId),
      formItemValueId = rs.long(formItemValueId),
      answerValue = rs.string(answerValue),
      answerAt = rs.timestampOpt(answerAt).map(_.toDateTime),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncAnswerDetails] = {
    SQL("""select * from answer_details where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
    SQL("""select * from answer_details""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from answer_details""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncAnswerDetails] = {
    SQL("""select * from answer_details where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from answer_details where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    answerId: String,
    formItemId: String,
    formItemValueId: Long,
    answerValue: String,
    answerAt: Option[DateTime] = None,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncAnswerDetails = {
    val generatedKey = SQL("""
      insert into answer_details (
        answer_id,
        form_item_id,
        form_item_value_id,
        answer_value,
        answer_at,
        created_at,
        updated_at
      ) values (
        /*'answerId*/'abc',
        /*'formItemId*/'abc',
        /*'formItemValueId*/1,
        /*'answerValue*/'abc',
        /*'answerAt*/'1958-09-06 12:00:00',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'answerId -> answerId,
        'formItemId -> formItemId,
        'formItemValueId -> formItemValueId,
        'answerValue -> answerValue,
        'answerAt -> answerAt,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncAnswerDetails(
      id = generatedKey, 
      answerId = answerId,
      formItemId = formItemId,
      formItemValueId = formItemValueId,
      answerValue = answerValue,
      answerAt = answerAt,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncAnswerDetails)(implicit session: DBSession = autoSession): EncAnswerDetails = {
    SQL("""
      update
        answer_details
      set
        id = /*'id*/1,
        answer_id = /*'answerId*/'abc',
        form_item_id = /*'formItemId*/'abc',
        form_item_value_id = /*'formItemValueId*/1,
        answer_value = /*'answerValue*/'abc',
        answer_at = /*'answerAt*/'1958-09-06 12:00:00',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'answerId -> m.answerId,
        'formItemId -> m.formItemId,
        'formItemValueId -> m.formItemValueId,
        'answerValue -> m.answerValue,
        'answerAt -> m.answerAt,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncAnswerDetails)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from answer_details where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
