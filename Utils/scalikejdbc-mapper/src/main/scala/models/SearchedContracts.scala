package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class SearchedContracts(
  id: Long, 
  mail: String, 
  createdAt: DateTime, 
  updatedAt: DateTime, 
  status: String, 
  registToken: Option[String] = None, 
  tokenLimit: DateTime, 
  name: Option[String] = None, 
  pass: Option[String] = None, 
  cancelDate: Option[DateTime] = None) {

  def save()(implicit session: DBSession = SearchedContracts.autoSession): SearchedContracts = SearchedContracts.save(this)(session)

  def destroy()(implicit session: DBSession = SearchedContracts.autoSession): Unit = SearchedContracts.destroy(this)(session)

}
      

object SearchedContracts {

  val tableName = "contracts"

  object columnNames {
    val id = "id"
    val mail = "mail"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val status = "status"
    val registToken = "regist_token"
    val tokenLimit = "token_limit"
    val name = "name"
    val pass = "pass"
    val cancelDate = "cancel_date"
    val all = Seq(id, mail, createdAt, updatedAt, status, registToken, tokenLimit, name, pass, cancelDate)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => SearchedContracts(
      id = rs.long(id),
      mail = rs.string(mail),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      status = rs.string(status),
      registToken = rs.stringOpt(registToken),
      tokenLimit = rs.timestamp(tokenLimit).toDateTime,
      name = rs.stringOpt(name),
      pass = rs.stringOpt(pass),
      cancelDate = rs.timestampOpt(cancelDate).map(_.toDateTime))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val mail = as(columnNames.mail)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val status = as(columnNames.status)
    val registToken = as(columnNames.registToken)
    val tokenLimit = as(columnNames.tokenLimit)
    val name = as(columnNames.name)
    val pass = as(columnNames.pass)
    val cancelDate = as(columnNames.cancelDate)
    val all = Seq(id, mail, createdAt, updatedAt, status, registToken, tokenLimit, name, pass, cancelDate)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => SearchedContracts(
      id = rs.long(id),
      mail = rs.string(mail),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      status = rs.string(status),
      registToken = rs.stringOpt(registToken),
      tokenLimit = rs.timestamp(tokenLimit).toDateTime,
      name = rs.stringOpt(name),
      pass = rs.stringOpt(pass),
      cancelDate = rs.timestampOpt(cancelDate).map(_.toDateTime))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[SearchedContracts] = {
    SQL("""select * from contracts where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[SearchedContracts] = {
    SQL("""select * from contracts""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from contracts""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[SearchedContracts] = {
    SQL("""select * from contracts where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from contracts where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    mail: String,
    createdAt: DateTime,
    updatedAt: DateTime,
    status: String,
    registToken: Option[String] = None,
    tokenLimit: DateTime,
    name: Option[String] = None,
    pass: Option[String] = None,
    cancelDate: Option[DateTime] = None)(implicit session: DBSession = autoSession): SearchedContracts = {
    val generatedKey = SQL("""
      insert into contracts (
        mail,
        created_at,
        updated_at,
        status,
        regist_token,
        token_limit,
        name,
        pass,
        cancel_date
      ) values (
        /*'mail*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'status*/'abc',
        /*'registToken*/'abc',
        /*'tokenLimit*/'1958-09-06 12:00:00',
        /*'name*/'abc',
        /*'pass*/'abc',
        /*'cancelDate*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'mail -> mail,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt,
        'status -> status,
        'registToken -> registToken,
        'tokenLimit -> tokenLimit,
        'name -> name,
        'pass -> pass,
        'cancelDate -> cancelDate
      ).updateAndReturnGeneratedKey.apply()

    SearchedContracts(
      id = generatedKey, 
      mail = mail,
      createdAt = createdAt,
      updatedAt = updatedAt,
      status = status,
      registToken = registToken,
      tokenLimit = tokenLimit,
      name = name,
      pass = pass,
      cancelDate = cancelDate)
  }

  def save(m: SearchedContracts)(implicit session: DBSession = autoSession): SearchedContracts = {
    SQL("""
      update
        contracts
      set
        id = /*'id*/1,
        mail = /*'mail*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        status = /*'status*/'abc',
        regist_token = /*'registToken*/'abc',
        token_limit = /*'tokenLimit*/'1958-09-06 12:00:00',
        name = /*'name*/'abc',
        pass = /*'pass*/'abc',
        cancel_date = /*'cancelDate*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'mail -> m.mail,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt,
        'status -> m.status,
        'registToken -> m.registToken,
        'tokenLimit -> m.tokenLimit,
        'name -> m.name,
        'pass -> m.pass,
        'cancelDate -> m.cancelDate
      ).update.apply()
    m
  }
      
  def destroy(m: SearchedContracts)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from contracts where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
