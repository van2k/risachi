package models

import scalikejdbc._
import org.joda.time.{LocalDate, DateTime}

case class EncFormConfigs(
  id: Long, 
  formId: String, 
  collectNumber: Int, 
  startSt: LocalDate, 
  endSt: Option[LocalDate] = None, 
  stop: String, 
  outputNumber: Int, 
  term: Int, 
  outputLimitNumber: Int, 
  saveTerm: Option[Int] = None, 
  price: Option[Int] = None, 
  paymentConfirm: Option[Int] = None, 
  paymentConfirmDate: Option[DateTime] = None, 
  chargePlanId: Option[Long] = None, 
  encResultOpen: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncFormConfigs.autoSession): EncFormConfigs = EncFormConfigs.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormConfigs.autoSession): Unit = EncFormConfigs.destroy(this)(session)

}
      

object EncFormConfigs {

  val tableName = "form_configs"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val collectNumber = "collect_number"
    val startSt = "start_st"
    val endSt = "end_st"
    val stop = "stop"
    val outputNumber = "output_number"
    val term = "term"
    val outputLimitNumber = "output_limit_number"
    val saveTerm = "save_term"
    val price = "price"
    val paymentConfirm = "payment_confirm"
    val paymentConfirmDate = "payment_confirm_date"
    val chargePlanId = "charge_plan_id"
    val encResultOpen = "enc_result_open"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, collectNumber, startSt, endSt, stop, outputNumber, term, outputLimitNumber, saveTerm, price, paymentConfirm, paymentConfirmDate, chargePlanId, encResultOpen, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormConfigs(
      id = rs.long(id),
      formId = rs.string(formId),
      collectNumber = rs.int(collectNumber),
      startSt = rs.date(startSt).toLocalDate,
      endSt = rs.dateOpt(endSt).map(_.toLocalDate),
      stop = rs.string(stop),
      outputNumber = rs.int(outputNumber),
      term = rs.int(term),
      outputLimitNumber = rs.int(outputLimitNumber),
      saveTerm = rs.intOpt(saveTerm),
      price = rs.intOpt(price),
      paymentConfirm = rs.intOpt(paymentConfirm),
      paymentConfirmDate = rs.timestampOpt(paymentConfirmDate).map(_.toDateTime),
      chargePlanId = rs.longOpt(chargePlanId),
      encResultOpen = rs.string(encResultOpen),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val collectNumber = as(columnNames.collectNumber)
    val startSt = as(columnNames.startSt)
    val endSt = as(columnNames.endSt)
    val stop = as(columnNames.stop)
    val outputNumber = as(columnNames.outputNumber)
    val term = as(columnNames.term)
    val outputLimitNumber = as(columnNames.outputLimitNumber)
    val saveTerm = as(columnNames.saveTerm)
    val price = as(columnNames.price)
    val paymentConfirm = as(columnNames.paymentConfirm)
    val paymentConfirmDate = as(columnNames.paymentConfirmDate)
    val chargePlanId = as(columnNames.chargePlanId)
    val encResultOpen = as(columnNames.encResultOpen)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, collectNumber, startSt, endSt, stop, outputNumber, term, outputLimitNumber, saveTerm, price, paymentConfirm, paymentConfirmDate, chargePlanId, encResultOpen, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormConfigs(
      id = rs.long(id),
      formId = rs.string(formId),
      collectNumber = rs.int(collectNumber),
      startSt = rs.date(startSt).toLocalDate,
      endSt = rs.dateOpt(endSt).map(_.toLocalDate),
      stop = rs.string(stop),
      outputNumber = rs.int(outputNumber),
      term = rs.int(term),
      outputLimitNumber = rs.int(outputLimitNumber),
      saveTerm = rs.intOpt(saveTerm),
      price = rs.intOpt(price),
      paymentConfirm = rs.intOpt(paymentConfirm),
      paymentConfirmDate = rs.timestampOpt(paymentConfirmDate).map(_.toDateTime),
      chargePlanId = rs.longOpt(chargePlanId),
      encResultOpen = rs.string(encResultOpen),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormConfigs] = {
    SQL("""select * from form_configs where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormConfigs] = {
    SQL("""select * from form_configs""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_configs""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormConfigs] = {
    SQL("""select * from form_configs where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_configs where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    formId: String,
    collectNumber: Int,
    startSt: LocalDate,
    endSt: Option[LocalDate] = None,
    stop: String,
    outputNumber: Int,
    term: Int,
    outputLimitNumber: Int,
    saveTerm: Option[Int] = None,
    price: Option[Int] = None,
    paymentConfirm: Option[Int] = None,
    paymentConfirmDate: Option[DateTime] = None,
    chargePlanId: Option[Long] = None,
    encResultOpen: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncFormConfigs = {
    val generatedKey = SQL("""
      insert into form_configs (
        form_id,
        collect_number,
        start_st,
        end_st,
        stop,
        output_number,
        term,
        output_limit_number,
        save_term,
        price,
        payment_confirm,
        payment_confirm_date,
        charge_plan_id,
        enc_result_open,
        created_at,
        updated_at
      ) values (
        /*'formId*/'abc',
        /*'collectNumber*/1,
        /*'startSt*/'1958-09-06',
        /*'endSt*/'1958-09-06',
        /*'stop*/'abc',
        /*'outputNumber*/1,
        /*'term*/1,
        /*'outputLimitNumber*/1,
        /*'saveTerm*/1,
        /*'price*/1,
        /*'paymentConfirm*/1,
        /*'paymentConfirmDate*/'1958-09-06 12:00:00',
        /*'chargePlanId*/1,
        /*'encResultOpen*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'formId -> formId,
        'collectNumber -> collectNumber,
        'startSt -> startSt,
        'endSt -> endSt,
        'stop -> stop,
        'outputNumber -> outputNumber,
        'term -> term,
        'outputLimitNumber -> outputLimitNumber,
        'saveTerm -> saveTerm,
        'price -> price,
        'paymentConfirm -> paymentConfirm,
        'paymentConfirmDate -> paymentConfirmDate,
        'chargePlanId -> chargePlanId,
        'encResultOpen -> encResultOpen,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncFormConfigs(
      id = generatedKey, 
      formId = formId,
      collectNumber = collectNumber,
      startSt = startSt,
      endSt = endSt,
      stop = stop,
      outputNumber = outputNumber,
      term = term,
      outputLimitNumber = outputLimitNumber,
      saveTerm = saveTerm,
      price = price,
      paymentConfirm = paymentConfirm,
      paymentConfirmDate = paymentConfirmDate,
      chargePlanId = chargePlanId,
      encResultOpen = encResultOpen,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncFormConfigs)(implicit session: DBSession = autoSession): EncFormConfigs = {
    SQL("""
      update
        form_configs
      set
        id = /*'id*/1,
        form_id = /*'formId*/'abc',
        collect_number = /*'collectNumber*/1,
        start_st = /*'startSt*/'1958-09-06',
        end_st = /*'endSt*/'1958-09-06',
        stop = /*'stop*/'abc',
        output_number = /*'outputNumber*/1,
        term = /*'term*/1,
        output_limit_number = /*'outputLimitNumber*/1,
        save_term = /*'saveTerm*/1,
        price = /*'price*/1,
        payment_confirm = /*'paymentConfirm*/1,
        payment_confirm_date = /*'paymentConfirmDate*/'1958-09-06 12:00:00',
        charge_plan_id = /*'chargePlanId*/1,
        enc_result_open = /*'encResultOpen*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'collectNumber -> m.collectNumber,
        'startSt -> m.startSt,
        'endSt -> m.endSt,
        'stop -> m.stop,
        'outputNumber -> m.outputNumber,
        'term -> m.term,
        'outputLimitNumber -> m.outputLimitNumber,
        'saveTerm -> m.saveTerm,
        'price -> m.price,
        'paymentConfirm -> m.paymentConfirm,
        'paymentConfirmDate -> m.paymentConfirmDate,
        'chargePlanId -> m.chargePlanId,
        'encResultOpen -> m.encResultOpen,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormConfigs)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_configs where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
