package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class ChargeOptionPlans(
  id: Long, 
  charge: Int, 
  tax: Int, 
  description: String, 
  applyStartDate: DateTime, 
  createdAt: DateTime, 
  updatedAt: DateTime, 
  optionType: String, 
  optionValue: Int) {

  def save()(implicit session: DBSession = ChargeOptionPlans.autoSession): ChargeOptionPlans = ChargeOptionPlans.save(this)(session)

  def destroy()(implicit session: DBSession = ChargeOptionPlans.autoSession): Unit = ChargeOptionPlans.destroy(this)(session)

}
      

object ChargeOptionPlans {

  val tableName = "charge_option_plans"

  object columnNames {
    val id = "id"
    val charge = "charge"
    val tax = "tax"
    val description = "description"
    val applyStartDate = "apply_start_date"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val optionType = "option_type"
    val optionValue = "option_value"
    val all = Seq(id, charge, tax, description, applyStartDate, createdAt, updatedAt, optionType, optionValue)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => ChargeOptionPlans(
      id = rs.long(id),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      applyStartDate = rs.timestamp(applyStartDate).toDateTime,
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      optionType = rs.string(optionType),
      optionValue = rs.int(optionValue))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val charge = as(columnNames.charge)
    val tax = as(columnNames.tax)
    val description = as(columnNames.description)
    val applyStartDate = as(columnNames.applyStartDate)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val optionType = as(columnNames.optionType)
    val optionValue = as(columnNames.optionValue)
    val all = Seq(id, charge, tax, description, applyStartDate, createdAt, updatedAt, optionType, optionValue)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => ChargeOptionPlans(
      id = rs.long(id),
      charge = rs.int(charge),
      tax = rs.int(tax),
      description = rs.string(description),
      applyStartDate = rs.timestamp(applyStartDate).toDateTime,
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      optionType = rs.string(optionType),
      optionValue = rs.int(optionValue))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[ChargeOptionPlans] = {
    SQL("""select * from charge_option_plans where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[ChargeOptionPlans] = {
    SQL("""select * from charge_option_plans""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from charge_option_plans""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[ChargeOptionPlans] = {
    SQL("""select * from charge_option_plans where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from charge_option_plans where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: Long,
    charge: Int,
    tax: Int,
    description: String,
    applyStartDate: DateTime,
    createdAt: DateTime,
    updatedAt: DateTime,
    optionType: String,
    optionValue: Int)(implicit session: DBSession = autoSession): ChargeOptionPlans = {
    SQL("""
      insert into charge_option_plans (
        id,
        charge,
        tax,
        description,
        apply_start_date,
        created_at,
        updated_at,
        option_type,
        option_value
      ) values (
        /*'id*/1,
        /*'charge*/1,
        /*'tax*/1,
        /*'description*/'abc',
        /*'applyStartDate*/'1958-09-06 12:00:00',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'optionType*/'abc',
        /*'optionValue*/1
      )
      """)
      .bindByName(
        'id -> id,
        'charge -> charge,
        'tax -> tax,
        'description -> description,
        'applyStartDate -> applyStartDate,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt,
        'optionType -> optionType,
        'optionValue -> optionValue
      ).update.apply()

    ChargeOptionPlans(
      id = id,
      charge = charge,
      tax = tax,
      description = description,
      applyStartDate = applyStartDate,
      createdAt = createdAt,
      updatedAt = updatedAt,
      optionType = optionType,
      optionValue = optionValue)
  }

  def save(m: ChargeOptionPlans)(implicit session: DBSession = autoSession): ChargeOptionPlans = {
    SQL("""
      update
        charge_option_plans
      set
        id = /*'id*/1,
        charge = /*'charge*/1,
        tax = /*'tax*/1,
        description = /*'description*/'abc',
        apply_start_date = /*'applyStartDate*/'1958-09-06 12:00:00',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        option_type = /*'optionType*/'abc',
        option_value = /*'optionValue*/1
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'charge -> m.charge,
        'tax -> m.tax,
        'description -> m.description,
        'applyStartDate -> m.applyStartDate,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt,
        'optionType -> m.optionType,
        'optionValue -> m.optionValue
      ).update.apply()
    m
  }
      
  def destroy(m: ChargeOptionPlans)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from charge_option_plans where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
