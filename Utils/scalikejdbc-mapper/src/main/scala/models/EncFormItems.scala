package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncFormItems(
  id: String, 
  formId: String, 
  categoryId: Long, 
  mFormItemId: Long, 
  name: String, 
  pageNo: Int, 
  required: String, 
  selectNumber: Option[Int] = None, 
  condition: Option[String] = None, 
  dispodr: Int, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncFormItems.autoSession): EncFormItems = EncFormItems.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormItems.autoSession): Unit = EncFormItems.destroy(this)(session)

}
      

object EncFormItems {

  val tableName = "form_items"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val categoryId = "category_id"
    val mFormItemId = "m_form_item_id"
    val name = "name"
    val pageNo = "page_no"
    val required = "required"
    val selectNumber = "select_number"
    val condition = "condition"
    val dispodr = "dispodr"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, categoryId, mFormItemId, name, pageNo, required, selectNumber, condition, dispodr, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormItems(
      id = rs.string(id),
      formId = rs.string(formId),
      categoryId = rs.long(categoryId),
      mFormItemId = rs.long(mFormItemId),
      name = rs.string(name),
      pageNo = rs.int(pageNo),
      required = rs.string(required),
      selectNumber = rs.intOpt(selectNumber),
      condition = rs.stringOpt(condition),
      dispodr = rs.int(dispodr),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val categoryId = as(columnNames.categoryId)
    val mFormItemId = as(columnNames.mFormItemId)
    val name = as(columnNames.name)
    val pageNo = as(columnNames.pageNo)
    val required = as(columnNames.required)
    val selectNumber = as(columnNames.selectNumber)
    val condition = as(columnNames.condition)
    val dispodr = as(columnNames.dispodr)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, categoryId, mFormItemId, name, pageNo, required, selectNumber, condition, dispodr, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormItems(
      id = rs.string(id),
      formId = rs.string(formId),
      categoryId = rs.long(categoryId),
      mFormItemId = rs.long(mFormItemId),
      name = rs.string(name),
      pageNo = rs.int(pageNo),
      required = rs.string(required),
      selectNumber = rs.intOpt(selectNumber),
      condition = rs.stringOpt(condition),
      dispodr = rs.int(dispodr),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: String)(implicit session: DBSession = autoSession): Option[EncFormItems] = {
    SQL("""select * from form_items where id = /*'id*/'abc'""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormItems] = {
    SQL("""select * from form_items""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_items""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormItems] = {
    SQL("""select * from form_items where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_items where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: String,
    formId: String,
    categoryId: Long,
    mFormItemId: Long,
    name: String,
    pageNo: Int,
    required: String,
    selectNumber: Option[Int] = None,
    condition: Option[String] = None,
    dispodr: Int,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncFormItems = {
    SQL("""
      insert into form_items (
        id,
        form_id,
        category_id,
        m_form_item_id,
        name,
        page_no,
        required,
        select_number,
        condition,
        dispodr,
        created_at,
        updated_at
      ) values (
        /*'id*/'abc',
        /*'formId*/'abc',
        /*'categoryId*/1,
        /*'mFormItemId*/1,
        /*'name*/'abc',
        /*'pageNo*/1,
        /*'required*/'abc',
        /*'selectNumber*/1,
        /*'condition*/'abc',
        /*'dispodr*/1,
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'id -> id,
        'formId -> formId,
        'categoryId -> categoryId,
        'mFormItemId -> mFormItemId,
        'name -> name,
        'pageNo -> pageNo,
        'required -> required,
        'selectNumber -> selectNumber,
        'condition -> condition,
        'dispodr -> dispodr,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).update.apply()

    EncFormItems(
      id = id,
      formId = formId,
      categoryId = categoryId,
      mFormItemId = mFormItemId,
      name = name,
      pageNo = pageNo,
      required = required,
      selectNumber = selectNumber,
      condition = condition,
      dispodr = dispodr,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncFormItems)(implicit session: DBSession = autoSession): EncFormItems = {
    SQL("""
      update
        form_items
      set
        id = /*'id*/'abc',
        form_id = /*'formId*/'abc',
        category_id = /*'categoryId*/1,
        m_form_item_id = /*'mFormItemId*/1,
        name = /*'name*/'abc',
        page_no = /*'pageNo*/1,
        required = /*'required*/'abc',
        select_number = /*'selectNumber*/1,
        condition = /*'condition*/'abc',
        dispodr = /*'dispodr*/1,
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/'abc'
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'categoryId -> m.categoryId,
        'mFormItemId -> m.mFormItemId,
        'name -> m.name,
        'pageNo -> m.pageNo,
        'required -> m.required,
        'selectNumber -> m.selectNumber,
        'condition -> m.condition,
        'dispodr -> m.dispodr,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormItems)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_items where id = /*'id*/'abc'""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
