package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class Information(
  id: Long, 
  sendDate: DateTime, 
  subject: String, 
  content: String) {

  def save()(implicit session: DBSession = Information.autoSession): Information = Information.save(this)(session)

  def destroy()(implicit session: DBSession = Information.autoSession): Unit = Information.destroy(this)(session)

}
      

object Information {

  val tableName = "information"

  object columnNames {
    val id = "id"
    val sendDate = "send_date"
    val subject = "subject"
    val content = "content"
    val all = Seq(id, sendDate, subject, content)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Information(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toDateTime,
      subject = rs.string(subject),
      content = rs.string(content))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val sendDate = as(columnNames.sendDate)
    val subject = as(columnNames.subject)
    val content = as(columnNames.content)
    val all = Seq(id, sendDate, subject, content)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => Information(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toDateTime,
      subject = rs.string(subject),
      content = rs.string(content))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[Information] = {
    SQL("""select * from information where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[Information] = {
    SQL("""select * from information""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from information""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[Information] = {
    SQL("""select * from information where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from information where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    sendDate: DateTime,
    subject: String,
    content: String)(implicit session: DBSession = autoSession): Information = {
    val generatedKey = SQL("""
      insert into information (
        send_date,
        subject,
        content
      ) values (
        /*'sendDate*/'1958-09-06 12:00:00',
        /*'subject*/'abc',
        /*'content*/'abc'
      )
      """)
      .bindByName(
        'sendDate -> sendDate,
        'subject -> subject,
        'content -> content
      ).updateAndReturnGeneratedKey.apply()

    Information(
      id = generatedKey, 
      sendDate = sendDate,
      subject = subject,
      content = content)
  }

  def save(m: Information)(implicit session: DBSession = autoSession): Information = {
    SQL("""
      update
        information
      set
        id = /*'id*/1,
        send_date = /*'sendDate*/'1958-09-06 12:00:00',
        subject = /*'subject*/'abc',
        content = /*'content*/'abc'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'sendDate -> m.sendDate,
        'subject -> m.subject,
        'content -> m.content
      ).update.apply()
    m
  }
      
  def destroy(m: Information)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from information where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
