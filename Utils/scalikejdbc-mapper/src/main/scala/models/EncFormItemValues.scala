package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncFormItemValues(
  id: Long, 
  formItemId: String, 
  formValue: String, 
  chLength: Int, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncFormItemValues.autoSession): EncFormItemValues = EncFormItemValues.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormItemValues.autoSession): Unit = EncFormItemValues.destroy(this)(session)

}
      

object EncFormItemValues {

  val tableName = "form_item_values"

  object columnNames {
    val id = "id"
    val formItemId = "form_item_id"
    val formValue = "form_value"
    val chLength = "ch_length"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formItemId, formValue, chLength, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormItemValues(
      id = rs.long(id),
      formItemId = rs.string(formItemId),
      formValue = rs.string(formValue),
      chLength = rs.int(chLength),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formItemId = as(columnNames.formItemId)
    val formValue = as(columnNames.formValue)
    val chLength = as(columnNames.chLength)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formItemId, formValue, chLength, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormItemValues(
      id = rs.long(id),
      formItemId = rs.string(formItemId),
      formValue = rs.string(formValue),
      chLength = rs.int(chLength),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormItemValues] = {
    SQL("""select * from form_item_values where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
    SQL("""select * from form_item_values""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_item_values""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormItemValues] = {
    SQL("""select * from form_item_values where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_item_values where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    formItemId: String,
    formValue: String,
    chLength: Int,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncFormItemValues = {
    val generatedKey = SQL("""
      insert into form_item_values (
        form_item_id,
        form_value,
        ch_length,
        created_at,
        updated_at
      ) values (
        /*'formItemId*/'abc',
        /*'formValue*/'abc',
        /*'chLength*/1,
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'formItemId -> formItemId,
        'formValue -> formValue,
        'chLength -> chLength,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncFormItemValues(
      id = generatedKey, 
      formItemId = formItemId,
      formValue = formValue,
      chLength = chLength,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncFormItemValues)(implicit session: DBSession = autoSession): EncFormItemValues = {
    SQL("""
      update
        form_item_values
      set
        id = /*'id*/1,
        form_item_id = /*'formItemId*/'abc',
        form_value = /*'formValue*/'abc',
        ch_length = /*'chLength*/1,
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'formItemId -> m.formItemId,
        'formValue -> m.formValue,
        'chLength -> m.chLength,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormItemValues)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_item_values where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
