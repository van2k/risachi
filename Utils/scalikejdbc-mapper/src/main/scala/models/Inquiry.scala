package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class Inquiry(
  id: Long, 
  sendDate: DateTime, 
  subject: String, 
  mail: String, 
  content: String, 
  status: String, 
  contractId: Option[Long] = None, 
  name: String) {

  def save()(implicit session: DBSession = Inquiry.autoSession): Inquiry = Inquiry.save(this)(session)

  def destroy()(implicit session: DBSession = Inquiry.autoSession): Unit = Inquiry.destroy(this)(session)

}
      

object Inquiry {

  val tableName = "inquiry"

  object columnNames {
    val id = "id"
    val sendDate = "send_date"
    val subject = "subject"
    val mail = "mail"
    val content = "content"
    val status = "status"
    val contractId = "contract_id"
    val name = "name"
    val all = Seq(id, sendDate, subject, mail, content, status, contractId, name)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Inquiry(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toDateTime,
      subject = rs.string(subject),
      mail = rs.string(mail),
      content = rs.string(content),
      status = rs.string(status),
      contractId = rs.longOpt(contractId),
      name = rs.string(name))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val sendDate = as(columnNames.sendDate)
    val subject = as(columnNames.subject)
    val mail = as(columnNames.mail)
    val content = as(columnNames.content)
    val status = as(columnNames.status)
    val contractId = as(columnNames.contractId)
    val name = as(columnNames.name)
    val all = Seq(id, sendDate, subject, mail, content, status, contractId, name)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => Inquiry(
      id = rs.long(id),
      sendDate = rs.timestamp(sendDate).toDateTime,
      subject = rs.string(subject),
      mail = rs.string(mail),
      content = rs.string(content),
      status = rs.string(status),
      contractId = rs.longOpt(contractId),
      name = rs.string(name))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[Inquiry] = {
    SQL("""select * from inquiry where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[Inquiry] = {
    SQL("""select * from inquiry""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from inquiry""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[Inquiry] = {
    SQL("""select * from inquiry where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from inquiry where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    sendDate: DateTime,
    subject: String,
    mail: String,
    content: String,
    status: String,
    contractId: Option[Long] = None,
    name: String)(implicit session: DBSession = autoSession): Inquiry = {
    val generatedKey = SQL("""
      insert into inquiry (
        send_date,
        subject,
        mail,
        content,
        status,
        contract_id,
        name
      ) values (
        /*'sendDate*/'1958-09-06 12:00:00',
        /*'subject*/'abc',
        /*'mail*/'abc',
        /*'content*/'abc',
        /*'status*/'abc',
        /*'contractId*/1,
        /*'name*/'abc'
      )
      """)
      .bindByName(
        'sendDate -> sendDate,
        'subject -> subject,
        'mail -> mail,
        'content -> content,
        'status -> status,
        'contractId -> contractId,
        'name -> name
      ).updateAndReturnGeneratedKey.apply()

    Inquiry(
      id = generatedKey, 
      sendDate = sendDate,
      subject = subject,
      mail = mail,
      content = content,
      status = status,
      contractId = contractId,
      name = name)
  }

  def save(m: Inquiry)(implicit session: DBSession = autoSession): Inquiry = {
    SQL("""
      update
        inquiry
      set
        id = /*'id*/1,
        send_date = /*'sendDate*/'1958-09-06 12:00:00',
        subject = /*'subject*/'abc',
        mail = /*'mail*/'abc',
        content = /*'content*/'abc',
        status = /*'status*/'abc',
        contract_id = /*'contractId*/1,
        name = /*'name*/'abc'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'sendDate -> m.sendDate,
        'subject -> m.subject,
        'mail -> m.mail,
        'content -> m.content,
        'status -> m.status,
        'contractId -> m.contractId,
        'name -> m.name
      ).update.apply()
    m
  }
      
  def destroy(m: Inquiry)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from inquiry where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
