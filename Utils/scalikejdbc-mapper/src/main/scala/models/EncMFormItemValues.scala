package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncMFormItemValues(
  id: Long, 
  mFormItemsId: Long, 
  itemValue: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncMFormItemValues.autoSession): EncMFormItemValues = EncMFormItemValues.save(this)(session)

  def destroy()(implicit session: DBSession = EncMFormItemValues.autoSession): Unit = EncMFormItemValues.destroy(this)(session)

}
      

object EncMFormItemValues {

  val tableName = "m_form_item_values"

  object columnNames {
    val id = "id"
    val mFormItemsId = "m_form_items_id"
    val itemValue = "item_value"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, mFormItemsId, itemValue, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMFormItemValues(
      id = rs.long(id),
      mFormItemsId = rs.long(mFormItemsId),
      itemValue = rs.string(itemValue),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val mFormItemsId = as(columnNames.mFormItemsId)
    val itemValue = as(columnNames.itemValue)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, mFormItemsId, itemValue, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncMFormItemValues(
      id = rs.long(id),
      mFormItemsId = rs.long(mFormItemsId),
      itemValue = rs.string(itemValue),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncMFormItemValues] = {
    SQL("""select * from m_form_item_values where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncMFormItemValues] = {
    SQL("""select * from m_form_item_values""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_form_item_values""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncMFormItemValues] = {
    SQL("""select * from m_form_item_values where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_form_item_values where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    mFormItemsId: Long,
    itemValue: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncMFormItemValues = {
    val generatedKey = SQL("""
      insert into m_form_item_values (
        m_form_items_id,
        item_value,
        created_at,
        updated_at
      ) values (
        /*'mFormItemsId*/1,
        /*'itemValue*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'mFormItemsId -> mFormItemsId,
        'itemValue -> itemValue,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncMFormItemValues(
      id = generatedKey, 
      mFormItemsId = mFormItemsId,
      itemValue = itemValue,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncMFormItemValues)(implicit session: DBSession = autoSession): EncMFormItemValues = {
    SQL("""
      update
        m_form_item_values
      set
        id = /*'id*/1,
        m_form_items_id = /*'mFormItemsId*/1,
        item_value = /*'itemValue*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'mFormItemsId -> m.mFormItemsId,
        'itemValue -> m.itemValue,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncMFormItemValues)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from m_form_item_values where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
