package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncMFormItems(
  id: Long, 
  name: String, 
  kind: String, 
  dataType: String, 
  graphKind: String, 
  createdAt: DateTime, 
  updatedAt: DateTime, 
  regex: Option[String] = None) {

  def save()(implicit session: DBSession = EncMFormItems.autoSession): EncMFormItems = EncMFormItems.save(this)(session)

  def destroy()(implicit session: DBSession = EncMFormItems.autoSession): Unit = EncMFormItems.destroy(this)(session)

}
      

object EncMFormItems {

  val tableName = "m_form_items"

  object columnNames {
    val id = "id"
    val name = "name"
    val kind = "kind"
    val dataType = "data_type"
    val graphKind = "graph_kind"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val regex = "regex"
    val all = Seq(id, name, kind, dataType, graphKind, createdAt, updatedAt, regex)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMFormItems(
      id = rs.long(id),
      name = rs.string(name),
      kind = rs.string(kind),
      dataType = rs.string(dataType),
      graphKind = rs.string(graphKind),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      regex = rs.stringOpt(regex))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val name = as(columnNames.name)
    val kind = as(columnNames.kind)
    val dataType = as(columnNames.dataType)
    val graphKind = as(columnNames.graphKind)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val regex = as(columnNames.regex)
    val all = Seq(id, name, kind, dataType, graphKind, createdAt, updatedAt, regex)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncMFormItems(
      id = rs.long(id),
      name = rs.string(name),
      kind = rs.string(kind),
      dataType = rs.string(dataType),
      graphKind = rs.string(graphKind),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime,
      regex = rs.stringOpt(regex))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncMFormItems] = {
    SQL("""select * from m_form_items where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncMFormItems] = {
    SQL("""select * from m_form_items""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_form_items""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncMFormItems] = {
    SQL("""select * from m_form_items where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_form_items where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: Long,
    name: String,
    kind: String,
    dataType: String,
    graphKind: String,
    createdAt: DateTime,
    updatedAt: DateTime,
    regex: Option[String] = None)(implicit session: DBSession = autoSession): EncMFormItems = {
    SQL("""
      insert into m_form_items (
        id,
        name,
        kind,
        data_type,
        graph_kind,
        created_at,
        updated_at,
        regex
      ) values (
        /*'id*/1,
        /*'name*/'abc',
        /*'kind*/'abc',
        /*'dataType*/'abc',
        /*'graphKind*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00',
        /*'regex*/'abc'
      )
      """)
      .bindByName(
        'id -> id,
        'name -> name,
        'kind -> kind,
        'dataType -> dataType,
        'graphKind -> graphKind,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt,
        'regex -> regex
      ).update.apply()

    EncMFormItems(
      id = id,
      name = name,
      kind = kind,
      dataType = dataType,
      graphKind = graphKind,
      createdAt = createdAt,
      updatedAt = updatedAt,
      regex = regex)
  }

  def save(m: EncMFormItems)(implicit session: DBSession = autoSession): EncMFormItems = {
    SQL("""
      update
        m_form_items
      set
        id = /*'id*/1,
        name = /*'name*/'abc',
        kind = /*'kind*/'abc',
        data_type = /*'dataType*/'abc',
        graph_kind = /*'graphKind*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00',
        regex = /*'regex*/'abc'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'name -> m.name,
        'kind -> m.kind,
        'dataType -> m.dataType,
        'graphKind -> m.graphKind,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt,
        'regex -> m.regex
      ).update.apply()
    m
  }
      
  def destroy(m: EncMFormItems)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from m_form_items where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
