package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncFormCategories(
  id: Long, 
  formId: String, 
  name: String, 
  description: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncFormCategories.autoSession): EncFormCategories = EncFormCategories.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormCategories.autoSession): Unit = EncFormCategories.destroy(this)(session)

}
      

object EncFormCategories {

  val tableName = "form_categories"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val name = "name"
    val description = "description"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, name, description, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormCategories(
      id = rs.long(id),
      formId = rs.string(formId),
      name = rs.string(name),
      description = rs.string(description),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val name = as(columnNames.name)
    val description = as(columnNames.description)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, name, description, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormCategories(
      id = rs.long(id),
      formId = rs.string(formId),
      name = rs.string(name),
      description = rs.string(description),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormCategories] = {
    SQL("""select * from form_categories where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormCategories] = {
    SQL("""select * from form_categories""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_categories""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormCategories] = {
    SQL("""select * from form_categories where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_categories where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    formId: String,
    name: String,
    description: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncFormCategories = {
    val generatedKey = SQL("""
      insert into form_categories (
        form_id,
        name,
        description,
        created_at,
        updated_at
      ) values (
        /*'formId*/'abc',
        /*'name*/'abc',
        /*'description*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'formId -> formId,
        'name -> name,
        'description -> description,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncFormCategories(
      id = generatedKey, 
      formId = formId,
      name = name,
      description = description,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncFormCategories)(implicit session: DBSession = autoSession): EncFormCategories = {
    SQL("""
      update
        form_categories
      set
        id = /*'id*/1,
        form_id = /*'formId*/'abc',
        name = /*'name*/'abc',
        description = /*'description*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'name -> m.name,
        'description -> m.description,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormCategories)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_categories where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
