package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncAnswers(
  id: String, 
  formId: String, 
  startAt: DateTime, 
  endAt: Option[DateTime] = None, 
  foreignKey: Option[String] = None, 
  attribute1: Option[String] = None, 
  attribute2: Option[String] = None, 
  attribute3: Option[String] = None, 
  attribute4: Option[String] = None, 
  attribute5: Option[String] = None, 
  isPreview: String, 
  ipAddress: String, 
  webBrowser: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncAnswers.autoSession): EncAnswers = EncAnswers.save(this)(session)

  def destroy()(implicit session: DBSession = EncAnswers.autoSession): Unit = EncAnswers.destroy(this)(session)

}
      

object EncAnswers {

  val tableName = "answers"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val startAt = "start_at"
    val endAt = "end_at"
    val foreignKey = "foreign_key"
    val attribute1 = "attribute1"
    val attribute2 = "attribute2"
    val attribute3 = "attribute3"
    val attribute4 = "attribute4"
    val attribute5 = "attribute5"
    val isPreview = "is_preview"
    val ipAddress = "ip_address"
    val webBrowser = "web_browser"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, startAt, endAt, foreignKey, attribute1, attribute2, attribute3, attribute4, attribute5, isPreview, ipAddress, webBrowser, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncAnswers(
      id = rs.string(id),
      formId = rs.string(formId),
      startAt = rs.timestamp(startAt).toDateTime,
      endAt = rs.timestampOpt(endAt).map(_.toDateTime),
      foreignKey = rs.stringOpt(foreignKey),
      attribute1 = rs.stringOpt(attribute1),
      attribute2 = rs.stringOpt(attribute2),
      attribute3 = rs.stringOpt(attribute3),
      attribute4 = rs.stringOpt(attribute4),
      attribute5 = rs.stringOpt(attribute5),
      isPreview = rs.string(isPreview),
      ipAddress = rs.string(ipAddress),
      webBrowser = rs.string(webBrowser),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val startAt = as(columnNames.startAt)
    val endAt = as(columnNames.endAt)
    val foreignKey = as(columnNames.foreignKey)
    val attribute1 = as(columnNames.attribute1)
    val attribute2 = as(columnNames.attribute2)
    val attribute3 = as(columnNames.attribute3)
    val attribute4 = as(columnNames.attribute4)
    val attribute5 = as(columnNames.attribute5)
    val isPreview = as(columnNames.isPreview)
    val ipAddress = as(columnNames.ipAddress)
    val webBrowser = as(columnNames.webBrowser)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, startAt, endAt, foreignKey, attribute1, attribute2, attribute3, attribute4, attribute5, isPreview, ipAddress, webBrowser, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncAnswers(
      id = rs.string(id),
      formId = rs.string(formId),
      startAt = rs.timestamp(startAt).toDateTime,
      endAt = rs.timestampOpt(endAt).map(_.toDateTime),
      foreignKey = rs.stringOpt(foreignKey),
      attribute1 = rs.stringOpt(attribute1),
      attribute2 = rs.stringOpt(attribute2),
      attribute3 = rs.stringOpt(attribute3),
      attribute4 = rs.stringOpt(attribute4),
      attribute5 = rs.stringOpt(attribute5),
      isPreview = rs.string(isPreview),
      ipAddress = rs.string(ipAddress),
      webBrowser = rs.string(webBrowser),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: String)(implicit session: DBSession = autoSession): Option[EncAnswers] = {
    SQL("""select * from answers where id = /*'id*/'abc'""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncAnswers] = {
    SQL("""select * from answers""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from answers""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncAnswers] = {
    SQL("""select * from answers where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from answers where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: String,
    formId: String,
    startAt: DateTime,
    endAt: Option[DateTime] = None,
    foreignKey: Option[String] = None,
    attribute1: Option[String] = None,
    attribute2: Option[String] = None,
    attribute3: Option[String] = None,
    attribute4: Option[String] = None,
    attribute5: Option[String] = None,
    isPreview: String,
    ipAddress: String,
    webBrowser: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncAnswers = {
    SQL("""
      insert into answers (
        id,
        form_id,
        start_at,
        end_at,
        foreign_key,
        attribute1,
        attribute2,
        attribute3,
        attribute4,
        attribute5,
        is_preview,
        ip_address,
        web_browser,
        created_at,
        updated_at
      ) values (
        /*'id*/'abc',
        /*'formId*/'abc',
        /*'startAt*/'1958-09-06 12:00:00',
        /*'endAt*/'1958-09-06 12:00:00',
        /*'foreignKey*/'abc',
        /*'attribute1*/'abc',
        /*'attribute2*/'abc',
        /*'attribute3*/'abc',
        /*'attribute4*/'abc',
        /*'attribute5*/'abc',
        /*'isPreview*/'abc',
        /*'ipAddress*/'abc',
        /*'webBrowser*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'id -> id,
        'formId -> formId,
        'startAt -> startAt,
        'endAt -> endAt,
        'foreignKey -> foreignKey,
        'attribute1 -> attribute1,
        'attribute2 -> attribute2,
        'attribute3 -> attribute3,
        'attribute4 -> attribute4,
        'attribute5 -> attribute5,
        'isPreview -> isPreview,
        'ipAddress -> ipAddress,
        'webBrowser -> webBrowser,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).update.apply()

    EncAnswers(
      id = id,
      formId = formId,
      startAt = startAt,
      endAt = endAt,
      foreignKey = foreignKey,
      attribute1 = attribute1,
      attribute2 = attribute2,
      attribute3 = attribute3,
      attribute4 = attribute4,
      attribute5 = attribute5,
      isPreview = isPreview,
      ipAddress = ipAddress,
      webBrowser = webBrowser,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncAnswers)(implicit session: DBSession = autoSession): EncAnswers = {
    SQL("""
      update
        answers
      set
        id = /*'id*/'abc',
        form_id = /*'formId*/'abc',
        start_at = /*'startAt*/'1958-09-06 12:00:00',
        end_at = /*'endAt*/'1958-09-06 12:00:00',
        foreign_key = /*'foreignKey*/'abc',
        attribute1 = /*'attribute1*/'abc',
        attribute2 = /*'attribute2*/'abc',
        attribute3 = /*'attribute3*/'abc',
        attribute4 = /*'attribute4*/'abc',
        attribute5 = /*'attribute5*/'abc',
        is_preview = /*'isPreview*/'abc',
        ip_address = /*'ipAddress*/'abc',
        web_browser = /*'webBrowser*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/'abc'
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'startAt -> m.startAt,
        'endAt -> m.endAt,
        'foreignKey -> m.foreignKey,
        'attribute1 -> m.attribute1,
        'attribute2 -> m.attribute2,
        'attribute3 -> m.attribute3,
        'attribute4 -> m.attribute4,
        'attribute5 -> m.attribute5,
        'isPreview -> m.isPreview,
        'ipAddress -> m.ipAddress,
        'webBrowser -> m.webBrowser,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncAnswers)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from answers where id = /*'id*/'abc'""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
