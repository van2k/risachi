package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncFormConfigValues(
  id: Long, 
  formId: String, 
  optionId: Long, 
  optionNumber: Long, 
  charge: Long, 
  price: Long, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncFormConfigValues.autoSession): EncFormConfigValues = EncFormConfigValues.save(this)(session)

  def destroy()(implicit session: DBSession = EncFormConfigValues.autoSession): Unit = EncFormConfigValues.destroy(this)(session)

}
      

object EncFormConfigValues {

  val tableName = "form_config_values"

  object columnNames {
    val id = "id"
    val formId = "form_id"
    val optionId = "option_id"
    val optionNumber = "option_number"
    val charge = "charge"
    val price = "price"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, formId, optionId, optionNumber, charge, price, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncFormConfigValues(
      id = rs.long(id),
      formId = rs.string(formId),
      optionId = rs.long(optionId),
      optionNumber = rs.long(optionNumber),
      charge = rs.long(charge),
      price = rs.long(price),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val formId = as(columnNames.formId)
    val optionId = as(columnNames.optionId)
    val optionNumber = as(columnNames.optionNumber)
    val charge = as(columnNames.charge)
    val price = as(columnNames.price)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, formId, optionId, optionNumber, charge, price, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncFormConfigValues(
      id = rs.long(id),
      formId = rs.string(formId),
      optionId = rs.long(optionId),
      optionNumber = rs.long(optionNumber),
      charge = rs.long(charge),
      price = rs.long(price),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncFormConfigValues] = {
    SQL("""select * from form_config_values where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncFormConfigValues] = {
    SQL("""select * from form_config_values""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_config_values""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncFormConfigValues] = {
    SQL("""select * from form_config_values where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from form_config_values where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    formId: String,
    optionId: Long,
    optionNumber: Long,
    charge: Long,
    price: Long,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncFormConfigValues = {
    val generatedKey = SQL("""
      insert into form_config_values (
        form_id,
        option_id,
        option_number,
        charge,
        price,
        created_at,
        updated_at
      ) values (
        /*'formId*/'abc',
        /*'optionId*/1,
        /*'optionNumber*/1,
        /*'charge*/1,
        /*'price*/1,
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'formId -> formId,
        'optionId -> optionId,
        'optionNumber -> optionNumber,
        'charge -> charge,
        'price -> price,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).updateAndReturnGeneratedKey.apply()

    EncFormConfigValues(
      id = generatedKey, 
      formId = formId,
      optionId = optionId,
      optionNumber = optionNumber,
      charge = charge,
      price = price,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncFormConfigValues)(implicit session: DBSession = autoSession): EncFormConfigValues = {
    SQL("""
      update
        form_config_values
      set
        id = /*'id*/1,
        form_id = /*'formId*/'abc',
        option_id = /*'optionId*/1,
        option_number = /*'optionNumber*/1,
        charge = /*'charge*/1,
        price = /*'price*/1,
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'formId -> m.formId,
        'optionId -> m.optionId,
        'optionNumber -> m.optionNumber,
        'charge -> m.charge,
        'price -> m.price,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncFormConfigValues)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from form_config_values where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
