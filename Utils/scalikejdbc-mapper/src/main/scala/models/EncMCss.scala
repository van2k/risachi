package models

import scalikejdbc._
import org.joda.time.{DateTime}

case class EncMCss(
  id: Long, 
  name: String, 
  cssCode: String, 
  dispImageUrl: String, 
  createdAt: DateTime, 
  updatedAt: DateTime) {

  def save()(implicit session: DBSession = EncMCss.autoSession): EncMCss = EncMCss.save(this)(session)

  def destroy()(implicit session: DBSession = EncMCss.autoSession): Unit = EncMCss.destroy(this)(session)

}
      

object EncMCss {

  val tableName = "m_css"

  object columnNames {
    val id = "id"
    val name = "name"
    val cssCode = "css_code"
    val dispImageUrl = "disp_image_url"
    val createdAt = "created_at"
    val updatedAt = "updated_at"
    val all = Seq(id, name, cssCode, dispImageUrl, createdAt, updatedAt)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => EncMCss(
      id = rs.long(id),
      name = rs.string(name),
      cssCode = rs.string(cssCode),
      dispImageUrl = rs.string(dispImageUrl),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val name = as(columnNames.name)
    val cssCode = as(columnNames.cssCode)
    val dispImageUrl = as(columnNames.dispImageUrl)
    val createdAt = as(columnNames.createdAt)
    val updatedAt = as(columnNames.updatedAt)
    val all = Seq(id, name, cssCode, dispImageUrl, createdAt, updatedAt)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => EncMCss(
      id = rs.long(id),
      name = rs.string(name),
      cssCode = rs.string(cssCode),
      dispImageUrl = rs.string(dispImageUrl),
      createdAt = rs.timestamp(createdAt).toDateTime,
      updatedAt = rs.timestamp(updatedAt).toDateTime)
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[EncMCss] = {
    SQL("""select * from m_css where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[EncMCss] = {
    SQL("""select * from m_css""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_css""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[EncMCss] = {
    SQL("""select * from m_css where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from m_css where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    id: Long,
    name: String,
    cssCode: String,
    dispImageUrl: String,
    createdAt: DateTime,
    updatedAt: DateTime)(implicit session: DBSession = autoSession): EncMCss = {
    SQL("""
      insert into m_css (
        id,
        name,
        css_code,
        disp_image_url,
        created_at,
        updated_at
      ) values (
        /*'id*/1,
        /*'name*/'abc',
        /*'cssCode*/'abc',
        /*'dispImageUrl*/'abc',
        /*'createdAt*/'1958-09-06 12:00:00',
        /*'updatedAt*/'1958-09-06 12:00:00'
      )
      """)
      .bindByName(
        'id -> id,
        'name -> name,
        'cssCode -> cssCode,
        'dispImageUrl -> dispImageUrl,
        'createdAt -> createdAt,
        'updatedAt -> updatedAt
      ).update.apply()

    EncMCss(
      id = id,
      name = name,
      cssCode = cssCode,
      dispImageUrl = dispImageUrl,
      createdAt = createdAt,
      updatedAt = updatedAt)
  }

  def save(m: EncMCss)(implicit session: DBSession = autoSession): EncMCss = {
    SQL("""
      update
        m_css
      set
        id = /*'id*/1,
        name = /*'name*/'abc',
        css_code = /*'cssCode*/'abc',
        disp_image_url = /*'dispImageUrl*/'abc',
        created_at = /*'createdAt*/'1958-09-06 12:00:00',
        updated_at = /*'updatedAt*/'1958-09-06 12:00:00'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'name -> m.name,
        'cssCode -> m.cssCode,
        'dispImageUrl -> m.dispImageUrl,
        'createdAt -> m.createdAt,
        'updatedAt -> m.updatedAt
      ).update.apply()
    m
  }
      
  def destroy(m: EncMCss)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from m_css where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
