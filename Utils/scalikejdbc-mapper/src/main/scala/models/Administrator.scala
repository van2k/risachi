package models

import scalikejdbc._

case class Administrator(
  id: Long, 
  userid: String, 
  password: String) {

  def save()(implicit session: DBSession = Administrator.autoSession): Administrator = Administrator.save(this)(session)

  def destroy()(implicit session: DBSession = Administrator.autoSession): Unit = Administrator.destroy(this)(session)

}
      

object Administrator {

  val tableName = "administrator"

  object columnNames {
    val id = "id"
    val userid = "userid"
    val password = "password"
    val all = Seq(id, userid, password)
    val inSQL = all.mkString(", ")
  }
      
  val * = {
    import columnNames._
    (rs: WrappedResultSet) => Administrator(
      id = rs.long(id),
      userid = rs.string(userid),
      password = rs.string(password))
  }
      
  object joinedColumnNames {
    val delimiter = "__ON__"
    def as(name: String) = name + delimiter + tableName
    val id = as(columnNames.id)
    val userid = as(columnNames.userid)
    val password = as(columnNames.password)
    val all = Seq(id, userid, password)
    val inSQL = columnNames.all.map(name => tableName + "." + name + " AS " + as(name)).mkString(", ")
  }
      
  val joined = {
    import joinedColumnNames._
    (rs: WrappedResultSet) => Administrator(
      id = rs.long(id),
      userid = rs.string(userid),
      password = rs.string(password))
  }
      
  val autoSession = AutoSession

  def find(id: Long)(implicit session: DBSession = autoSession): Option[Administrator] = {
    SQL("""select * from administrator where id = /*'id*/1""")
      .bindByName('id -> id).map(*).single.apply()
  }
          
  def findAll()(implicit session: DBSession = autoSession): List[Administrator] = {
    SQL("""select * from administrator""").map(*).list.apply()
  }
          
  def countAll()(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from administrator""").map(rs => rs.long(1)).single.apply().get
  }
          
  def findAllBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[Administrator] = {
    SQL("""select * from administrator where """ + where)
      .bindByName(params: _*).map(*).list.apply()
  }
      
  def countBy(where: String, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    SQL("""select count(1) from administrator where """ + where)
      .bindByName(params: _*).map(rs => rs.long(1)).single.apply().get
  }
      
  def create(
    userid: String,
    password: String)(implicit session: DBSession = autoSession): Administrator = {
    val generatedKey = SQL("""
      insert into administrator (
        userid,
        password
      ) values (
        /*'userid*/'abc',
        /*'password*/'abc'
      )
      """)
      .bindByName(
        'userid -> userid,
        'password -> password
      ).updateAndReturnGeneratedKey.apply()

    Administrator(
      id = generatedKey, 
      userid = userid,
      password = password)
  }

  def save(m: Administrator)(implicit session: DBSession = autoSession): Administrator = {
    SQL("""
      update
        administrator
      set
        id = /*'id*/1,
        userid = /*'userid*/'abc',
        password = /*'password*/'abc'
      where
        id = /*'id*/1
      """)
      .bindByName(
        'id -> m.id,
        'userid -> m.userid,
        'password -> m.password
      ).update.apply()
    m
  }
      
  def destroy(m: Administrator)(implicit session: DBSession = autoSession): Unit = {
    SQL("""delete from administrator where id = /*'id*/1""")
      .bindByName('id -> m.id).update.apply()
  }
          
}
