package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class ContractsSpec extends Specification {

  "Contracts" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = Contracts.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = Contracts.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = Contracts.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = Contracts.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = Contracts.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = Contracts.create(mail = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now, status = "MyString", tokenLimit = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = Contracts.findAll().head
      val updated = Contracts.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = Contracts.findAll().head
      Contracts.destroy(entity)
      val shouldBeNone = Contracts.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        