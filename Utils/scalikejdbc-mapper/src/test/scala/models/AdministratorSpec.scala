package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class AdministratorSpec extends Specification {

  "Administrator" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = Administrator.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = Administrator.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = Administrator.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = Administrator.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = Administrator.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = Administrator.create(userid = "MyString", password = "MyString")
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = Administrator.findAll().head
      val updated = Administrator.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = Administrator.findAll().head
      Administrator.destroy(entity)
      val shouldBeNone = Administrator.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        