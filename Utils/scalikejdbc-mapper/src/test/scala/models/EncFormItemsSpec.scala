package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormItemsSpec extends Specification {

  "EncFormItems" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormItems.find("MyString")
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormItems.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormItems.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormItems.findAllBy("id = {id}", 'id -> "MyString")
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormItems.countBy("id = {id}", 'id -> "MyString")
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormItems.create(id = "MyString", formId = "MyString", categoryId = 1L, mFormItemId = 1L, name = "MyString", pageNo = 123, required = "MyString", dispodr = 123, createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormItems.findAll().head
      val updated = EncFormItems.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormItems.findAll().head
      EncFormItems.destroy(entity)
      val shouldBeNone = EncFormItems.find("MyString")
      shouldBeNone.isDefined should beFalse
    }
  }

}
        