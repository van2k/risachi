package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormCategoriesSpec extends Specification {

  "EncFormCategories" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormCategories.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormCategories.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormCategories.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormCategories.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormCategories.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormCategories.create(formId = "MyString", name = "MyString", description = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormCategories.findAll().head
      val updated = EncFormCategories.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormCategories.findAll().head
      EncFormCategories.destroy(entity)
      val shouldBeNone = EncFormCategories.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        