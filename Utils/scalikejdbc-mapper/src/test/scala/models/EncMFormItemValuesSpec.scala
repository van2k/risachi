package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncMFormItemValuesSpec extends Specification {

  "EncMFormItemValues" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncMFormItemValues.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncMFormItemValues.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncMFormItemValues.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncMFormItemValues.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncMFormItemValues.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncMFormItemValues.create(mFormItemsId = 1L, itemValue = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncMFormItemValues.findAll().head
      val updated = EncMFormItemValues.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncMFormItemValues.findAll().head
      EncMFormItemValues.destroy(entity)
      val shouldBeNone = EncMFormItemValues.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        