package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncAnswerDetailsSpec extends Specification {

  "EncAnswerDetails" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncAnswerDetails.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncAnswerDetails.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncAnswerDetails.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncAnswerDetails.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncAnswerDetails.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncAnswerDetails.create(answerId = "MyString", formItemId = "MyString", formItemValueId = 1L, answerValue = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncAnswerDetails.findAll().head
      val updated = EncAnswerDetails.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncAnswerDetails.findAll().head
      EncAnswerDetails.destroy(entity)
      val shouldBeNone = EncAnswerDetails.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        