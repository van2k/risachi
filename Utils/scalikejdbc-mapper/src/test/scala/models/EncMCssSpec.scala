package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncMCssSpec extends Specification {

  "EncMCss" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncMCss.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncMCss.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncMCss.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncMCss.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncMCss.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncMCss.create(id = 1L, name = "MyString", cssCode = "MyString", dispImageUrl = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncMCss.findAll().head
      val updated = EncMCss.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncMCss.findAll().head
      EncMCss.destroy(entity)
      val shouldBeNone = EncMCss.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        