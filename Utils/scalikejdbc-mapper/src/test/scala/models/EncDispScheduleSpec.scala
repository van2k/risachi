package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncDispScheduleSpec extends Specification {

  "EncDispSchedule" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncDispSchedule.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncDispSchedule.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncDispSchedule.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncDispSchedule.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncDispSchedule.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncDispSchedule.create(id = 1L, formId = "MyString", kind = "MyString", settingValue = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncDispSchedule.findAll().head
      val updated = EncDispSchedule.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncDispSchedule.findAll().head
      EncDispSchedule.destroy(entity)
      val shouldBeNone = EncDispSchedule.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        