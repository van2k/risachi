package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormChargeOptionsSpec extends Specification {

  "EncFormChargeOptions" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormChargeOptions.find()
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormChargeOptions.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormChargeOptions.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormChargeOptions.findAllBy()
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormChargeOptions.countBy()
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormChargeOptions.create(formId = "MyString", chargeOptionPlanId = 1L)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormChargeOptions.findAll().head
      val updated = EncFormChargeOptions.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormChargeOptions.findAll().head
      EncFormChargeOptions.destroy(entity)
      val shouldBeNone = EncFormChargeOptions.find()
      shouldBeNone.isDefined should beFalse
    }
  }

}
        