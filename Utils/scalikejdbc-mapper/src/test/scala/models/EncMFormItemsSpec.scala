package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncMFormItemsSpec extends Specification {

  "EncMFormItems" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncMFormItems.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncMFormItems.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncMFormItems.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncMFormItems.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncMFormItems.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncMFormItems.create(id = 1L, name = "MyString", kind = "MyString", dataType = "MyString", graphKind = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncMFormItems.findAll().head
      val updated = EncMFormItems.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncMFormItems.findAll().head
      EncMFormItems.destroy(entity)
      val shouldBeNone = EncMFormItems.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        