package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class ChargePlansSpec extends Specification {

  "ChargePlans" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = ChargePlans.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = ChargePlans.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = ChargePlans.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = ChargePlans.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = ChargePlans.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = ChargePlans.create(id = 1L, charge = 123, tax = 123, description = "MyString", outputNumber = 123, saveTerm = 123, applyStartDate = DateTime.now, createdAt = DateTime.now, updatedAt = DateTime.now, csvOutputNum = 1L, answersDispType = 1L, isStartSet = "MyString", isLogoSet = "MyString", isTemplate = "MyString", isCss = "MyString", isInnerHtml = "MyString", isForeignKey = "MyString", name = "MyString", pageNum = 123, itemNum = 123)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = ChargePlans.findAll().head
      val updated = ChargePlans.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = ChargePlans.findAll().head
      ChargePlans.destroy(entity)
      val shouldBeNone = ChargePlans.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        