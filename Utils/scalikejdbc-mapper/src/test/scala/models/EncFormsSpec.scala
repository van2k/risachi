package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormsSpec extends Specification {

  "EncForms" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncForms.find("MyString")
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncForms.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncForms.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncForms.findAllBy("id = {id}", 'id -> "MyString")
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncForms.countBy("id = {id}", 'id -> "MyString")
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncForms.create(id = "MyString", contractId = 1L, formName = "MyString", formDesc = "MyString", resultMessage = "MyString", totalPageCount = 123, createdAt = DateTime.now, updatedAt = DateTime.now, status = "MyString")
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncForms.findAll().head
      val updated = EncForms.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncForms.findAll().head
      EncForms.destroy(entity)
      val shouldBeNone = EncForms.find("MyString")
      shouldBeNone.isDefined should beFalse
    }
  }

}
        