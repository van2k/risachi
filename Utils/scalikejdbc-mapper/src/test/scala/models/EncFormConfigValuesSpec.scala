package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormConfigValuesSpec extends Specification {

  "EncFormConfigValues" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormConfigValues.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormConfigValues.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormConfigValues.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormConfigValues.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormConfigValues.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormConfigValues.create(formId = "MyString", optionId = 1L, optionNumber = 1L, charge = 1L, price = 1L, createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormConfigValues.findAll().head
      val updated = EncFormConfigValues.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormConfigValues.findAll().head
      EncFormConfigValues.destroy(entity)
      val shouldBeNone = EncFormConfigValues.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        