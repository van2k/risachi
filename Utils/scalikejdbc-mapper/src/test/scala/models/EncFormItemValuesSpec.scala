package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormItemValuesSpec extends Specification {

  "EncFormItemValues" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormItemValues.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormItemValues.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormItemValues.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormItemValues.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormItemValues.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormItemValues.create(formItemId = "MyString", formValue = "MyString", chLength = 123, createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormItemValues.findAll().head
      val updated = EncFormItemValues.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormItemValues.findAll().head
      EncFormItemValues.destroy(entity)
      val shouldBeNone = EncFormItemValues.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        