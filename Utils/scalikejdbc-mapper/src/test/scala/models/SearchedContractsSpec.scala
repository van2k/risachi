package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class SearchedContractsSpec extends Specification {

  "SearchedContracts" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = SearchedContracts.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = SearchedContracts.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = SearchedContracts.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = SearchedContracts.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = SearchedContracts.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = SearchedContracts.create(mail = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now, status = "MyString", tokenLimit = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = SearchedContracts.findAll().head
      val updated = SearchedContracts.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = SearchedContracts.findAll().head
      SearchedContracts.destroy(entity)
      val shouldBeNone = SearchedContracts.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        