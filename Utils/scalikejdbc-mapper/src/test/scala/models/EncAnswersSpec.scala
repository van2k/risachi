package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncAnswersSpec extends Specification {

  "EncAnswers" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncAnswers.find("MyString")
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncAnswers.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncAnswers.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncAnswers.findAllBy("id = {id}", 'id -> "MyString")
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncAnswers.countBy("id = {id}", 'id -> "MyString")
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncAnswers.create(id = "MyString", formId = "MyString", startAt = DateTime.now, isPreview = "MyString", ipAddress = "MyString", webBrowser = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncAnswers.findAll().head
      val updated = EncAnswers.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncAnswers.findAll().head
      EncAnswers.destroy(entity)
      val shouldBeNone = EncAnswers.find("MyString")
      shouldBeNone.isDefined should beFalse
    }
  }

}
        