package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class ChargeOptionPlansSpec extends Specification {

  "ChargeOptionPlans" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = ChargeOptionPlans.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = ChargeOptionPlans.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = ChargeOptionPlans.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = ChargeOptionPlans.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = ChargeOptionPlans.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = ChargeOptionPlans.create(id = 1L, charge = 123, tax = 123, description = "MyString", applyStartDate = DateTime.now, createdAt = DateTime.now, updatedAt = DateTime.now, optionType = "MyString", optionValue = 123)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = ChargeOptionPlans.findAll().head
      val updated = ChargeOptionPlans.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = ChargeOptionPlans.findAll().head
      ChargeOptionPlans.destroy(entity)
      val shouldBeNone = ChargeOptionPlans.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        