package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class InformationSpec extends Specification {

  "Information" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = Information.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = Information.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = Information.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = Information.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = Information.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = Information.create(sendDate = DateTime.now, subject = "MyString", content = "MyString")
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = Information.findAll().head
      val updated = Information.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = Information.findAll().head
      Information.destroy(entity)
      val shouldBeNone = Information.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        