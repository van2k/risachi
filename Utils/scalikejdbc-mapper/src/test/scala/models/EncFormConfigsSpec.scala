package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class EncFormConfigsSpec extends Specification {

  "EncFormConfigs" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = EncFormConfigs.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = EncFormConfigs.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = EncFormConfigs.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = EncFormConfigs.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = EncFormConfigs.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = EncFormConfigs.create(formId = "MyString", collectNumber = 123, startSt = LocalTime.now, stop = "MyString", outputNumber = 123, term = 123, outputLimitNumber = 123, encResultOpen = "MyString", createdAt = DateTime.now, updatedAt = DateTime.now)
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = EncFormConfigs.findAll().head
      val updated = EncFormConfigs.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = EncFormConfigs.findAll().head
      EncFormConfigs.destroy(entity)
      val shouldBeNone = EncFormConfigs.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        