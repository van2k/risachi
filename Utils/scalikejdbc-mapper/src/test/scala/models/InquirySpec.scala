package models

import scalikejdbc.specs2.mutable.AutoRollback
import org.specs2.mutable._
import org.joda.time._


class InquirySpec extends Specification {

  "Inquiry" should {
    "find by primary keys" in new AutoRollback {
      val maybeFound = Inquiry.find(1L)
      maybeFound.isDefined should beTrue
    }
    "find all records" in new AutoRollback {
      val allResults = Inquiry.findAll()
      allResults.size should be_>(0)
    }
    "count all records" in new AutoRollback {
      val count = Inquiry.countAll()
      count should be_>(0L)
    }
    "find by where clauses" in new AutoRollback {
      val results = Inquiry.findAllBy("id = {id}", 'id -> 1L)
      results.size should be_>(0)
    }
    "count by where clauses" in new AutoRollback {
      val count = Inquiry.countBy("id = {id}", 'id -> 1L)
      count should be_>(0L)
    }
    "create new record" in new AutoRollback {
      val created = Inquiry.create(sendDate = DateTime.now, subject = "MyString", mail = "MyString", content = "MyString", status = "MyString", name = "MyString")
      created should not beNull
    }
    "save a record" in new AutoRollback {
      val entity = Inquiry.findAll().head
      val updated = Inquiry.save(entity)
      updated should not equalTo(entity)
    }
    "destroy a record" in new AutoRollback {
      val entity = Inquiry.findAll().head
      Inquiry.destroy(entity)
      val shouldBeNone = Inquiry.find(1L)
      shouldBeNone.isDefined should beFalse
    }
  }

}
        